var $ = jQuery;

function dup(obj) {
    if (Array.isArray(obj)) {
        var copy = [];
        for (var i = 0; i < obj.length; i++) {
            copy[i] = dup(obj[i]);
        }
        return copy;
    }
    return obj;
};

/**
 * Capitalizes the first letter of the string.
 */
function ucfirst(str) {
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

/**
 * Binds the object to the function.
 */
function bind(obj, fn) {
    return function() {
        return fn.apply(obj, arguments);
    }
}



var EventHandler = function() {
    this.listeners = {};
};

EventHandler.prototype.addListener = function(type, fn) {
    if (!(type in this.listeners)) {
        this.listeners[type] = [];
    }
    this.listeners[type].push(fn);
};

EventHandler.prototype.fire = function(type) {
    var args = Array.prototype.slice.call(arguments, 1);

    if (type in this.listeners) {
        for (var key in this.listeners[type]) {
            var fn = this.listeners[type][key];
            fn.apply(null, args);
        }
    }
};



var num = 1;

/**
 * Creates a UForm object.
 */
var uform = function(data) {
    this.id = num++;
    this.editable = data.editable;
    this.layout = data.layout;
    this.parent = data.parent;
    this.data = data.data;
    this.style = data.style;
    this.event = new EventHandler();
    this.url = data.url;
    this.ajax = data.ajax;

    if (this.ajax) {
        this.addListener('submit', bind(this, this.submitAjax));
    }
};

uform.MONTHS = [
    {'label': 'January', 'value': 1},
    {'label': 'February', 'value': 2},
    {'label': 'March', 'value': 3},
    {'label': 'April', 'value': 4},
    {'label': 'May', 'value': 5},
    {'label': 'June', 'value': 6},
    {'label': 'July', 'value': 7},
    {'label': 'August', 'value': 8},
    {'label': 'September', 'value': 9},
    {'label': 'October', 'value': 10},
    {'label': 'November', 'value': 11},
    {'label': 'December', 'value': 12}
];

uform.prototype.submitAjax = function(data) {
    $.post(this.url, data, function(data) {
        if (data.status == 'ok') {
            if (data.redirect) {
                parent.location = data.redirect;
            } else {
                alert("success but: " + data);
            }
        } else if (data.status == 'error') {
            alert(data.message);
        } else {
            alert('unknown response: ' + data);
        }
    });
};

uform.editorConfig = {
    'text': [
        {
            'type': 'text',
            'content': 'Label',
            'slug': 'content'
        },
        {
            'type': 'text',
            'content': 'Slug',
            'slug': 'slug'
        },
        {
            'type': 'checkbox',
            'content': 'Required',
            'slug': 'required'
        },
        {
            'type': 'dropdown',
            'content': 'Validate',
            'slug': 'validate',
            'options': {
                '': 'None',
                'email': 'Email'
            }
        }
    ],

    'heading': [
        {
            'type': 'text',
            'content': 'Heading',
            'slug': 'content'
        }
    ],

    'blob': [
        {
            'type': 'html',
            'content': 'Content',
            'slug': 'content'
        }
    ]
};

/**
 * Returns the id of the <form> element corresponding to this form.
 */
uform.prototype.formId = function() {
    return 'uform_' + this.id;
};

/**
 * Returns the id of the form element in the DOM.
 */
uform.prototype.fieldId = function(item) {
    return 'uform_' + this.id + '_' + item.slug;
};

/**
 * Displays the form on the page.
 */
uform.prototype.display = function() {
    var wrap = $('#' + this.parent);
       // .empty();

    // hide the form while it is populated
    wrap.css('display', 'none');

    if (this.style) {
        wrap.addClass('uform-' + this.style);
    }


    var form = $('<form/>')
        .attr('method', 'POST')
        .attr('action', '')
        .attr('id', this.formId())
        .appendTo(wrap);

    var group = null;
    var itemList = null;

    for (var key in this.layout) {
        var item = this.layout[key];
        if (item.type == 'group') {
            group = $('<div/>')
                .addClass('stuffbox')
                .appendTo(form);
            if (item.label) {
                $('<h3/>').append(item.label)
                    .appendTo(group);
            }
            itemList = $('<ul/>')
                .appendTo(group);
        } else if (item.type == 'endgroup') {
            itemList = $('<ul/>')
                .appendTo(form);
        } else {
            var el = this.createItem(item);
            itemList.append(el);

            if (item.slug && this.data && (item.slug in this.data)) {
                this.setValue(item, this.data[item.slug]);
            }
        }
    }

    form.submit(bind(this, this.submit));

    var hack = $('<input/>')
        .attr('type', 'submit')
        .attr('value', 'submit')
        .css('visibility', 'hidden')
        .appendTo(form);

    // make the form visible
    wrap.css('display', 'block');

    if (this.editable) {
        itemList.sortable();
        itemList.disableSelection();
    }
};

uform.prototype.setValue = function(item, value) {
    if (item.type == 'checkbox') {
        $('#' + this.fieldId(item)).attr('checked', value == '1' ? 'checked' : '');
    } else if (item.type == 'datetime') {
        var date = new Date(value * 1000);
        var id = this.fieldId(item);
        $('#' + id + '_year').val(date.getFullYear());
        $('#' + id + '_month').val(date.getMonth() + 1);
        $('#' + id + '_day').val(date.getDate());
    } else {
        $('#' + this.fieldId(item)).val(value);
    }
};

uform.prototype.editItem = function(item) {
    var el = $('#uform_item_' + item.id);

    var config = {};
    config.parent = 'uform_editor';
    config.style = 'compact';
    config.layout = dup(uform.editorConfig[item.type]);
    config.layout.push({
        'type': 'button',
        'content': 'Save Changes'
    });
    config.data = item;

    var form = new uform(config);
    form.addListener('submit', bind(this, function(data) {
        for (var key in data) {
            item[key] = data[key];
        }
        var newItem = this.createItem(item);
        $('#uform_item_' + item.id).replaceWith(newItem);
        $('#uform_editor').hide();
    }));
    form.display();

    var ed = $('#uform_editor');
    ed.css('top', el.offset().top);
    ed.css('left', el.offset().left);
    ed.show();
};

/**
 * Lays out the item and returns the resulting DOM element.
 */
uform.prototype.createItem = function(item) {
    var itemWrap = $('<li/>')
        .attr('id', 'uform_item_' + item.id)
        .addClass('uform-item');

    // delegate the layout according to the item type
    var fn = 'create' + ucfirst(item.type);
    if (!this[fn]) {
        // should never happen
        alert(fn + ' does not exist');
    }

    var el = this[fn].call(this, item);

    if (this.editable) {
        itemWrap.addClass('uform-editable');
        itemWrap.click(bind(this, function() {
            this.editItem(item);
        }));
        $('input', itemWrap).attr('disabled', 'disabled');
    }

    if (item.type == 'hidden') {
        return el;
    }

    itemWrap.append(el);

    if (item.optional) {
        var cb = $('<input type="checkbox"/>')
            .addClass('uform-optional');
        cb.change(function() {
            itemWrap.toggleClass('uform-disabled');
        });
        itemWrap.prepend(cb);
        itemWrap.addClass('uform-disabled');
    }
    return itemWrap;
};

/**
 * Creates a heading element within the form.
 */
uform.prototype.createHeading = function(item) {
    var el = $('<div/>')
        .addClass('uform-heading')
        .append(item.description);
    return el;
};

/**
 * Creates a text field within the form.
 */
uform.prototype.createHidden = function(item) {
    var content = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));
    return content;
};


/**
 * Creates a text field within the form.
 */
uform.prototype.createText = function(item) {
    var content = $('<input/>')
        .attr('type', 'text')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));
    content.change(bind(this, function() {
        this.validateText(item);
    }));
    return this.createField(content, item);
};

/**
 * Creates a text field within the form.
 */
uform.prototype.createPassword = function(item) {
    var content = $('<input/>')
        .attr('type', 'password')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));
    content.change(bind(this, function() {
        this.validateText(item);
    }));
    return this.createField(content, item);
};

/**
 * Creates a currency field within the form.
 */
uform.prototype.createCurrency = function(item) {
    var content = $('<input/>')
        .attr('type', 'text')
        .addClass('uform-currency')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));
    content.change(bind(this, function() {
        this.validateCurrency(item);
    }));
    var field = this.createField(content, item);
    content.before('$ ');
    return field;
};

/**
 * Creates a text field within the form.
 */
uform.prototype.createHtml = function(item) {
    var mce = $('#uform-mce');
    mce.detach();
    mce.css('display', 'block');
    return mce;
};

/**
 * Creates a checkbox within the form.
 */
uform.prototype.createCheckbox = function(item) {
    var content = $('<input/>')
        .attr('type', 'checkbox')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));
    return this.createField(content, item);
};

/**
 * Creates a checkbox within the form.
 */
uform.prototype.createDatetime = function(item) {
    var now = new Date();
    var curYear = now.getFullYear();
    var curMonth = now.getMonth() + 1;
    var curDay = now.getDate();

    var id = this.fieldId(item);

    var el = $('<div/>');

    var month = $('<select/>')
        .attr('id', id + '_month')
        .appendTo(el);
    for (var i = 0; i < uform.MONTHS.length; i++) {
        var m = uform.MONTHS[i];

        var opt = $('<option/>')
            .attr('value', m.value)
            .append(m.label)
            .appendTo(month);

        if (m.value == curMonth) {
            opt.attr('selected', 'selected');
        }
    }

    var day = $('<select/>')
        .attr('id', id + '_day')
        .appendTo(el);
    for (var i = 1; i <= 31; i++) {
        var opt = $('<option/>')
            .attr('value', i)
            .append(i)
            .appendTo(day);

        if (i == curDay) {
            opt.attr('selected', 'selected');
        }
    }

    var year = $('<select/>')
        .attr('id', id + '_year')
        .appendTo(el);
    for (var i = curYear - 5; i <= curYear + 5; i++) {
        var opt = $('<option/>')
            .attr('value', i)
            .append(i)
            .appendTo(year);
        if (i == curYear) {
            opt.attr('selected', 'selected');
        }
    }

    return this.createField(el, item);
};

/**
 * Creates a button.
 */
uform.prototype.createButton = function(item) {
    var el = $('<div/>')
        .addClass('uform-menu');

    for (var key in item.items) {
        var btnItem = item.items[key];
        var btn = $('<input type="button"/>')
            .attr('value', btnItem.label)
            .appendTo(el);

        if (btnItem.action == 'submit') {
            btn.addClass('button-primary')
            btn.click(bind(this, this.submit));
        } else {
            btn.addClass('button');
            if (btnItem.action == 'cancel') {
                btn.click(function() {
                    parent.location = parent.location;
                });
            }
        }
    }



    return el;
};

/**
 * Creates an HTML blob in the form.
 */
uform.prototype.createBlob = function(item) {
    var el = $('<div/>')
        .addClass('uform-blob')
        .append(item.description);
    return el;
};

/**
 * Creates a dropdown menu in the form.
 */
uform.prototype.createDropdown = function(item) {
    var content = $('<select/>')
        .attr('name', item.slug)
        .attr('id', this.fieldId(item));

    var options = item.options;
    for (var key in options) {
        var value = options[key];

        var opt = $('<option/>')
            .attr('value', key)
            .append(value)
            .appendTo(content);
    }
    return this.createField(content, item);
};


/**
 * Validates the form.
 */
uform.prototype.validate = function() {
    var valid = true;
    for (var key in this.layout) {
        var item = this.layout[key];
        var fn = 'validate' + ucfirst(item.type);
        var res = true;
        if (this[fn]) {
            var res = this[fn].call(this, item);
            if (!res) {
                valid = false;
            }
        }
    }
    return valid;
};

uform.prototype.validateCurrency = function(item) {
    var val = $('#' + this.fieldId(item)).val();
    var matcher = new RegExp('^[0-9]*(\.[0-9]{2,2})?$');
    if (!matcher.test(val)) {
        this.setError(item, 'Please enter a valid amount');
        return false;
    }
    return true;
};

uform.prototype.getText = function(item) {
    return $('#' + this.fieldId(item)).val();
};

uform.prototype.getPassword = function(item) {
    return $('#' + this.fieldId(item)).val();
};

uform.prototype.getDropdown = function(item) {
    return $('#' + this.fieldId(item)).val();
};

uform.prototype.getHtml = function(item) {
    return tinyMCE.activeEditor.getContent();
};

uform.prototype.getCheckbox = function(item) {
    return $('#' + this.fieldId(item)).attr('checked') ? '1' : '0';
};

uform.prototype.getDatetime = function(item) {
    var id = this.fieldId(item);
    var year = $('#' + id + '_year').val();
    var month = $('#' + id + '_month').val() - 1;
    var day = $('#' + id + '_day').val();

    var date = new Date(year, month, day);
    return date.getTime() / 1000;
};

uform.prototype.getCurrency = function(item) {
    return $('#' + this.fieldId(item)).val();
};

uform.prototype.getHidden = function(item) {
    return $('#' + this.fieldId(item)).val();
};

uform.prototype.getFormData = function() {
    var data = {};
    for (var key in this.layout) {
        var item = this.layout[key];
        var fn = 'get' + ucfirst(item.type);
        if (this[fn]) {
            var res = this[fn].call(this, item);
            data[item.slug] = res;
        }
    }
    return data;
};

uform.prototype.addListener = function(type, fn) {
    this.event.addListener(type, fn);
};

/**
 * Validates and submits the form.
 */
uform.prototype.submit = function(e) {
    if (!this.validate()) {
        //alert('Please correct the errors');
        return;
    }
    var data = this.getFormData();

    this.event.fire('submit', data);
    e.stopPropagation();
    return false;
};

/**
 * Sets an error message on the given field.
 */
uform.prototype.setError = function(item, msg) {
    $('#' + this.fieldId(item)).addClass('uform-error');

    var el = $('#' + this.fieldId(item) + '_error');
    el.html(msg);
    el.fadeIn(100);
};

uform.prototype.isRequired = function(item) {
    return item.required == '1';
};

/**
 * Clears the error status on the given field.
 */
uform.prototype.clearError = function(item) {
    $('#' + this.fieldId(item.slug)).removeClass('uform-error');
    var el = $('#' + this.fieldId(item) + '_error');
    el.hide();
};

/**
 * Determines whether the given string is a valid email address.
 */
uform.prototype.isEmail = function(value) {
    var matcher = new RegExp('^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$', 'i');
    return matcher.test(value);
};

/**
 * Validates a text field.
 */
uform.prototype.validateText = function(item) {
    var el = $('#' + this.fieldId(item));
    var val = el.val();

    if (this.isRequired(item) && val.length == 0) {
        this.setError(item, 'This field is required');
        return false;
    }

    if (item.validate == 'email') {
        if (!this.isEmail(val)) {
            this.setError(item, 'Please enter a valid email address');
            return false;
        }
    }

    this.clearError(item);
    return true;
};

uform.prototype.slugToLabel = function(slug) {
    var words = slug.split('_');
    for (var i = 0; i < words.length; i++) {
        words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
    }
    return words.join(' ');
};


/**
 * Creates a form field.
 */
uform.prototype.createField = function(content, item) {
    var el = $('<div/>').addClass('uform-field');

    var labelStr = item.label;
    if (!labelStr) {
        labelStr = this.slugToLabel(item.slug);
    }

    var label = $('<label/>')
        .attr('for', this.fieldId(item))
        .append(labelStr);

    var labelWrap = $('<div/>')
        .addClass('uform-label')
        .append(label);

    if (this.isRequired(item)) {
        label.addClass('uform-required');
    }

    labelWrap.appendTo(el);

    var wrap = $('<div/>')
        .addClass('uform-value')
        .append(content)
        .appendTo(el);

    var errors = $('<div/>')
        .attr('id', this.fieldId(item) + '_error')
        .addClass('uform-field-error')
        .appendTo(el);

    if (item.description) {
        $('<div/>')
            .addClass('uform-description')
            .append(item.description)
            .appendTo(el);
    }

    return el;
};