
(function($) {
	
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	
	var DatePicker = function(el) {
		this.input = el;
		
		var val = el.val();
		if (val.length > 0) {
			var date = new Date(val);
			this.year = date.getFullYear();
			this.month = date.getMonth();
			this.day = date.getDate();
			this.hour = date.getHours();
			this.minute = date.getMinutes();
		} else {
			var date = new Date();
			this.year = date.getFullYear();
			this.month = date.getMonth();
			this.day = -1;
		}
		
		
		
		var p = $('<div/>')
			.addClass('dropdown-menu')
			.addClass('datepicker')
			.append(this.calendar())
			.append(this.timePicker());
		this.picker = p;
		el.after(p);
		
		var icon = $('<span class="add-on"/>')
			.append('<i class="fa fa-clock-o"></i>');
		el.after(icon);
		var self = this;
		icon.click(function(e) {
			p.show();
			self.redraw();
			e.stopPropagation();
		});
		
		p.click(function(e) {
			e.stopPropagation();
			return false;
		});
		
		$(document).click(function(e) { 
			var parents = $(e).parents();
		    if (parents.index(p) == -1) {
		        p.hide();
		    }
		});
		
		
	};
	
	DatePicker.prototype.formatTime = function(h, m) {
		var label = "";
		if (h == 0) {
			label += "12";
		} else if (h > 12) {
			label += (h - 12) + "";
		} else {
			label += h;
		}
		label += ":";
		
		if (m < 10) {
			label += "0" + m;
		} else {
			label += m;
		}
		if (h < 12) {
			label += " am";
		} else {
			label += " pm";
		}
		return label;
	}
	
	DatePicker.prototype.getTimeOption = function(h, m) {
		
		var opt = $('<div/>')
			.addClass('dp-btn')
			.append(this.formatTime(h, m));
		
		var self = this;
		opt.click(function() {
			self.hour = h;
			self.minute = m;
			if (self.day >= 0) {
				self.picker.hide();
				self.updateInput();
			} else {
				self.redraw();
			}
		});
		
		return opt;
	}
	
	DatePicker.prototype.updateInput = function() {
		this.input.val((this.month + 1) + "/" + this.day + "/" + this.year + " " + this.formatTime(this.hour, this.minute));
	};
	
	DatePicker.prototype.timePicker = function() {
		var wrap = $('<div/>').addClass('time-wrap');
		
		this.times = {}
		for (var h = 0; h < 24; h++) {
			this.times[h] = {};
			for (var m = 0; m < 60; m += 15) {
				var t = this.getTimeOption(h, m);
				this.times[h][m] = t;
				wrap.append(t);
			}
		}
		this.timeWrap = wrap;
		return wrap;
	};
	
	DatePicker.prototype.redraw = function() {
		var title = MONTHS[this.month] + " " + this.year;
		this.title.html(title);
		
		var first = new Date(this.year, this.month, 1);
		var firstDay = first.getDay();
		
		var cur = 0;
		for (var i = 0; i < 6; i++) {
			for (var j = 0; j < 7; j++) {
				cur++;
				var date = new Date(this.year, this.month, cur - firstDay);
				
				if (j == 0) {
					if (i > 0 && j == 0 && date.getMonth() != this.month) {
						this.rows[i].hide();
					} else {
						this.rows[i].show();
					}	
				}
				
				if (date.getMonth() != this.month) {
					this.cells[i][j].addClass('disabled');
				} else {
					this.cells[i][j].removeClass('disabled');
				}
				if (date.getMonth() == this.month && date.getFullYear() == this.year && date.getDate() == this.day) {
					this.cells[i][j].addClass('selected');
				} else {
					this.cells[i][j].removeClass('selected');
				}
				this.cells[i][j].html(date.getDate() + "");
			}
		}
		
		var scroll = 0;
		
		for (var h in this.times) {
			for (var m in this.times[h]) {
				var el = this.times[h][m];
				if (h == this.hour && m == this.minute) {
					el.addClass('selected');
					scroll = el.position().top - this.times[0][0].position().top;
				} else {
					el.removeClass('selected');
				}
			}
		}
		
		this.timeWrap.scrollTop(scroll - this.timeWrap.height() / 2 + this.times[0][0].height() / 2);
	};
	
	DatePicker.prototype.getCellDate = function(i, j) {
		var first = new Date(this.year, this.month, 1);
		var firstDay = first.getDay();
		return new Date(this.year, this.month, i * 7 + j + 1 - firstDay, 0, 0, 0, 0);
	};
	
	DatePicker.prototype.selector = function(input) {
		var el = $('<span/>')
			.addClass('add-on')
			.append('<i class="fa fa-clock-o"></i>');
		return el;
	};
	
	DatePicker.prototype.header = function() {
		var head = $('<div/>').addClass('dp-head');
		head.append(this.leftYearBtn());
		head.append(this.leftBtn());
		head.append(this.rightYearBtn());
		head.append(this.rightBtn());	
		
		this.title = $('<div class="dp-title"/>')
			.append('November 2014');
		head.append(this.title);
		
		return head;
	};
	
	DatePicker.prototype.calendar = function() {
		var wrap = $('<div/>')
			.addClass('cal-wrap')
			.append(this.header());
		
		var cal = $('<table/>')
			.addClass('dp-cal');
		
		cal.append('<tr class="labels"><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>');
		this.cells = [];
		this.rows = [];
		
		for (var i = 0; i < 6; i++) {
			this.cells[i] = [];
			var row = $('<tr/>');
			for (var j = 0; j < 7; j++) {
				row.append(this.calendarCell(i, j));
			}
			cal.append(row);
			this.rows[i] = row;
		}
		
		wrap.append(cal);
		return wrap;
	};
	
	DatePicker.prototype.clickCell = function(i, j) {
		var date = this.getCellDate(i, j);
		this.month = date.getMonth();
		this.day = date.getDate();
		this.year = date.getFullYear();
		this.hour = -1;
		this.redraw();
	};
	
	DatePicker.prototype.calendarCell = function(i, j) {
		var el = $('<td/>');
		
		var btn = $('<div/>')
			.addClass('dp-btn')
			.appendTo(el);
	
		this.cells[i][j] = btn;
		
		var self = this;
		btn.click(function() {
			self.clickCell(i, j);
		});
		
		btn.append((i * 7 + j) + "");
		return el;
	};
	
	DatePicker.prototype.prev = function() {
		if (this.month == 0) {
			this.month = 11;
			this.year--;
		} else {
			this.month--;
		}
		this.day = -1;
		this.hour = -1;
		this.redraw();
	}
	
	DatePicker.prototype.next = function() {
		if (this.month == 11) {
			this.month = 0;
			this.year++;
		} else {
			this.month++;
		}
		this.day = -1;
		this.hour = -1;
		this.redraw();
	}
	
	DatePicker.prototype.prevYear = function() {
		this.year--;
		this.day = -1;
		this.hour = -1;
		this.redraw();
	}
	
	DatePicker.prototype.nextYear = function() {
		this.year++;
		this.day = -1;
		this.hour = -1;
		this.redraw();
	}

	DatePicker.prototype.leftBtn = function() {
		var btn = $('<div/>')
		.addClass('dp-left')
		.addClass('dp-btn')
		.append('<i class="fa fa-angle-left"></i>');
		
		btn.click($.proxy(this.prev, this));
		
		return btn;	
	};
	
	DatePicker.prototype.rightBtn = function() {
		var btn = $('<div/>')
			.addClass('dp-right')
			.addClass('dp-btn')
			.append('<i class="fa fa-angle-right"></i>');
		
		btn.click($.proxy(this.next, this));
		return btn;
	};
	
	DatePicker.prototype.leftYearBtn = function() {
		var btn = $('<div/>')
		.addClass('dp-left')
		.addClass('dp-btn')
		.append('<i class="fa fa-angle-double-left"></i>');
		
		btn.click($.proxy(this.prevYear, this));
		
		return btn;	
	};
	
	DatePicker.prototype.rightYearBtn = function() {
		var btn = $('<div/>')
			.addClass('dp-right')
			.addClass('dp-btn')
			.append('<i class="fa fa-angle-double-right"></i>');
		
		btn.click($.proxy(this.nextYear, this));
		return btn;
	};
	
	$.fn.datepicker = function ( option, val ) {
		return this.each(function () {
			var el = $(this);
			new DatePicker(el);
		});
	};
	
})(jQuery);