<?php

/**
 * Error sandbox.
 */
class Sandbox {


    private $error_types;
    private $enabled;

    /**
     * Initializes the sandbox.
     */
    public function __construct() {
        $this->enabled = false;

        $this->error_types = array(
            E_ERROR => 'Fatal Error',
            E_WARNING => 'Warning',
            E_PARSE => 'Parse Error',
            E_NOTICE => 'Notice',
            E_STRICT => 'Strict'
        );
    }

    /**
     * Enables the sandbox. Most errors will be caught and redirected to an
     * appropriate display.
     */
    public function enable() {
        // skip if already enabled
        if ($this->enabled) {
            return;
        }
        $this->enabled = true;

        // turn off default error reporting
        error_reporting(0);

        // turn off internal error printing
        ini_set('display_errors', 0);

        // register our error handler
        set_error_handler(array($this, 'php_error'), E_ALL ^ E_NOTICE);

        // register our exception handler
        set_exception_handler(array($this, 'php_exception'));

        // catch fatal errors
        register_shutdown_function(array($this, 'php_shutdown'));

        // catch output
        ob_start();
    }

    /**
     * Disables the sandbox.
     */
    public function disable() {
        // restore the previous handlers
        restore_error_handler();
        restore_exception_handler();

        $this->enabled = false;
    }

    /**
     * Ends execution of the program and prints useful information
     */
    public function debug() {
        $this->php_exception(new Exception("DEBUG"));
    }

    /**
     * Shutdown handler.
     */
    public function php_shutdown() {
        $error = error_get_last();

        if (!is_null($error)) {
            $errno = $error['type'];
            if ($errno == E_STRICT) return;
            $errfile = $error['file'];
            $errline = $error['line'];
            $errstr = $error['message'];

            // funnel to the exception handler
            $this->php_exception(new ErrorException($errstr, $errno, 0, $errfile, $errline));
        }
    }

    /**
     * Error handler
     */
    public function php_error($errno, $errstr, $errfile, $errline, $errcontext=array()) {
        // funnel to the exception handler
        $this->php_exception(new ErrorException($errstr, $errno, 0, $errfile, $errline));
    }

    /**
     * Exception handler.
     */
    public function php_exception($e) {
        $output = '';

        // clear out any output
        while (ob_get_level() > 1) {
            $output = ob_get_contents();
            ob_end_clean();
        }

        if (!headers_sent()) {
            header('Content-type: text/html');
        }
        $title = 'Uncaught Exception';
        if (isset($this->error_types[$e->getCode()])) {
            $title = $this->error_types[$e->getCode()];
        }
        print "</script>";
        print "<div style=\"padding:10px 20px;border:1px solid red;background-color:#ffdddd\">";
        print "<h3>{$title}</h3>";
        print "<h4>".$e->getMessage()."</h4>";
        print "<div>".$e->getFile().":".$e->getLine()."</div>";

        $this->print_snippet($e->getFile(), $e->getLine());
        print "</div>";

        $this->print_backtrace($e->getTrace());

        print "<h4>Output</h4>";
        print "<div style=\"font-family:monospace;border:1px solid #c1c1c1;padding:20px;white-space:pre;box-shadow:0 0 10px 0 rgba(0, 0, 0, 0.2)\">" . htmlentities($output) . "</div>";

        if (!empty($_POST)) {
            print "<h3>POST Data</h3>";
            $this->print_array($_POST);
        }

        if (!empty($_GET)) {
            print "<h3>GET Data</h3>";
            $this->print_array($_GET);
        }

        print "<h3>Server Variables</h3>";
        $this->print_array($_SERVER);

        print "<h3>Response Headers</h3>";
        $this->print_array(apache_response_headers());

    }

    private function print_array($headers) {
        print "<table cellpadding=0 cellspacing=0>";
        foreach ($headers as $key => $value) {
            print "<tr>";
            print "<td style=\"padding: 4px 20px 4px 4px; border-bottom:1px dotted #c1c1c1\">" . htmlentities($key) . "</td>";
            print "<td style=\"font-family:monospace;border-bottom:1px dotted #c1c1c1\">" . htmlentities((string) $value) . "</td>";
            print "</tr>";
        }
        print "</table>";
    }

    private function print_backtrace($trace) {
        for ($i = 0; $i < count($trace); $i++) {
            $frame = $trace[$i];
            if ($frame['function'] == 'php_error') continue;
            $this->print_stack_frame($trace[$i]);
        }
    }

    private function print_stack_frame($frame) {
        $fn = $frame['function'];
        if (isset($frame['class'])) {
            $fn = $frame['class'] . '::' . $fn;
        }
        print "<div style=\"padding:5px 20px;border:1px solid #c1c1c1;background-color:#f0f0f0;border-width:0 1px 1px 1px;\">";
        print "<div style=\"margin: 10px 0\">";
        print '<div>' . $fn . "()</div>";
        print "</div>";

        if (isset($frame['file'])) {
            print '<div>Called from: ' . $frame['file'] . ', line ' . $frame['line'] . "</div>";
            $this->print_snippet($frame['file'], $frame['line']);
        }
        print "</div>";
    }

    private function print_snippet($file, $line_number, $n = 5) {
        $contents = file_get_contents($file);
        $lines = explode("\n", $contents);
        $l1 = max(0, $line_number - $n);
        $l2 = min(count($lines), $line_number + $n);

        for ($i = $l1; $i < $l2; $i++) {
            $line = $lines[$i];
            $style = ($i + 1 == $line_number) ? 'font-weight:bold' : '';
            print "<div style='font-family:monospace;white-space:pre;line-height:16px;padding-left:20px;font-size:12px'>";
            print "<div style='{$style}'>";
            print "<span style='color:#999;margin-right:10px'>" . str_pad($i + 1, 5, ' ', STR_PAD_LEFT) . "</span>";
            print htmlentities($line);
            print "</div>";
            print "</div>";
        }
    }
}





