<?php

function load_cli_args() {
	$args = $_SERVER['argv'];

	// parse any extra args
	for ($i = 2; $i < sizeof($args); $i++) {
		$arg = $args[$i];
		if (strpos($arg, '-') === 0) {
			$arg_name = substr($arg, 1);
		} else {
			switch($arg_name) {
				case 'get':
					parse_str($arg, $_GET);
					break;
				case 'post':
					parse_str($arg, $_POST);
					break;
				default:
					die("unknown CLI parameter: " . $arg_name . "\n");
			}
		}
	}
}

class Controller {
	public function setup() {
	}
}


define('PLUGIN_DIR', dirname(dirname(__FILE__)));



// run the job

if (PHP_SAPI != 'cli') {
	die("Access must be via CLI only\n");
}

$args = $_SERVER['argv'];
if (sizeof($args) < 2) {
	die("No job specified\n");
}

$job = $args[1];
$parts = explode("/", $job);

$controller = $parts[0];
$action = $parts[1];
$job_args = array_slice($parts, 2);

if (is_null($action)) {
	$action = 'index';
}

// load the controller file
$file = 'controllers/' . $controller . '.php';
if (!file_exists($file)) {
	die("controller not found\n");
}
require_once $file;

// check that the controller method exists
$controller_class = ucfirst($controller);
$cls = new ReflectionClass($controller_class);
if (!$cls->hasMethod($action)) {
	die("action not found\n");
}

// check that the action is available
$method = $cls->getMethod($action);
if (!$method->isPublic() || $method->isStatic()) {
	die("action is not accessible\n");
}

// check that all required params are passed
$num_required = $method->getNumberOfRequiredParameters();
if (count($job_args) < $num_required) {
	$num_missing = $num_required - count($job_args);
	die("job is missing {$num_missing} required args\n");
}

// instantiate the controller
load_cli_args();
$obj = $cls->newInstance();
$obj->setup();

// run
$method->invokeArgs($obj, $job_args);