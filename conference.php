<?php
/*
Plugin Name: Conference Management
Description: Stanford AIM Lab conference management software.
Version: 0.1
Author: Karl Uhlig
*/

///////////////////////////
// Wordpress integration //
///////////////////////////

// session and output hacks
ob_start();
date_default_timezone_set('America/Los_Angeles');
session_start();

// load the WPCI interface
require_once 'wpci_adapter.php';


// PER-SERVER CONFIGURATION
define('PRIVATE_DIR', '/tmp/medx_private/');

define('PDF_MAKER_DIR', '/Users/knuhlig/Documents/workspace/badges/');
define('PDF_MAKER', PDF_MAKER_DIR . 'generate.sh');
define('SPEAKER_LETTER_XMLFILE', PDF_MAKER_DIR . "examples/speaker_letter.xml");
define('SPEAKER_LETTER_ALT_XMLFILE', PDF_MAKER_DIR . "examples/speaker_letter_alt.xml");
define('SPEAKER_LETTER_FONTFILE', "/tmp/letterhead.pdf");

define('PDF_GENERATOR', "/medx/wkhtmltopdf");

// common definitions
$wp_dir = wp_upload_dir();
define('WPCM_EMAIL_MODE', 'smtp');

define('WPCM_UPLOADS_DIR', $wp_dir['basedir'] . '/conference/');
define('WPCM_UPLOADS_URL', $wp_dir['baseurl'] . '/conference/');
define('CM_PATH', 'cm_path');
define('DATE_FMT_FULL', 'n/j/Y g:i a');
define('DATE_FMT_PRETTY', 'n/j/y, ga');
define('DATE_FMT_LETTER', 'F j, Y');
define('DATE_FMT_TIME', 'g:i a');
define('CONFERENCE_START', strtotime('2015-09-25 09:00:00'));
define('CONFERENCE_END', strtotime('2015-09-27 18:00:00'));


if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$parts = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
	define('CM_REMOTE_IP', trim($parts[0]));
} else {
	define('CM_REMOTE_IP', $_SERVER['REMOTE_ADDR']);
}

if (!empty($_GET['__unarchive'])) {
	unset($_SESSION['archive_key']);
	unset($_GET['__unarchive']);
}

if (!empty($_SESSION['archive_key'])) {
	define('ARCHIVE_KEY', $_SESSION['archive_key']);
}

class ConferenceManagement extends WPCI {

    public function url_identifier() {
        return get_option(CM_PATH);
    }

    public function create_admin_menu() {
        $pos = 31;

        add_admin_menu_separator($pos++);

		//$parent = $this->add_menu_page('Conference', $pos++, 'browse_conference');
        //$this->add_submenu_page($parent, 'Conferences', 'admin/browse/conference', $parent);

        $parent = $this->add_menu_page('Schedule', $pos++, 'browse_event');
        $this->add_submenu_page($parent, 'Talks/Events', 'admin/browse/event', $parent);
        $this->add_submenu_page(null, 'Eveny Summary', 'admin/event_summary', 'event_summary');
        $this->add_submenu_page($parent, 'Speakers', 'admin/browse/speaker', 'browse_speaker');
        $this->add_submenu_page($parent, 'Locations', 'admin/browse/location', 'browse_location');

        $parent = $this->add_menu_page('Registration', $pos++, 'registration_overview');
        $this->add_submenu_page($parent, 'Overview', 'admin/registration_overview', $parent);
        $this->add_submenu_page($parent, 'Registrations', 'admin/browse/order', 'browse_order');
        $this->add_submenu_page(null, 'Add-On summary', 'admin/addon_summary', 'addon_summary');
        $this->add_submenu_page(null, 'Rate summary', 'admin/rate_summary', 'rate_summary');
        $this->add_submenu_page($parent, 'Rates', 'admin/browse/rate', 'browse_rate');
        $this->add_submenu_page($parent, 'Campaigns', 'admin/browse/offer', 'browse_offer');
        $this->add_submenu_page($parent, 'Add-Ons', 'admin/browse/addon', 'browse_addon');
        $this->add_submenu_page($parent, 'Access Levels', 'admin/browse/regtype', 'browse_regtype');
        $this->add_submenu_page($parent, 'Special Payments', 'admin/browse/payment', 'browse_payment');
        $this->add_submenu_page($parent, 'Donations', 'admin/browse/donation', 'browse_donation');
        $this->add_submenu_page($parent, 'Badges', 'admin/browse/badge', 'browse_badge');
        
        $parent = $this->add_menu_page('Abstracts', $pos++, 'abstracts_overview');
        $this->add_submenu_page($parent, 'Overview', 'submission/overview', $parent);
        $this->add_submenu_page($parent, 'Submissions', 'admin/browse/submission', 'browse_submission');
        $this->add_submenu_page($parent, 'Tracks', 'admin/browse/track', 'browse_track');
        $this->add_submenu_page($parent, 'Categories', 'admin/browse/category', 'browse_category');
        $this->add_submenu_page($parent, 'Reviewers', 'submission/reviewers', 'abstract_reviewers');

		$parent = $this->add_menu_page('Users', $pos++, 'browse_user');
        $this->add_submenu_page($parent, 'Users', 'admin/browse/user', 'browse_user');
        $this->add_submenu_page($parent, 'User Connections', 'admin/browse/connection', 'browse_connection');

        $parent = $this->add_menu_page('General', $pos++, 'edit_settings');
        $this->add_submenu_page($parent, 'Settings', 'admin/edit/settings', $parent);
        $this->add_submenu_page($parent, 'Email Templates', 'admin/browse/template', 'browse_template');
        $this->add_submenu_page($parent, 'Sent Emails', 'admin/browse/email', 'browse_email');
        $this->add_submenu_page($parent, 'Archives', 'admin/archive', 'archives');
        $this->add_submenu_page($parent, 'Assume Identity', 'admin/identity', 'admin_identity');


        $this->add_submenu_page(null, 'New Event', 'admin/edit/event', 'edit_event');
        $this->add_submenu_page(null, 'New Speaker', 'admin/edit/speaker', 'edit_speaker');
        $this->add_submenu_page(null, 'New Badge', 'admin/edit/badge', 'edit_badge');
        $this->add_submenu_page(null, 'New Registration', 'admin/edit/order', 'edit_order');
        $this->add_submenu_page(null, 'New Campaign', 'admin/edit/offer', 'edit_offer');
        $this->add_submenu_page(null, 'New Rate', 'admin/edit/rate', 'edit_rate');
        $this->add_submenu_page(null, 'New Add-On', 'admin/edit/addon', 'edit_addon');
        $this->add_submenu_page(null, 'New Access Level', 'admin/edit/regtype', 'edit_regtype');
        $this->add_submenu_page(null, 'Email Offer', 'admin/email/offer', 'email_offer');
        $this->add_submenu_page(null, 'Email Speaker', 'admin/email/speaker', 'email_speaker');
        $this->add_submenu_page(null, 'New Payment', 'admin/edit/payment', 'edit_payment');
        $this->add_submenu_page(null, 'New Connection', 'admin/edit/connection', 'edit_connection');
        $this->add_submenu_page(null, 'New Submission', 'admin/edit/submission', 'edit_submission');
        $this->add_submenu_page(null, 'New Track', 'admin/edit/track', 'edit_track');
        $this->add_submenu_page(null, 'New Category', 'admin/edit/category', 'edit_category');
        $this->add_submenu_page(null, 'New User', 'admin/edit/user', 'edit_user');
        $this->add_submenu_page(null, 'New Template', 'admin/edit/template', 'edit_template');
        $this->add_submenu_page(null, 'New Email', 'admin/edit/email', 'edit_email');
        $this->add_submenu_page(null, 'New Conference', 'admin/edit/conference', 'edit_conference');
        $this->add_submenu_page(null, 'New Location', 'admin/edit/location', 'edit_location');
        $this->add_submenu_page(null, 'New Donation', 'admin/edit/donation', 'edit_donation');

        add_admin_menu_separator($pos++);



        add_settings_section('section_id', 'Section Title', array($this, 'test_settings'), $parent);
    }

	public function show_admin_page() {
		if (defined('ARCHIVE_KEY')) {
			echo "<div class='error'>";
			echo "<p>You are browsing the Medicine X archive \"".ARCHIVE_KEY."\"</p>";
			echo "<p><a href='".modified_url(array('__unarchive' => '1'))."'>Click Here</a> to switch back.</p>";
			echo "</div>";
		}
		return parent::show_admin_page();
	}

    public function test_settings() {
        print "HELLO!";
    }

    public function shortcode($attrs) {
    	$page = $attrs['page'];
    	return "<div class='wpcm-wrap'>" . $this->fetch($page, $attrs) . "</div>";
    }

    public function register_post_types() {
    	add_shortcode('wpcm', array($this, 'shortcode'));
    }

    public function filter_request($r) {
    	if (is_null($r)) return $r;

    	$parts = explode("/", substr($r, 1));
        if ($parts[0] == 'a') {
            ArchiveDB::set($parts[1]);
            $r = "/" . join("/", array_slice($parts, 2));
        }
        
    	return $r;
    }

    public function register_scripts() {

        wp_register_script('uform-script', plugins_url('/media/uform.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('uform-script');

        wp_register_script('wizard-script', plugins_url('/media/jquery.smartWizard-2.0.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('wizard-script');

        wp_register_script('dd-script', plugins_url('/media/js/jquery.ddslick.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('dd-script');

        wp_register_style('wizard-style', plugins_url('/media/smart_wizard.css', WPCI_FILE));
        wp_enqueue_style('wizard-style');

        wp_enqueue_script(array('jquery', 'editor', 'thickbox', 'media-upload'));
        wp_enqueue_style('thickbox');

        wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js');
        wp_enqueue_style('jquery-ui-css', 'http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css');

        wp_register_style('wysi-style', 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css');
        wp_enqueue_style('wysi-style');

		wp_register_script('rt-rules', plugins_url('/media/js/xing/parser_rules/advanced.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('rt-rules');
		wp_register_script('rt-script', plugins_url('/media/js/xing/dist/wysihtml5-0.3.0.min.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('rt-script');
        
        wp_register_script('dp-script', plugins_url('/media/js/datepicker/datepicker.js', WPCI_FILE), array('jquery'));
        wp_enqueue_script('dp-script');
    }

    public function admin_init() {
    	global $concatenate_scripts;

    	$concatenate_scripts = false;
    	wp_deregister_script('jquery');
    	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js', false, '1.x', true);


        wp_enqueue_script('word-count');
        wp_enqueue_script('post');
        wp_enqueue_script('editor');
        wp_enqueue_script('media-upload');
    }

    public function load_styles() {
        add_thickbox();
	    wp_enqueue_script('media-upload');
    }

    public function wp_head() {
        print '<link rel="stylesheet" type="text/css" href="'.plugins_url('/media/style_compiled.css?'.time(), WPCI_FILE).'"/>';
        print '<link rel="stylesheet" type="text/css" media="print" href="'.plugins_url('/media/print.css?'.time(), WPCI_FILE).'"/>';
        print '<script type="text/javascript" src="'. ci_url('api/js') . '"></script>';
    }

}

$c = new ConferenceManagement();
$c->initialize();


?>