<?php

// -------- CONSTANTS ---------------
/**
 * Defines the root directory that this installation lives in.
 */
define('WPCI_PATH', WP_PLUGIN_DIR.'/'.basename(dirname($plugin)));

/**
 * Defines a full path to this (plugin) file.
 */
define('WPCI_FILE', WPCI_PATH.'/'.basename($plugin));

// load common/global functions
require_once WPCI_PATH . '/' . 'functions.php';


/**
 * This class provides an interface between the Wordpress plugin system and
 * CodeIgniter.
 */
class WPCI {

    private $menu_id;
    private $admin_pages;

    public function __construct() {
        $this->menu_id = 1;
        $this->admin_pages = array();

        // necessary for custom functions like ci_url().
        $GLOBALS['WPCI'] = $this;
    }


    // ----------- Subclass Overrides ---------------------

    public function create_admin_menu() {}
    public function register_post_types() {}
    public function register_scripts() {}
	public function admin_init() {}
	public function load_styles() {}
	public function wp_head() {}

    /**
     * Returns the request prefix that this plugin uses to identify requests meant
     * for it.
     * @return Request prefix.
     */
    public function url_identifier() {
        throw new Exception("Subclass must override");
    }

    public function load_mce() {
        wp_tiny_mce();
    }

    public function mce_css($url) {
    	if (!empty($url)) $url .= ',';

    	// Change the path here if using sub-directory
    	$url .= media_url('mce.css?t=' . time());

    	return $url;
    }

    public function filter_request($request) {
    	return $request;
    }


    // ----------- Core Interface -----------------------

    /**
     * Adds hooks to Wordpress to enable the CodeIgniter application.
     */
    public function initialize() {
        session_start();

        // installation/activation hook
        register_activation_hook(WPCI_FILE, array($this, 'install'));

        add_action('admin_init', array($this, 'admin_init'));

        add_action('admin_menu', array($this, 'register_scripts'));
        add_action('admin_menu', array($this, 'load_styles'));
        add_action('admin_menu', array($this, 'create_admin_menu'));

        add_action('admin_head', array($this, 'load_mce'));

        add_action('wp_enqueue_scripts', array($this, 'register_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
        add_action('init', array($this, 'register_post_types'));
        //add_filter('login_redirect', array($this, 'login_redirect'));
        add_action('wp', array($this, 'run'));

        add_action('wp_head', array($this, 'wp_head'));
        add_action('admin_head', array($this, 'wp_head'));

        //add_filter('authenticate', array($this, 'authenticate'), 100, 3);
        add_filter('mce_css', array($this, 'mce_css'));
    }

    /**
     * Runs the Wordpress-CodeIgniter adapter interface. Checks if the URL is requesting
     * a CI page, and if so reroutes the request accordingly.
     */
    public function run() {
        global $wp_query, $CFG, $wpdb;
        $GLOBALS['wpdb'] = $wpdb;

        // determine if a CI action is being requested
        $request = $this->ci_request();
        if (is_null($request)) {
            return;
        }

    	// filter request
		$request = $this->filter_request($request);

        // Wordpress thinks the request is an error until we decide to handle it
        // Reset WP state.
        status_header(200);
        $wp_query->is_404  = false;

        // send over to CI
        $_SERVER['REQUEST_URI'] = $request;
        include 'wpci_index.php';
        die;
    }

    public function authenticate($user, $username, $password) {
    	return $user;
    }



    /**
     * Performs a CI request and returns the results.
     * @param ci_request The CI request, e.g. user/home
     * @return The response output.
     */
    function fetch($ci_request, $attrs=array()) {
        global $wp_query, $CFG, $wpdb;
        $GLOBALS['wpdb'] = $wpdb;

        if ($wp_query->is_404) {
	        status_header(200);
	        $wp_query->is_404  = false;
        }

        // send over to CI via command-line interface
        if (!defined('STDIN')) {
        	define('STDIN', 'STDIN');
        }

		// filter request
		$ci_request = $this->filter_request($ci_request);
        $_SERVER['argv'] = array_merge(array(null), explode('/',$ci_request));

        ob_start();


        $level = ob_get_level();

        $prev = ArchiveDB::get();
        if (isset($attrs['archive'])) {
            ArchiveDB::set($attrs['archive']);
        }

        // may or may not return to correct level
        include 'wpci_index.php';

        ArchiveDB::set($prev);

        // force correct level
        while (ob_get_level() > $level) {
        	ob_end_flush();
        }

        $contents = ob_get_clean();
        return $contents;
    }

    /**
     * Returns a complete URL for a CI request path.
     * @param $path The CI request, e.g. user/home
     * @param $params Optional array of GET parameters.
     * @return Complete URL.
     */
    function ci_url($path, $params=array()) {
        if (empty($path) || $path[0] != '/') $path = '/' . $path;

        $db = ArchiveDB::get();
        if (!empty($db)) {
            $path = '/a/' . $db . $path;
        }
        $url = $this->wp_url_host() . $this->wp_url_path() . $this->url_identifier() . $path;
        if (!empty($params)) {
            $url .= '?' . param_str($params);
        }
        return $url;
    }

    /**
     * Installs/Activates the plugin.
     */
    public function install() {
        $path = WPCI_PATH . '/install.php';
        if (file_exists($path)) {
            include $path;
        }
    }

    public function add_menu_page($title, $pos, $slug=null) {
        if (is_null($slug)) {
            $slug = $this->next_slug();
        }
        // add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        add_menu_page(
            $title,
            $title,
            'administrator',
            $slug,
            null,
            null,
            $pos
        );
        return $slug;
    }

    public function show_admin_page() {
        $slug = $_GET['page'];
        if (empty($this->admin_pages[$slug])) {
            print "ERROR: this page is not registered";
            return;
        }

        $ci_page = $this->admin_pages[$slug];

        print "<div class='wpcm-wrap'>";
        print $this->fetch($ci_page);
        print "</div>";
    }

    public function add_submenu_page($parent, $title, $ci_page, $slug=null) {
        if (is_null($slug)) {
            $slug = $this->next_slug();
        }
        $this->admin_pages[$slug] = $ci_page;

        $self = $this;
        add_submenu_page(
            $parent,
            $title, // page title
            $title, // menu title
            'administrator', // capability
            $slug, // slug
            array($this, 'show_admin_page') // function
        );
        return $slug;
    }


    /**
     * Redirects to a CI-formatted url.
     * @param $ci_path The CI page, e.g. user/home
     * @params GET parameters (optional)
     */
    public function ci_redirect($ci_path, $params=array()) {
        $path = $this->url_identifier() . '/' . $ci_path;
        if (!empty($params)) {
            $path .= '?' . param_str($params);
        }
        redirect($path);
    }

    /**
     * Takes the server request, parses it, and determines whether the request is meant
     * to be handled by CodeIgniter in this plugin. If so, returns the CI-formatted
     * request; otherwise NULL.
     * @return The CI request, or NULL if not intended for CI.
     */
    public function ci_request() {
        $ci_identifier = $this->url_identifier();
        $wp_request = $this->wp_request();

        // see if request starts with our identifier
        if (strpos($wp_request, $ci_identifier) !== 0) {
            return null;
        }

        // snip off the identifier
        $request = substr($wp_request, strlen($ci_identifier));

        // make sure the rest of the request starts with '/'
        if (!empty($request) && $request[0] != '/') {
            return null;
        }

        return $request;
    }

    /**
     * Returns the requested URI, relative to the Wordpress installation root. If
     * Wordpress is installed at "http://mysite.com/abc/wordpress" and the requested page
     * is "http://mysite.com/abc/wordpress/foo/bar", this method returns "foo/bar".
     * @return Request URI relative to wordpress root, e.g. "foo/bar"
     */
    public function wp_request() {
        $base = $this->wp_url_path();
        $uri = $_SERVER['REQUEST_URI'];

        // edge case
        if (empty($base)) {
            return $uri;
        }

        // the request URI should always start with $base
        if (strpos($uri, $base) !== 0) {
            throw new Exception("mismatch between wordpress site path and request URI");
        }

        // cut off the Wordpress path, leaving extras.
        return substr($uri, strlen($base));
    }


    /**
     * Returns the path of the Wordpress installation, relative to the host root.
     * For example, if Wordpress is installed at http://mysite.com/abc/wordpress, this
     * method returns "/abc/wordpress".
     * @return The url path of the wordpress installation.
     */
    private function wp_url_path() {
        $url = parse_url(get_site_url());
        if (empty($url['path'])) return '';
        return $url['path'];
    }

    /**
     * Returns the protocol and hostname of the wordpress installation.
     * For example, if wordpress is installed at http://mysite.com/abc/wordpress, this
     * method returns "http://mysite.com". Note that this method is complementary to
     * wp_url_path().
     * @return The hostname, e..g "http://mysite.com"
     */
    private function wp_url_host() {
        $url = parse_url(get_site_url());
        return $url['scheme'] . "://" . $url['host'];
    }


    private function next_slug() {
        return 'wpci_' . ($this->menu_id++);
    }
}

?>