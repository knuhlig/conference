<?php

define ('SECRET_KEY', 'd366d4ab00d041fea4f405f3b07d0a2b7e24539323ab43b29999fcec969d62c3639c9c57c20e490badfcc89c0a63f2434a2ea038859f49aeb551c0c801956bf51717c63b6b0e4e619e0715d77d47f9d2d5f3257511154f849e793b0368f81314436f0e8b0f904460b06be33a123fb3a7912ed97d75144b249d704ca2a3d441ff');


function cybs_endpoint() {
  return 'https://secureacceptance.cybersource.com/pay';
}

function cybs_sign($params) {
  return cybs_signData(cybs_buildDataToSign($params), SECRET_KEY);
}

function cybs_signData($data, $secretKey) {
  return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function cybs_buildDataToSign($params) {
  $signedFieldNames = explode(",",$params["signed_field_names"]);
  foreach ($signedFieldNames as &$field) {
    $dataToSign[] = $field . "=" . $params[$field];
  }
  return cybs_commaSeparate($dataToSign);
}

function cybs_commaSeparate($dataToSign) {
  return implode(",",$dataToSign);
}

function cybs_verify_signature($params) {
  return strcmp($params['signature'], cybs_sign($params)) == 0;
}

?>