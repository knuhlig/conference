<?php

class Node {

	public function __construct($g) {
		$this->g = $g;
		$this->children = array();
	}
}

/**
 * Class for layout and display of the conference schedule.
 * @author knuhlig
 *
 */
class Schedule {

	private $events;
	private $speakers;
	private $photos;
	private $locations;
	private $empty_location;
	private $attendance;
	private $day_schedule;
	private $buttons;
	private $show_signups;
	private $user;
	private $disable_buttons;

	/**
	 * Creates a new schedule containing the given events.
	 * @param unknown $events
	 */
	public function __construct($events=null) {
		if (!empty($events)) {
			$this->load_events($events);
		}
		$this->show_signups = false;
		$this->disable_buttons = false;
	}

	public function load_events($events) {
		$this->events = array();
		foreach ($events as $e) {
			$this->events[$e->id] = $e;
		}
		$this->load_speakers();
		$this->load_photos();
		$this->load_locations();
		$this->day_schedule = $this->day_schedule();
	}

	public function set_user($user) {
		$this->user = $user;
	}

	public function load_attendance($user) {
		$this->show_signups = true;
		$beans = R::find('attendance', 'user_id = ?', array($user->id));
		if (empty($beans)) {
			$beans = $this->create_default_attendance($user);
		}

		$this->attendance = array();
		foreach ($beans as $bean) {
			$this->attendance[$bean->event_id] = $bean->will_attend;
		}
	}

	private function create_default_attendance($user) {
		$events = R::find('event', 'location_id in (select id from wp_cm_location where is_default)');
		$arr = array();
		foreach ($events as $event) {
			// create the object
			$b = R::dispense('attendance');
			$b->user = $user;
			$b->event = $event;
			$b->will_attend = 1;
			$b->event_date = $event->event_date;
			$b->date_updated = time();
			$arr[] = $b;
		}
		R::storeAll($arr);
		return $arr;
	}

	private function load_locations() {
		$this->locations = array();

		$locs = R::findAll('location');
		foreach ($locs as $loc) {
			$this->locations[$loc->id] = $loc;
		}

		$this->empty_location = R::dispense('location');
		$this->empty_location->id = null;
		$this->empty_location->name = '';
	}

	/**
	 * Returns the preloaded list of speakers for an event.
	 * @param array speaker list for $e
	 */
	public function get_speakers($e) {
		return $this->speakers[$e->id];
	}

	/**
	 * Returns a hierarchical schedule, grouped by day.
	 * @return array day => listings pairs
	 */
	public function day_schedule() {
		$sched = $this->grouped_schedule();
		if (property($_GET, 'signups')) {
			$sched = $this->filter_schedule($sched);
		}
		if (property($_GET, 'personal')) {
			$sched = $this->filter_personal($sched);
		}

		$day = null;
		$cur_time = null;
		$arr = array();
		
		foreach ($sched as $time => $items) {
			$cur_day = date('z', $time);
			if ($cur_day != $day) {
				$day = $cur_day;
				$cur_time = $time;
				$arr[$cur_time] = array();
			}
			$arr[$cur_time][$time] = $items;
		}
			
		return $arr;
	}

	private function filter_schedule($sched) {
		$res = array();

		foreach ($sched as $time => $items) {
			// eliminate disabled
			$total = 0;
			foreach ($items as $item) {
				$total += $this->count_signups($item);
			}
			if ($total > 1) {
				$res[$time] = $items;
			}
		}

		return $res;
	}

	private function filter_personal($sched) {
		$res = array();

		foreach ($sched as $time => $items) {
			foreach ($items as $item) {
				if (is_array($item)) {
					$item = $this->filter_group($item);
					$loc = $this->get_location($item['group']);
					$is_default = $loc->is_default;
					if (!empty($item['events'])) {
						$res[$time][] = $item;
					}
				} else {
					$loc = $this->get_location($item);
					$is_default = $loc->is_default;
					if ($this->is_selected($item)) {
						$res[$time][] = $item;
					}
				}
			}
		}

		return $res;
	}

	private function filter_group($group, $depth=0) {

		if (!$this->is_selected($group['group'], $depth)) {
			$res_events = array();
			foreach ($group['events'] as $time => $items) {
				foreach ($items as $item) {
					if (is_array($item)) {
						$item = $this->filter_group($item, $depth + 1);
						$loc = $this->get_location($item['group']);
						$is_default = $loc->is_default;

						if ($this->is_selected($item['group']) || !empty($item['events'])) {
							$res_events[$time][] = $item;
						}
					} else {
						$loc = $this->get_location($item);
						$is_default = $loc->is_default;
						if ($this->is_selected($item, $depth + 1)) {
							$res_events[$time][] = $item;
						}
					}
				}
			}
			$group['events'] = $res_events;
		}
		return $group;
	}

	private function count_signups($item) {
		if (!is_array($item)) {
			if ($item->disable_signups) return 0;
			return 1;
		}

		$heading = $item['group'];
		if (!empty($heading->location_id)) {
			return 1;
		}

		$keys = array_keys($item);
		$count = 0;
		foreach ($item['events'] as $time => $list) {
			foreach ($list as $e) {
				$count += $this->count_signups($e);
			}
		}

		return $count;
	}

	/**
	 * Returns the URL of a speaker photo to use.
	 * @param unknown $s
	 * @param string $size
	 */
	public function speaker_photo($s, $size='wide') {
		// for wide photos, use profile photo if available
		if ($size == 'wide') {
			$pid = $s->profile_photo_id;
			if ($pid) return $this->photos[$pid]->thumbnail_url($size);
		}
		
		// regular speaker photo
		$pid = $s->photo_id;
		if ($pid) {
			return $this->photos[$pid]->thumbnail_url($size);
		}
		
		// TODO: replace with placeholder
		return null;
	}
	
	public function event_photo($e, $size='wide') {
		$pid = $e->photo_id;
		if ($pid) {
			return $this->photos[$pid]->thumbnail_url($size);
		}
		return null;
	}

	public function create_pdf_file($pdffile) {
		$CI =& get_instance();

        $htmlfile = "/medx/receipts/schedule_" . uniqid() . ".html";

        // check if receipt already exists and is up to date

        $html = $this->generate_pdf_html();
        $pdf_generator = PDF_GENERATOR;
		
		file_put_contents($htmlfile, $html);

        $cmd = "{$pdf_generator} {$htmlfile} {$pdffile}";
        $res = shell_exec($cmd);

        if (file_exists($pdffile)) {
            return $pdffile;
        }
        return false;
	}

	public function create_pdf() {
		ob_start();
		$sched = $this->grouped_schedule();

		foreach ($this->day_schedule as $day_stamp => $items) {
			// print day header
			print "<table class='day-table' cellspacing=0 cellpadding=0 border=0>";
			$day = date('l, F j', $day_stamp);
			print "<tr><td colspan=100 class='day'>{$day}</td></tr>";

			// print day blocks
			foreach ($items as $time => $list) {
				print "<tr>";

				print "<td class='time'>";
				print date("g:i a", $time);
				print "</td>";

				print "<td class='block'>";
				foreach ($list as $item) {
					$this->print_pdf_item($item);
				}
				print "</td>";
				print "</tr>";
			}

			print "</table>";
		}

		$content = ob_get_clean();

		$CI =& get_instance();
		$data = array(
			'content' => $content
		);
		$CI->load->view('conference/pdf_schedule', $data);
	}

	private function print_pdf_item($item, $depth=0) {
		
		if (is_array($item)) {
			$e = $item['group'];
		} else {
			$e = $item;
		}

		$loc = $this->location_name($e);
		print "<div class='event type-".$e->type."'>";
		if ($depth == 0) print "<div class='location'>" . $this->location_name($e) . "</div>";

		print "<div class='wrap'>";
		if ($depth > 0) print "<div class='interval'>" . $this->time_interval($e->event_date, $e->duration) . "</div>";
		print "<div class='title'>" . $e->full_title() . "</div>";
		print "<div class='speakers'>" . $e->speaker_list() . "</div>";
		print "</div>";

		if (is_array($item)) {
			$new_depth = $depth + (empty($loc) ? 0 : 1);
			$list = $item['events'];

			print "<table class='sub-items' cellspacing=0 cellpadding=0 border=0 width='100%'>";
			foreach ($list as $time => $items) {
				$cls = '';
				if (sizeof($items) > 1) $cls = 'columns';
				print "<tr class='{$cls}'>";
				foreach ($items as $item) {
					print "<td class='block' style='width:".(100/sizeof($items))."%'>";
					$this->print_pdf_item($item, $new_depth);
					print "</td>";
					if (sizeof($items) > 3) {
						print "</tr><tr>";
					}
				}
				print "</tr>";
			}
			print "</table>";
		}

		print "</div>";
	}

	/**
	 * Prints the schedule in an HTML-formatted view.
	 */
	public function print_schedule() {
		$page = get_page_by_path('2015-schedule');
		$schedule_page = get_permalink($page->ID);

		if (!$this->show_signups) {
			if (empty($this->user)) {
				$url = ci_url("user/login", array('r' => $schedule_page . "?personal=1"));

				print "<div style='padding:10px;background-color:#f0f0f0;margin-bottom:10px'>";
				print "<div>Registered to attend Medicine X? <a href='".$url."'>Log in</a> to view or customize your personal schedule</div>";
				print "</div>";
			}
		} else {
			print "<h3>Schedule for ".$this->user->full_name()."</h3>";
			print "<div style='padding:10px 20px;background-color:#f0f0f0;margin-bottom:10px'>";

			if (property($_GET, 'personal')) {
				print "<div><b>Customize your schedule</b></div>";
				print "<div style='margin: 2px 0'>Visit the <a href='".$schedule_page."?signups=1'>Parallel Sessions</a> page to find additional sessions to attend.</div>";
				print "<div>You can also visit the <a href='".$schedule_page."'>Complete Schedule</a> page to see all options.</div>";
				$this->disable_buttons = true;
			} else {
				print "<div>To sign up for an event, click the checkbox next to the event name, like this: <i class='fa fa-check-circle-o selected' style='margin-left:10px;font-size:20px;color:green'></i></div>";
				print "<div><i>Please note that some events require special registration and do not have a signup button.</i></div>";
					
				if (property($_GET, 'signups')) {
					print "<div><b>Showing parallel sessions only</b></div>";
					print "<div><a href='".$schedule_page."'>Show Complete Schedule</a>";
				} else {
					print "<div><a href='{$schedule_page}?signups=1'>View Parallel Sessions Only</a>";
				}
				print " | <a href='{$schedule_page}?personal=1'>View Personalized Schedule</a></div>";
			}

			print "</div>";
		}

		// quick navigation links
		$days = $this->day_schedule;
		if (sizeof($days) > 1) {
			print "<div class='schedule-nav-title'>Quick Navigation</div>";
			print "<ul class='schedule-nav'>";
			foreach ($days as $day_stamp => $items) {
				$day = date('l, F j', $day_stamp);
				print "<li><a href='#{$day_stamp}'>{$day}</a></li>";
			}
			print "</ul>";
		}
		
		$first_day = true;
		foreach ($this->day_schedule as $day_stamp => $items) {
			// print day header
			$day = date('l, F j', $day_stamp);
			print "<a name='{$day_stamp}'></a>";
			$cls = 'schedule-day';
			if ($first_day) {
				$first_day = false;
				$cls .= ' first-day';
			}
			print "<div class='{$cls}'>{$day}</div>";

			// print day blocks
			foreach ($items as $time => $list) {
				$this->print_items($time, $list);
			}
		}

		if (empty($this->day_schedule)) {
			print "<h4>You haven't added any special events to your schedule yet.<br/><br/>Visit the <a href='{$schedule_page}?signups=1'>Parallel Sessions</a> or <a href='{$schedule_page}'>Complete Schedule</a> page to customize your schedule.</h4>";
		}

		if (!$this->disable_buttons) {
			$this->print_script();
		}
	}

	private function print_script() {
		print '
		<script>
			$(".event-selection").click(function() {
				var el = $(this);
				var willAttend = (el.hasClass("selected") ? 0 : 1);

				var data = {
					"id" : el.data("id"),
					"will_attend": willAttend
				};

				var time = el.data("time");
				$(".time-" + time).removeClass("selected");

				el.removeClass("fa-check-circle-o");
				el.html("<img src=\"'.media_url('images/ajax-loader.gif').'\"/>");
				
				$.post("'.ci_url('conference/attend').'", data, function(data) {
					el.html("");
					el.addClass("fa-check-circle-o");

					var res = $.parseJSON(data);

					if (res.status == "waitlist") {
						alert("This event is currently full. You are currently position "+res.position+" on the waitlist.");
						e.addClass("selected");
					} else {
						if (willAttend) {
							el.addClass("selected");
						}
					}
				});


			});
		</script>
		';
	}

	private function print_items($time, $list) {
		$first = reset($list);
		$e = is_array($first) ? $first['group'] : $first;
		$duration = $e->duration;


		$clusters = $this->get_group_clusters($list);

		$this->start_block($time, $duration, $clusters);
		if (sizeof($clusters) > 1) {
			//print "<div class='event-title type-message'>Multiple Sessions</div>";
		}

		foreach ($clusters as $cluster) {
			$this->print_cluster($cluster);
		}

		$this->end_block();
	}



	private function start_block($time, $duration, $clusters) {
		$extra_class = '';
		if (sizeof($clusters) == 1) {
			$first = $clusters[0][0];
			$e = is_array($first) ? $first['group'] : $first;
			$extra_class = "single-block type-" . $e->type;
		}
		print "<div id='timeblock_{$time}' class='time-block {$extra_class}'>";

		print "<div class='time'>";
		print $this->time_interval($time, $duration);
		print "<span class='day'>" . $this->event_date($time) . "</span>";
		print '</div>';

	}

	private function event_date($time) {
		return date('l, F j', $time);
	}

	private function end_block() {
		print "</div>";
	}

	private function print_cluster($cluster, $width=1, $depth=0) {
		$num_items = sizeof($cluster);
		$child_width = $width / $num_items;

		print "<table class='cluster'><tr>";

		if ($depth == 0 || is_array($cluster[0])) {
			foreach ($cluster as $item) {
				$heading = is_array($item) ? $item['group'] : $item;
				$loc = $this->get_location($heading);
				print "<td class='outer-title event-title type-{$heading->type} depth-{$depth} columns-{$num_items}'>";
				if ($loc->id) {
					print "<div class='event-location type-{$heading->type} depth-{$depth}'>";
					print $this->location_name($heading);
					print "</div>";
				}
				$this->print_button($heading, $item);
				print $heading->full_title();
				print "</td>";
			}
			print "</tr><tr>";
		}

		for ($i = 0; $i < sizeof($cluster); $i++) {
			$item = $cluster[$i];
			print "<td class='column columns-{$num_items}'>";
			$this->print_item($item, $child_width, $depth + 1);
			print "</td>";
			if (sizeof($cluster) > 4 && $i < sizeof($cluster) - 1) {
				print "</tr><tr>";
			}
		}
		print "</tr></table>";
	}

	private function location_name($e) {
		$loc = $this->get_location($e);
		return $loc->name;
	}

	private function is_selected($item, $depth=0) {
		if ($item->type != 'group' && $depth > 0) return false;
		return !empty($this->attendance[$item->id]);
	}

	private function print_button($e, $item) {
		if (!$this->show_signups) {
			return;
		}
		if ($e->disable_signups && !$this->disable_buttons) {
			return;
		}
		if (is_array($item) && empty($e->location_id)) {
			return;
		}

		$selected = $this->is_selected($e);
		if ($this->disable_buttons) $title = "You are signed up for this event.";
		else $title = "Click to select this event";
		print '<i data-time="'.$e->event_date.'" data-id="'.$e->id.'"';
		print ' class="fa fa-check-circle-o time-'.$e->event_date.' event-selection'.($selected ? " selected" : "").'"';
		print ' style="font-size:20px;width:25px'.($this->disable_buttons ? ';cursor:default' : '').'"';
		print ' title="'.$title.'"></i>';
	}

	private function print_item($item, $width=1, $depth=0) {
		print "<div class='schedule-item depth-{$depth}'>";


		if ($depth == 1 || is_array($item)) {
			$heading = is_array($item) ? $item['group'] : $item;
			$loc = $this->get_location($heading);
			print "<div class='inner-title event-title type-{$heading->type} depth-".($depth-1)."'>";
			if ($loc->id) {
				print "<div class='event-location type-{$heading->type} depth-".($depth-1)."'>";
				print $this->location_name($heading);
				print "</div>";
			}

			$this->print_button($heading, $item);
			print $heading->full_title();
			print "</div>";
		}


		if (is_array($item)) {
			foreach ($item['events'] as $time => $list) {
				$this->print_cluster($list, $width, $depth);	
			}
		} else {
			$this->print_content($item, $width, $depth);
		}

		print "</div>";
	}

	private function print_content($e, $width=1, $depth=0) {
		$time = $e->event_date;
		$this->buttons[$time][] = $e;

		$speakers = $this->get_speakers($e);
		$snippet = $e->event_snippet();

		if (empty($speakers) && empty($snippet) && empty($e->photo_id)) {
			return;
		}

		print "<div class='event-content depth-{$depth} width-".round($width*100)."'>";

		// photo size
		if ($width > 0.5) {
			if (sizeof($speakers) == 1 && $depth <= 1) {
				$size = 'wide';
			} else {
				$size = 'square';
			}
		} else {
			$size = 'tiny';
		}
		if ($depth > 1) {
			$size = 'tiny';
		}

		// output the photo(s)
		print "<div class='event-photo'>";
		if (!empty($e->photo_id) && $e->type != 'keynote' && $e->type != 'masterclass') {
			// load the custom image
			$image_url = $this->event_photo($e, $size);
			$title = $e->title;
			print "<img src='" . $image_url . "' title='{$title}'/>";
		} else {
			$c = 0;
			foreach ($speakers as $speaker) {
				if ($e->type == 'panel' && !$this->is_moderator($speaker, $e)) {
					continue;
				}
				$url = $this->speaker_photo($speaker, $size);
				print "<img src='" . $url . "' title='".$speaker->full_name()."'/>";
				$c++;
				if ($size == 'tiny') {
					print "<br/>";
				} else if (sizeof($speakers) == 4 && $c == 2) {
					print "<br/>";
				}
			}
		}
		print "</div>";

		// text content
		print "<div class='event-body depth-{$depth} width-".round($width*100)."'>";
		if ($depth > 1) {		
			print "<div class='inner-time'>" . $this->time_interval($e->start_date(), $e->duration) . "</div>";
			print "<div class='title'><a href='".$e->url()."'>" . $e->full_title() . "</a></div>";
			print "<div>";
			$names = array();
			foreach ($speakers as $s) {
				$names[] = $s->full_name();
			}
			print join(", ", $names);
			print "</div>";
			if ($e->type == 'panel') {
				print "<div>";
				foreach ($speakers as $speaker) {
					if (!$this->is_moderator($speaker, $e)) {
						$url = $this->speaker_photo($speaker, $size);
						print "<a href='".$speaker->url()."'><img src='" . $url . "' title='".$speaker->full_name()."'/></a>";
					}
				}
				print "</div>";
			}
		} else {
			if (!empty($speakers)) {
				print "<div class='event-speakers'>";
				foreach ($speakers as $speaker) {
					print "<div class='event-speaker'>";
					print "<div class='speaker-name'><a href='".$speaker->url()."'>" . $speaker->full_name() . "</a></div>";
					print "<div class='speaker-title'>" . $speaker->title . "</div>";
					print "</div>";
				}
				print "</div>";
			}
			print "<div class='event-snippet'>";
			print $snippet;
			print "</div>";
			if ($e->type == 'panel') {
				print "<div>";
				foreach ($speakers as $speaker) {
					if (!$this->is_moderator($speaker, $e)) {
						$url = $this->speaker_photo($speaker, 'tiny');
						print "<a href='".$speaker->url()."'><img src='" . $url . "' title='".$speaker->full_name()."'/></a>";
					}
				}
				print "</div>";
			}
		}
		print "</div>";

		print "</div>";
	}

	/**
	 * Returns a time range, e.g. 9:00 - 10:30am for a given (start_date, duration) pair.
	 * @param unknown $date
	 * @param unknown $duration
	 * @return string
	 */
	private function time_interval($date, $duration) {
		$end = $date + $duration * 60;
		
		$a1 = date('a', $date);
		$a2 = date('a', $end);
		
		if ($a1 == $a2) {
			return date('g:i', $date) . ' - ' . date('g:i a', $end);
		}
		
		return date('g:i a', $date) . ' - ' . date('g:i a', $end);
	}

	public function get_group_clusters($list) {
		$arr = array();
		$cols = array();
		$prev_type = null;

		foreach ($list as $block) {
			if (is_array($block)) {
				if (!empty($cols)) {
					$arr[] = $cols;
					$cols = array();
				}
				$arr[] = array($block);
			} else {
				if ($block->type == $prev_type) {
					$cols[] = $block;
				} else {
					if (!empty($cols)) {
						$arr[] = $cols;
						$cols = array();
					}
					$cols[] = $block;
					$prev_type = $block->type;
				}
			}
		}

		if (!empty($cols)) {
			$arr[] = $cols;
		}
		return $arr;
	}

	private function get_location($e) {
		return property($this->locations, $e->location_id, $this->empty_location);
	}

	private function get_locations($e) {
		$pivot_type = $e->pivot_type('locations');
		$ownKey = 'own' . ucfirst($pivot_type);
		$related = $e->$ownKey;

		$arr = array();
		foreach ($related as $row) {
			$arr[] = $this->locations[$row->locations_id];
		}
		return $arr;
	}

	private function find_place($e, $root) {
		if (is_null($root)) {
			return null;
		}

		$g = $root->g;

		// does it fit time-wise? we know e starts >= group
		if ($e->event_date >= $g->event_date + $g->duration * 60) {
			return null;
		}

		// check child groups first
		foreach ($root->children as $child) {
			$res = $this->find_place($e, $child);
			if (!empty($res)) {
				return $res;
			}
		}

		// maybe it will fit in root
		$loc = $this->get_location($e);
		$root_loc = $this->get_location($g);
		if ($loc->id == $root_loc->id) {
			return $root;
		}

		// check root's sub-locations
		$root_locs = $this->get_locations($g);
		foreach ($root_locs as $root_loc) {

			if ($loc->id == $root_loc->id) {
				return $root;
			}
		}

		// nope
		return null;
	}

	/**
	 * Groups the schedule events into a hierarchical list suitable for displaying.
	 * @return array
	 */
	public function grouped_schedule() {
		// this code accomplishes the following:
		//   assign group_id to events that fall into a particular group
		//   build hierarchy accordingly
		$flat = array();
		$roots = array();

		foreach ($this->events as $e) {
			$loc = $this->get_location($e);

			// add a group entry to the the flat array
			if ($e->type == 'group') {
				$flat[$e->id] = array(
					'group' => $e,
					'events' => array()
				);
			}

			$found = false;
			for ($i = sizeof($roots) - 1; $i >= 0; $i--) {
				$root = $roots[$i];
				$g = $root->g;
				if ($e->event_date >= $g->event_date + $g->duration * 60) {
					break;
				}

				$res = $this->find_place($e, $root);
				if (!empty($res)) {
					$found = true;
					if ($e->type == 'group') {
						$res->children[] = new Node($e);
					}

					$g = $res->g;
					$f =& $flat[$g->id];
					if ($e->type == 'group') {
						$f['events'][$e->event_date][] =& $flat[$e->id];
					} else {
						$f['events'][$e->event_date][] = $e;
					}
					$e->group_id = $g->id;
				}
			}

			if (!$found && $e->type == 'group') {
				$roots[] = new Node($e);
			}
		}
			
		$sched = array();
		foreach ($this->events as $e) {
			// assigned to a group?
			if (empty($e->group_id)) {
				if ($e->type == 'group') {
					$sched[$e->event_date][] = $flat[$e->id];
				} else {
					$sched[$e->event_date][] = $e;
				}
					
			}
		}
		return $sched;
	}
	




	//// event details
	
	public function print_details() {
		foreach ($this->events as $e) {
			$this->event_details($e, true);
		}
	}
	
	
	

	private function event_details($e) {
		$speakers = $this->get_speakers($e);
		$snippet = $e->event_snippet();
		
			
		$this->print_items($e->event_date, array($e));
	

		if (!empty($e->description) && !(empty($speakers) && !empty($e->photo_id))) {
			print "<div class='desc-full'>";
			print "<h3>Description</h3>";
			print "<div class='desc'>" . $e->description . "</div>";
			print "</div>";
		}
		
		if (!empty($speakers)) {
			print "<div class='speaker-bios'>";
			foreach ($speakers as $speaker) {
				if (!empty($speaker->biography)) {
					print "<div class='speaker-bio'>" . $speaker->biography . "</div>";
				}
			}
			print "</div>";
		}
	}
	
	
	
	//// utilities

	
	
	/**
	 * Batch loads speakers for the events in this schedule.
	 */
	private function load_speakers() {
		// call to database
		$es_map = R::findAll('eventspeaker', 'order by moderator desc');
		$speakers = R::findAll('speaker');
		
		$sp_map = array();
		foreach ($speakers as $s) {
			$sp_map[$s->id] = $s;
		}

		// create one array for each event
		$this->speakers = array();
		foreach ($this->events as $id => $e) {
			$this->speakers[$id] = array();
		}
		
		// add speakers to the list for each event
		foreach ($es_map as $es) {
			$e_id = $es->event_id;
			$s_id = $es->speaker_id;
				
			if (isset($this->speakers[$e_id]) && isset($sp_map[$s_id])) {
				$this->speakers[$e_id][$s_id] = $sp_map[$s_id];
				if ($es->moderator) {
					$this->moderators[$e_id][$s_id] = true;
				}
			}
		}
	}

	private function has_moderator($event) {
		return isset($this->moderators[$event->id]);
	}

	private function is_moderator($speaker, $event) {
		return $this->moderators[$event->id][$speaker->id];
	}
	
	/**
	 * Batch loads photos that will be needed to display the schedule.
	 */
	private function load_photos() {
		$this->photos = array();
		
		$ids = array();
		
		// add event photos to batch
		foreach ($this->events as $e) {
			$pid = $e->photo_id;
			if ($pid) $ids[$pid] = true;
		}
		
		// add speaker photos to the batch
		foreach ($this->speakers as $eid => $list) {
			foreach ($list as $s) {
				$pid = $s->photo_id;
				if ($pid) $ids[$pid] = true;
				$pid = $s->profile_photo_id;
				if ($pid) $ids[$pid] = true;
			}
		}
		
		// call to database
		if (!empty($ids)) {
			$uploads = R::find('upload', 'id in (' . join(",", array_keys($ids)) . ')');
			
			// populate, keyed by id
			$this->photos = array();
			foreach ($uploads as $u) {
				$this->photos[$u->id] = $u;
			}
		}
	}
	
	
	
	
}