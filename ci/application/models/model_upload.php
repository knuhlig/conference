<?php

class Model_Upload extends Base_Model {

	protected $relation_map = array(
		'owner' => 'user'
	);

	public function delete() {
		if ($this->id == 1) {
			throw new Exception("Placeholder image cannot be deleted");
		}
		// delete the underlying file
		unlink($this->abspath());

		// NOTE: cache files remain in place
		// could remove these as well
	}

	public function short_label() {
		return $this->filename;
	}

    public function is_image() {
        return preg_match('/^image/', $this->type);
    }
    
    public function get_embed_html($title=null) {
    	if (empty($title)) {
    		$title = $this->filename;
    	}
    	return "<div class='file-embed'>
					<img src='".$this->icon_url()."'/>
					<a target='_blank' href='".$this->url()."'>".$title."</a>
				</div>";
    }

    public function icon_url() {
    	$dot = strrpos($this->filename, '.');
    	$ext = strtolower(substr($this->filename, $dot + 1));

    	switch ($ext) {
    		case 'pdf':
    		case 'doc':
    		case 'ai':
    		case 'avi':
    		case 'flv':
    		case 'mp3':
    		case 'mp4':
    		case 'mpg':
    		case 'ppt':
    		case 'psd':
    		case 'txt':
    		case 'xls':
    		case 'xlsx':
    			return media_url('images/icons/32px/'.$ext.'.png');
    		default:
    			return media_url('images/icons/32px/_blank.png');
    	}

    }

    public function abspath() {
    	if ($this->is_private) {
    		return PRIVATE_DIR . $this->filename;
    	}
    	return WPCM_UPLOADS_DIR . $this->filename;
    }

    public function file_exists() {
    	return file_exists($this->abspath());
    }

    public function url() {
    	if ($this->is_private) {
    		return ci_url('uploads/restricted/' . $this->id);
    	}
        return WPCM_UPLOADS_URL . $this->filename;
    }

    public function thumbnail_cache_file($size='thumb') {
    	return WPCM_UPLOADS_DIR . 'cache/' . $size.'_' . $this->id . '.jpg';
    }

    private function thumbnail_cache_url($size='thumb') {
    	return WPCM_UPLOADS_URL . 'cache/'.$size.'_' . $this->id . '.jpg';
    }

    public function thumbnail_file($size='thumb') {
    	$cache_file = $this->thumbnail_cache_file($size);

    	if (!file_exists($cache_file) || filemtime($cache_file) < $this->last_updated) {
    		file_get_contents(ci_url("uploads/{$size}/" . $this->id));
    	}

    	return $cache_file;
    }

    public function thumbnail_url($size='thumb') {
       	if ($size == 'full') {
    		return $this->url();
    	}

    	if ($this->is_private) {
    		return false;
    	}
    	$cache_file = $this->thumbnail_cache_file($size);
    	if (file_exists($cache_file)) {
    		if (filemtime($cache_file) >= $this->last_updated) {
    			return $this->thumbnail_cache_url($size);
    		}
    	}

    	// cache miss
    	return ci_url("uploads/{$size}/" . $this->id);
    }

    public function generate_thumbnail($width=162, $height=213) {
    	if (!$this->is_image()) {
    		throw new Exception("cannot generate thumbnail for non-image");
    	}

    	$src = $this->create_image();
    	$dst = imagecreatetruecolor($width, $height);

    	$crop = $this->get_crop($width, $height);
    	$x = (int) $crop[0];
    	$y = (int) $crop[1];
    	$w = (int) $crop[2];
    	$h = (int) $crop[3];

    	// best to look at this vertically
    	imagecopyresampled(
    		$dst, 				$src,
    		0, 0, 				$x, $y,
    		$width, $height, 	$w, $h
    	);

    	return $dst;
    }

    public function u_crop($width, $height, $x_rel=0, $face_height=140) {
    	$src = $this->create_image();
    	$dst = imagecreatetruecolor($width, $height);

    	$crop = $this->crop_to_face($width, $height, $x_rel, $face_height);
    	$x = (int) $crop[0];
    	$y = (int) $crop[1];
    	$w = (int) $crop[2];
    	$h = (int) $crop[3];

    	// best to look at this vertically
    	imagecopyresampled(
	    	$dst, 				$src,
	    	0, 0, 				$x, $y,
	    	$width, $height, 	$w, $h
    	);

    	return $dst;
    }

    public function crop_to_face($width, $height, $x_rel=0.0, $face_height=140) {
    	// desired ratio
    	$ratio = $height / $width;

    	// calculate the crop region
    	list($w, $h) = getimagesize($this->abspath());
    	$pcts = explode(",", $this->face);
    	$r_face = array(
    		$pcts[0] * $w,
    		$pcts[1] * $h,
    		$pcts[2] * $w,
    		$pcts[3] * $h
    	);
    	$r = array(
    		$pcts[0] * $w,
    		$pcts[1] * $h,
    		$pcts[2] * $w,
    		$pcts[3] * $h
    	);

    	// rescale so that face is the right final size
    	if ($r[3] > $face_height) {
    		$sc = $face_height / $r[3];
    		$width /= $sc;
    		$height /= $sc;
    	}

    	$r_ratio = $r[3] / $r[2];

    	// adust the crop area to fit the required crop ratio
    	if ($r_ratio > $ratio) {
    		// crop region is taller than desired region
    		$diff = $r[3] / $ratio - $r[2];
    		$r[0] -= $diff / 2;
    		$r[2] += $diff;
    	} else {
    		// crop region is fatter than desired region
    		$diff = $r[2] * $ratio - $r[3];
    		$r[1] -= $diff / 2;
    		$r[3] += $diff;
    	}

    	// is the crop area too small?
    	if ($r[2] < $width) {
    		// scale up from the center
    		$diff = $width - $r[2];
    		$r[0] -= $diff / 2;
    		$r[2] += $diff;
    	}
    	if ($r[3] < $height) {
    		$diff = $height - $r[3];
    		$r[1] -= $diff / 2;
    		$r[3] += $diff;
    	}

    	// asymmetric positioning
    	// starts off centered
    	$gap = $r[2] - $r_face[2];
    	if ($gap > 0) {
    		$r[0] -= $x_rel * $gap / 2;
    	}
    	// make sure the window is no bigger than the image
    	if ($r[2] > $w) {
    		$sc = $w / $r[2];

    		$diff = $r[2] - $r[2] * $sc;
    		$r[0] += $diff / 2;
    		$r[2] *= $sc;

    		$diff = $r[3] - $r[3] * $sc;
    		$r[1] += $diff / 2;
    		$r[3] *= $sc;
    	}
    	if ($r[3] > $h) {
    		$sc = $h / $r[3];

    		$diff = $r[2] - $r[2] * $sc;
    		$r[0] += $diff / 2;
    		$r[2] *= $sc;

    		$diff = $r[3] - $r[3] * $sc;
    		$r[1] += $diff / 2;
    		$r[3] *= $sc;
    	}

    	// make sure the window is within the image bounds
    	$r[0] = max(0, $r[0]);
    	$r[0] = min($r[0], $w - $r[2]);
    	$r[1] = max(0, $r[1]);
    	$r[1] = min($r[1], $h - $r[3]);
    	return $r;
    }

    /**
     * Determines a good crop to create a thumbnail of size [width x height].
     * @param unknown $width
     * @param unknown $height
     * @return array
     */
    public function get_crop($width, $height) {
    	$ratio = $height / $width;
    	$size = getimagesize($this->abspath());

    	$src_width = $size[0];
    	$src_height = $size[1];
    	$src_ratio = $src_height / $src_width;


    	$sx = 0;
    	$sy = 0;
    	$sw = $src_width;
    	$sh = $src_height;

    	if ($src_ratio < $ratio) {
    		// original image is too "fat", crop off left/right
    		$gap = $src_width - $src_height / $ratio;
    		$sx = round($gap / 2);
    		$sw = $src_width - 2 * $sx;
    	} else if ($src_ratio > $ratio) {
    		// original image is too "skinny", crop off top/bottom
    		$gap = $src_height - $src_width * $ratio;
    		$sy = round($gap / 4);
    		$sh = $src_height - $gap;
    	}

    	return array($sx, $sy, $sw, $sh);
    }

    public function image_size() {
    	$path = $this->abspath();
    	$info = getimagesize($path);
    	return $info;
    }

    public function create_image() {
    	$path = $this->abspath();
    	$info = getimagesize($path);
    	$type = $info[2];

    	if (!$info) {
    		return false;
    	}

    	$functions = array(
    			IMAGETYPE_GIF => 'imagecreatefromgif',
    			IMAGETYPE_JPEG => 'imagecreatefromjpeg',
    			IMAGETYPE_PNG => 'imagecreatefrompng',
    			IMAGETYPE_XBM => 'imagecreatefromwxbm',
    	);


    	if (!isset($functions[$type]) || !function_exists($functions[$type])) {
    		return false;
    	}

    	return $functions[$type]($path);
    }

    public function configure_table($tbl) {
		$tbl->columns = array(
			'thumb' => array('Thumbnail', '70px'),
			'type' => array('Type', '50px')
		);
		$tbl->page_size = 30;
	}

	public function table_thumb($key) {
		return '<img src="'.$this->thumbnail_url('square').'"/>' . $this->controls();
	}

}