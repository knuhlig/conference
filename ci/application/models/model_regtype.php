<?php

/**
 * The idea is:
 * 
 * Registration types
 *     student, academic, VIP, ePatient, staff, etc.
 * each of these may have different metadata/preferences associated with them
 * 
 * Registration offers
 *     prices/options configured.
 *     links to a certain registration type.
 * can be emailed out/restricted in some way.
 * 
 * 
 * @author knuhlig
 *
 */
class Model_Regtype extends Base_Model {
	
	public function type_label() {
		return 'Registration Type';
	}
	
	public function short_label() {
		return $this->name;
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'name' => array('Name', '200px'),
			'badge_color' => array('Badge Color', '200px')
		);
		$tbl->rb_fields = array('name');
	}
	
	public function table_name($key) {
		return $this->short_label() . $this->controls();
	}

	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text'
			),
			'badge_color' => array(
				'type' => 'dropdown',
				'opts' => array(
					'blue' => 'Blue',
					'red' => 'Red',
					'gold' => 'Gold',
					'purple' => 'Purple',
					'black' => 'Black',
					'green' => 'Green'
				)
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Type Information',
					'fields' => array('name', 'badge_color')
				),
			)
		);
	}
	
}