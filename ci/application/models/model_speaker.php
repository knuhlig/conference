<?php

class Model_Speaker extends Base_Model {

	protected $relation_map = array(
		'photo' => 'upload',
		'profile_photo' => 'upload',
		'registration_offer' => 'offer'
	);

	public function update() {
		if (empty($this->access_key)) {
			$this->access_key = random_key(20);
		}
	}

	public function delete() {
		$photo = $this->r('photo');
		if ($photo) {
			R::trash($photo);
		}
	}

	private function pdf_benefits() {
		$parts = array();

		// free registration
		if ($this->complimentary_registration) {
			$parts[] = 'Complimentary registration to the three day conference Sept 25-27, 2015.';
		}

		// lodging
		$num_map = array(
			'2' => 'two',
			'3' => 'three',
			'4' => 'four'
		);
		if ($this->lodging) {
			$days = $this->lodging_days;
			if ($days > 1) {
				$parts[] = ucfirst($num_map[$this->lodging_days]) . " nights complimentary lodging.";
			} else {
				$parts[] = "One night complimentary lodging.";
			}
		}

		// airfare
		if ($this->airfare) {
			$amt = usd($this->airfare_amount) . ' USD';
			$parts[] = "Reimbursement for round-trip airfare from your home to Stanford (SFO or SJC airports) up to {$amt}.";
		}

		// transport
		if ($this->transport) {
			$parts[] = "Transportation to/from the airport.";
		}

		// honorarium
		if ($this->honorarium) {
			$amt = usd($this->honorarium_amount) . ' USD';
			$parts[] = "An honorarium of {$amt}.";
		}

		if (sizeof($parts) == 1) {
			$parts[0] = strtolower(substr($parts[0], 0, 1)) . substr($parts[0], 1);
		}

		return join("<br/>", $parts);
	}

	public function get_letter_vars() {
		return array(
			'salutation' => $this->salutation,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'title' => $this->title,
			'email' => $this->email,
			'date' => date(DATE_FMT_LETTER),
			'two_week_date' => date(DATE_FMT_LETTER, time() + 2 * 7 * 86400),
			'talk_description' => $this->talk_description,
			'benefit_value' => usd($this->benefit_value) . ' USD',
			'accept_url' => ci_url('speaker/accept/' . $this->access_key),
			'decline_url' => ci_url('speaker/decline/' . $this->access_key)
		);
	}

	public function generate_letter_pdf() {
		// relevant filenames
		$infile = SPEAKER_LETTER_XMLFILE;
		$import = SPEAKER_LETTER_FONTFILE;

		$varfile = "/tmp/vars" . uniqid() . ".txt";
		$outfile = simplify_charset(str_replace("'", '', str_replace(' ', '_', "/tmp/MedicineX_" . $this->first_name . "_" . $this->last_name . ".pdf")));

		// generate the varfile
		$vars = $this->get_letter_vars();
		$vars['benefits'] = $this->pdf_benefits();

		if (strpos($vars['benefits'], "<br/>") === false) {
			$infile = SPEAKER_LETTER_ALT_XMLFILE;
		}

		$lines = array();
		$errors = array();
		foreach ($vars as $key => $value) {
			if (empty($value) && $key != 'salutation') {
				$errors[] = $key;
			}
			//$value = simplify_charset($value);
			$lines[] = "{$key}\t{$value}";
		}
		if (!empty($errors)) {
			throw new Exception("The PDF generator needs the following missing variables: " . join(", ", $errors));
		}

		file_put_contents($varfile, join("\n", $lines));

		// normalize the characters (necessary on production server, but why??)
		$locale = 'en_US.utf-8';
		setlocale(LC_ALL, $locale);
		putenv('LC_ALL=' . $locale);

		// run the generator command
		$cmd = PDF_MAKER . " \
				-infile  {$infile} \
				-outfile {$outfile} \
				-varfile {$varfile} \
				-import {$import} \
				-verbose";
		$res = shell_exec($cmd);

		// clean up
		unlink($varfile);

		if (!file_exists($outfile)) {
			return false;
		}

		return $outfile;
	}

	public function template_data() {
		$data = $this->get_letter_vars();
		$data['_attachments'] = $this->generate_letter_pdf();
		$fields = array_keys($data);
		$values = array();
		foreach ($data as $key => $value) {
			$values[] = '"' . addslashes($value) . '"';
		}
		return '# ' . join(", ", $fields) . "\n" . join(", ", $values);
	}

	public function export_data() {
		$types = array();
		$titles = array();
		$times = array();

		foreach ($this->associated('event') as $event) {
			$types[] = $event->type;
			$titles[] = $event->title;
			$times[] = date(DATE_FMT_FULL, $event->event_date);
		}

		$talk_type = join(",", $types);
		$talk_title = join(",", $titles);
		$talk_time = join(",", $times);

		return array(
			'ID' => $this->id,
			'Salutation' => $this->salutation,
			'First Name' => $this->first_name,
			'Last Name' => $this->last_name,
			'Email' => $this->email,
			'Title' => $this->title,
			'Talk Time' => $talk_time,
			'Talk Type' => $talk_type,
			'Talk Title' => $talk_title,

			//'Street1' => $this->home_street1,
			//'Street2' => $this->home_street2,
			//'City' => $this->city,
			//'State' => $this->state,
			//'Country' => $this->country
		);
	}

	public function action_speaker_letter() {
		// save changes
		$this->save();

		redirect('wp-admin/admin.php?page=email_speaker&id='.$this->id);
		exit();
	}

	public function action_download_letter() {
		$this->save();

		$file = $this->generate_letter_pdf();
		redirect(ci_url('admin/download?path=' . $file));
		exit();
	}

	public function editor_options() {
		print '<div class="misc-pub-section">';
		print '<input type="button" value="Email Speaker Letter" onclick="doAction(\'speaker_letter\')"/>';
		print '</div>';

		print '<div class="misc-pub-section">';
		print '<a href="javascript:void(0)" onclick="doAction(\'download_letter\')">Download Speaker Letter (PDF)</a>';
		print '</div>';
	}

	public function editor_fields() {
		return array(
			'photo' => array(
				'label' => 'Photo',
				'type' => 'file',
			),
			'profile_photo' => array(
				'label' => 'Profile Photo',
				'type' => 'file',
			),
			'salutation' => array(
				'type' => 'dropdown',
				'opts' => salutations()
			),
			'first_name' => array(
				'type' => 'text',
				'label' => 'First Name',
				'style' => 'width:250px'
			),
			'last_name' => array(
				'type' => 'text',
				'label' => 'Last Name',
				'style' => 'width:250px'
			),
			'title' => array(
				'label' => 'Title/Affiliation',
				'type' => 'text',
				'style' => 'width:400px'
			),
			'bio_snippet' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:70px'
			),
			'biography' => array(
				'type' => 'html',
				'label' => 'Extended Bio',
				'description' => 'Extended Biography'
			),
			'twitter_username' => array(
				'type' => 'text',
				'style' => 'width:200px'
			),
			'email' => array(
				'type' => 'text'
			),
			'home_phone' => array(
				'type' => 'text'
			),
			'cell_phone' => array(
				'type' => 'text'
			),
			'work_phone' => array(
				'type' => 'text'
			),
			'airfare' => array(
				'type' => 'checkbox'
			),
			'airfare_type' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'domestic' => 'Domestic',
					'international' => 'International'
				),
				'deps' => array(array('airfare', '==', '1'))
			),
			'airfare_class' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'economy' => 'Economy',
					'business' => 'Business'
				),
				'deps' => array(array('airfare', '==', '1'))
			),
			'airfare_amount' => array(
				'type' => 'currency',
				'style' => 'width:70px',
				'deps' => array(array('airfare', '==', '1'))
			),
			'honorarium' => array(
				'type' => 'checkbox',
			),
			'honorarium_amount' => array(
				'type' => 'currency',
				'deps' => array(array('honorarium', '==', '1')),
				'style' => 'width:70px'
			),
			'lodging' => array(
				'type' => 'checkbox'
			),
			'lodging_days' => array(
				'type' => 'text',
				'style' => 'width:70px',
				'deps' => array(array('lodging', '==', '1')),
				'description' => 'Number of days provided for lodging (e.g. 3)'
			),
			'transport' => array(
				'type' => 'checkbox',
				'description' => 'Transportation to/from the airport'
			),
			'benefit_value' => array(
				'type' => 'currency',
				'style' => 'width:70px',
				'description' => 'Total value of benefits'
			),
			'status' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'accept' => 'Invitation accepted',
					'decline' => 'Invitation declined'
				),
				'description' => 'Status of the invitation'
			),
			'complimentary_registration' => array(
				'type' => 'checkbox'
			),
			'registration_offer' => array(
				'type' => 'relation',
				'model' => 'offer',
				'relation' => 'one',
				'description' => 'Email will automatically link to this registration offer'
			),
			'talk_description' => array(
				'type' => 'text',
				'style' => 'width:400px',
				'default' => 'speak about emerging technologies and the future of healthcare',
				'description' => '<br/>...your acceptance of our invitation to {{TEXT_HERE}} at Stanford Medicine X...'
			),
		);
	}


	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Basic Information',
					'fields' => array(
						'photo', 'profile_photo',
						'salutation', 'first_name', 'last_name', 'title',
						'bio_snippet', 'twitter_username', 'status', 'biography')
				),
				array(
					'title' => 'Confidential Information',
					'fields' => array(
					'email', 'home_phone', 'cell_phone', 'work_phone')
				),
				array(
					'title' => 'Offer Details',
					'fields' => array(
						'talk_description',
						'complimentary_registration', 'airfare', 'airfare_type', 'airfare_class', 'airfare_amount',
						'honorarium', 'honorarium_amount',
						'lodging', 'lodging_days',
						'transport',
						'benefit_value',
						'registration_offer'
					)
				)
			)
		);
	}


	public function configure_table($tbl) {
		$tbl->columns = array(
			'photo' => array('Photo', '30px'),
			'name' => array('Name', '150px'),
			'bio_snippet' => array('Bio Snippet', '300px')
		);
		$tbl->sortable_columns = array(
			'name' => 'last_name, first_name'
		);
		$tbl->rb_fields = array('first_name', 'last_name');
		$tbl->default_sort = 'last_name';
		$tbl->page_size = 100;
	}

	public function prepare_results($items) {
		$this->preload($items, array(
			'photo'
		));
	}

	public function table_name($key) {
		$parts = array();

		$parts[] = "<div><a href='".$this->url()."'>".$this->short_label()."</a></div>";
		$parts[] = "<div>".$this->short_desc()."</div>";

		$parts[] = "<div class='hovershow' style='margin-top:5px'>";
		$parts[] = "<a href='?page=edit_speaker&id={$this->id}'>Edit</a>";
		$parts[] = ' <a href="javascript:deleteObject(\'speaker\', '.$this->id.')" class="warn">Delete</a>';
		$parts[] = "</div>";
		return join("", $parts);

	}

	function table_photo($key) {
		$photo = $this->r('photo');
		if (!$photo) {
			return "<span class='warn'>no photo</span>";
		}
		return "<img src='".$this->thumbnail_url('tiny') . "'/>";
	}

    function full_name() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function url() {
    	return ci_url("conference/speaker/" . $this->id);
    }

    public function short_label() {
    	return $this->last_name . ', ' . $this->first_name;
    }

    public function short_desc() {
    	return $this->title;
    }

    public function thumbnail_url($size='thumb') {
    	$photo = $this->r('photo');
    	if (!$photo) {
    		$photo = R::load('upload', 1);
    	}
    	return $photo->thumbnail_url($size);
    }
}