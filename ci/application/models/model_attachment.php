<?php

class Model_Attachment extends Base_Model {

	protected $relation_map = array(
		'file' => 'upload'
	);

	public function editor_fields() {
		return array(
			'title' => array(
				'type' => 'text',
				'label' => 'Title'
			),
			'file' => array(
				'type' => 'file',
				'label' => 'Upload File'
			)
		);
	}


	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Attachment Information',
					'fields' => array(
						'title', 'file')
				),
			)
		);
	}
}