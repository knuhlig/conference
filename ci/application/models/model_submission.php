<?php

class Model_Submission extends Base_Model {

	protected $relation_map = array(
		'user' => 'user',
		'reviewer' => 'user'
	);

	public function edit_url() {
		$tid = $this->track->id;
		$cid = $this->category->id;
		return ci_url("submission/submit/{$tid}/{$cid}/{$this->id}");
	}

	public function submit() {
		$this->submission_date = time();

		$user = $this->r('user');
		// send emails to each author
		
		$authors = $this->ownAuthor;
		$others = array();
		foreach ($authors as $a) {
			$email = $a->email;
			if ($email == $user->email) {
				$a->status = 'confirmed';
				$a->user = $user;
			} else {
				$others[] = $a;
				$a->status = 'pending';
			}
		}

		$this->save();

		if (!empty($others)) {
			foreach ($others as $a) {
				$a->send_confirmation_email();
			}
		}

		return $this->update_progress();
	}

	public function update_progress() {
		// check authors

		if (!$this->confirmed) {
			$authors = $this->ownAuthor;
			$confirmed = true;
			foreach ($authors as $a) {
				if ($a->status != 'confirmed') {
					$confirmed = false;
					break;
				}
			}

			if ($confirmed) {
				// ready to confirm
				$this->confirmed = true;
				$this->save();
				$this->send_confirmation_email();
				return true;
			}

			return false;
		}

		return true;
	}

	public function send_confirmation_email() {
		$email_slug = 'submission-confirmation';
    	$tpl = R::findOne('template', 'slug=:slug', array(':slug' => $email_slug));

    	
    	$email = $tpl->create_email(array(
    		'title' => $this->title,
    		'first' => $this->user->first_name,
    		'last' => $this->user->last_name
    	));

    	$email->to = $this->user->email;
    	

    	$email->send();
    	return true;
	}

	public function update() {
		// called on update
		parent::update();
	}

	public function status() {
		$fields = $this->editor_fields();
		$field = $fields['status'];
		$opts = $field['opts'];
		return $opts[$this->status];
	}
	
	public function export_data() {
		if ($this->is_duplicate) {
			return null;
		}
		
		$user = $this->r('user');
		$order = R::findOne('order', "user_id=? and paid", array($user->id));

		list($avg, $count) = $this->average_score();
		if (is_null($avg)) $avg = 'n/a';



		// exports data
		$data = array(
			'ID' => $this->id,
			'Date' => date(DATE_FMT_FULL, $this->submission_date),
			'Track' => property($this->r('track'), 'title'),
			'Category' => property($this->r('category'), 'title'),
			'Submitted By' => $user->full_name(),
			'Registered' => !empty($order) ? 'Yes' : 'No',
			'Email' => $user->email,
			'Title' => $this->title,
			'Status' => $this->status(),
			'Average Score' => $avg,
			'Completed Reviews' => $count
		);

		// placeholders
		for ($i = 0; $i < 4; $i++) {
			$num = $i + 1;
			$data["R{$num}"] = '';
			$data["R{$num} Score"] = '';
			$data["R{$num} Recommendation"] = '';
		}
		
		$reviews = array_values($this->ownReview);
		for ($i = 0; $i < sizeof($reviews); $i++) {
			$review = $reviews[$i];
			$num = $i + 1;
			$user = $review->r('user');
			
			if ($user) {
				$data["R{$num}"] = $user->full_name();
			}
			if ($review->completed) {
				$data["R{$num} Score"] = $review->score;
				$data["R{$num} Recommendation"] = $review->recommendation;
			}
		}
		
		return $data;
	}
	
	public function average_score() {
		$reviews = $this->ownReview;
		$sum = 0;
		$count = 0;
		foreach ($reviews as $review) {
			if ($review->completed) {
				$count++;
				$sum += $review->score;
			}
		}
		if ($count == 0) return array(null, 0);
		return array($sum / $count, $count);
	}
	
	public function short_label() {
		return $this->title;
	}
	
	public function short_desc() {
		$u = $this->r('user');
		return $u->full_name();
	}
	
	public function controls($hover=true) {
		$type = $this->bean->getMeta('type');
		
		$cls = $hover ? 'hovershow' : '';
		
		$parts = array();
		$parts[] = "<div class='{$cls}' style='float:right'>";
		$parts[] = " <a href='?page=edit_{$type}&id={$this->id}'>Edit</a>";
		$parts[] = " <a href='".$this->url()."'>Review</a>";
		$parts[] = ' <a href="javascript:deleteObject(\''.$type.'\', '.$this->id.')" class="warn">Delete</a>';
		$parts[] = "</div>";
		
		return join("", $parts);
	}
	
	public function author_list() {
		$arr = array();
		foreach ($this->bean->ownAuthor as $author) {
			$arr[] = $author->full_name();
		}
		return join(", ", $arr);
	}

	public function url() {
		return ci_url("submission/review/" . $this->id);
	}
	
	public function public_url() {
		return ci_url("submission/view/" . $this->id);
	}

	public function is_reviewer($user) {
		return R::count('review', 'user_id=?', array($user->id)) > 0;
	}

	public function trim_abstract() {
		$html = trim($this->description);

		$found = true;
		while ($found) {
			$found = false;

			if (preg_match('/^([^|]*)<br\/?>$/', $html, $matches)) {
				$html = trim($matches[1]);
				$found = true;
			}
			if (preg_match('/^<p>&nbsp;<\/p>([^|]*)$/', $html, $matches)) {
				$html = trim($matches[1]);
				$found = true;
			}
		}
		return $html;
	}


	public function is_submitted() {
		return !empty($this->submission_date);
	}

	public function template_data() {
		$user = $this->r('user');
		$data = array(
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'title' => $this->title,
			'email' => $user->email
		);

		$fields = array_keys($data);
		$values = array();
		foreach ($data as $key => $value) {
			$values[] = '"' . addslashes($value) . '"';
		}
		return '# ' . join(", ", $fields) . "\n" . join(", ", $values);
	}

	public function action_email_decision() {
		$this->save();

		if ($this->status == 'accepted') {
			$template = R::findOne('template', 'slug=?', array('accept-email'));
		} else {
			$template = R::findOne('template', 'slug=?', array('reject-email'));
		}

		$email = R::dispense('email');
		$email->to = $this->template_data();
		$email->is_template = 1;
		$email->subject = $template->title;
		$email->body = $template->body;
		$email->save();

		redirect("wp-admin/admin.php?page=edit_email&action=preview&id=" . $email->id);
		exit();
	}

	public function action_add_to_schedule() {
		$u = $this->r('user');

		$e = R::dispense('event');

		$e->description = $this->description;
		$e->title = $this->title;

		if (!empty($u)) {
			$s = $u->get_speaker_profile();
			$e->associate('speaker', $s->id);
		}

		R::store($e);
		redirect("wp-admin/admin.php?page=edit_event&id=" . $e->id);
		exit();
	}

	public function editor_options() {
		print '<div class="misc-pub-section">';
		print '<input type="button" value="Email Decision" class="button" onclick="doAction(\'email_decision\')"/>';
		print '</div>';

		print '<div class="misc-pub-section">';
		print '<input type="button" value="Add to Schedule" class="button" onclick="doAction(\'add_to_schedule\')"/>';
		print '</div>';

    	if ($this->is_submitted()) {
    		print '<div class="misc-pub-section">';
    		print 'Date Submitted: <b>' . date(DATE_FMT_FULL, $this->submission_date) . '</b>';
    		print '</div>';
    	}
    }

	public function check_word_count($key, $value, $bean) {
		$limit = 500;
		$char_limit = 10000;

		$words = preg_replace('/<[^>]+>/', ' ', $value);
		$wc = str_word_count($words);

		if ($wc > $limit) {
			throw new Form_Exception($key, "Please limit your abstract to {$limit} words or less. (Your submission contained {$wc})");
		}

		$len = strlen($value);
		if ($len > $char_limit) {
			throw new Form_Exception($key, "Abstracts are limited to {$char_limit} total characters. (Yours contained {$len})");
		}


	}

	public function editor_fields() {
		return array(
			'description' => array(
				'type' => 'rt',
				'label' => 'Abstract',
				'style' => 'height:400px',
				'required' => true,
				//'validate' => array(array($this, 'check_word_count')),
				'description' => 'Please limit your abstract to 500 words or less.'
			),
			'comments' => array(
				'type' => 'rt',
				'label' => 'Additional Comments',
				'style' => 'height:200px'
			),
			'title' => array(
				'type' => 'text',
				'style' => 'width:500px',
				'required' => true
			),
			'track' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'track',
				'placeholder' => 'Select a Track',
				'required' => true
			),
			'author' => array(
				'type' => 'own_multiple',
				'model' => 'author',
				'label' => 'Author(s)',
				'fields' => array('first_name', 'last_name', 'email', 'phone', 'is_primary', 'status')
			),
			'confirmed' => array(
				'type' => 'checkbox',
				'description' => 'Indicates that all authors have consented to participation'
			),
			'attachment' => array(
				'type' => 'own_multiple',
				'model' => 'attachment',
				'label' => 'Supplementary Files',
				'fields' => array('title', 'file')
			),
			'category' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'category',
				'label' => 'Category',
				'rich' => true,
				'placeholder' => 'Select a Category',
				'required' => true
			),
			'user' => array(
				'label' => 'Creator',
				'type' => 'relation',
				'model' => 'user',
				'relation' => 'one',
				'sort' => 'last_name asc, first_name asc',
				'search' => true
			),
			'submission_date' => array(
				'type' => 'timestamp'
			),
			'status' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => 'Pending Review',
					'accepted' => 'Accepted',
					'rejected' => 'Not Accepted'
				),
				'label' => 'Decision'
			),
			'review' => array(
				'type' => 'own_multiple',
				'model' => 'review',
				'label' => 'Reviews',
				'fields' => array('completed', 'user', 'score', 'recommendation', 'plos_one', 'comments')
			)
		);
	}


	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Editorial Review',
					'fields' => array(
						'confirmed', 'status')
				),
				array(
					'title' => 'Reviewer Feedback',
					'fields' => array(
						'review')
				),
				array(
					'title' => 'Submission Information',
					'fields' => array(
						'user', 'title', 'track', 'category', 'description', 'author', 'attachment', 'comments')
				),
			)
		);
	}


	public function configure_table($tbl) {
		$tbl->columns = array(
			'date' => array('Date', '60px'),
			'status' => array('Status', '60px'),
			'title' => array('Title', '200px'),
			//'user' => array('User', '100px'),
			'track' => array('Track', '50px'),
			'category' => array('Category', '50px'),
		);
		$tbl->rb_fields = array('title');
		$tbl->default_sort = 'id';
		$tbl->default_dir = 'desc';
		$tbl->page_size = 100;


		$tbl->callback = array($this, 'rb_search');
	}

	public function prepare_results($items) {
		// do something to prepare them
	}

	public function rb_search($search, $offset, $count, $sort, $dir) {
		$data = array();

		$terms = explode(" ", $search);


		// build the search condition
		$vars = array();
		$conditions = array();
		$fields = array('s.title', 'a.first_name', 'a.last_name');

		$idx = 0;
		foreach ($terms as $key => $word) {
			$parts = array();
			$opts = array(
				'%' . $word . ' %',
				'% ' . $word . '%',
				$word
			);
			foreach ($fields as $field) {
				foreach ($opts as $opt) {
					$var = ':term' . ($idx++);
					$vars[$var] = $opt;
					$parts[] = $field . ' like ' . $var;
				}
			}
			$conditions[] = '(' . join(" or ", $parts) . ')';
		}
		$searchCond = join(" and ", $conditions);
		$sql = "
			select count(distinct s.id)
			from
				wp_cm_author a,
				wp_cm_submission s
			where a.submission_id=s.id and {$searchCond}";
		$data['total_count'] = R::getCell($sql, $vars);

		$sql = "
			select s.*
			from
				wp_cm_author a,
				wp_cm_submission s
			where a.submission_id=s.id and {$searchCond}
			group by s.id
			order by {$sort} {$dir}
			limit {$offset}, {$count}";
		$rows = R::getAll($sql, $vars);
		$data['results'] = R::convertToBeans('submission', $rows);
		return $data;
	}

	public function table_date($key) {
		return date(DATE_FMT_FULL, $this->created) . $this->controls();
	}

	public function table_status($key) {
		if (!$this->is_submitted()) {
			return '<span class="notice">Not Submitted</span>';
		}

		if (!$this->confirmed) {
			return "<span class='notice'>Pending Confirmation</span>";
		}

		$label = $this->human_readable('status');
		if ($this->status == 'rejected') {
			return '<span class="warn">'.$label.'</span>';
		}
		if ($this->status == 'accepted') {
			return '<span class="success">'.$label.'</span>';
		}
		return $label;
	}

	public function table_type($key) {
		$lines = array();

		$t = $this->r('track');
		if (!$t) $lines[] = 'n/a';
		$lines[] = $t->title;

		$t = $this->r('category');
		if (!$t) $lines[] = 'n/a';
		$lines[] = "<i>" . $t->title . "</i>";

		return join("<br/>", $lines);
	}

	public function table_user($key) {
		$parts = array();
    	if ($this->user) {
	    	$parts[] = "<div style='float:left;margin-right:5px'><img src='".$this->user->thumbnail_url('tiny')."'/></div>";
	    	$parts[] = "<a href='?page=edit_user&id=".$this->user->id."'>" . $this->user->short_label() . "</a>";
	    	if ($this->user->returner) {
	    		$parts[] = " <span class='notice'>Returner</span>";
	    	}
    	}
    	return join("", $parts);
	}

	public function table_category($key) {
		$t = $this->r('category');
		if (!$t) return 'n/a';
		return $t->title;
	}

	public function table_track($key) {
		$t = $this->r('track');
		if (!$t) return 'n/a';
		return $t->title;
	}

	public function table_title($key) {
		$list = $this->ownAuthor;
		$parts = array();
		$parts[] = "<div><b>" . $this->title . "</b></div>";

		$parts[] = "<div style='padding:10px;margin-left:20px'>";
		foreach ($list as $author) {
			if ($author->status != 'confirmed') {
				$parts[] = "<div style='color:red'>" . $author->first_name . ' ' . $author->last_name . " &lt;" . $author->email . "&gt;</div>";
			} else {
				$parts[] = "<div>" . $author->first_name . ' ' . $author->last_name . " &lt;" . $author->email . "&gt;</div>";
			}
		}
		$parts[] = "</div>";
		return join("", $parts);
	}
}