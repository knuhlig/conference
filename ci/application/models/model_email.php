<?php

class Model_Email extends Base_Model {
	
	protected $relation_map = array(
		'template' => 'email'
	);

	public function update() {
		parent::update();
		
		// set the access key
		if (!$this->access_key) {
			$this->access_key = random_key();
		}
	}
	
	public function short_label() {
		return $this->subject;
	}

	public function configure_table($tbl) {
		$tbl->columns = array(
			'created' => array('Created', '150px'),
			'to' => array('To', '200px'),
			'subject' => array('Subject', '300px')
		);
		$tbl->rb_cond = "1";
        $tbl->rb_fields = array('to', 'subject');
		$tbl->default_sort = 'created';
		$tbl->default_dir = 'desc';
	}
	
	public function table_to() {
		if ($this->is_template) {
			return "(template)";
		}
		return $this->to;
	}
	
	public function table_subject() {
		return "<a href='?page=edit_email&id=".$this->id."'>".$this->subject."</a>";
	}
	
	public function action_send() {
		return $this->send();
	}
	
	/**
	 * Shows the stats page for the email
	 * @return boolean
	 */
	public function action_stats() {
		$CI =& get_instance();
		$data = array();
		
		$msgs = R::find('email', 'template_id=?', array($this->id));
		$data['msgs'] = $msgs;
		
		$CI->load->view('admin/email_stats', $data);
		return true;
	}
	
	public function action_preview() {
		$CI =& get_instance();
		
		$msgs = $this->create_template_messages();
		$msg = $msgs[0];
		
		$data = array();
		$data['email'] = $msg;
		$CI->load->view('admin/email_preview', $data);
		return true;
	}
	
	public function editor_options() {
		print '<div class="misc-pub-section">';
		print '<div style="float:right"><input class="button" type="button" value="Preview" onclick="doAction(\'preview\')"/></div>';
		print '<input class="button" type="button" value="Send" onclick="doAction(\'send\')"/>';
		print '</div>';
		
		print '<div class="misc-pub-section">';
		print '<a href="javascript:doAction(\'stats\')">View Stats</a>';
		print '</div>';
	}
	
	public function editor_fields() {
		return array(
			'to' => array(
				'type' => 'textarea',
				'label' => 'Recipients/Data',
				'style' => 'width:400px;height:100px;white-space:nowrap;overflow:auto',
				'default' => '# email, first, last'
			),
			'body' => array(
				'type' => 'rt',
				'style' => 'height:500px'
			),
			'subject' => array(
				'type' => 'text'
			),
			'is_template' => array(
				'type' => 'checkbox',
				'label' => 'Template?',
			),
			'sent' => array(
				'type' => 'checkbox',
				'label' => 'Sent?'
			),
			'date_sent' => array(
				'type' => 'timestamp',
				'deps' => array(array('sent', '==', '1'))
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Email Information',
					'fields' => array('to', 'subject', 'is_template', 'sent', 'date_sent', 'body')	
				)
			)
		);
	}
	
	public function table_created($key) {
		return date(DATE_FMT_FULL, $this->$key) . $this->controls();
	}
	
	
	
	
	
	/**
	 * Sends the email.
	 * @param string $save If true, saves data to the database.
	 */
    public function send($save=true) {
    	if (!$this->id && $save) {
    		$this->save();
    	}
    	
    	// handle templates separately
    	if ($this->is_template) {
    		return $this->send_template();
    	}
    	
        return $this->send_simple();
    }
    
    public function send_simple() {
    	// set up variables
    	$to = $this->to;
    	$subject = $this->subject;
    	$body = $this->get_full_html($this->body);
    	$headers = array(
    		'Content-type: text/html'
    	);
    	$attachments = $this->attachments;
    	
        switch (WPCM_EMAIL_MODE) {
        case 'smtp':
            $res = $this->smtp_send($to, $subject, $body, $headers, $attachments);
            break;   
        case 'stub':
            $res = $this->stub_send($to, $subject, $body, $headers, $attachments);
            break;
        }
    	
    	if ($res) {
    		$this->sent = true;
    		$this->date_sent = time();
    		$this->save();
    	}
    	
    	return true;
    }
    
    /**
     * Displays an interface to send individualized messages for a template email.
     * @return boolean
     */
    public function send_template() {
    	$msgs = $this->create_template_messages();
    	foreach ($msgs as $msg) {
    		$msg->save();
    	}
    	
    	$CI =& get_instance();
    
    	$data = array(
    		'msgs' => $msgs
    	);
    	$CI->load->view('admin/ajax_email', $data);
    	return true;
    }
    
    /**
     * Creates individual messages from the email template and row vars.
     * @return array of individualized messages.
     */
    public function create_template_messages() {
    	$msgs = array();
    	
    	// get the rows of data
    	$data = $this->get_recipient_data();
    	
    	foreach ($data as $row) {
    		// fill in the template using row vars
    		$msg = $this->fill_template($row);
    		
    		// extras
    		$msg->template = $this->bean;
    		$msgs[] = $msg;
    	}
    	
    	return $msgs;
    }
    
    /**
     * Replaces template variables in the subject and body with the given values 
     * and returns a new email with the replaced content.
     * @param unknown $vars
     * @return Email bean
     */
    public function fill_template($vars=array()) {
    	$subject = $this->subject;
    	$body = $this->body;
    	
    	// cleanup
    	$body = preg_replace('/<p>/', '<div>', $body);
    	$body = preg_replace('/<\/p>/', '</div>', $body);
    
    	// replace variables
    	foreach ($vars as $key => $value) {
    		$regex = '/\['.$key.'\]/';
    		$subject = preg_replace($regex, $value, $subject);
    		$body = preg_replace($regex, $value, $body);
    	}
    
    	$email = R::dispense('email');
    	$email->subject = $subject;
    	$email->body = $body;
    	$email->to = $vars['email'];
    	if (isset($vars['_attachments'])) {
    		$email->attachments = $vars['_attachments'];
    	}
    	return $email;
    }
    
    /**
     * Raw method to send an email via SMTP.
     * @param unknown $to
     * @param unknown $subject
     * @param unknown $body
     * @param unknown $headers
     * @param unknown $attachments
     */
    public function smtp_send($to, $subject, $body, $headers, $attachments) {
    	return wp_mail($to, $subject, $body, $headers, $attachments);
    }

    public function stub_send($to, $subject, $body, $headers, $attachments) {
        $filename = "[" . time() . "] (" . $to . ") " . $subject . ".txt";
        $lines = array();
        $lines[] = "To: " . $to;
        $lines[] = "Subject: " . $subject;
        $lines[] = "";
        $lines[] = $body;

        $data = join("\n", $lines);
        $file = "/medx/stub/{$filename}";

        $res = file_put_contents($file, $data);
        if (!$res) {
            die("ERROR");
        }
        return true;
    }

    /**
     * Returns the complete HTML for this email, with $body as the main
     * body content.
     * @param unknown $body
     */
    public function get_full_html($body) {
    	$html = array();
    	$html[] = "<html><body>";
    	$html[] = $this->email_header();
    	$html[] = $body;
    	$html[] = $this->email_footer();
    	$html[] = "</body></html>";
    	return join("", $html);
    }
    
    
    /**
     * Returns the email address of the first recipient in the user list.
     * @return string
     */
    public function get_recipient_email() {
    	$data = $this->get_recipient_data();
    	if (empty($data)) return null;
    	
    	$user = $data[0];
    	return $user['email'];
    }
    
    /**
     * Returns an array of recipient data entries. Each entry is an associative array
     * with key => value pairs, e.g.
     * $data = array(
     *    array('email' => 'test@gmail.com', 'first' => 'John', 'last' => 'Doe')
     *    ...
     * );
     * @return array
     */
    public function get_recipient_data() {
		$data = array();
 
    	// default var_map
    	$var_map = array('email' => 0, 'first' => 1, 'last' => 2);
    	$lines = explode("\n", stripslashes($this->to));
    	    	 
    	foreach ($lines as $line) {
    		$line = trim($line);
    		if (empty($line)) continue;
    		 
    		if ($line[0] == '#') {
    			// stores the var map
    			$line = substr($line, 1);
    			$parts = explode(",", $line);
    			for ($i = 0; $i < sizeof($parts); $i++) {
    				$part = trim($parts[$i]);
    				$var_map[$part] = $i;
    			}
    		} else {
    			$parts = $this->get_var_line($line);
    			$vars = $this->get_vars($parts, $var_map);
    			$data[] = $vars;
    		}
    	}
    	return $data;
    }
    
    private function get_var_line($line) {
    	$parts = array();
    	
    	$len = strlen($line);
    	$in_string = false;
    	$cur = '';
    	
    	for ($i = 0; $i < $len; $i++) {
    		$ch = $line[$i];
    		if ($in_string) {
    			if ($ch == '"') {
    				$in_string = false;
    			} else {
    				$cur .= $ch;
    			}
    		} else {
    			if ($ch == ',') {
    				$parts[] = trim($cur);
    				$cur = '';
    			} else if ($ch == '"') {
    				$in_string = true;
    			} else {
    				$cur .= $ch;
    			}
    		}
    	}
    	$parts[] = trim($cur);
    	return $parts;
    }
    
    /**
     * Takes a row of values, numerically indexed, and returns a variable
     * map. The var_map parameter specifies key => index pairs. For example,
     *
     * $parts = array('John', 'Doe');
     * $var_map = array('first' => 0, 'last' => 1)
     * $vars = get_vars($parts, $var_map);
     * // array('first' => 'John', 'last' => 'Doe');
     *
     * @param array $parts
     * @param array $var_map
     * @return multitype:unknown
     */
    private function get_vars($parts, $var_map) {
    	$vars = array();
    	foreach ($var_map as $key => $idx) {
    		if ($idx < sizeof($parts)) {
    			$vars[$key] = $parts[$idx];
    		}
    	}
    	return $vars;
    }
    
    /**
     * Returns whether the given email is blacklisted, e.g.
     * the user has unsubscribed from Medicine X emails.
     * @param unknown $email
     * @return number
     */
    public function is_blacklisted($email) {
		return R::count("emailblacklist", "email=?", array($email));
    }
    
    /**
     * Returns the HTML header for this email.
     * @return string
     */
    public function email_header() {
    	return "";
    }
    
    /**
     * Returns the HTML footer for this email.
     */
    public function email_footer() {
    	$f = array();
    	$f[] = "<div style=\"font-size:11px;margin-top:20px;border-top:1px solid #c1c1c1;padding-top:20px;color:#888\">";
    	$f[] = "If you do not want to receive further emails from Medicine X, ";
    	$f[] = "<a href=\"".$this->unsubscribe_url()."\">Click Here</a>";
    	$f[] = "</div>";
    	$f[] = $this->tracker_image();
    	return join("", $f);
    }
    
    /**
     * Returns HTML for the 1x1 tracker image.
     * @return string
     */
    public function tracker_image() {
    	return "<img src=\"" . $this->tracker_url() . "\"/>";
    }
    
    /**
     * Returns the URL for the tracker image.
     */
    public function tracker_url() {
    	return ci_url("email/o/" . $this->access_key);
    }
    
    /**
     * Returns the unsubscribe URL for this email.
     */
    public function unsubscribe_url() {
    	return ci_url('email/unsubscribe/' . $this->access_key);
    }

}