<?php

class Model_Template extends Base_Model {
	
	public function create_email($vars=array()) {
		$subject = $this->title;
		$body = $this->body;
		

		$body = preg_replace('/<p>/', '<div>', $body);
		$body = preg_replace('/<\/p>/', '</div>', $body);
	
		foreach ($vars as $key => $value) {
			$regex = '/\['.$key.'\]/';
			$subject = preg_replace($regex, $value, $subject);
			$body = preg_replace($regex, $value, $body);
		}
	
		$email = R::dispense('email');
		$email->subject = $subject;
		$email->body = $body;
		return $email;
	}
	
	public function get_body($vars=array()) {
		$body = $this->body;
	
		foreach ($vars as $key => $value) {
			$regex = '/\['.$key.'\]/';
			$body = preg_replace($regex, $value, $body);
		}
	
		return $body;
	}
	
	public function editor_fields() {
		return array(
			'slug' => array(
				'type' => 'text',
			),
			'title' => array(
				'type' => 'text',
				'style' => 'width:400px'
			),
			'notes' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:200px'
			),
			'body' => array(
				'type' => 'rt',
				'style' => 'height:500px'
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Template Information',
					'fields' => array('slug', 'title', 'notes', 'body')
				)
			)
		);
	}
	
	public function short_label() {
		return $this->title;
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'slug' => array('Slug', '30px'),
			'title' => array('Title', '150px'),
		);
		$tbl->rb_fields = array('slug', 'title');
		$tbl->default_sort = 'slug';
	}
	
	public function table_slug($key) {
		return $this->slug . $this->controls();
	}

}