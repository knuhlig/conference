<?php

class Model_Category extends Base_Model {


	protected $relation_map = array(
		'photo' => 'upload'
	);

	public function pluralize_type() {
		return 'Categories';
	}

	public function editor_fields() {
		return array(
			'title' => array(
				'type' => 'text',
				'label' => 'Title'
			),
			'description' => array(
				'type' => 'textarea',
				'label' => 'Description',
				'style' => 'width:300px;height:100px'
			),
			'photo' => array(
				'type' => 'file'
			),
			'is_panel' => array(
				'type' => 'checkbox',
			),
			'single_presenter' => array(
				'type' => 'checkbox',
			)
		);
	}

	public function thumbnail_url($size='thumb') {
		$p = $this->r('photo');
		if ($p) {
			return $p->thumbnail_url($size);
		}
		return '';
	}

	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Category Information',
					'fields' => array(
						'title', 'description', 'photo', 'is_panel', 'single_presenter')
				),
			)
		);
	}

	public function configure_table($tbl) {
		$tbl->columns = array(
			'photo' => array('Photo', '60px'),
			'desc' => array('Category', '200px'),
		);
		$tbl->rb_fields = array('title');
		$tbl->page_size = 100;
	}

	public function short_label() {
		return $this->title;
	}

	public function short_desc() {
		return $this->description;
	}

	public function table_desc() {
		return $this->controls()."<b>".$this->title."</b><br/>" . $this->description;
	}

	public function table_photo() {
		return "<img src='".$this->thumbnail_url('inch')."'/>";
	}
}