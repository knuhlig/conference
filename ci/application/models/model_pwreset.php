<?php

/**
 * Stores credential information for resetting user passwords
 * @author knuhlig
 *
 */
class Model_Pwreset extends Base_Model {
	
	/**
	 * Returns a url that can be used to reset the user password
	 */
	public function url() {
		return ci_url('user/new_password/' . $this->reset_key);
	}
	
	/**
	 * Returns whether this set of credentials has expired.
	 * @return boolean
	 */
	public function is_expired() {
		$time = time();
		return $time > $this->expires;
	}
}