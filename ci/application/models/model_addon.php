<?php

class Model_Addon extends Base_Model {

	protected $relation_map = array(
		'photo' => 'upload'
	);
	
	public function thumbnail_url($size='thumb') {
		$photo = $this->r('photo');
		if (!$photo) return '';
		return $photo->thumbnail_url($size);
	}
	
	public function is_available() {
		if ($this->trashed) return false;
		// cap?
		return true;
	}

	public function export_data() {
		$rows = array();
		$oas = R::find('addonorder', 'addon_id=?', array($this->id));
		foreach ($oas as $oa) {
			$order = R::load('order', $oa->order_id);
			if (!$order || !$order->paid) continue;

			$row = array();
			$row['Addon'] = $this->name;
			$row['Price'] = $oa->unit_price;

			$row = array_merge($row, $order->export_data());
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function after_update() {
		if ($this->add_all) {
			$this->add_to_offers();
		}
	}
	
	public function current_price() {
		if ($this->sold_out) return 0;
		return $this->price;
	}
	
	public function get_warning() {
		$str = $this->warning;
		$str = str_replace('[date]', date(DATE_FMT_FULL, time()), $str);
		return $str;
	}
	
	public function short_label() {
		return $this->name;
	}
	
	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text'
			),
			'photo' => array(
				'type' => 'file'
			),
			'description' => array(
				'type' => 'html'
			),
			'cap' => array(
				'type' => 'text',
				'style' => 'width:100px',
				'description' => 'Blank or zero indicates no cap'
			),
			'price' => array(
				'type' => 'currency',
				'label' => 'Default Price',
				'style' => 'width:100px'
			),
			'master_class' => array(
				'type' => 'checkbox',
				'label' => 'Master Class?',
				'default' => '0'
			),
			'text_prompt' => array(
				'type' => 'checkbox',
				'label' => 'Registration Textbox?'
			),
			'prompt_label' => array(
				'type' => 'text',
				'label' => 'Prompt Label',
				'description' => 'default: What would you like to get out of this event?'
			),
			'box_rows' => array(
				'type' => 'text',
				'label' => 'Box Height',
				'description' => 'Enter a number of rows (default: 5)',
				'style' => 'width:80px'
			),
			'sold_out' => array(
				'type' => 'checkbox',
				'label' => 'Sold out?'
			),
			'waitlist_message' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:200px'
			),
			'display_policy' => array(
				'type' => 'dropdown',
				'opts' => array(
					'show' => 'Show this add-on by default (unless specifically excluded in a custom offer)',
					'hide' => 'Hide this add-on by default (unless specifically added to a custom offer)'
				)
			),
			'warning_type' => array(
				'type' => 'dropdown',
				'opts' => array(
					'none' => 'No Warning',
					'select' => 'Show warning when user has selected the add-on',
					'noselect' => 'Show warning when user has NOT selected the add-on'
				),
			),
			'warning' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:100px',
				'description' => '[date]=Current date and time'
			),
			'add_all' => array(
				'type' => 'checkbox',
				'label' => 'Add to customized offers?',
				'description' => 'Check if you want to add this to offers whose add-ons have already been customized'
			),
			'event' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'event',
				'sort' => 'event_date asc'
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Addon Information',
					'fields' => array(
						'name', 'photo', 'price', 'cap', 'master_class', 'text_prompt', 'prompt_label', 'box_rows', 'sold_out', 'waitlist_message', 'display_policy',
						'description', 'warning_type', 'warning', 'event')
				)
			)
		);
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'photo' => array('Photo', '40px'),
			'name' => array('Name', '160px'),
			'cap' => array('Cap', '100px'),
			'price' => array('Regular Price', '100px'),
			'display_policy' => array('Default', '100px'),
			'master_class' => array('Master Class?', '100px')
		);
		$tbl->rb_fields = array('name');
	}
	
	public function table_master_class($key) {
		return $this->master_class ? 'Yes' : 'No';
	}
	
	public function table_price($key) {
		return usd($this->$key);
	}
	
	public function table_cap($key) {
		if (!empty($this->$key)) {
			return $this->$key;
		}
		return 'n/a';
	}
	
	
	
	public function table_name($key) {
		return $this->$key . $this->controls();
	}

	public function controls($hover=true) {
        $type = $this->bean->getMeta('type');

        $cls = $hover ? 'hovershow' : '';

        $parts = array();
        $parts[] = "<div class='{$cls}' style='float:right'>";

        $parts[] = "<a href='?page=edit_{$type}&id={$this->id}'>Edit</a>";
        $parts[] = ' <a href="javascript:deleteObject(\''.$type.'\', '.$this->id.')" class="warn">Delete</a>';
        $parts[] = ' <a href="?page=addon_summary&id='.$this->id.'" class="">Summary</a>';

        $parts[] = "</div>";

        return join("", $parts);
    }
	
	
    

    public function add_to_offers() {
        $existing = R::find('optionaddon', 'addon_id=:id', array(':id' => $this->id));
        $existing_opts = array();
        foreach ($existing as $bean) {
            $existing_opts[$bean->offeroption_id] = true;
        }

        $options = R::find('offeroption', 'customize_addons=1');
        foreach ($options as $option) {
            if (!isset($existing_opts[$option->id])) {
                $bean = R::dispense('optionaddon');
                $bean->offeroption = $option;
                $bean->addon = $this;
                $bean->price = $this->price;
                R::store($bean);
            }
        }
    }

}