<?php

class Model_Connection extends Base_Model {

	protected $relation_map = array(
		'user' => 'user',
		'target' => 'user'
	);

	public function editor_fields() {
		return array(
			'type' => array(
				'type' => 'dropdown',
				'opts' => array(
					'friend' => 'Friend',
					'meet' => 'Meet',
					'like '=> 'Like',
					'scan' => 'Badge Scan'
				)
			),
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
    			'sort' => 'last_name asc, first_name asc'
			),
			'target' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
    			'sort' => 'last_name asc, first_name asc'
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Connection Details',
					'fields' => array(
						'type', 'user', 'target')
				)
			)
		);
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'date' => array('Date', '70px'),
			'type' => array('Type', '30px'),
			'user' => array('User', '150px'),
			'target' => array('Target', '150px'),
		);
		
		$tbl->filters = array(
			'all' => 'All',
			'scan' => 'Badge Scans',
		);
		$tbl->filter_conds = array(
			'all' => '1',
			'scan' => "type='scan'"
		);
		$tbl->default_sort = "last_updated";
		$tbl->default_dir = "desc";
		$tbl->callback = array($this, 'rb_search');
	}
	
	public function rb_search($search, $offset, $count, $sort, $dir) {
		$data = array();
	
		$terms = explode(" ", $search);
		foreach ($terms as &$term) {
			$term = $term . '%';
		}
	
		// build the search condition
		$vars = array();
		$conditions = array();
		$fields = array('u.first_name', 'u.last_name', 't.first_name', 't.last_name');
	
		$idx = 0;
		foreach ($terms as $key => $word) {
			$parts = array();
			foreach ($fields as $field) {
				$var = ':term' . ($idx++);
				$vars[$var] = $word;
				$parts[] = $field . ' like ' . $var;
			}
			$conditions[] = '(' . join(" or ", $parts) . ')';
		}
		$searchCond = join(" and ", $conditions);
		 
		$sql = "select count(*)
		from wp_cm_connection o
		left join wp_cm_user u on o.user_id=u.id
		left join wp_cm_user t on o.target_id=t.id
		where {$searchCond}";
		$data['total_count'] = R::getCell($sql, $vars);
	
	
		$sql = "select o.*
		from wp_cm_connection o
		left join wp_cm_user u on o.user_id=u.id
		left join wp_cm_user t on o.target_id=t.id
		where {$searchCond}
		order by {$sort} {$dir}
		limit {$offset}, {$count}";
		 
		$rows = R::getAll($sql, $vars);
		$data['results'] = R::convertToBeans('connection', $rows);
		return $data;
    }
	
	public function table_date() {
		return date(DATE_FMT_FULL, $this->last_updated);
	}
	
	public function table_user() {
		$u = $this->r('user');
		if (!$u) return "";
		return $u->full_name();
	}
	
	public function table_target() {
		$u = $this->r('target');
		if (!$u) return "";
		return $u->full_name();
	}
}