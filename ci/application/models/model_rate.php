<?php

/**
 * The idea is:
 * 
 * Conference Access Level
 *     student, academic, VIP, ePatient, staff, etc.
 * each of these may have different metadata/preferences associated with them
 *     e.g. badge color, ...?
 * 
 * Packages:
 * Come to the conference.
 * Pay XYZ
 * Get ABC privileges
 * 
 * Offers:
 * One or more choices,
 * access restrictions,
 * automated emails, etc.
 * 
 * 
 * @author knuhlig
 *
 */
class Model_Rate extends Base_Model {
	
	public function short_label() {
		return $this->name;
	}
	
	public function short_desc() {
		$parts = array();
		if ($this->early_enabled) {
			$parts[] = usd($this->early_price);
		}
		$parts[] = usd($this->price);
		if ($this->late_enabled) {
			$parts[] = usd($this->late_price);
		}
		return join("/", $parts);
	}
	
	public function editor_options() {
		if (!$this->id) return;
		print '<div class="misc-pub-section">';
		print '<input type="button" value="Create Campaign" onclick="parent.location=\'?page=edit_offer&name='.$this->name.'&rate_id='.$this->id.'\'"/>';
		print '</div>';
	}
	
	public function verification_message() {
		$key = $this->verification;
		$msgs = array(
			'academic' => 'Eligibility must be verified with a photo ID from a degree-granting instutition.',
			'student' => 'Eligibility must be verified with a student ID from a degree-granting institution.'
		);
		if (isset($msgs[$key])) {
			return $msgs[$key];
		}
		return 'Requires Verification';
	}
	
	public function get_available_addons($master_class=false) {
		$addons = R::find('addon', 'not trashed and display_policy=? and master_class=?', array('show', $master_class));
		$related = $this->associated('addon', true);
		foreach ($related as $bean) {
			$addon = $bean->addon;
			$id = $addon->id;
			
			if (!$addon->is_available() || $addon->master_class != $master_class) {
				continue;
			}
			if ($bean->hide) {
				unset($addons[$id]);
				continue;
			}

			if (!isset($addons[$id])) {
				$addons[$id] = $bean->addon;
			}
			if (!is_null($bean->price)) {
				$addons[$id]->price = $bean->price;
			}
		}
	
		return $addons;
	}
	
	
	public function calculate_price() {
		$time = time();
		
		if ($this->early_enabled) {
			if ($time < $this->early_end) {
				return $this->early_price;
			}
		}
		
		if ($this->late_enabled) {
			if ($time >= $this->late_start) {
				return $this->late_price;
			}
		}
		
		return $this->price;
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'name' => array('Name', '200px'),
			'pricing' => array('Pricing', '200px')
		);
		$tbl->rb_fields = array('name');
	}
	
	public function table_name($key) {
		return $this->short_label() . $this->controls();
	}
	
	public function table_pricing($key) {
		$parts = array();
		$parts[] = "<table class='pricing'>";

		if ($this->early_enabled) {
			$early_end = date(DATE_FMT_PRETTY, $this->early_end);
			$parts[] = "<tr><td>Early</td><td>".usd($this->early_price)." (ends {$early_end})</td></tr>";
		}
		
		$parts[] = "<tr><td>Regular</td><td>".usd($this->price)."</td></tr>";
		
		if ($this->late_enabled) {
			$late_start = date(DATE_FMT_PRETTY, $this->late_start);
			$parts[] = "<tr><td>Late</td><td>".usd($this->late_price)." (starts {$late_start})</td></tr>";
		}
		
		$parts[] = "</table>";
		return join("", $parts);
	}
	
	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text'
			),
			'description' => array(
				'type' => 'html',
				'description' => 'Description'
			),
			'regtype' => array(
				'model' => 'regtype',
				'type' => 'relation',
				'relation' => 'one',
				'label' => 'Access Level'
			),

			'verification' => array(
				'label' => 'Verification',
				'type' => 'dropdown',
				'opts' => array(
					'none' => 'Not Required',
					'academic' => 'Faculty Affiliation',
					'student' => 'Student Affiliation'
				)
			),
			'price' => array(
				'label' => 'Price',
				'type' => 'currency',
				'style' => 'width:100px',
			),
			'early_enabled' => array(
				'type' => 'checkbox',
				'label' => 'Add Early Rate?'
			),
			'early_price' => array(
				'label' => 'Early Rate',
				'type' => 'currency',
				'style' => 'width:100px',
				'description' => "You may specify an early-bird rate if desired.",
				'deps' => array(
					array('early_enabled', '==', '1')
				)
			),
			'early_end' => array(
				'label' => 'Early Rate Expires',
				'type' => 'timestamp',
				'description' => 'Required only if an early-bird rate is set above.',
				'deps' => array(
					array('early_enabled', '==', '1')
				)
			),
			
			'late_enabled' => array(
				'label' => 'Add Late Rate?',
				'type' => 'checkbox'
			),
			'late_price' => array(
				'label' => 'Late Rate',
				'type' => 'currency',
				'style' => 'width:100px',
				'description' => "You may specify a late rate if desired.",
				'deps' => array(
					array('late_enabled', '==', '1')
				)
			),
			'late_start' => array(
				'label' => 'Late Rate Starts',
				'type' => 'timestamp',
				'description' => 'Required only if a late rate is set above.',
				'deps' => array(
					array('late_enabled', '==', '1')
				)
			),
			'exclude_badges' => array(
				'type' => 'checkbox',
				'label' => 'Exclude Badges?',
				'description' => 'If checked, registrants will not receive auto-generated badges'
			),
			'exclude_total' => array(
				'type' => 'checkbox',
				'label' => 'Exclude from Counts?',
				'description' => 'If checked, registrants will not count towards registration totals'
			),
			'has_portrait' => array(
				'type' => 'checkbox',
				'label' => 'Portraits?',
				'description' => 'If checked, registrants will be marked to receive professional portraits'
			),
			'addon' => array(
				'type' => 'relation',
				'label' => 'Addon Customization',
				'relation' => 'multiple',
				'fields' => array(
					'hide' => array(
						'type' => 'checkbox',	
					),
					'price' => array(
						'type' => 'currency',
						'style' => 'width:50px',
					)
				),
				'description' => 'You may set custom prices for add-ons here. Otherwise, the default price will be used.'
			),
            'email_slug' => array(
                'type' => 'text',
                'style' => 'width:200px'
            )
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Rate Information',
					'fields' => array(
						'name', 'description', 'regtype', 'verification', 'exclude_badges', 'exclude_total', 'has_portrait','email_slug'
						)
				),
				array(
					'title' => 'Pricing',
					'fields' => array(
						'price',
						'early_enabled', 'early_price', 'early_end',
						'late_enabled', 'late_price', 'late_start',
						'addon',
					)
				),
			)
		);
	}
	
}