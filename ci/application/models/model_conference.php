<?php

class Model_Conference extends Base_Model {

	protected $relation_map = array(
		// 'photo' => 'upload'
	);
	
	public function short_label() {
		return $this->name;
	}
	
	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text',
				'style' => 'width:300px'
			),
			'start_date' => array(
				'type' => 'timestamp'
			),
			'end_date' => array(
				'type' => 'timestamp'
			),
			
			// registration settings
			'registration_start' => array(
				'type' => 'timestamp',
				'label' => 'Registration Opens'
			),
			'registration_goal' => array(
				'type' => 'text',
				'style' => 'width:100px'
			),
			'paid_goal' => array(
				'type' => 'text',
				'style' => 'width:100px',
				'label' => 'Paid Registration Goal'
			),
			'revenue_goal' => array(
				'type' => 'currency',
				'style' => 'width:90px'
			),
			'payment_method' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => 'Select a payment method',
					'cybersource' => 'Cybersource'
				)
			),
			'cybs_account' => array(
				'type' => 'text',
				'style' => 'width:150px',
				'deps' => array(
					array('payment_method', '==', 'cybersource')
				),
				'label' => 'Merchant ID'
			),
			
			// submission settings
			'submission_start' => array(
				'type' => 'timestamp',
				'label' => 'Submissions Open'
			),
			'submission_end' => array(
				'type' => 'timestamp',
				'label' => 'Submissions Close'
			),
			'submission_instructions' => array(
				'type' => 'rt',
				'label' => 'Submission Instructions',
				'style' => 'height: 200px'
			),
			'submission_copyright' => array(
				'type' => 'rt',
				'label' => 'Copyright Information',
				'style' => 'height: 200px'
			),
			
			// website info
			'slug' => array(
				'type' => 'text',
				'label' => 'Conference Slug',
				'style' => 'width:150px',
				'description' => 'Choose a unique "slug" identifier',
			),
			'db_prefix' => array(
				'type' => 'text',
				'style' => 'width:150px',
				'label' => 'Table Prefix'
			),
			'db_name' => array(
				'type' => 'text',
				'style' => 'width:150px',
				'label' => 'Database Name',
				'description' => 'Leave blank to use the same database as Wordpress',
			),
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Conference Information',
					'fields' => array(
						'name', 'start_date', 'end_date'
					)
				),
				array(
					'title' => 'Registration Information',
					'fields' => array(
						'registration_start', 'registration_goal', 'paid_goal', 'revenue_goal', 'payment_method', 'cybs_account'
					)
				),
				array(
					'title' => 'Abstract Submissions',
					'fields' => array(
						'submission_start', 'submission_end', 'submission_instructions', 'submission_copyright'
					)
				),
				array(
					'title' => 'Website Settings',
					'fields' => array(
						'slug', 'db_name', 'db_prefix'
					)
				)
			)
		);
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'name' => array('Name', '160px'),
			'start_date' => array('Start Date', '100px'),
			'end_date' => array('End Date', '100px'),
		);
		$tbl->rb_fields = array('name');
		$tbl->default_sort = 'start_date';
	}
	
	public function table_name($key) {
		return $this->$key . $this->controls();
	}
	
	public function table_start_date($key) {
		if (empty($this->$key)) return 'n/a';
		return date(DATE_FMT_FULL, $this->$key);
	}
	
	
	public function table_end_date($key) {
		if (empty($this->$key)) return 'n/a';
		return date(DATE_FMT_FULL, $this->$key);
	}

}