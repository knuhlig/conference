<?php

/**
 * FROM WP:
 * select
 * 	u.ID, u.user_login, u.user_email, m1.meta_value as 'first_name', m2.meta_value as 'last_name'
 * from
 * 	wp_users u,
 *  wp_usermeta m1,
 *  wp_usermeta m2
 * where
 *  u.ID=m1.user_id and m1.meta_key='first_name' and
 *  u.ID=m2.user_id and m2.meta_key='last_name'
 *
 *  select * from wp_usermeta where meta_key='author_image';
 *  insert into wp_cm_upload (filename, type) (select meta_value as filename, 'image/jpg' as type from wp_usermeta where meta_key='author_image')
 *
 * @author knuhlig
 *
 */
class Model_User extends Base_Model {

	protected $relation_map = array(
		'photo' => 'upload'
	);

	public function update() {
		parent::update();

		if (empty($this->access_key)) {
			$this->access_key = random_key(15);
		}

		if (empty($this->login_key)) {
			$this->login_key = random_key(15);
		}

		// create the wp user
		if (!$this->wp_id) {
			$wp_id = wp_create_user($this->username, $this->password, $this->email); // @wordpress
			if (is_wp_error($wp_id)) {
				throw new Exception($wp_id->get_error_message());
			}
			$this->wp_id = $wp_id;
		}

		// update wp user data
		$data = array(
			'ID' => $this->wp_id,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'user_email' => $this->email,
		);
		wp_update_user($data);
	}

	public function wp_user() {
		return new WP_User($this->wp_id);
	}

	public function country_flag() {
		if (empty($this->country)) {
			return false;
		}

		$list = country_list();
		if (!isset($list[$this->country])) {
			return false;
		}

		$name = $list[$this->country];
		$filename = str_replace(' ', '_', $name) . '.png';
		$url = media_url('images/flags/' . $filename);

		return "<img src='{$url}' title='{$name}'/>";
	}

	public function country_name() {
		$list = country_list();
		if (!isset($list[$this->country])) {
			return false;
		}
		return $list[$this->country];
	}

	public function profile_url() {
		return ci_url('conference/user/' . $this->id);
	}

	public function scan_url() {
		return ci_url('conference/scan/' . $this->access_key);
	}

	public function login_url() {
		return ci_url('conference/login/' . $this->login_key);
	}

	public function attendee_row() {
		print "<div class='attendee'>";
		print "<div class='photo'><img src='" . $this->thumbnail_url('inch') . "'></div>";

		print "<div class='info'>";
		print "<div class='name'><a href='" . $this->url() . "'>" . $this->full_name() . "</a></div>";
		print "<div class='title'>" . $this->title . "</div>";

		if (!empty($this->speaker_id)) {
			$speaker = R::load('speaker', $this->speaker_id);
			print "<div><a href='".$speaker->url()."'>&#187; View Speaker Profile</a></div>";
		}
		print "</div>";
		print "</div>";
	}

	public function get_speaker_profile() {
		if (!empty($this->speaker_id)) {
			return R::load('speaker', $this->speaker_id);
		}

		$s = R::dispense('speaker');
		$s->first_name = $this->first_name;
		$s->last_name = $this->last_name;
		$s->photo_id = $this->photo_id;
		$s->title = $this->title;
		$s->email = $this->email;
		$s->salutation = $this->salutation;
		$s->work_phone = $this->phone;
		$s->bio_snippet = $this->bio_snippet;
		$s->biography = $this->biography;
		R::store($s);

		$this->speaker_id = $s->id;
		R::store($this);
		
		return $s;
	}

	public function connect_to($target, $type) {
		$c = R::dispense('connection');
		$c->user = $this;
		$c->target = $target;
		$c->type = $type;
		R::store($c);

		if ($type == 'scan') {
			$cs = R::find('connection', "type='meet' and user_id=? and target_id=?", array($this->id, $target->id));
			R::trashAll($cs);
		}
		return $c;
	}

	public function load_connections($reverse=false) {
		if ($reverse) {
			$cs = R::find('connection', 'target_id=? group by user_id', array($this->id));
			$key = 'user';
		} else {
			$cs = R::find('connection', 'user_id=? group by target_id', array($this->id));
			$key = 'target';
		}

		$arr = array();
		foreach ($cs as $c) {
			$t = $c->type;
			if (!isset($arr[$t])) {
				$arr[$t] = array();
			}
			$arr[$t][] = $c->r($key);
		}

		return $arr;
	}

	public function delete() {
		// delete photo
		$photo = $this->r('photo');
		if ($photo) {
			R::trash($photo);
		}

		// delete wp user
		//wp_delete_user($this->wp_id);
	}

	public function bitly_test() {
		$url = $this->scan_url(); //'http://medicinex.stanford.edu/conf/conference/scan/' . $this->access_key;//
		$login = 'knuhlig';
		$appkey = 'R_edb0edcc935a3567828af934a6b38a55';
		$short_url = trim(get_bitly_short_url($url, $login, $appkey));
		var_dump($short_url);
	}

	public function qr_file($force=false) {
		$cache_file = "/medx/qr/scan_" . $this->id . ".jpg";

		if ($force || !file_exists($cache_file)) {
			R::store($this);
			$url = $this->scan_url();
			$qr_url = googl_url($url) . '.qr';
			$img = file_get_contents($qr_url);
			if (!empty($img)) file_put_contents($cache_file, $img);
		}

		return $cache_file;
	}

	public function login_qr_file($force=false) {
		$cache_file = "/medx/qr/login_" . $this->id . ".jpg";

		if ($force || !file_exists($cache_file)) {
			R::store($this);
			$url = $this->login_url();
			$qr_url = googl_url($url) . '.qr';
			$img = file_get_contents($qr_url);
			if (!empty($img)) file_put_contents($cache_file, $img);
		}

		return $cache_file;
	}

	public function is_complete() {
		if (empty($this->first_name)) return false;
		if (empty($this->last_name)) return false;
		if (empty($this->email)) return false;
		if (empty($this->title)) return false;
		if (empty($this->photo_id)) return false;
		if (empty($this->country)) return false;
		return true;
	}

	public function generate_reset_key() {
		$bean = R::dispense('pwreset');
		$bean->user = $this->bean;
		$bean->reset_key = random_key();
		$bean->expires = time() + 86400;
		R::store($bean);
		return $bean;
	}

	public function check_email($key, $value, $bean) {
		// validate against wp user database
		if ((!$bean->id || $bean->$key != $value) && email_exists($value)) {
			throw new Form_Exception($key, "This email address is associated with an existing account. <a href='".ci_url('user/login') . "?r=" . property($_GET, 'r') ."'>Login Here</a>");
		}
	}

	public function check_username($key, $value, $bean) {
		// validate against wp user database
		if ((!$bean->id || $bean->$key != $value) && username_exists($value)) {
			throw new Form_Exception($key, "This username is already taken");
		}
	}

	public function contact_email() {
		if (!empty($this->admin_email)) {
			return $this->admin_email;
		}
		return $this->email;
	}

	public function hash_password($pw) {
		return md5($this->username . $pw);
	}


	public function check_password($pw) {
		return $this->hash_password($pw) == $this->password;
	}

	public function set_password($pw) {
		$this->password = $this->hash_password($pw);
	}

	public function editor_fields() {
		return array(
			'first_name' => array(
				'type' => 'text',
				'style' => 'width:60%',
				'required' => true
			),
			'last_name' => array(
				'type' => 'text',
				'style' => 'width:60%',
				'required' => true
			),
			'salutation' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'mr' => 'Mr.',
					'mrs' => 'Mrs.',
					'ms' => 'Ms.',
					'dr' => 'Dr.'
				)
			),
			'email' => array(
				'type' => 'text',
				'required' => true,
				'style' => 'width:50%',
				'validate' => array('email', array($this, 'check_email'))
			),
			'username' => array(
				'type' => 'text',
				'required' => true,
				'style' => 'width:50%',
				'validate' => array(array($this, 'check_username'))
			),
			'twitter_username' => array(
				'type' => 'text',
				'style' => 'width:50%',
			),
			'password' => array(
				'type' => 'password',
				'label' => 'Password',
				'style' => 'width:50%',
				'validate' => array('confirm'),
				'ignore-empty' => true,
				'filter' => array(array($this, 'hash_password'))
			),
			'password_confirm' => array(
				'type' => 'password',
				'style' => 'width:50%',
				'label' => 'Confirm Password',
				'ignore' => true
			),
			'photo' => array(
				'label' => 'Photo',
				'type' => 'file',
			),
			'title' => array(
				'label' => 'Title/Affiliation',
				'type' => 'text',
			),
			'bio_snippet' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:70px'
			),
			'biography' => array(
				'type' => 'html',
				'style' => 'height:200px'
			),
			'admin_name' => array(
				'type' => 'text',
				'style' => 'width:300px'
			),
			'admin_email' => array(
				'type' => 'text',
				'style' => 'width:300px'
			),
			'country' => array(
				'type' => 'dropdown',
				'opts' => country_list()
			),
			'interests' => array(
				'type' => 'rt',
				'style' => 'height:200px'
			),
			'website' => array(
				'type' => 'text'
			),
			'phone' => array(
				'type' => 'text',
				'style' => 'width:50%',
			),
			'fax' => array(
				'type' => 'text',
				'style' => 'width:50%',
			),
			'mailing_address' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:100px'
			),
			'returner' => array(
				'type' => 'checkbox',
				'label' => 'Returner?'
			),
			'speaker' => array(
				'type' => 'relation',
				'relation' => 'one',
				'label' => 'Speaker Profile',
				'sort' => 'last_name asc, first_name asc'
			),
			'is_reviewer' => array(
				'type' => 'checkbox',
				'label' => 'Reviewer?'
			)
		);
	}

	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Basic Information',
					'fields' => array(
						'photo', 'salutation', 'first_name', 'last_name', 'email', 'title',
						'country', 'returner')
				),
				array(
					'title' => 'Additional Information',
					'fields' => array(
						'twitter_username', 'website', 'biography',
						'phone', 'fax', 'mailing_address', 'interests', 'speaker'
					)
				),
				array(
					'title' => 'Account Information',
					'fields' => array('username', 'password', 'password_confirm', 'admin_name', 'admin_email', 'is_reviewer')
				)
			)
		);
	}

	public function configure_table($tbl) {
		$tbl->columns = array(
			'photo' => array('Photo', '30px'),
			'name' => array('Name', '150px'),
			'username' => array('Username', '150px'),
			'email' => array('Email', '150px'),
			'country' => array('Country', '150px'),
			'returner' => array('Returner?', '100px')
		);
		$tbl->sortable_columns = array(
			'name' => 'last_name, first_name'
		);
		$tbl->rb_fields = array('first_name', 'last_name', 'username');
		$tbl->default_sort = 'last_name';
	}

	public function table_returner() {
		if ($this->returner) return "<span class='notice'>Returner</span>";
		return "<span style='font-style:italic'>n/a</span>";
	}

	public function prepare_results($items) {
		$this->preload($items, array(
			'photo'
		));
	}

	public function table_country() {
		return $this->country_flag();
	}

	public function table_name($key) {
		$parts = array();

		$parts[] = "<div><a href='".$this->url()."'>".$this->short_label()."</a></div>";
		$parts[] = "<div>".$this->short_desc()."</div>";

		$parts[] = $this->controls();
		return join("", $parts);
	}

	function table_photo($key) {
		return "<img src='".$this->thumbnail_url('tiny') . "'/>";
	}

	function full_name() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function url() {
		return ci_url("conference/user/" . $this->id);
	}

	public function short_label() {
		return $this->last_name . ', ' . $this->first_name;
	}

	public function short_desc() {
		return $this->email;
	}

	public function thumbnail_url($size='thumb') {
		$photo = $this->r('photo');
		if (!$photo) {
			$photo = R::load('upload', 1);
		}
		return $photo->thumbnail_url($size);
	}

	public function validate_password($p1, $p2) {
		if ($p1 != $p2) {
			throw new Exception("Passwords do not match.");
		}
		if (empty($p1)) {
			throw new Exception("Password must not be blank");
		}
		if (strlen($p1) < 3) {
			throw new Exception("Password is too short");
		}
		if (strlen($p1) > 100) {
			throw new Exception("Password is too long");
		}
	}

}