<?php

class Model_Author extends Base_Model {


	public function update() {
		parent::update();

		if (empty($this->access_key)) {
			$this->access_key = random_key(15);
		}
	}

	public function send_confirmation_email() {
		$email_slug = 'author-confirmation';
    	$tpl = R::findOne('template', 'slug=:slug', array(':slug' => $email_slug));

    	$email = $tpl->create_email(array(
    		'first' => $this->first_name,
    		'last' => $this->last_name,
    		'url' => ci_url('submission/confirm/' . $this->access_key)
    	));
    	$email->to = $this->email;
    	$email->send();
    	return true;
	}

	public function full_name() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function editor_fields() {
		return array(
			'first_name' => array(
				'type' => 'text',
				'label' => 'First Name',
				'required' => true
			),
			'last_name' => array(
				'type' => 'text',
				'label' => 'Last Name',
				'required' => true
			),
			'email' => array(
				'type' => 'text',
				'label' => 'Email',
				'required' => true
			),
			'phone' => array(
				'type' => 'text',
				'label' => 'Phone'
			),
			'is_primary' => array(
				'type' => 'checkbox',
				'label' => 'Primary Author?'
			),
			'status' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => 'Pending',
					'confirmed' => 'Confirmed',
					'rejected' => 'Rejected'
				)
			)
		);
	}


	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Author Information',
					'fields' => array(
						'first_name', 'last_name', 'email', 'phone', 'is_primary')
				),
			)
		);
	}
}