<?php

/**
 * Stores credential information for resetting user passwords
 * @author knuhlig
 *
 */
class Model_Location extends Base_Model {
	
	public function short_label() {
		return $this->name;
	}

	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text'
			),
			'capacity' => array(
				'type' => 'text',
				'style' => 'width:100px'
			),
			'importance' => array(
				'type' => 'dropdown',
				'opts' => array(
					'1' => 1, 
					'2' => 2, 
					'3' => 3, 
					'4' => 4, 
					'5' => 5, 
					'6' => 6, 
					'7' => 7, 
					'8' => 8,
					'9' => 9,
					'10' => 10)
			),
			'is_default' => array(
				'type' => 'checkbox',
				'label' => 'Is Default?'
			)
		);
	}

	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Location Information',
					'fields' => array(
						'name', 'capacity', 'importance', 'is_default')
				)
			)
		);
	}

	public function configure_table($tbl) {
		$tbl->rb_fields = array('name');
		$tbl->default_sort = 'name';
	}

	public function table_name($key) {
		$parts = array();
		$parts[] = $this->name;
		$parts[] = $this->controls();
		return join("", $parts);
	}
}