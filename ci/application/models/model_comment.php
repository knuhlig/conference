<?php

class Model_Comment extends Base_Model {

	public function editor_options() {

    }
    

	public function editor_fields() {
		return array(
			'comment' => array(
				'type' => 'rt',
				'style' => 'height:200px'
			),
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
				'cond' => 'is_reviewer=1'
			),
			'is_public' => array(
				'type' => 'checkbox',
				'label' => 'Make Public?',
				'description' => 'If selected, your comment will be visible to authors as well as reviewers.',
			)
		);
	}

	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Comment Information',
					'fields' => array(
						'user', 'comment', 'is_public')
				)
			)
		);
	}

}