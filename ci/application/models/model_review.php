<?php

class Model_Review extends Base_Model {

	public function editor_fields() {
		return array(
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
				'cond' => 'is_reviewer=1',
				'rich' => true,
				'sort' => 'last_name asc, first_name asc',
				'label' => 'Reviewer'
			),
			'completed' => array(
				'type' => 'checkbox'
			),
			'submission' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'submission',
				'cond' => 'submission_date is not null',
				'rich' => true
			),
			'score' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
					'7' => '7',
					'8' => '8',
					'9' => '9',
					'10' => '10',
				),
			),
			'recommendation' => array(
				'type' => 'dropdown',
				'opts' => array(
					'' => '',
					'accept' => 'Accept',
					'revise' => 'Revise',
					'reject' => 'Reject'
				)
			),
			'plos_one' => array(
				'type' => 'checkbox',
				'description' => 'Recommend submission of full paper to PLoS ONE?'
			),
			'comments' => array(
				'type' => 'rt',
				'style' => 'height:200px',
				'label' => 'Reviewer Comments'
			),
		);
	}

}