<?php

/**
 * Quick offer:
 * name
 * start, stop
 * price, addons
 * regtype, verification
 * @author knuhlig
 *
 */
class Model_Offer extends Base_Model {
	
	public function type_label() {
		return "Campaign";
	}
	
	public function update() {
		parent::update();
		if (!$this->access_key) {
			$this->access_key = random_key();
		}
	}
	
	public function calculate_price($rate) {
		if ($this->customize_prices) {
			if (!is_null($this->price)) {
				return $this->price;
			}
		}
		
		return $rate->calculate_price();
	}
	
	public function get_available_addons($rate, $master_class=false) {
		
		$addons = $rate->get_available_addons($master_class);
		
		if ($this->customize_prices) {
			$related = $this->associated('addon', true);
			foreach ($related as $bean) {
				$addon = $bean->addon;
				$id = $addon->id;
				
				if (!$addon->is_available() || $addon->master_class != $master_class) {
					continue;
				}
				if ($bean->hide) {
					unset($addons[$id]);
					continue;
				}
				
				if (!isset($addons[$id])) {
					$addons[$id] = $bean->addon;
				}
				if (!is_null($bean->price)) {
					$addons[$id]->price = $bean->price;
				}
			}
		}
		
		return $addons;
	}
	
	public function access_url($email=null) {
		$params = array(
			'key' => $this->access_key
		);
		if (!is_null($email)) {
			$params['email'] = $email;
		}
		return ci_url('register', $params);
	}
	
	public function preview_url() {
		$users = $this->get_user_list();
		$email = null;
		
		if (!empty($users)) {
			$user = reset($users);
			$email = $user['email'];
		}
		
		return $this->access_url($email);
	}
	
	public function short_label() {
		return $this->name;
	}
	
	public function access_labels() {
		return array(
			'public' => 'Public Access',
			'email' => 'Email List'
		);
	}
	
	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text',
				'default' => property($_GET, 'name')
			),
			'start_date' => array(
				'type' => 'timestamp'
			),
			'end_date' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_END
			),
			'rate' => array(
				'type' => 'relation',
				'label' => 'Registration Options',
				'relation' => 'multiple',
				'description' => 'If multiple rates are offered, users will choose between them.',
				'default' => property($_GET, 'rate_id')
			),
			'customize_prices' => array(
				'type' => 'checkbox',
				'label' => 'Custom Pricing?'
			),
			'access' => array(
				'type' => 'dropdown',
				'label' => 'Access',
				'opts' => $this->access_labels(),
				'description' => 'Access Restrictions'
			),
			'email_list' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:300px',
				'description' => 'each line: [email], [first], [last]',
				'deps' => array(
					array('access', '==', 'email')
				)
			),

			'price' => array(
				'label' => 'Custom Price',
				'type' => 'currency',
				'style' => 'width:100px',
				'description' => 'This price will override the base rate.',
				'deps' => array(array('customize_prices', '==', 1))
			),
			'addon' => array(
				'type' => 'relation',
				'label' => 'Addon Customization',
				'relation' => 'multiple',
				'fields' => array(
					'hide' => array(
						'type' => 'checkbox',
							
					),
					'price' => array(
						'type' => 'currency',
						'style' => 'width:50px',
					)
				),
				'description' => 'You may set custom prices for add-ons here. Otherwise, the default price will be used.',
				'deps' => array(array('customize_prices', '==', 1))
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Campaign Information',
					'fields' => array('name', 'start_date', 'end_date', 'rate', 'customize_prices', 'price', 'addon')		
				),
				array(
					'title' => 'Access',
					'fields' => array('access', 'email_list')
				)
			)
		);
	}
	
	public function editor_options() {
		if (!$this->id) return;
		print '<div class="misc-pub-section">';
		print '<input type="button" value="Email Wizard" onclick="openWindow(\'?page=email_offer&id='.$this->id.'\')"/>';
		print '<div style="float:right"><input type="button" value="Preview" onclick="openWindow(\''.$this->preview_url().'\')"/></div>';
		print '</div>';
	}
	
	public function template_data() {
		$output = array();
		$output[] = "# email, first, last, url";
		
		$lines = explode("\n", $this->email_list);
		
		foreach ($lines as $line) {
			$parts = explode(",", $line);
				
			$email = $parts[0];
			$first = $parts[1];
			$last = $parts[2];
			$url = $this->access_url($email);
			$output[] = "{$email}, {$first}, {$last}, {$url}";
		}
		
		return join("\n", $output);
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'name' => array('Name', '200px'),
			'access' => array('Access', '100px'),
			'rate' => array('Base Rate(s)', '200px'),
			'discount' => array('Custom Price', '100px'),
			'status' => array('Status', '100px')
		);
		$tbl->rb_fields = array('name');
		$tbl->default_sort = '(start_date <= UNIX_TIMESTAMP(NOW()) and end_date > UNIX_TIMESTAMP(NOW())) desc, name';
	}
	
	public function table_status($key) {
		if ($this->is_active()) {
			return '<span class="success" style="padding:5px">Active</span>';
		}
		return '<span class="warn" style="padding:5px">Not Active</span>';
	}
	
	public function is_active() {
		$time = time();
		if ($time < $this->start_date) return false;
		if ($time > $this->end_date) return false;
		return true;
	}
	
	public function table_discount($key) {
		if (!is_null($this->price)) {
			return usd($this->price);
		}
		return 'n/a';
	}
	
	public function table_access($key) {
		$arr = $this->access_labels();
		return $arr[$this->$key];
	}
	
	public function table_start_date($key) {
		return date(DATE_FMT_FULL, $this->$key);
	}
	
	public function table_end_date($key) {
		return date(DATE_FMT_FULL, $this->$key);
	}
	
	public function table_name($key) {
		return $this->$key . $this->controls();
	}
	
	public function table_rate($key) {
		$arr = $this->associated($key);
		$parts = array();
		foreach ($arr as $item) {
			$parts[] = "<div>".$item->short_label()."</div>";
		}
		return join("", $parts);
	}

	
	
	
	
    public function generate_key($length=10, $chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $idx = rand(0, strlen($chars) - 1);
            $key .= $chars[$idx];
        }

        return $key;
    }

    /** Event Hooks
    public function open() {}
    public function dispense() {}
    public function after_update() {}
    public function delete() {}
    public function after_delete() {}
    */

    public function check_validity($email=null) {
        $time = time();

        if ($this->trashed) {
            return 'This offer has been deleted';
        }

        // too early?
        if ($time < $this->start_date) {
            return 'This offer is not available until: ' . date(DATE_FMT_PRETTY, $this->start_date);
        }

        // too late?
        if ($time > $this->end_date) {
            return 'This offer expired on: ' . date(DATE_FMT_PRETTY, $this->end_date);
        }

        if ($this->access != 'public') {
            if (empty($email)) {
                return "This offer has access restrictions and is not available to the general public.";
            }

            // on the list?
            $user_list = $this->get_user_list();
            if (!isset($user_list[$email])) {
                return "This offer has access restrictions and is not available to {$email}";
            }
            if (R::count('order', 'date_paid is not null and email=:email and offer_id=:offer_id', array(
            		':email' => $email, 
            		':offer_id' => $this->id))) {
                return "This offer has already been used by {$email}";
            }
            return $user_list[$email];
        }

        // everything checks out :)
        return null;
    }

    public function public_url($oid=null) {
        if ($oid) {
            return ci_url('register', array('key' => $this->access_key, 'oid' => $oid));
        } else {
            return ci_url('register', array('key' => $this->access_key));
        }
    }

    public function private_url($email) {
        return ci_url('register', array('key' => $this->access_key, 'email' => $email));
    }

    

    public function get_user_list() {
        $users = array();

        $raw = $this->email_list;
        $lines = explode("\n", $raw);
        foreach ($lines as $line) {
            $line = trim($line);
            if (empty($line)) continue;

            $cols = explode(",", $line);
            $email = trim($cols[0]);
            $first = trim($cols[1]);
            $last = trim($cols[2]);

            $users[$email] = array(
                'first_name' => ucfirst(strtolower($first)),
                'last_name' => ucfirst(strtolower($last)),
                'email' => strtolower($email)
            );
        }

        return $users;
    }
}