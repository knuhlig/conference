<?php

/**
 * Stores credential information for resetting user passwords
 * @author knuhlig
 *
 */
class Model_Payment extends Base_Model {
	
	public function short_label() {
		return $this->name;
	}
	
	public function update() {
		if (!$this->access_key) {
			$this->access_key = random_key(20);
		}
	}
	
	public function editor_fields() {
		return array(
			'name' => array(
				'type' => 'text'
			),
			'description' => array(
				'type' => 'html'
			),
			'price' => array(
				'type' => 'currency',
				'style' => 'width:70px'
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Payment Information',
					'fields' => array(
						'name', 'description', 'price')
				)
			)
		);
	}
	
	public function url() {
		return ci_url('register/payment/' . $this->access_key);
	}
	
	
	public function table_name() {
		return "<a href='".$this->url()."'>" . $this->name . "</a>" . $this->controls();
	}
	
	public function table_price() {
		return usd($this->price);
	}
}