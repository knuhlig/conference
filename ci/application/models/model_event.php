<?php

class Model_Event extends Base_Model {
	
	protected $relation_map = array(
		'photo' => 'upload'
	);
	
	public function delete() {
		$photo = $this->r('photo');
		if ($photo) {
			R::trash($photo);
		}
	}

    public function time_interval() {
        $end = $this->event_date + $this->duration * 60;
        $a1 = date('a', $this->event_date);
        $a2 = date('a', $end);
        
        if ($a1 == $a2) {
            return date('g:i', $this->event_date) . ' - ' . date('g:i a', $end);
        }
        
        return date('g:i a', $this->event_date) . ' - ' . date('g:i a', $end);
    }

    public function attendee_count() {
        return R::count('attendance', 'event_id = ? and will_attend = 1', array($this->id));
    }

    public function is_full() {
        $count = $this->attendee_count();
        $capacity = $this->get_room_capacity();
        if ($capacity > 0 && $count >= $capacity) {
            return true;
        }
        return false;
    }

    public function get_room_capacity() {
        $loc = $this->location;
        if ($loc) {
            return $loc->capacity;
        }
        return null;
    }

    public function get_attendees() {
        return R::find('attendance', 'event_id = ? and will_attend = 1 order by date_updated asc', array($this->id));
    }

	public function editor_options() {	
		print '<div class="misc-pub-section">';
		print '<a target="_blank" href="'.$this->url().'">Event Page</a>';
		print '</div>';

        print '<div class="misc-pub-section">';
        print '<a target="_blank" href="?page=event_summary&id='.$this->id.'">Event Summary</a>';
        print '</div>';
	}
	
	public function start_date() {
		return $this->event_date;
	}
	
	public function find_session() {
		$parent_cond = "1";
		$sql = "type='group' and location_id = ? and event_date <= ? and event_date + 60 * duration >= ? order by event_date desc limit 1";
		$session = R::findOne('event', 
			$sql,
			array($this->location_id, $this->event_date, $this->end_date()));
		return $session;
	}
	
	public function end_date() {
		return $this->event_date + 60 * $this->duration;
	}
	
	public function is_contained_in($parent) {
		if ($this->start_date() < $parent->start_date()) {
			return false;
		}
		if ($this->end_date() > $parent->end_date()) {
			return false;
		}
		return true;
	}
	
    public static function conference_locations() {
    	return array(
    		'plenary' => 'Plenary Hall',
    		'demopavilion' => 'Demo Pavilion',
            'brest' => 'Paul Brest Hall',
            'ideo' => 'IDEO Headquarters',
            'lk101a' => 'LK101',
            'lk102' => 'LK102',
    		'lk101' => 'LK101/102',
    		'lk120' => 'LK120',
    		'lk130' => 'LK130',
    		'lk208' => 'LK208',
            'lk209' => 'LK209',
            'lk304' => 'LK304',
            'lk305' => 'LK305',
    		'lk306' => 'LK306',
    		'lk308' => 'LK308',
            'a120130' => 'Multiple Locations',
            'a304305' => 'LK304/305',
    		'lawn' => 'Alumni Lawn',
    		'lobby' => 'Upper Lobby',
    		'all_lobby' => 'Upper/Lower Lobby',
    		'all' => 'All Locations'
    	);
    }
    
    public function event_types() {
    	return array(
    		'core' => 'Core Theme Presentation',
    		'keynote' => 'Keynote Presentation',
    		'session' => 'Session Presentation',
    		'masterclass' => 'Master Class',
            'workshop_cap' => 'Workshop',
    		'panel' => 'Session - Panel',
    		'oral' => 'Session - Oral',
    		'poster' => 'Session - Poster',
    		'workshop' => 'Session - Workshop',
    		'ignite' => 'ePatient Ignite Talk',
    		'tech' => 'Technology Discovery Panel',
    		'intl' => 'International Correspondent',
    		'artist' => 'Artist in Residence',
    		'startup' => 'Startup Presentation',
    		'next' => 'Patient neXt Presentation',
    		'group' => 'Grouping',
    		'other' => 'Other',
    		'food' => 'Food/Break'
    	);
    }
    
    public function short_label() {
    	return $this->full_title();
    }
    
    public function editor_fields() {
    	return array(
    		'speaker' => array(
    			'type' => 'relation',
    			'label' => 'Speaker(s)',
    			'relation' => 'multiple',
    			'rich' => true,
    			'sort' => 'last_name asc, first_name asc',
    			'fields' => array(
    				'moderator' => array(
    					'type' => 'checkbox',
    					'label' => 'Moderator?',
    					'style' => 'width:60px'
    				),
    			),
    			'description' => 'Check box to mark speaker as moderator'
    		),
    		'type' => array(
    			'label' => 'Event Type',
    			'type' => 'dropdown',
    			'opts' => self::event_types()
    		),
    		'title' => array(
    			'type' => 'text',
    			'style' => 'width:400px'
    		),
    		'photo' => array(
    			'label' => 'Event Photo',
    			'type' => 'file',
    		),
    		'description' => array(
    			'label' => 'Event Description',
    			'type' => 'html',
    			'description' => 'Event Description'
    		),
    		'event_date' => array(
    			'label' => 'Date',
    			'type' => 'timestamp',
    			'default' => CONFERENCE_START
    		),
    		'duration' => array(
    			'label' => 'Duration',
    			'type' => 'text',
    			'description' => 'minutes',
    			'style' => 'width:60px'
    		),
    		'location' => array(
    			'type' => 'relation',
                'relation' => 'one',
                'model' => 'location'
    		),
            'locations' => array(
                'type' => 'relation',
                'relation' => 'multiple',
                'model' => 'location',
                'label' => 'Sub-Locations'
            ),
    		'scheduled' => array(
    			'label' => 'Include in Schedule?',
    			'type' => 'checkbox',
    			'description' => 'Check this box to add the event to the conference schedule'
    		),
            'cap' => array(
                'label' => 'Attendance Cap',
                'type' => 'text',
                'style' => 'width:100px'
            ),
            'disable_signups' => array(
                'label' => 'Disable Signups?',
                'type' => 'checkbox'
            )
    	);
    }
    
    public function editor_layout() {
    	return array(
    		'sections' => array(
    			array(
    				'title' => 'Event Information',
    				'fields' => array('type', 'title', 'speaker', 'photo', 'description')
    			),
    			array(
    				'title' => 'Scheduling',
    				'fields' => array('event_date', 'duration', 'location', 'locations', 'cap', 'disable_signups')
    			),
    		)
    	);
    }
    
    public function configure_table($tbl) {
    	$tbl->columns = array(
    		'name' => array('Event', '200px'),
    		'event_date' => array('Date', '120px'),
    		'location' => array('Location', '100px'),
    		'type' => array('Type', '100px')
    	);
    	$tbl->default_sort = 'event_date asc, duration desc, location_id';
    	$tbl->default_dir = 'asc';
    	$tbl->rb_fields = array('title');
    	$tbl->page_size = 200;
    }
    
    public function prepare_results($items) {
    	$this->preload($items, array(
    		'photo',
    	));
    }
    
    public function schedule_thumbnail($size='thumb') {
    	if ($this->type != 'keynote' && $this->type != 'masterclass') {
	    	$photo = $this->r('photo');
	    	if ($photo) {
	    		return $photo->thumbnail_url($size);
	    	}
    	}
    	
    	$speakers = $this->associated('speaker');
    	if ($speakers) {
    		$speaker = reset($speakers);
    		$pf = $speaker->r('profile_photo');
    		if ($pf) {
    			return $pf->thumbnail_url($size);
    		}
    		return $speaker->thumbnail_url($size);
    	}
    }
    
    public function thumbnail_url($size='thumb') {
    	$photo = $this->r('photo');
    	if ($photo) {
    		return $photo->thumbnail_url($size);
    	}
    	
    	$speakers = $this->associated('speaker');
    	if ($speakers) {
    		$speaker = reset($speakers);
    		$pf = $speaker->r('profile_photo');
    		if ($pf) {
    			return $pf->thumbnail_url($size);
    		}
    		return $speaker->thumbnail_url($size);
    	}
    	
    	return '';
    }
    
    public function location_name() {
        $loc = $this->location;
        if ($loc) {
            return $loc->name;
        }
        return "";
    }
    
    public function url() {
    	return ci_url('conference/event/' . $this->id);
    }
    
    public function type_name() {
    	$arr = $this->event_types();
    	return $arr[$this->type];
    }
    
    public function event_snippet($length_limit=200) {
    	$link = '<a href="'.$this->url().'">Read more</a>';
    	 
    	$d = $this->description;
    	if (empty($d)) return '';
    	
    	$d = preg_replace('/<[^>]+>/', '', $d);

    	if (strlen($d) < $length_limit) {
    		return $d . ' ' . $link;
    	}
    	
    	$words = explode(" ", $d);
    	$arr = array();
    	$len = 0;
    	foreach ($words as $word) {
    		if (!empty($arr)) $len++;
    		$len += strlen($word);
    		$arr[] = $word;
    		if ($len >= $length_limit) {
    			break;
    		}
    	}
    	
    	$str = join(" ", $arr) . '... ' . $link;
    	return $str;
    }
    
    public function first_speaker() {
    	$speakers = $this->associated('speaker');
    	if (empty($speakers)) return null;
    	return reset($speakers);
    }
    
    public function speaker_list($links=false) {
    	$speakers = $this->associated('speaker');
    	$parts = array();
    	
    	$count = 0;
    	$num_speakers = sizeof($speakers);
    	
    	foreach ($speakers as $speaker) {
    		if ($count > 0) {
    			if ($num_speakers == 2) {
    				$parts[] = ' and ';
    			} else {
    				if ($count == $num_speakers - 1) {
    					$parts[] = ' and ';
    				} else {
    					$parts[] = ', ';
    				}
    			}
    		}
    		if ($links) {
    			$parts[] = '<a href="'.$speaker->url().'">'.$speaker->full_name().'</a>';
    		} else {
    			$parts[] = $speaker->full_name();
    		}
    		$count++;
    	}
    	
    	return join("", $parts);
    }
    
    public function full_title() {
    	if ($this->type == 'masterclass') {
    		return "Masterclass " . $this->title;
    	}
    	return $this->title;
    }

    public function controls($hover=true) {
        $type = $this->bean->getMeta('type');

        $cls = $hover ? 'hovershow' : '';

        $parts = array();
        $parts[] = "<div class='{$cls}' style='float:right'>";

        $parts[] = "<a href='?page=edit_{$type}&id={$this->id}'>Edit</a>";
        $parts[] = ' <a href="javascript:deleteObject(\''.$type.'\', '.$this->id.')" class="warn">Delete</a>';
        $parts[] = ' <a href="?page=event_summary&id='.$this->id.'" class="">Summary</a>';

        $parts[] = "</div>";

        return join("", $parts);
    }
    
    public function table_name($key) {
    	$parts = array();
    	$parts[] = $this->controls();
    	 
    	$speakers = $this->associated('speaker');

    	$parts[] = "<div><a target='_blank' href='".$this->url()."'>" . $this->full_title() . "</a></div>";
    	
    	
    	$parts[] = "<div>" . $this->speaker_list(false) . "</div>";
    	
    	$style = '';
    	
    	$html = join("", $parts);
    	$html = '<div style="'.$style.'">' . $html . '</div>';
    	
    	return $html;
    }
    
    public function table_photo($key) {
    	$upload = $this->r('photo');
    	if (!$upload) return 'n/a';
    	return $upload->thumbnail();
    }
    
    public function table_event_date($key) {
		return date(DATE_FMT_FULL, $this->$key) . ' - ' . date('g:i a', $this->$key + $this->duration * 60);
    }
    
    public function table_scheduled($key) {
    	if ($this->$key) return 'Yes';
    	return '<span class="warn" style="padding:5px">No</span>';
    }
    
    public function table_location($key) {
    	return $this->location_name();
    }
}

