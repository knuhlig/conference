<?php

class Model_Donation extends Base_Model {
	
	public function editor_fields() {
		return array(
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'search' => true,
				'sort' => 'last_name asc, first_name asc'
			),
			'amount' => array(
				'type' => 'currency',
				'style' => 'width:100px'
			),
			'donation_date' => array(
				'type' => 'timestamp'
			),
			'is_paid' => array(
				'type' => 'checkbox',
				'label' => 'Paid?'
			),
			'twitter_id' => array(
				'type' => 'text',
				'style' => 'width:200px',
				'label' => 'Twitter ID'
			)
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Donation Information',
					'fields' => array(
						'user', 'amount', 'donation_date', 'is_paid',
						'twitter_id'
					)
				),
			)
		);
	}

	public function configure_table($tbl) {
		$tbl->columns = array(
			'user' => array('User', '100px'),
			'amount' => array('Amount', '100px'),
			'date' => array('Date', '100px'),
			'twitter' => array('Twitter ID', '100px')
		);
	}

	public function table_twitter() {
		return $this->twitter_id;
	}

	public function table_user() {
		$u = $this->r('user');
		if (!$u) return;
		return $u->full_name() . $this->controls();
	}

	public function table_amount() {
		return '$' . number_format($this->amount, 2);
	}

	public function table_date() {
		if (!$this->is_paid) {
			return "<span class='warn' style='padding:3px'>Not Paid</span>";
		}

		return date(DATE_FMT_FULL, $this->donation_date);
	}
	
}