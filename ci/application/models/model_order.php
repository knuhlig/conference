<?php

class Model_Order extends Base_Model {

	protected $relation_map = array(
		'verification_photo' => 'upload'
	);

	public function type_label() {
		return 'Registration';
	}

	public function short_label() {
		return $this->user->short_label();
	}

	public function short_desc() {
		return $this->rate->name;
	}

	public function export_data() {
		if (!$this->paid) {
            return null;
        }
        
        $user = $this->user;
		$rate = $this->rate;

		return array(
			'Order ID' => $this->id,
            'Paid' => ($this->paid ? 'Paid' : 'Not Paid'),
			'Date Paid' => date(DATE_FMT_FULL, $this->date_paid),
			'Rate' => $rate->name,
			'Registration Price' => usd($this->registration_price),
			'Last Name' => $user->last_name,
			'First Name' => $user->first_name,
			'Email' => $user->email,
			'Title' => $user->title,
            'Phone' => $user->phone,
            'Fax' => $user->fax,
            'Mailing Address' => $user->mailing_address,
            'Country' => $user->country,
			'Twitter' => $user->twitter_username,
			'Returner' => $user->returner ? '1' : ''
		);
	}

    public function total_price() {
        $total = $this->registration_price;
        foreach ($this->associated('addon', true) as $addon) {
            $total += $addon->unit_price * $addon->quantity;
        }
        return $total;
    }

    public function editor_options() {
    	print '<div class="misc-pub-section">';
    	//print '<div style="float:right"><input type="button" value="View Stats" onclick="doAction(\'stats\')"/></div>';

    	if ($this->paid) {
    		print '<a href="'.$this->receipt_url().'">Download Receipt</a>';
    	}
    	print '</div>';

    	$u = $this->r('user');
    	if ($u) {
	    	print '<div class="misc-pub-section">';
	    	print "<a href='?page=edit_user&id=".$u->id."'>Edit User</a>";
	    	print '</div>';
    	}

    	print '<div class="misc-pub-section">';
    	print "<a href='".ci_url('badges/order/' . $this->id)."'>View Badge</a>";
    	print '</div>';
    }

    public function receipt_url() {
    	return ci_url('register/receipt/' . $this->resume_key);
    }

    public function update() {
    	parent::update();

        if (!$this->resume_key) {
            $this->resume_key = random_key(20);
        }

        if (!$this->regtype_id) {
            $rate = $this->r('rate');
            if (!empty($rate)) {
                $regtype = $rate->r('regtype');
                if (!empty($regtype)) {
                    $this->regtype = $regtype;     
                }
            }
        }
    }

    public function resume_url() {
        return ci_url('register', array('resume' => $this->resume_key));
    }

    public function generate_receipt($verbose=false) {
        $CI =& get_instance();
        $user = $this->user;

        $htmlfile = "/medx/receipts/Receipt_" . $this->id . ".html";
        $pdffile = "/medx/receipts/Receipt_" . $this->id . ".pdf";

        // check if receipt already exists and is up to date
        if (file_exists($pdffile)) {
        	$mtime = filemtime($pdffile);
        	if ($mtime >= $this->last_updated && $mtime >= $user->last_updated) {
        		return $pdffile;
        	}
        }

        $receipt_html = $CI->load->view('register/receipt', array(
            'user' => $user,
            'order' => $this
        ), true);

        $pdf_generator = PDF_GENERATOR;

        // remove whitespace from the filename, which would break the command below
        $pdffile = preg_replace('/\s+/', '', $pdffile);

        file_put_contents($htmlfile, $receipt_html);

        $cmd = "{$pdf_generator} {$htmlfile} {$pdffile}";
        $res = shell_exec($cmd);

        if (file_exists($pdffile)) {
            return $pdffile;
        }
        return null;
    }

    public function editor_fields() {
    	return array(
    		'user' => array(
    			'type' => 'relation',
    			'relation' => 'one',
    			'label' => 'Attendee',
    			'sort' => 'last_name asc, first_name asc',
    			'search' => true
    		),
    		'offer' => array(
    			'type' => 'relation',
    			'relation' => 'one'
    		),
    		'verification_photo' => array(
    			'type' => 'file',
    			'label' => 'Verification Photo',
    			'private' => true,
    			'no_face' => true
    		),
    		'regtype' => array(
    			'type' => 'relation',
    			'relation' => 'one',
    			'label' => 'Category'
    		),
    		'rate' => array(
    			'type' => 'relation',
    			'relation' => 'one'
    		),

    		'paid' => array(
    			'type' => 'checkbox',
    			'label' => 'Paid?'
    		),
    		'date_paid' => array(
    			'type' => 'timestamp',
    			'deps' => array(
    				array('paid', '==', '1')
    			)
    		),
    		'email' => array(
    			'type' => 'text',
    			'description' => 'Offer email used for this order.'
    		),
    		'addon' => array(
    			'type' => 'relation',
    			'relation' => 'multiple',
    			'label' => 'Add-Ons',
    			'fields' => array(
    				'unit_price' => array(
    					'type' => 'currency',
    					'style' => 'width:60px'
    				),
    				'quantity' => array(
    					'type' => 'text',
    					'style' => 'width:60px',
    					'default' => '1'
    				),
    				'response' => array(
    					'type' => 'textarea',
    					'style' => 'width:300px;height:100px'
    				),
    				'waitlist' => array(
    					'type' => 'checkbox'
    				)
    			)
    		),
    		'registration_price' => array(
    			'type' => 'currency',
    			'style' => 'width:60px'
    		),
    		'reference_number' => array(
    			'type' => 'text',
    			'style' => 'width:100px'
    		)
    	);
    }

    public function editor_layout() {
    	return array(
    		'sections' => array(
    			array(
    				'title' => 'Registration Information',
    				'fields' => array('user', 'rate', 'reference_number', 'paid', 'date_paid')
    			),
    			array(
    				'title' => 'Receipt/Payment Details',
    				'fields' => array('registration_price', 'addon')
    			),
    			array(
    				'title' => 'Other Information',
    				'fields' => array('regtype', 'offer', 'email')
    			)
    		)
    	);
    }

    public function configure_table($tbl) {
    	$tbl->columns = array(
    		'paid' => array('Paid?', '100px'),
    		'user' => array('User', '150px'),
    		'rate' => array('Details', '200px'),
    		'total' => array('Total Paid', '100px'),
    		'country' => array('Country', '100px'),
    		'reference_number' => array('Ref', '50px'),
    	);
    	$tbl->sortable_columns = array(
    		'paid' => 'date_paid'
    	);

    	$tbl->callback = array($this, 'rb_search');

    	$tbl->rb_fields = array('reference_number');
    	$tbl->default_sort = 'created';
    	$tbl->default_dir = 'desc';
    }

    public function rb_search($search, $offset, $count, $sort, $dir) {
		$data = array();

		$terms = explode(" ", $search);
		foreach ($terms as &$term) {
			$term = $term . '%';
		}

		// build the search condition
		$vars = array();
		$conditions = array();
		$fields = array('u.first_name', 'u.last_name');

		$idx = 0;
		foreach ($terms as $key => $word) {
			$parts = array();
			foreach ($fields as $field) {
				$var = ':term' . ($idx++);
				$vars[$var] = $word;
				$parts[] = $field . ' like ' . $var;
			}
			$conditions[] = '(' . join(" or ", $parts) . ')';
		}
		$searchCond = join(" and ", $conditions);

		$sql = "select count(*)
			from wp_cm_order o
			left join wp_cm_user u on o.user_id=u.id
			where {$searchCond}";
		$data['total_count'] = R::getCell($sql, $vars);


    	$sql = "select o.*
	    			from wp_cm_order o
    				left join wp_cm_user u on o.user_id=u.id
    				where {$searchCond}
	    			order by {$sort} {$dir}
    				limit {$offset}, {$count}";

    	$rows = R::getAll($sql, $vars);
    	$data['results'] = R::convertToBeans('order', $rows);
    	return $data;
    }

    public function table_country() {
    	if ($this->user) {
    		return $this->user->country_flag();
    	}
    }

    public function prepare_results($items, $tbl=null) {
    	self::preload($items, array(
    		'user',
    		'user.photo',
    		'rate',
    	));
    }

    public function table_total() {
    	if (!$this->date_paid) return "n/a";
    	return usd($this->total_price());
    }

    public function table_user($key) {
    	$parts = array();
    	if ($this->user) {
	    	$parts[] = "<div style='float:left;margin-right:5px'><img src='".$this->user->thumbnail_url('tiny')."'/></div>";
	    	$parts[] = "<a href='?page=edit_user&id=".$this->user->id."'>" . $this->user->short_label() . "</a>";
	    	if ($this->user->returner) {
	    		$parts[] = " <span class='notice'>Returner</span>";
	    	}
    	}
    	return join("", $parts);
    }

    public function table_rate($key) {
    	$parts = array();
    	if ($this->rate) {
    		$parts[] = $this->rate->short_label();
    	}
    	foreach ($this->associated('addon', true) as $orderaddon) {
    		$addon = $orderaddon->addon;
    		if ($orderaddon->waitlist) {
    			$str = 'Waitlist for ' . $addon->name;
    		} else {
    			$str = $addon->name;
    		}
    		$maxlen = 40;
    		if (strlen($str) > $maxlen) {
    			$str = substr($str, 0, $maxlen - 3) . '...';
    		}
    		$parts[] = "+ " . $str;
    	}
    	return join("<br/>", $parts);
    }

    public function table_paid($key) {
    	if ($this->paid) {
    		return date(DATE_FMT_FULL, $this->date_paid) . $this->controls();
    	}
    	return '<span class="warn" style="padding:5px">Not paid</span>' . $this->controls();
    }
}