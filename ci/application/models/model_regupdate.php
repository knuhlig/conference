<?php

class Model_Regupdate extends Base_Model {
	
	
	public function total_price() {
		$total = 0;
		foreach ($this->associated('addon', true) as $addon) {
			$total += $addon->unit_price * $addon->quantity;
		}
		return $total;
	}
}