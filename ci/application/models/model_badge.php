<?php

class Model_Badge extends Base_Model {

	const STATUS_UNLOCKED = 1;
	const STATUS_LOCKED = 2;
	const STATUS_HIDDEN = 3;
	
	const TITLE_LENGTH = 26;
	
	const TIME_FMT = 'g:ia';
	
	protected $relation_map = array(
		'photo' => 'upload'
	);
	
	public function short_label() {
		return $this->full_name();
	}
	
	public function editor_fields() {
		return array(
			'status' => array(
				'type' => 'dropdown',
				'opts' => array(
					self::STATUS_UNLOCKED => 'Unlocked',
					self::STATUS_LOCKED => 'Locked'
				)
			),
			'color' => array(
				'type' => 'dropdown',
				'opts' => array(
					'black' => 'Black',
					'blue' => 'Blue',
					'gold' => 'Gold',
					'purple' => 'Purple',
					'red' => 'Red'
				),
				'label' => 'Badge Color'
			),
			'photo' => array(
				'label' => 'Photo',
				'type' => 'file',
			),
			'first_name' => array(
				'type' => 'text'
			),
			'last_name' => array(
				'type' => 'text'
			),
			'email' => array(
				'type' => 'text'
			),
			'title' => array(
				'type' => 'text',
				'label' => 'Affiliation',
				'description' => 'For badge purposes, ~'.self::TITLE_LENGTH.' characters max'
			),
			'twitter_handle' => array(
				'type' => 'text'
			),
			'returner' => array(
				'type' => 'checkbox'
			),
			'self_tracking' => array(
				'type' => 'checkbox',
				'default' => 1
			),
			'is_sponsor' => array(
				'type' => 'checkbox',
				'label' => 'Sponsor?'
			),
			'core' => array(
				'type' => 'checkbox',
				'default' => 1
			),
			'is_speaker' => array(
				'type' => 'checkbox',
				'label' => 'Speaker?'
			),
			'order' => array(
				'type' => 'relation',
				'relation' => 'one'
			),
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'sort' => 'last_name asc, first_name asc'
			),
			'extra_bus_pass' => array(
				'type' => 'checkbox'
			),
			'extra_reception_ticket' => array(
				'type' => 'checkbox'
			),
			'has_portrait' => array(
				'type' => 'checkbox',
				'description' => 'Check if the speaker should receive a professional portrait',
				'label' => 'Portrait?'
			),
			'small_font' => array(
				'type' => 'checkbox',
				'description' => 'Check for badges with long names (smaller font needed)',
				'label' => 'Smaller Font?'
			),
			'multi_speaker' => array(
				'type' => 'checkbox',
				'description' => 'Check if speaker has multiple speaking times',
				'label' => 'Multi Speaker?'
			),
			'portrait_time' => array(
				'type' => 'timestamp',
				'default' => '0'
			),



			'session_type' => array(
				'type' => 'text',
			),
			'session_location' => array(
				'type' => 'text'
			),
			'session_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'session_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk_title' => array(
				'type' => 'text'
			),
			'talk_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'instructions' => array(
				'type' => 'textarea',
				'style' => 'width:300px;height:100px'
			),

			'session2_type' => array(
				'type' => 'text',
			),
			'session2_location' => array(
				'type' => 'text'
			),
			'session2_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'session2_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk2_title' => array(
				'type' => 'text'
			),
			'talk2_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk2_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'instructions2' => array(
				'type' => 'textarea',
				'style' => 'width:300px;height:100px'
			),

			'session3_type' => array(
				'type' => 'text',
			),
			'session3_location' => array(
				'type' => 'text'
			),
			'session3_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'session3_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk3_title' => array(
				'type' => 'text'
			),
			'talk3_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk3_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'instructions3' => array(
				'type' => 'textarea',
				'style' => 'width:300px;height:100px'
			),

			'session4_type' => array(
				'type' => 'text',
			),
			'session4_location' => array(
				'type' => 'text'
			),
			'session4_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'session4_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk4_title' => array(
				'type' => 'text'
			),
			'talk4_start' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'talk4_end' => array(
				'type' => 'timestamp',
				'default' => CONFERENCE_START
			),
			'instructions4' => array(
				'type' => 'textarea',
				'style' => 'width:300px;height:100px'
			),
			
		);
	}
	
	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Badge Information',
					'fields' => array(
						'photo', 'first_name', 'last_name', 'title', 'email', 'twitter_handle', 
						'returner', 'is_sponsor', 'core', 'color', 'small_font'
					)
				),
				array(
					'title' => 'Packet Information',
					'fields' => array(
						'extra_bus_pass', 'extra_reception_ticket'
					)
				),
				array(
					'title' => 'Speaker Information',
					'fields' => array(
						'is_speaker', 'portrait_time',
					)
				),
				array(
					'title' => 'Talk 1 Information',
					'fields' => array(
						'session_type', 'session_location',
						'session_start', 'session_end', 'talk_title', 'talk_start', 'talk_end',
						'instructions',
					)
				),
				array(
					'title' => 'Talk 2 Information',
					'fields' => array(
						'session2_type', 'session2_location',
						'session2_start', 'session2_end', 'talk2_title', 'talk2_start', 'talk2_end',
						'instructions2',
					)
				),
				array(
					'title' => 'Talk 3 Information',
					'fields' => array(
						'session3_type', 'session3_location',
						'session3_start', 'session3_end', 'talk3_title', 'talk3_start', 'talk3_end',
						'instructions3',
					)
				),
				array(
					'title' => 'Talk 4 Information',
					'fields' => array(
						'session4_type', 'session4_location',
						'session4_start', 'session4_end', 'talk4_title', 'talk4_start', 'talk4_end',
						'instructions4',
					)
				),
				array(
					'title' => 'Backend Information',
					'fields' => array(
						'user'
					)
				),
			)
		);
	}
	
	public function full_name() {
		return $this->first_name . " " . $this->last_name;
	}
	
	public function get_export_lines($packet=false) {
		$photo = $this->r('photo');
		$order = $this->r('order');
		$user = $this->r('user');

		if (empty($photo)) {
			$photo = R::load('upload', 1);
		}
		
		$vars = array(
			'First' => $this->first_name,
			'Last' => $this->last_name,
			'Title' => $this->title,
			'TwitterHandle' => $this->twitter_handle,
			'Color' => $this->color,
			'Photo' => $photo->thumbnail_file('tile'),
			'QRCode' => $user->qr_file(),
			'Returner' => $this->returner ? "Yes" : "No",
			'Speaker' => $this->is_speaker ? "Yes": "No",
			'Portrait' => $this->has_portrait ? "Yes" : "No",
			'SmallFont' => $this->small_font ? "Yes": "No",
			'Sponsor' => $this->is_sponsor ? "Yes": "No"
		);

		if ($this->is_speaker) {
			if (!empty($this->portrait_time)) {
				$vars['PortraitTime'] = "time: " . date("l, n/j g:i a", $this->portrait_time);
			} else {
				$vars['PortraitTime'] = '';
			}
			for ($i = 1; ; $i++) {
				$num = '';
				if ($i > 1) $num = $i;
				
				$title = $this->{"talk{$num}_title"};
				if (empty($title)) break;

				$vars["Session{$num}Location"] = $this->{"session{$num}_location"};
				$vars["Session{$num}Title"] = $this->{"talk{$num}_title"};
				$vars["Session{$num}Type"] = $this->{"session{$num}_type"};
				$vars["Session{$num}Start"] = date(self::TIME_FMT, $this->{"session{$num}_start"});
				$vars["Session{$num}End"] = date(self::TIME_FMT, $this->{"session{$num}_end"});
				$vars["Start{$num}"] = date(self::TIME_FMT, $this->{"talk{$num}_start"});
				$vars["End{$num}"] = date(self::TIME_FMT, $this->{"talk{$num}_end"});
				$vars["Instructions{$num}"] = $this->{"instructions{$num}"};
			}
		}
		
		if ($packet) {
			$vars += array(
				'ExtraBusPass' => $this->extra_bus_pass ? "Yes" : "No",
				'ExtraReceptionTicket' => $this->extra_reception_ticket ? "Yes" : "No",
				'LoginQRCode' => $user->login_qr_file(),
				'Email' => $this->email
			);

			if (!property($_GET, 'skip_receipt')) {
				$vars['ReceiptFile'] = $order->generate_receipt();
			}
		}
		
		$lines = array();
		$lines[] = "%badge";
		foreach ($vars as $key => $value) {
			$lines[] = "{$key}\t{$value}";
		}
		return join("\n", $lines);
	}
	
	public function get_session_type($type, $moderator=false) {
		if ($moderator) {
			$labels = array(
				'core' => 'Core Theme Moderator',
				'next' => 'Patient neXt Panel Moderator',
				'panel' => 'Panel Moderator',
				'tech' => 'Technology Discovery Moderator'
			);
			if (isset($labels[$type])) {
				return $labels[$type];
			}
		} else {
			$labels = array(
				'core' => 'Core Theme Presenter',
				'keynote' => 'Keynote Speaker',
				'masterclass' => 'Master class Instructor',
				'panel' => 'Panel Presenter',
				'oral' => 'Oral Session Presenter',
				'poster' => 'Poster Presenter',
				'workshop' => 'Workshop Presenter',
				'workshop_cap' => 'Workshop',
				'ignite' => 'ePatient Presenter',
				'startup' => 'Startup Presenter',
				'artist' => 'Artist in Residence',
				'intl' => 'International Correspondent',
				'tech' => 'Technology Discovery Panel',
				'next' => 'Patient neXt Presenter',
				'other' => 'Medicine X Speaker'
			);
			if (isset($labels[$type])) {
				return $labels[$type];
			}
		}
		
		return '';
	}
	
	public function get_instructions($type, $is_moderator=false) {
		$mainstage = "Please present yourself to the stage manager no later than 30 minutes before your session will start in order to be microphoned for sound. Remain seated at the front of the auditorium in your designated speaker's seat until you are called to the stage. Slides must be submitted no later than 60 minutes before your session.";
		$session = "Please present yourself to the session A/V manager no later than 30 minutes before your session will start in order to be microphoned for sound. Remain seated at the front of the auditorium in your designated speaker's seat until you are called to present. Slides must be submitted no later than 60 minutes before your session.";
		$oral =    "Please present yourself to the session A/V manager no later than 30 minutes before your session will start in order to be microphoned for sound. Remain seated at the front of the auditorium in your designated speaker's seat until you are called to the stage. Slides must be submitted no later than 60 minutes before your session.";
		$poster = "Please arrive at least 15 minutes before your scheduled session to pin your poster. Your poster should remain posted until Sunday evening.";
		$workshop = "Please plan to arrive no later than 30 minutes before the start of your workshop session to set up the room. Slides must be submitted no later than 60 minutes before your session.";
		$moderator = "Please present yourself to the stage manager no later than 30 minutes before your session will start in order to be microphoned for sound. Remain seated at the front of the auditorium in your designated speaker's seat until you are called to the stage. Panelists will join you for a seated panel discussion. Please take the moderator's chair.";
		
		if ($is_moderator) {
			return $moderator;
		}
		
		$instr = array(
			'core' => $mainstage,
			'keynote' => $mainstage,
			'masterclass' => $mainstage,
			'panel' => $session,
			'oral' => $oral,
			'poster' => $poster,
			'workshop' => $workshop,
			'ignite' => $mainstage,
			'startup' => $mainstage,
			'artist' => 'Artist in Residence',
			'intl' => $mainstage,
			'tech' => $session,
			'next' => $mainstage,
			'other' => $mainstage
		);
		
		if (isset($instr[$type])) {
			return $instr[$type];
		}
		return '';
	}
	
	public function get_speaker_color($type, $moderator, $loc) {
		if ($moderator) return 'gold';
		if ($loc == 'plenary') return 'gold';
		
		$colors = array(
			'core' => 'gold',
			'keynote' => 'gold',
			'masterclass' => 'gold'
		);
		if (isset($colors[$type])) {
			return $colors[$type];
		}
		return 'red';
	}
	
	public function populate($order) {
		$user = $order->r('user');
		$rate = $order->r('rate');
		$regtype = $order->r('regtype');
		if (!$regtype) {
			$regtype = $rate->r('regtype');
		}
		$addons = $order->associated('addon');
		
		if (!$user) {
			return false;
		}

		R::store($user);
		$color = 'red';
		if ($regtype) {
			$color = $regtype->badge_color;
		}
		
		$this->status = self::STATUS_UNLOCKED;
		$this->user = $user;
		$this->order = $order;
		
		$photo = $user->r('photo');
		if (!$photo || !$photo->id) {
			$photo = R::load('upload', 1);
		}
		
		$bus_pass = false;
		$reception_ticket = false;
		
		foreach ($addons as $addon) {
			if ($addon->id == 1) {
				$bus_pass = true;
			} else if ($addon->id == 4) {
				$reception_ticket = true;
			}
		}
		
		
		$this->photo = $photo;
		$this->color = $color;
		$this->first_name = ucwords($user->first_name);
		$this->last_name = ucwords($user->last_name);
		$this->email = strtolower($user->email);
		$this->title = $user->title;
		$this->twitter_handle = $this->normalized_twitter($user->twitter_username);
		$this->returner = $user->returner;
		$this->self_tracking = true;
		$this->core = true;
		$this->extra_bus_pass = $bus_pass;
		$this->extra_reception_ticket = $reception_ticket;
		$this->has_portrait = !empty($rate->has_portrait);
		$this->small_font = strlen($user->last_name) >= 12;
		
		if (!empty($user->speaker_id)) {
			$speaker = R::load('speaker', $user->speaker_id);

			$sql = "select es.*
		    			from wp_cm_eventspeaker es
	    				left join wp_cm_event e on es.event_id=e.id
	    				where es.speaker_id=? and es.event_id is not null
	    				order by e.event_date asc
	    			";
	    	$rows = R::getAll($sql, array($speaker->id));
	    	$pivot = array_values(R::convertToBeans('eventspeaker', $rows));
			
			if (!empty($pivot)) {
				$it = 0;
				$row = $pivot[$it];
				$event = R::load('event', $row->event_id);
				
				while ($event->type == 'food') {
					$it++;
					if ($it >= sizeof($pivot)) break;
					$row = $pivot[$it];
					$event = R::load('event', $row->event_id);
				}

				$count = 0;
				$this->is_speaker = true;			
				
				while ($it < sizeof($pivot)) {
					$row = $pivot[$it];
					$event = R::load('event', $row->event_id);

					$it++;
					$count++;

					$num = '';
					if ($count > 1) $num = $count;

					$session = $event->find_session();
					if (!$session || !$session->id) {
						$session = $event;
					}
	
					$this->{"talk{$num}_title"} = $event->full_title();
					$this->{"session{$num}_start"} = $session->event_date;
					$this->{"session{$num}_end"} = $session->end_date();
					$this->{"talk{$num}_start"} = $event->event_date;
					$this->{"talk{$num}_end"} = $event->end_date();
					$this->{"session{$num}_location"} = date('l', $event->event_date) . ', ' . $event->location_name();
					$this->{"session{$num}_type"} = $this->get_session_type($event->type, $row->moderator);
					$this->{"instructions{$num}"} = $this->get_instructions($event->type, $row->moderator);
				}

				$color_levels = array(
					'red' => 1,
					'blue' => 2,
					'gold' => 3
				);
				$cur_level = $color_levels[$this->color];
				foreach ($pivot as $row) {
					$event = R::load('event', $row->event_id);
					$c = $this->get_speaker_color($event->type, $row->moderator, $event->location);
					if (!empty($color_levels[$c])) {
						$level = $color_levels[$c];
						if ($level > $cur_level) {
							$cur_level = $level;
							$this->color = $c;
						}
					}
				}
			}
			
		}
		
		return true;
	}

	public function action_sync() {
		$this->sync();
	}
	
	private function sync() {
		$sql = "select 
					o.* 
			    from wp_cm_order o 
				where date_paid is not null and 
					id not in (select order_id from wp_cm_badge where order_id is not null)
					and rate_id not in (select id from wp_cm_rate where exclude_badges)";
		
		$rows = R::getAll($sql);
		$orders = R::convertToBeans('order', $rows);
		
		$num_created = 0;
		$badges = array();
		foreach ($orders as $order) {
			$badge = R::dispense('badge');
			if ($badge->populate($order)) {
				$badges[] = $badge;
				$num_created++;
			}
		}
		R::storeAll($badges);
		
		if ($num_created > 0) {
			print "<div class='notice' style='padding:5px;margin-bottom:10px'>{$num_created} badge(s) were added automatically.</div>";
		}
	}
	
	public function customize_table($tbl) {
		$this->sync();

		print "<div style='margin-bottom:10px'>";
		print "<input class='button button-primary' type='button' value='Create All Badges' onclick=\"parent.location='".ci_url('badges/export')."'\"/>";
		print "<input class='button button-primary' type='button' value='Create All Packets' onclick=\"parent.location='".ci_url('badges/export')."?packet=1'\"/>";
		print "<input class='button' type='button' value='Reset All (Unlocked only)' onclick=\"parent.location='".ci_url('badges/regenerate')."'\"/>";
		print "</div>";
	}
	
	public function action_export() {
		R::store($this);
		redirect(ci_url('badges/export/' . $this->id));
		die();
	}
	
	public function action_label() {
		R::store($this);
		redirect(ci_url('badges/export/' . $this->id . "?label=1"));
		die();
	}
	
	public function action_packet() {
		R::store($this);
		redirect(ci_url('badges/export/' . $this->id . "?packet=1"));
		die();
	}
	
	public function editor_options() {
		print '<div class="misc-pub-section">';
		print "<input type='button' value='Badge' onclick=\"doAction('export')\"/>";
		print " <input type='button' value='Packet' onclick=\"doAction('packet')\"/>";
		print " <input type='button' value='Label' onclick=\"doAction('label')\"/>";
		print '</div>';
	}
	
	public function configure_table($tbl) {
		$tbl->columns = array(
			'name' => array('Attendee Information', '200px'),
			//'status' => array('Status', '100px')
		);
		$tbl->sortable_columns = array(
			//'paid' => 'date_paid'
		);
		$tbl->page_size = 10000;
		$tbl->customize = array($this, 'customize_table');
    	$tbl->default_sort = 'last_name asc, first_name';
    	$tbl->default_dir = 'asc';
    	$tbl->rb_fields = array('first_name', 'last_name');
    	
    	$tbl->filters = array(
    		'all' => 'All',
    		'locked' => 'Locked Only',
    		'unlocked' => 'Unlocked Only',
    		'hidden' => 'Hidden Items'
    	);
    	$tbl->filter_conds = array(
    		'all' => 'status != ' . self::STATUS_HIDDEN,
    		'locked' => 'status = ' . self::STATUS_LOCKED,
    		'unlocked' => 'status = ' . self::STATUS_UNLOCKED,
    		'hidden' => 'status = ' . self::STATUS_HIDDEN
    	);
	}
	
	public function is_complete() {
		$keys = array('first_name', 'last_name', 'title', 'twitter_handle');
		foreach ($keys as $key) {
			if (empty($this->$key)) {
				return false;
			}
		}
	}
	
	public function table_status($key) {
		switch ($this->status) {
			case self::STATUS_UNLOCKED:
				return "<span class='notice'>Unlocked</span>";
			case self::STATUS_LOCKED:
				return "<span class='success'>Locked</span>"; 
		}
		return "<span class='warn'>Unknown</span>";
	}
	
	public function normalized_twitter($h = null) {
		if (empty($h)) {
			$h = $this->twitter_handle;
		}
		if (empty($h)) return "";
		if ($h[0] == '#') $h[0] = '@';
		if ($h[0] != '@') {
			$h = '@' . $h;
		}
		return $h;
	}
	
	public function is_locked() {
		return $this->status == self::STATUS_LOCKED;
	}
	
	public function is_hidden() {
		return $this->status == self::STATUS_HIDDEN;
	}
	
	public function controls() {
		$type = $this->bean->getMeta('type');
		
		$parts = array();
		
		
		$parts[] = "<div class='hovershow' style='float:right;text-align:right'>";
		

		$parts[] = "<div style='margin-bottom:5px'>";
		$parts[] = " <a class='button' href='".ci_url('badges/export/' . $this->id)."'>Create Badge</a>";
		$parts[] = " <a class='button' href='".ci_url('badges/export/' . $this->id . "?packet=1")."'>Create Packet</a>";
		$parts[] = " <a class='button' href='".ci_url('badges/export/' . $this->id . "?label=1")."'>Create Label</a>";
		$parts[] = "</div>";
		
		$parts[] = " <a href='?page=edit_{$type}&id={$this->id}'>Edit</a>";
		
		if (!$this->is_hidden()) {
			if (!$this->is_locked()) {
				$parts[] = " <a href='".ci_url('badges/lock/' . $this->id)."' class='notice'>Lock</a>";
			} else {
				$parts[] = " <a href='".ci_url('badges/unlock/' . $this->id)."' class='notice'>Unlock</a>";
			}
			$parts[] = " <a href='".ci_url('badges/hide/' . $this->id)."' class='notice'>Hide</a>";
		} else {
			$parts[] = " <a href='".ci_url('badges/unhide/' . $this->id)."' class='notice'>Unhide</a>";
		}

		$parts[] = " <a href='".ci_url('badges/reset_qrs/' . $this->id)."' class='notice'>Reset QRs</a>";
		
		$parts[] = ' <a href="javascript:deleteObject(\''.$type.'\', '.$this->id.')" class="warn">Reset</a>';
		$parts[] = "</div>";
			
		return join("", $parts);
	}
	
	public function table_name($key) {
		$user = $this->r('user');
		$order = $this->r('order');
		
		if (!$user) {
			$user = R::dispense('user');
		}
		
		if (!$order) {
			$order = R::dispense('order');
			$rate = R::dispense('rate');
		} else {
			$rate = $order->r('rate');
		}
		
		if ($user->last_updated > $this->last_updated) {
			print "<div class='warn' style='margin-bottom:5px'>WARNING: This user has updated their profile since the badge was created.</div>";
		}
		
		$colors = array(
			'red' => '#b30239',
			'blue' => '#00afe9',
			'gold' => '#b49762',
			'black' => '#000',
			'purple' => '#ac63a3'
		);
		$color = $colors[$this->color];
		
		$parts = array();
		$parts[] = "<div>";
		
		$parts[] = "<div style='float:left;background-color:{$color};padding:0 12px;margin-right:10px'>";
		$parts[] = "<img src='" . $this->r('photo')->thumbnail_url('square') . "'>";
		$parts[] = "</div>";
		
		// content
		$parts[] = "<div style='float:left;width:450px'>";
		
		// name
		$parts[] = "<div style='margin-bottom:5px'>";
		$parts[] = "<a href='?page=edit_user&id=".$user->id."' style='font-size:16px'>" . $this->full_name() . "</a>";
		$parts[] = " ({$rate->name})";
		if ($this->returner) {
			$parts[] = "<span class='success' style='margin-left:10px'>Returner</span>";
		}
		$parts[] = "</div>";
		
		// table
		$style = '';
		if (strlen($this->title) > self::TITLE_LENGTH) {
			$style = 'background-color: #f99';
		}
		else if (empty($this->title)) {
			$style = 'background-color: #ff9';
		}
		
		$parts[] = "<table class='compact-table' style='width:450px'>";
		$parts[] = "<tr><td style='width:100px;{$style}'>Affiliation</td><td style='{$style}'>" . $this->title . "</td></tr>";
		$parts[] = "<tr><td>Twitter</td><td>" . $this->normalized_twitter() . "</td></tr>";

		if (!empty($this->portrait_time)) {
			$parts[] = "<tr><td>Portrait</td><td>" . date("l, n/j g:i a", $this->portrait_time) . "</td></tr>";
		}
		$parts[] = "</table>";
		
		// speaker information
		if ($this->is_speaker) {
			$parts[] = "<div style='font-weight:bold;margin-top:10px'>Speaker Info</div>";

			for ($i = 1; $i <= 4; $i++) {
				$num = '';
				if ($i > 1) $num = $i;
				$title = $this->{"talk{$num}_title"};
				if (empty($title)) continue;

				$parts[] = "<table class='compact-table' style='width:450px'>";
				$parts[] = "<tr><td style='width:100px'>Session Type</td><td>" . $this->{"session{$num}_type"} . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Location</td><td>" . $this->{"session{$num}_location"} . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Talk Title</td><td>" . $this->{"talk{$num}_title"} . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Session Start</td><td>" . date(self::TIME_FMT, $this->{"session{$num}_start"}) . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Session End</td><td>" . date(self::TIME_FMT, $this->{"session{$num}_end"}) . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Talk Start</td><td>" . date(self::TIME_FMT, $this->{"talk{$num}_start"}) . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Talk End</td><td>" . date(self::TIME_FMT, $this->{"talk{$num}_end"}) . "</td></tr>";
				$parts[] = "<tr><td style='width:100px'>Instructions</td><td>" . $this->{"instructions{$num}"} . "</td></tr>";
				$parts[] = "</table>";
			}
		}
		
		if ($this->extra_bus_pass || $this->extra_reception_ticket || $this->has_portrait || $this->small_font || $this->multi_speaker) {
			$parts[] = "<div style='margin-top:5px;color:#060;padding-left:10px'>";
			if ($this->extra_bus_pass) {
				$parts[] = "<div>Extra Bus Pass</div>";
			}
			if ($this->extra_reception_ticket) {
				$parts[] = "<div>Extra Reception Ticket</div>";
			}
			if ($this->small_font) {
				$parts[] = "<div>Small Font</div>";
			}
			if ($this->multi_speaker) {
				$parts[] = "<div>Multiple Speaker</div>";
			}
			$parts[] = "</div>";
		}
		
		
		$parts[] = "</div>";
		
		if ($this->is_locked()) {
			$parts[] = "<div style='float:left;margin-left:10px'><img style='width:32px' src='".media_url('images/lock.png')."'></div>";
		}
		
		$parts[] = "<div class='spacer'></div>";
		$parts[] = "</div>";
		return $this->controls() . join("", $parts);
	}
}