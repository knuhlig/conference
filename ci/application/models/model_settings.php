<?php

define('CM_PREFIX', 'cm_');

class Model_Settings extends Base_Model {

	public function dispense() {
		// hack: make editor say "Edit", not "New"
		$this->id = 1;

		foreach ($this->editor_fields() as $key => $info) {
			$this->$key = get_option(CM_PREFIX . $key, '');
		}
	}

	public function short_label() {
		return "Conference Settings";
	}

	public function save() {
		foreach ($this->editor_fields() as $key => $info) {
			update_option(CM_PREFIX . $key, $this->$key);
		}
	}

	public function editor_fields() {
		return array(
			'path' => array(
				'type' => 'text',
				'label' => 'Site Prefix',
				'description' => 'URL fragment that reroutes request to the Conference plugin'
			),
			'contact_email' => array(
				'type' => 'text'
			),
			'email_signature' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:150px'
			),
			'cybs_profile_id' => array(
				'label' => 'Profile ID',
				'type' => 'text'
			),
			'cybs_access_key' => array(
				'label' => 'Access Key',
				'type' => 'text',
				'description' => 'Note: For security, the Secret Key must be added by manually editing the file cybersource_helper.php.'
			),
			'registration_goal' => array(
				'type' => 'text',
				'style' => 'width:70px'
			),
			'paid_goal' => array(
				'type' => 'text',
				'style' => 'width:70px'
			),
			'livestream_embed' => array(
				'type' => 'textarea',
				'style' => 'width:400px;height:150px'
			),
			'submission_info' => array(
				'type' => 'rt',
				'label' => 'Submission Instructions',
				'style' => 'height: 200px'
			),
			'copyright_info' => array(
				'type' => 'rt',
				'label' => 'Copyright Information',
				'style' => 'height: 200px'
			),
			'submissions_closed' => array(
				'type' => 'checkbox'
			)
		);
	}

	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Contact Information',
					'fields' => array('contact_email', 'email_signature')
				),
				array(
					'title' => 'Abstract Information',
					'fields' => array('submissions_closed', 'submission_info', 'copyright_info')
				),
				array(
					'title' => 'Livestream Information',
					'fields' => array('livestream_embed')
				),
				array(
					'title' => 'Site Information',
					'fields' => array('path', 'registration_goal', 'paid_goal')
				),
				array(
					'title' => 'Cybersource Information',
					'fields' => array('cybs_profile_id', 'cybs_access_key')
				)
			)
		);
	}
}