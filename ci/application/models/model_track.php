<?php

class Model_Track extends Base_Model {

	public function editor_fields() {
		return array(
			'title' => array(
				'type' => 'text',
				'label' => 'Title'
			),
		);
	}


	public function editor_layout() {
		return array(
			'sections' => array(
				array(
					'title' => 'Track Information',
					'fields' => array(
						'title')
				),
			)
		);
	}

	public function short_label() {
		return $this->title;
	}

	public function table_title($key) {
		return $this->title . $this->controls();
	}
}