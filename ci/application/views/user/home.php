<?php $this->template->title('User Home'); ?>

<h4 style="margin-bottom:20px;margin-top:10px">
	Welcome, <?php echo $user->first_name ?>!
</h4>

<?php if ($user->is_reviewer): ?>
<h3>Abstract Reviews</h3>

<div><b>Pending Reviews (<?= sizeof($pending_reviews) ?>)</b></div>
<?php if (!empty($pending_reviews)): ?>
<div id="pending_reviews">
	<?php foreach ($pending_reviews as $review): 
		$submission = $review->submission;
	?>
	<div style="margin-bottom:10px;background-color:#f0f0f0;padding:3px 10px">
		<div style="float:right"><?= date(DATE_FMT_FULL, $submission->submission_date) ?></div>
		<div><?= $submission->track->title ?>/<?= $submission->category->title ?></div>
		<div style="font-size:1.2em"><?= $submission->title; ?></div>
		<div><a style="color:#900" href="<?= ci_url('submission/review/' . $submission->id) ?>">Review Abstract</a></div>
	</div>
	<?php endforeach; ?>
</div>
<?php else: ?>
<div>None</div>
<?php endif; ?>

<?php if (!empty($completed_reviews)): ?>
<div style="margin-top:20px"><a href="javascript:void(0)" onclick="$('#completed_reviews').toggle()">Completed Reviews (<?= sizeof($completed_reviews) ?>)</a></div>
<div id="completed_reviews" style="display:none">
	<?php foreach ($completed_reviews as $review): 
		$submission = $review->submission;
	?>
	<div style="margin-bottom:10px;background-color:#f0f0f0;padding:3px 10px">
		<div style="float:right"><?= date(DATE_FMT_FULL, $submission->submission_date) ?></div>
		<div><?= $submission->track->title ?>/<?= $submission->category->title ?></div>
		<div style="font-size:1.2em"><?= $submission->title; ?></div>
		<div><a style="color:#900" href="<?= ci_url('submission/review/' . $submission->id) ?>">Review Abstract</a></div>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>


<?php endif; ?>

<!--
<h3>Medicine X Livestream</h3>
<div style="margin:5px 0 20px 10px">
	<a href="<?= ci_url('conference/livestream') ?>" class='button button-primary'>View Livestream</a>
</div>
-->


<?php if (!empty($submissions)): ?>
<h3 class="heading">Submitted Abstracts</h3>
<div>
	<?php foreach ($submissions as $submission): ?>
	<div style="margin-bottom:10px;background-color:#f0f0f0;padding:3px 10px">
		<!-- status -->
		<?php if (!empty($submission->submission_date)): ?>
			<div style="float:right"><?= date(DATE_FMT_FULL, $submission->submission_date) ?></div>
		<?php else: ?>
			<div style="float:right">In Progress</div>
		<?php endif; ?>

		<!-- info -->
		<div><?= $submission->track->title ?>/<?= $submission->category->title ?></div>
		<div style="font-size:1.2em"><?= $submission->title; ?></div>
		
		<!-- options -->
		<?php if (empty($submission->submission_date)): ?>
			<div><a href="<?= $submission->edit_url() ?>">Edit/Submit Abstract</a></div>
		<?php else: ?>
			<div><a style="color:#900" href="<?= ci_url('submission/review/' . $submission->id) ?>">Review Abstract</a></div>
		<?php endif; ?>
		
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>


<h3 class="heading">Abstract Submission</h3>
<ul>
	<li><a href="<?php echo ci_url('submission/submit') ?>">Click here to submit an abstract</a></li>
</ul>


<!--
<h3>Conference Registration</h3>
<?php if (!empty($orders)): ?>
    <div>You are registered to attend Medicine X 2015! </div>
    <?php foreach ($orders as $order): ?>
    	<div style="margin-left:20px">
    		<div><b><?php echo $order->rate->short_label() ?></b></div>
    		<ul>
		        <li><a href="<?= $order->receipt_url() ?>">Print Receipt</a></li>
		        <li><a href="<?= ci_url('register/update/' . $order->id) ?>">Update Registration</a></li>
		    </ul>
    	</div>
    <?php endforeach; ?>
<?php else: ?>
    <div>Register to attend Medicine X 2015! Click the link below to see registration options:</div>
    <ul>
        <li><a href="http://medicinex.stanford.edu/registration-rates-2015/">Register to attend Medicine X 2015</a></li>
    </ul>
<?php endif; ?>
-->

<h3 class="heading">Account Details</h3>
<ul>
	<li><a href="<?php echo ci_url('user/edit_profile') ?>">Edit Profile</a></li>
	<li><a href="<?php echo ci_url('user/new_password') ?>">Change Password</a></li>
	<li><a href="<?php echo ci_url('user/logout') ?>">Logout</a></li>
</ul>