<?php $this->template->title('Login');

$this->load->view('common/messages');

?>

<h3>Login to Medicine X</h3>
<form method="POST" action="">
<div>
	<div class="login">
		<div>
			<label class="vert-label" for="email">Email</label>
			<input type="text" id="email" name="email"/>
		</div>
		<div>
			<label class="vert-label" for="password">Password</label>
			<input type="password" id="password" name="password"/>
		</div>

		<div class="login-button">
			<input type="submit" class="button button-primary" value="Log In"/>
			<div style="margin-top:3px">
				<input type="checkbox" name="remember" checked="checked"/>
				<label for="remember" class="remember">Remember me</label>
			</div>
		</div>
		<div class="login-options">
			<div><a href="<?php echo ci_url('user/lost_password') ?>">Lost password?</a></div>
			<div><a href="<?php echo ci_url('user/register') ?>">Create Account</a></div>
		</div>
	</div>
</div>
</form>
