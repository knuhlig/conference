<?php
$this->template->title('Reset Password');

$this->load->view('common/messages');
?>

<?php if (!empty($email_sent)): ?>
	<div class="success" style="padding:10px">An email has been sent to <b><?php echo $email ?></b> with instructions for resetting your password.</div>
<?php else: ?>
	<h3 style="margin: 20px 0">
		To reset your password, please enter the email address associated with your account.
	</h3>
	<form method="post" action="">
	<div class="reset">
		<label for="email" class="vert-label">Email Address</label>
		<input type="text" id="email" name="email"/>
	</div>
	<div style="margin-top:10px">
		<input type="submit" class="button button-primary" value="Reset Password"/>
	</div>
	</form>
<?php endif; ?>