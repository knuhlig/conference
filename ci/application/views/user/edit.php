<?php if (!$this->auth->logged_in): ?>
    <a href="<?= ci_url('user/login', array('r' => ci_url('oops'))) ?>">Already have an account? Login here</a>
<?php endif; ?>

<?php
$this->template->title('Create Account');

$form = new UForm();
$form->id = 1;
$form->config['ajax'] = true;
$form->config['url'] = ci_url('user/api_update_user');

$form->layout = array(
    array(
        'type' => 'group',
        'label' => 'Please fill in the fields below',
    ),
    array(
        'type' => 'hidden',
        'slug' => 'redirect'
    ),
    array(
        'type' => 'hidden',
        'slug' => 'id'
    ),
    array(
        'type' => 'text',
        'slug' => 'first_name',
        'required' => true
    ),
    array(
        'type' => 'text',
        'slug' => 'last_name',
        'required' => true
    ),
    array(
        'type' => 'text',
        'slug' => 'email',
        'required' => true
    ),
    array(
        'type' => 'button',
        'items' => array(
            array(
                'label' => 'Save Changes',
                'action' => 'submit'
            )
        )
    )
);

$form->data = $data;

$arr = array();
$arr['form'] = $form;

$this->load->view('uform/display', $arr);
?>