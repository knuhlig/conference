<?php
$this->template->title('User Reset');
?>

<form method="POST" action="">
<h3>Reset Password</h3>

Please enter one of the fields below:

<div class="form">
    <div class="form-label">Username</div>
    <div class="form-value"><input type="text" name="username"/></div>
</div>

<h3>OR</h3>
<div class="form">
    <div class="form-label">Email Address</div>
    <input type="text" name="email">
</div>

<input type="submit" name="submit" value="Reset Password"/>

</form>