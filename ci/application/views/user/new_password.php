<?php
$this->template->title('Update Password');

$this->load->view('common/messages');
?>

<?php if (!empty($reset_success)): ?>
	<div class="success" style="padding:10px">
		Your password has been updated successfully. <a href="<?php echo ci_url('user/home') ?>">User Home</a>
	</div>
<?php else: ?>
	<h3 style="margin:10px 0">Please enter a new password below.</h3>
	<form method="POST" action="">
	<div>
		<div>
			<label for="password" class="vert-label">New Password</label>
			<input type="password" name="password" id="password"/>
		</div>
		<div>
			<label for="password_confirm" class="vert-label">Confirm Password</label>
			<input type="password" name="password_confirm" id="password_confirm"/>
		</div>
	</div>
	<div style="margin-top:10px">
		<input type="submit" class="button button-primary" value="Update Password"/>
	</div>
	</form>
<?php endif; ?>