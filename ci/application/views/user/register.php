<?php
$this->template->title('Create an Account');

$this->load->view('common/messages');
?>

<h5 style="margin:20px 0">
	Fill out the fields below to create an account with Medicine X.<br/>
</h5>
<div>
	Already have an account? <a href="<?= ci_url("user/login", array('r' => property($_GET, 'r'))) ?>">Login here</a>
</div>
<?php
$this->rb_form->inline_editor($user, $fields, $layout);

?>