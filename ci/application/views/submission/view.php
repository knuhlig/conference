<?php
$this->template->title($submission->title);

$this->load->view('common/messages');
?>

<div class="metadata">
	<div class="submission-track"><?= $submission->track->title ?></div>
	<div class="submission-category"><?= $submission->category->title ?></div>
</div>

<h3><?= $submission->title; ?></h3>
<table class="authors">
	<?php foreach ($submission->ownAuthor as $author): ?>
		<tr class="author">
			<td style="white-space:nowrap" class="<?= $author->is_primary ? "primary" : "" ?>"><?= $author->first_name ?> <?= $author->last_name ?></td>
			<td style="width:100%"><a href="mailto:<?= $author->email?>"><?= $author->email ?></a></td>
		</tr>
	<?php endforeach; ?>
</table>

<h4 class="heading">Abstract</h4>
<div class="abstract">
	<?= $submission->description; ?>
</div>