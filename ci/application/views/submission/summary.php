
<div class="postbox closed" id="info_<?= $submission->id ?>">
	<div class="handlediv" title="Click to toggle"><br></div>
	<h3 class='hndle'>
		<div><span style="font-weight:normal"><?= $submission->track->title ?>/<?= $submission->category->title ?></span></div>
		<span><?= $submission->title; ?></span>
	</h3>
	<div class='inside'>
		<div class="submission-summary">
			<table class="authors">
				<?php foreach ($submission->ownAuthor as $author): ?>
					<tr class="author">
						<td style="white-space:nowrap" class="<?= $author->is_primary ? "primary" : "" ?>"><?= $author->first_name ?> <?= $author->last_name ?></td>
						<td style="width:100%"><a href="mailto:<?= $author->email?>"><?= $author->email ?></a></td>
					</tr>
				<?php endforeach; ?>
			</table>
			<div class="abstract" style="padding-top:5px">
				<?= $submission->trim_abstract() ?>
			</div>
			<div>
				<?php foreach ($submission->ownAttachment as $attachment): ?>
						<div>
							<?php
							$file = $attachment->r('file');
							if (!empty($file)) print $file->get_embed_html($attachment->title)
							?>
						</div>
				<?php endforeach; ?>
			</div>
		</div>

		<div class="submission-options">
			<a class="button" href="javascript:void(0)" onclick="$('#reviewers_<?= $submission->id ?>').toggle()">Assign Reviewers</a>
			<a class="button" href="?page=edit_submission&id=<?= $submission->id ?>">Make Decision</a>
			<a class="button" href="javascript:markDuplicate(<?= $submission->id ?>)">Mark as Duplicate</a>

			<div style="float:right">
				<a href='?page=edit_submission&id=<?= $submission->id ?>'>Edit</a>
				<a href='<?= $submission->url() ?>' target="blank">Review</a>
				<a href="javascript:deleteObject('submission', '<?= $submission->id ?>')" class="warn">Delete</a>
			</div>

			<div style="display:none" id="reviewers_<?= $submission->id ?>">
				<form method="POST" action="">
				<input type="hidden" name="submission_id" value="<?= $submission->id ?>"/>
				<div class="reviewer-options">
					<?php if (empty($reviewers)): ?>
						<div>No Reviewers Available</div>
					<?php endif; ?>
					<?php foreach ($reviewers as $reviewer): ?>
						<div><input type="checkbox" name="reviewers[]" id="reviewer_<?= $reviewer->id ?>_<?= $submission->id ?>" value="<?= $reviewer->id ?>"/> <label for="reviewer_<?= $reviewer->id ?>_<?= $submission->id ?>"><?= $reviewer->full_name() ?></label></div>
					<?php endforeach; ?>
				</div>
				<input type="submit" class="button button-primary" value="Save and Assign"/>
				</form>
			</div>
		</div>
	</div>
</div>