<?php
$this->template->title('Review Submission');

$this->load->view('common/messages');
?>


<div class="metadata">
	<div class="submission-track"><?= $submission->track->title ?></div>
	<div class="submission-category"><?= $submission->category->title ?></div>
</div>

<h3><?= $submission->title; ?></h3>
<table class="authors">
	<?php foreach ($submission->ownAuthor as $author): ?>
		<tr class="author">
			<td style="white-space:nowrap" class="<?= $author->is_primary ? "primary" : "" ?>"><?= $author->first_name ?> <?= $author->last_name ?></td>
			<td style="width:100%"><a href="mailto:<?= $author->email?>"><?= $author->email ?></a></td>
		</tr>
	<?php endforeach; ?>
</table>

<h4 class="heading">Abstract</h4>
<div class="abstract">
	<?= $submission->description; ?>
</div>

<h4 class="heading">Supplementary Files</h4>
<?php if (!empty($attachments)): ?>
	<?php foreach ($attachments as $attachment): 
		$file = $attachment->r('file');
		if (empty($file)) continue;
	?>
		<div class="attachment">
			<div>
				<?= $file->get_embed_html($attachment->title) ?>
			</div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<div>No files</div>
<?php endif; ?>

<h4 class="heading">Author Comments</h4>
<div>
	<?php 
	$author_comments = $submission->comments;
	if (empty($author_comments)) $author_comments = "No additional information.";
	?>
	<?= $author_comments ?>
</div>


<h4 class="heading">Review Comments</h4>
<div>
	<?php 
	$count = 0;
	foreach ($comments as $comment): 
		$user = $comment->r('user'); 
		if (!$is_reviewer && !$comment->is_public) {
			continue;
		}
		$count++;
	?>
		<div class="comment">
			<div class="thumb"><img src="<?= $user->thumbnail_url('tiny') ?>"/></div>
			<div class="date"><?= date(DATE_FMT_LETTER, $comment->created) ?> at <?= date(DATE_FMT_TIME, $comment->created) ?></div>
			<div class="content">
				<div class="user">
					<?= $user->full_name() ?>
				</div>
				<div class="comment-body">
					<?= $comment->comment ?>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php if ($count == 0): ?>
		<div>No Comments.</div>
	<?php endif; ?>
</div>

<h4 class="heading"><a href="javascript:void(0)" onclick="$('#comment_box').toggle()">Add a Comment</a></h4>
<div id="comment_box" style="display:none">
<?php 
$dummy = R::dispense('comment');
$fields = $dummy->editor_fields();

if ($is_reviewer) {
	$layout_fields = array('comment', 'is_public');
} else {
	$layout_fields = array('comment');
}
$layout = array(
	'sections' => array(
		array(
			'fields' => $layout_fields
		)
	),
	'buttons' => array(
		'comment' => 'Add Comment'
	),
);
$this->rb_form->display($fields, $layout);
?>
</div>


<?php if ($is_reviewer): ?>
<h4 class="heading">Editorial Review</h4>
<div class="review" id="review">
	<?php
	$fields = $review->editor_fields();
	$this->rb_form->inline_editor($review, $fields, $review_layout);
	?>
	</div>
<?php endif; ?>