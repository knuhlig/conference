<?php
$this->template->title('Submit an Abstract');

$this->load->view('common/messages');
?>

<h3>Medicine X 2015</h3>
<h4>Welcome, <?= $this->auth->user->first_name ?>! To begin, please select a track and category below.</h4>
<?php
$this->rb_form->inline_editor($s, $fields, $layout);

?>