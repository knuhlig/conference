<?php
$this->template->title('Review Submission');

$this->load->view('common/messages');
?>

<?php if ($author->status != 'confirmed'): ?>
	<form method="POST" action="" style="margin-bottom: 20px;background-color:#f0f0f0;padding:10px">
	<div>
		This confirmation page is for <b><?= $author->full_name() ?> (<?= $author->email ?>)</b>.

		<?php if ($this->auth->user && $this->auth->user->email != $author->email): ?>
		<div class="warn">It appears you may be logged in as a different user. You are logged in as <?= $this->auth->user->full_name() ?> (<?= $this->auth->user->email ?>).</div>
		<div>Please ensure you are logged in as <a href="<?= ci_url('user/logout') ?>"><?= $author->full_name() ?></a> and try again. If these accounts refer to the same person, please continue.</div>
		<?php endif; ?>

		<?php if (!$this->auth->user): ?>
		<div class="warn">If you are planning to attend Medicine X, we strongly recommend you <a href="<?= ci_url("user/register") ?>">Create an Account</a> before continuing.</div>
		<?php endif; ?>

		<div style="margin-top:10px">Please review the submission below. To confirm, please click the button below.</div>
	</div>
	<div>
		<input type="hidden" name="response" value="confirmed"/>
		<input type="submit" class="button button-primary" value="Confirm"/>
	</div>
	</form>
<?php else: ?>
	<div style="margin-bottom: 20px;padding:3px 7px" class="success">
		Thank you! Your response has been recorded. No further action is needed at this time.
	</div>
<?php endif; ?>



<div class="metadata">
	<div class="submission-track"><?= $submission->track->title ?></div>
	<div class="submission-category"><?= $submission->category->title ?></div>
</div>

<h3><?= $submission->title; ?></h3>
<table class="authors">
	<?php foreach ($submission->ownAuthor as $author): ?>
		<tr class="author">
			<td style="white-space:nowrap" class="<?= $author->is_primary ? "primary" : "" ?>"><?= $author->first_name ?> <?= $author->last_name ?></td>
			<td style="width:100%"><a href="mailto:<?= $author->email?>"><?= $author->email ?></a></td>
		</tr>
	<?php endforeach; ?>
</table>

<h4 class="heading">Abstract</h4>
<div class="abstract">
	<?= $submission->description; ?>
</div>

<h4 class="heading">Supplementary Files</h4>
<?php if (!empty($attachments)): ?>
	<?php foreach ($attachments as $attachment): 
		$file = $attachment->r('file');
		if (empty($file)) continue;
	?>
		<div class="attachment">
			<div>
				<?= $file->get_embed_html($attachment->title) ?>
			</div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<div>No files</div>
<?php endif; ?>
