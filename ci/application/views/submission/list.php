<script>
var toggleType = function(type) {
	var time = 1000;
	
	$('.submission-groups li').removeClass('selected');
	$('#group_' + type).addClass('selected');
	
	if (type == 'all') {
		$('.submission-group').fadeIn(time);
	} else {
		$('.submission-group').hide();
		$('#' + type).fadeIn(time);
	}
};
</script>
<ul class="submission-groups">
	<li class="selected" id="group_all"><a href="javascript:void()" onclick="toggleType('all')">All</li>
<?php foreach ($tracks as $track => $submissions): ?>
	<?php
	$id = property(explode(" ", $track), 0);
	?>
	<li id="group_<?= $id ?>"><a href="javascript:void()" onclick="toggleType('<?= $id ?>')"><?= $track ?> Track</a></li>
<?php endforeach; ?>
</ul>

<?php foreach ($tracks as $track => $submissions): ?>
	<?php
	$id = property(explode(" ", $track), 0);
	?>
	<div id="<?= $id ?>" class="submission-group">
		<h3><?= $track ?> Track</h3>
		<?php foreach ($submissions as $submission): ?>
			<div style="margin-bottom:10px;background-color:#f0f0f0;padding:10px">
				<div>
					<?= $submission->category->title ?>
				</div>
				<div style="font-size:1.2em">
					<a target="_blank" href="<?= $submission->public_url() ?>"><?= $submission->title; ?></a>
				</div>
				<div>
					<?php
					$author_names = array();
					foreach ($submission->ownAuthor as $author) {
						$author_names[] = $author->full_name();
					}
					print join(", ", $author_names);
					?>
				</div>
				<div>
					<div style="padding:10px;background-color:white;border:1px solid #c1c1c1">
						<?php
						$text = preg_replace('#</li>#', '. ', $submission->description);
						$text = preg_replace('#<ul>#', ': ', $text);
						$text = preg_replace('#<[^>]+>#', ' ', $text);
						$words = explode(". ", $text);
						if (sizeof($words) > 4) {
							$words = array_slice($words, 0, 4);
							$words[] = "<a target='_blank' href='".$submission->public_url()."'>&#187; Read More</a>";
						}
						print join(". ", $words);
						?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endforeach; ?>