<?php
$this->template->title('Abstract Reviewers');
?>

<h3>Add Reviewer</h3>
<?php 
print "<div class='admin-box'>";
$this->rb_form->display($fields, $layout);
print "</div>";
?>


<h3>Current Reviewers</h3>


<div class="metabox-holder meta-box-sortables">
<?php foreach ($reviewers as $user): 
	$reviews = $user->ownReview;
?>
	<div class="postbox closed" id="info_<?= $user->id ?>">
		<div class="handlediv" title="Click to toggle"><br></div>
		<h3 class='hndle' style="min-height:50px">
			<div style="float:left;margin-right:10px"><img src="<?= $user->thumbnail_url('tiny') ?>"/></div>
			<span><?= $user->full_name(); ?></span> <span style="font-weight:normal;font-size:0.9em">&lt;<?= $user->email ?>&gt;</span>
			<div style="font-weight:normal;margin-top:3px">
				<?= R::count('review', 'completed and user_id=?', array($user->id)) ?> completed,
				<?= R::count('review', 'not coalesce(completed, 0) and user_id=?', array($user->id)) ?> pending
			</div>
		</h3>
		<div class='inside'>
			<?php foreach ($reviews as $review): 
				$submission = $review->submission;
				if (!$submission) continue;
				$bgColor = $review->completed ? "#f0f0f0" : "#ffe0e0";
			?>
			<div style="margin-bottom:10px;background-color:<?= $bgColor ?>;padding:3px 10px">
				<div style="float:right"><?= date(DATE_FMT_FULL, $submission->submission_date) ?></div>
				<div><?= $submission->track->title ?>/<?= $submission->category->title ?></div>
				<div style="font-size:1.2em"><?= $submission->title; ?></div>
				<div style="float:right"><a style="color:#900" href="?page=edit_submission&id=<?= $submission->id ?>">View/Edit</a></div>
				<div><?= $submission->author_list() ?></div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endforeach; ?>
</div>