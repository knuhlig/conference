<?php
$this->template->title('Thank you for your submission!');
?>

<p>
Thank you for submitting an abstract to Medicine X 2015!
</p>


<p>
<b>Important:</b> emails have been sent to additional authors to confirm their participation. 
Your submission will be ready for review only after all authors have responded.
</p>

<p>
If you have any further questions, please don't hesitate to contact us:
<div style="margin-left:30px;margin-top:0px;line-height:18px;font-size:15px;color:black">
	Stanford AIM Lab<br/>
	(650) 723-4671<br/>
	<a href="mailto:aimlabstanford@gmail.com" style="color:#369">aimlabstanford@gmail.com</a><br/>
	<br/>
	300 Pasteur Drive<br/>
	Grant S268C<br/>
	Stanford, CA 94305<br/>
</div>
</p>
<p>
	<a href="<?= ci_url('user/home') ?>">&#187; User Home</a><br/>
	<a href="<?= ci_url('submission/submit') ?>">&#187; Submit another abstract</a>
</p>