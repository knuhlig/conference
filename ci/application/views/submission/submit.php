<?php
$this->template->title('Submit an Abstract');

$this->load->view('common/messages');
?>


<h3>Medicine X 2015</h3>
<h4><b><?= $s->track->short_label() ?></b> / <?= $s->category->short_label() ?></h4>
<h5><?= $instructions ?></h5>

<?php
$this->rb_form->inline_editor($s, $fields, $layout);
?>