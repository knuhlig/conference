<form method="POST" action="" id="common_form">
	<input type="hidden" name="form_action" id="form_action"/>
</form>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Abstracts Overview</h2>


    <?php $this->load->view('common/messages'); ?>

    <h3>Summary</h3>
    <div>
	    <table style="float:left" class="data-table">
	    	<tr>
	    		<th colspan="2">Summary</th>
	    	</tr>
	    	<?php foreach ($stats as $label => $value):
	    		if (empty($value)) continue;
	    	?>
				<tr>
					<td><?= $label ?></td>
					<td><?= $value ?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
	
    <div>
	    <table style="float:left;margin-left:15px" class="data-table">
	    	<tr>
	    		<th colspan="2">Tracks</th>
	    	</tr>
	    	<?php 
	    	foreach ($tracks as $id => $list) {
	    		$track = R::load('track', $id);
	    		print "<tr><td>{$track->title}</td><td>".sizeof($list)."</td></tr>";
	    	}
	    	?>
		</table>
	</div>
	<div>
	    <table style="float:left;margin-left:15px" class="data-table">
	    	<tr>
	    		<th colspan="2">Categories</th>
	    	</tr>
	    	<?php 
	    	foreach ($categories as $id => $list) {
	    		$category = R::load('category', $id);
	    		print "<tr><td>{$category->title}</td><td>".sizeof($list)."</td></tr>";
	    	}
	    	?>
		</table>
	</div>
	
	<div style="clear:both"></div>

	<div class="metabox-holder meta-box-sortables">

	<div style="clear:both"></div>
	<h3>Unassigned</h3>
		<?php
		foreach ($unassigned as $submission) {
			$this->load->view('submission/summary', array(
				'submission' => $submission,
				'reviewers' => $reviewers
			));
		}
		?>
    </div>
	<div style="clear:both"></div>

	<!--<h3>Awaiting Final Decision</h3>
	<?php
	if (!empty($ready)) {
		foreach ($ready as $submission) {
			$this->load->view('submission/summary', array(
				'submission' => $submission,
				'reviewers' => $reviewers
			));
		}
	} else {
		print 'None';
	}
	?>
	-->
</div>
