<?php

$page_layout = 'full';

get_header(); ?>
<div id="theme-page">
	<div id="mk-page-id-0" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout  mk-grid vc_row-fluid row-fluid">
		<div class="theme-content">
			<div class="wpcm-wrap"><?= $content ?></div>
		</div>
	<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
	<div class="clearboth"></div>
	</div>
	<div class="clearboth"></div>
</div>
<?php do_action( 'footer_twitter' ); ?>
<?php get_footer(); ?>
