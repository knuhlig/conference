<?php

if (!empty($keynotes)) {
	foreach ($keynotes as $event) {
		$speaker = $event->first_speaker();
		$photo = $event->fetchAs('upload')->photo;

		if ($photo) {
			$args = array();
			$args['image'] = $photo->url();
			$parts = array();

			if ($speaker) {
				$parts[] = '<h3 style="margin-top:20px"><a href="'.$speaker->url().'/">'.$speaker->full_name().'</a></h3>';
				$parts[] = '<p style="margin: 10px 0"><span style="">'.$speaker->title.'</span></p>';
				$parts[] = '<p style="margin: 10px 0">' . $speaker->bio_snippet . '</p>';
			}
			$content = join("", $parts);


			echo "<div style='margin-bottom:10px'>";
			echo wpci_shortcode('overlay', $args, $content);
			echo "</div>";
		}


	}

	print "<div class='spacer'></div>";
}


if (!empty($core)) {
	$idx = 0;
	foreach ($core as $speaker) {
		$photo = R::load('upload', $speaker->photo_id);

		$args = array();
		$args['image'] = $photo->thumbnail_url();

		$parts = array();
		$parts[] = '<p style="font-size:20px;margin:10px 0 5px 0"><a href="'.$speaker->url().'">'.$speaker->full_name().'</a></p>';
		$parts[] = '<p style="margin: 0 0 10px 0;line-height:1.1em"><span style="font-style:italic">'.$speaker->title.'</span></p>';
		$parts[] = '<p style="margin: 0;line-height:1.1em">' . $speaker->bio_snippet . '</p>';

		$content = join("", $parts);

		$style = 'float:left;margin-top:10px';
		if ($idx > 0) {
			$style .= ';margin-left:10px';
		}
		echo "<div style='{$style}'>";
		echo wpci_shortcode('overlay', $args, $content);
		echo "</div>";

		$idx++;
		if ($idx % 4 == 0) {
			$idx = 0;
		}
	}

	print "<div class='spacer'></div>";
}

if (!empty($masterclass)) {
	foreach ($masterclass as $event) {
		$speakers = $event->associated('speaker');
		$photo = $event->fetchAs('upload')->photo;

		$args = array();
		if ($photo) $args['image'] = $photo->url();

		$parts = array();
		if (!empty($speakers)) {
			$names = '';
			$titles = '';
			$bios = '';
			foreach ($speakers as $speaker) {
				if (!empty($names)) {
					$names .= ', ';
					$titles .= '; ';
					$bios .= '<br/>';
				}
				$names .= $speaker->full_name();
				$titles .= $speaker->title;
				$bios .= $speaker->bio_snippet;
			}
			$parts[] = '<h3 style="margin-top:20px"><a href="'.$event->url().'/">'.$names.'</a></h3>';
			$parts[] = '<p style="margin: 10px 0"><span style="">'.$titles.'</span></p>';
			$parts[] = '<p style="margin: 10px 0">' . $bios . '</p>';
		}


		$content = join("", $parts);

		echo "<div style='margin-bottom:10px'>";
		echo wpci_shortcode('overlay', $args, $content);
		echo "</div>";
	}

	print "<div class='spacer'></div>";
}

?>