<?php 
$this->template->title('Livestream Registration');
?>

<div style="font-size:18px;margin-top:10px">
Click below to register for access to the Medicine X 2015 Livestream.
</div>
<div style="padding-left:20px;margin-top:10px">
	<a href="http://medicinex.stanford.edu/conf/register?key=HiatrXlccD">
		<img src="<?= media_url('images/register_button.jpg') ?>"/>
	</a>
</div>


<div style="font-size:14px;margin-top:30px">
Already registered? Click below to login and watch the conference live.
</div>
<div style="padding-left:34px;margin-top:10px">
	<input type="button" class="button button-primary" value="Login Now" onclick="parent.location='<?= ci_url('user/login', array('r' => ci_url('conference/livestream'))) ?>'"/>
</div>



