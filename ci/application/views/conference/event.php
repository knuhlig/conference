<?php
switch ($event->type) {
	case 'masterclass':
		$this->template->title('A master class with ' . $event->speaker_list(false));
		break;
	case 'other':
		$this->template->title('Event Information');
	default:
		$this->template->title('Talk Information');
		break;
}

?>


<div class="entry-content">
	<?php if (!empty($photo)): ?>
		<div>
			<img src="<?php echo $photo->url() ?>"/>
		</div>
	<?php endif; ?>
	<?php if ($event->type == 'masterclass'): ?>
	<div style="padding:10px">
		<p>
			I'm proud to announce a Stanford Medicine X Master Class with <?php echo $event->speaker_list(true); ?> on <?php echo $event->title ?>. The Master Class will be offered as part of the 2013 Stanford Medicine X conference, held on the campus of Stanford University September 27-29, 2013.
		</p>
		<p><strong>About the Stanford Medicine X Master Class Program</strong></p>
		<p>
			The Stanford Medicine X Master Class program is designed to provide a once-in-a-lifetime learning experiences with thought leaders and innovators in emerging technologies and medicine who are masters in their field. The unique format of our Master Class program provides an intimate venue in a small classroom setting at the Stanford University School of Medicine. With attendance capped at 15 people, our Master Class program ensures learning is ignited by one-on-one conversations, dynamic group activities and discussion. Although no additional fee is charged, admission to a Master Class is made by application and students are selected by the Master faculty member.
		</p>
		<p><strong>About the Master Class</strong></p>
		<p>
			<?php echo stripslashes($event->description); ?>
		</p>
		<p><strong>About the Instructors</strong></p>
		<?php foreach ($speakers as $speaker): ?>
		<div><strong><?php echo $speaker->full_name() ?></strong></div>
		<p>
			<?php echo stripslashes($speaker->biography); ?>
		</p>
		<?php endforeach; ?>
	</div>

	<?php elseif ($event->type == 'poster' || $event->type == 'oral' || $event->type == 'panel' || $event->type == 'workshop' || $event->type == 'session'): ?>
		<h1><?php echo $event->title ?></h1>
		<div>
			<?= date('l, F j', $event->start_date()) ?>
		</div>
		<?php if (!empty($speakers)): ?>
			<div class="speaker-list">
			<?php foreach ($speakers as $speaker): ?>
				<div class="speaker">
					<div style="float:left;padding-right:10px">
						<img src="<?php echo $speaker->thumbnail_url('tiny') ?>"/>
					</div>
					<div style="font-size:16px"><a href="<?php echo $speaker->url() ?>"><?php echo $speaker->full_name() ?></a></div>
					<div><?php echo $speaker->title ?></div>
					<div style="clear:both"></div>
				</div>
			<?php endforeach; ?>
			</div>
		<?php endif; ?>
		
		<h3>Abstract</h3>
		<p><?php echo $event->description ?></p>
	<?php else: ?>
		<h1><?php echo $event->title ?></h1>
		<?php if (!empty($speakers)): ?>
			<div class="speaker-list">
			<?php foreach ($speakers as $speaker): ?>
				<div class="speaker">
					<div style="float:left;padding-right:10px">
						<img src="<?php echo $speaker->thumbnail_url('tiny') ?>"/>
					</div>
					<div style="font-size:16px"><a href="<?php echo $speaker->url() ?>"><?php echo $speaker->full_name() ?></a></div>
					<div><?php echo $speaker->title ?></div>
					<div style="clear:both"></div>
				</div>
			<?php endforeach; ?>
			</div>
		<?php endif; ?>
		
		<h3>Description</h3>
		<p><?php echo $event->description ?></p>
	<?php endif; ?>
</div>