<?php
$this->template->title('Speaker Profile: ' . $speaker->full_name());
?>

<div id="speaker">
	<!-- Left Column -->
	<div id="left-col">
		<!-- profile image -->
		<?php if (!empty($photo)): ?>
		<img src="<?php echo $photo->thumbnail_url('wide') ?>"/>
		<?php endif; ?>
		
		<!-- name -->
		<div id="speaker-name"><?php echo $speaker->full_name() ?></div>
		
		<!-- title -->
		<div id="speaker-title"><?php echo $speaker->title ?></div>
		
		<!-- social media -->
		<?php if (!empty($speaker->twitter_username)): ?>
		<div id="social">
			<a class="twitter-timeline" href="javascript:void()" data-screen-name="<?= $speaker->twitter_username ?>" data-widget-id="347411623817904132">Tweets by @<?= $speaker->twitter_username ?></a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>
		<?php endif; ?>
		
		<?php if (!empty($events)): ?>
			<h2 style="margin-top:20px;border-bottom:1px solid #c1c1c1">Scheduled Talks</h2>
			<?php foreach ($events as $event): ?>
			<div class="talk" style="margin:20px;box-shadow:0 0 10px 0 rgba(0, 0, 0, 0.4);padding:10px">
				<div><?php echo date(DATE_FMT_FULL, $event->event_date) . " &mdash; " . $event->location_name(); ?></div>
				<div><?php echo $event->type_name() ?></div>
				<?php if (!empty($event->title)): ?><div><?php echo $event->title ?></div><?php endif; ?>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<!-- Main Column -->
	<div id="main-col">
		<h3>About Me</h3>
		<div class="content"><?php echo stripslashes(!empty($speaker->biography) ? $speaker->biography : $speaker->bio_snippet) ?></div>
	
		<h3>At Medicine X 2015</h3>
		<?php foreach ($talks as $talk): ?>
		<div class="talk">
			<div class="talk-date"><?php echo date('l, F j g:i a', $talk->event_date) ?> (<?php echo $talk->location_name() ?>)</div>
			<div class="talk-title"><?php echo $talk->full_title() ?></div>
			<div class="talk-snippet"><?php echo $talk->event_snippet() ?></div>
		</div>
		<?php endforeach; ?>
	</div>
	
	<div class="spacer"></div>
	
</div>