<html>
<body>

<?php if ($paid): ?>
	<h1>Thank you!</h1>
<?php else: ?>
	<?php if ($summary): ?>
		<form method="POST" action="<?= cybsFormUrl() ?>">
		    <?php
		        $map = array();
		        $map['amount']= $donation->amount;
		        $map['currency']= 'usd';
		        $map['orderPage_transactionType']= "sale";
		        $map['orderNumber']= 'd' . $donation->id;
		        $map['orderPage_ignoreAVS'] = 'true';
		        InsertMapSignature($map);
		    ?>
		    <div>You will be directed to Cybersource, our secure payment processor.</div>
	    	<div><b>Please note that all donations are non-refundable.</b></div>
		    <input type="submit" value="Donate Now" class="button-primary"/>
		</form>
	<?php else: ?>
		<form method="POST" action="">
			<div>
				Amount: $<input type="text" name="amount"/>
			</div>
			<div>
				Twitter ID: <input type="text" name="twitter_id"/>
			</div>
			<div>
				<input type="submit" value="Continue"/>
			</div>
		</form>
	<?php endif; ?>
<?php endif; ?>
</body>
</html>