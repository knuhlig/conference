<?php $this->template->title('Medicine X Livestream'); ?>

<div style="background-color: black">
<?= $embed_code ?><div class="ticker-wrap">
	<ul id="ticker" class="ticker">
	</ul>
</div>
</div>

<script>
jQuery(function() {
	var el = $('#ticker');
	var wrap = el.parent();

	var speed = 75; // px per second
	
	var resetTicker = function() {
		var wrapWidth = wrap.outerWidth(true);
		el.css('left', wrapWidth);
	};

	var scrollTicker = function() {
		var contentWidth = parseInt(el.outerWidth(true));
		var scrollWidth = parseInt(el.css('left').replace("px", ""));
		var duration = (contentWidth + scrollWidth) * 1000 / speed;

		el.animate({
			'left': (-contentWidth) +'px'
		}, {
			'duration': duration,
			'easing': 'linear',
			'complete': function() {
				loadTicker();
			}
		});
	};

	var loadTicker = function() {
		$.get('<?= ci_url('conference/latest_tweets') ?>', function(data) {
			var arr = $.parseJSON(data);
			el.empty();

			for (var i = 0; i < arr.length; i++) {
				var tweet = arr[i];
				var text = tweet.text;
				el.append("<li>" + text + "</li>");
			}
			resetTicker();
			scrollTicker();
		});	
	};

	el.mouseover(function() {
		el.stop();
	});

	el.mouseout(function() {
		scrollTicker();
	});

	loadTicker();
});
</script>

<!--
<iframe src="<?= ci_url('conference/donate') ?>" style="width:500px;height:500px"/></iframe>
-->



