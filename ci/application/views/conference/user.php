<?php
$this->template->title($user->full_name());

$forward_headings = array(
	'scan' => "People I've met",
	'meet' => "People I'd love to meet",
	'friend' => "People I've added as friends"
);

$reverse_headings = array(
	'meet' => 'People who would love to meet me',
	'friend' => 'People who have added me as a friend'
);

if (!empty($user->speaker_id)) {
	$speaker = R::load('speaker', $user->speaker_id);
}

function user_thumb($u, $editable=false) {
	print "<a class='profile-link' href='".$u->url()."'>";
	print "<div class='medx-network-user'>";
	print "<div class='wrap'>";
	print "<div class='label'>".$u->full_name()."</div>";
	if ($editable) {
		print "<div class=\"remove\"><a href=\"javascript:removeConnection()\">remove</a></div>";
	}
	print "</div>";
	print "<div class='profile-image'><img src='".$u->thumbnail_url('tiny')."'/></div>";
	print "</div>";
	print "</a>";
}


function user_list($label, $list, $editable=false) {
	print "<div class='network-list'>";
	
	print "<div class='list-heading'>";
	print "<a href='javascript:void(0)' onclick='expandList(this)' class='btn'>";
	print "<img id='button' src='".media_url('images/icon-expand.gif')."'/>";
	print "</a>";
	print $label;
	print "</div>";
	
	print "<div>";
	$row_size = 6;
	for ($i = 0; $i < min($row_size, sizeof($list)); $i++) {
		$u = $list[$i];
		user_thumb($u, $editable);
	}
	print "</div>";
	
	if (sizeof($list) > $row_size) {
		print "<div class='expansion'>";
		for ($i = $row_size; $i < sizeof($list); $i++) {
			user_thumb($list[$i], $editable);
		}
		print "</div>";
		
		$num_extra = sizeof($list) - $row_size;
		print "<div class='expansion-label'>";
		if ($num_extra == 1) {
			$extra_label = "1 other";
		} else {
			$extra_label = $num_extra . " others";
		}
		print "and <a href='javascript:void(0)' onclick='expandList(this)'>{$extra_label}</a>";
		print "</div>";
	}
	
	
	print "</div>";
}
?>
<script>

var expandList = function() {
	var target = $(event.target);
	while (!target.hasClass('network-list')) {
		target = target.parent();
	}

	var exp = $('.expansion', target);
	if (exp.css('display') == 'block') {
		exp.fadeOut(100);
		$('.expansion-label', target).show();
		$('.btn img').attr('src', '<?= media_url('images/icon-expand.gif') ?>');
	} else {
		exp.fadeIn(100);
		$('.expansion-label', target).hide();
		$('.btn img').attr('src', '<?= media_url('images/icon-collapse.gif') ?>');
	}
    /*var el = $('#friends_' + type + '_' + id);
    if (el.css('display') == 'block') {
        el.fadeOut(100);
        $('#button_' + type + '_' + id).attr('src', expandUrl);
        $('#extra_' + type + '_' + id).show();
    } else {
        el.fadeIn(300);
        $('#button_' + type + '_' + id).attr('src', collapseUrl);
        $('#extra_' + type + '_' + id).hide();
    }*/
};

</script>

<?php if (!empty($message)): ?>
<div class="success" style="padding:10px"><?= $message ?></div>
<?php endif; ?>

<div class="profile-content">
    <div class="profile-left-col">
        <div class="profile-photo"><img src="<?= $user->thumbnail_url('square') ?>" alt="<?= $user->full_name() ?>" /></div>
		
		
		
		<ul class="profile-options">
			<?php if (!empty($speaker)): ?>
				<li><a href="<?= $speaker->url() ?>">View Speaker Profile</a></li>
			<?php endif; ?>
			<li><a href="<?= ci_url('conference/friend/' . $user->access_key) ?>">Add as friend</a></li>
			<li><a href="<?= ci_url('conference/meet/' . $user->access_key) ?>">Let's meet!</a></li>
		</ul>
	</div>
	
	
	<div class="profile-body">
		<div class="name"><?= $user->full_name() ?></div>
		<div class="extra-info">
			<?php if (!empty($user->twitter_username)): ?>
			<div>@<?= $user->twitter_username ?></div>
			<?php endif; ?>
			<div><?= $user->title ?></div>
			<div><?= $user->country_name() ?></div>
			<div><?= $user->country_flag() ?></div>
		</div>
		
		<div class="section-heading">Biography</div>
		<div class="section-content">
			<?= cascade($user->biography, "<div class='empty-block'>No biography listed.</div>") ?>
		</div>
		
		<div class="section-heading">Interest Areas</div>
		<div class="section-content">
			<?= cascade($user->interests, "<div class='empty-block'>No interests listed.</div>") ?>
		</div>
		
		
		<div class="section-heading">Medicine X Network</div>
		<div class="section-content">
			<?php 
			$editable = false;
			foreach ($forward as $type => $list) {
				if (!isset($forward_headings[$type])) {
					continue;
				}
				$label = $forward_headings[$type];
				user_list($label, $list);
			}
			foreach ($reverse as $type => $list) {
				if (!isset($reverse_headings[$type])) {
					continue;
				}
				$label = $reverse_headings[$type];
				user_list($label, $list);
			}
			?>
		</div>
	</div>
</div>