<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<style type="text/css">

body, div, tr, table, td {
	font-family: 'Open Sans', sans-serif;
	font-size: 12px;
	font-weight: 300;
}

table, tr, td {
	padding: 0;
	margin: 0;
}

td {
	vertical-align: top;
}


.day-table {
	margin-top: 2em;
	page-break-before: avoid;
	width: 100%;
}

.day-table:first-child {
	margin-top: 0;
	page-break-before: avoid;
}

.sub-items {
	border-left: 1px solid #c1c1c1;
	margin-left: 1em;
	padding-left: 0.5em;
}

.sub-items .sub-items {
	border: 0;
	padding: 0;
}

.day {
	font-size: 1.5em;
	padding: 1.5em 0 0.5em 0;
}

td.time {
	color: #666;
	white-space: nowrap;
	margin-top: 0.5em;
	text-align: right;
	padding-right: 1em;
}

td.block {
	width: 100%;
}

.title {
	font-family: 'Noto Serif';
	font-size: 1.1em;
	color: #333;
}

.location {
	float: right;
	text-align: right;
	opacity: 0.9;
	margin-left: 1em;
}

.speakers {
	font-style: italic;
	opacity: 0.9;
}

.event {
	margin-bottom: 0.5em;
}



.wrap {
	max-width: 500px;
}

.event .event .title {
	font-weight: normal;
}

.event .event.type-group > .title {
	font-weight: bold;
}

.interval {
	margin-top: 0.3em;
	font-size: 0.9em;
}

tr:first-child .day {
	padding-top: 0;
}

.event:last-child {
	margin-bottom: 0;
}

.block {
	padding-bottom: 0.5em;
}


.type-group > .title {
	font-weight: bold;
}

tr:last-child > td.block {
	padding-bottom: 0;
}

tr.columns > td {
	padding-right: 1em;
}

tr.columns > td:last-child {
	padding-right: 0;
}

.type-food .title {
	color: rgb(117, 79, 7);
	opacity: 0.8;
}

.type-group .title {
	font-weight: bold;
}

.type-keynote .title {
	color: #5462C7;
	font-weight: bold;
}


.type-masterclass .title, .type-workshop_cap .title {
	color: #115F11;
	opacity: 0.8;
}

.logo {
	height: 4em;
}

/*
.event {
	background-color: #f0f0f0;
	margin-bottom: 5px;
}




*/

</style>
</head>
<body>
<img class="logo" src="<?= media_url('images/medx_logo_black.png') ?>"/>
<?= $content ?>
</body>
</html>