

<form action="<?= modified_url(array()) ?>" method="POST" id="common_form">
<div>
    <h3 style="margin-bottom:15px">Please select one of the registration options below</h3>
    <div>
    <ul class="registration-options">
    <?php foreach ($rates as $rate): ?>
        <li>
            <div style="float:left;margin-right:10px">
                <input type="radio" name="rate_id" value="<?= $rate->id ?>" id="rate_<?= $rate->id ?>"/>
            </div>
            <div style="float:left">
            <label for="rate_<?= $rate->id ?>"><?= $rate->name ?></label>
            <?php if ($rate->verification != 'none'): ?>
                <div><?= $rate->verification_message() ?></div>
            <?php endif; ?>
            </div>
            <div class="spacer"></div>
        </li>
    <?php endforeach; ?>
    </ul>
    </div>
</div>

<input type="submit" value="Continue"/>
</form>
