<form method="POST" id="addon_form" action="?<?= param_str($_GET) ?>">

<div class="select-addons">

<h3>Registration Information</h3>
<div class="highlight-box">
    <div class="addon">
        <div class="photo"><?= stripslashes($rate->description) ?></div>
        <div class="info">
            <div class="title"><?php echo $rate->name ?></div>
            <div class="description">
                <?= stripslashes($rate->description) ?>
                <?php if ($rate->verification != 'none'): ?>
                    <div class="notice" style="padding:5px"><?= $rate->verification_message() ?></div>
                <?php endif; ?>
            </div>
            <ul class="prices">
                <li>
                    <input type="radio" checked="checked" name="rate_required"/>
                    <label><?= $rate->name ?> [<?= usd($offer->calculate_price($rate)) ?>]</label>
                </li>
            </ul>

        </div>
        <div class="spacer"></div>
    </div>
</div>

<?php if (!empty($addons)): ?>
<h3>You may select additional registration options below</h3>
<div class="highlight-box">
    <?php foreach ($addons as $addon): ?>
        <div class="addon">
            <div class="photo"><img src="<?= $addon->thumbnail_url('square') ?>"/></div>
            <div class="info">
                <div class="title">
                	<?php 
                	if ($addon->sold_out) {
                		print 'Waitlist for ' . $addon->name;	
                	} else {
						print $addon->name;
					}
                	?>
                </div>
                <div class="description">
                	<?php if ($addon->sold_out && !empty($addon->waitlist_message)): ?>
                		<div style="font-style:italic;margin-bottom:10px"><?php echo stripslashes($addon->waitlist_message); ?></div>
                	<?php endif; ?>
                	<?= stripslashes($addon->description) ?>
                </div>
                <ul class="prices">
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_0" value="0"<?= property($data, 'addon_' . $addon->id, 0) == 0 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_0">None</label></li>
                    <?php if ($addon->sold_out): ?>
                    	<li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">Sign up for waitlist [Add $<?= number_format(0, 2, '.', '') ?>]</label></li>
                    <?php else: ?>
                    	<li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">Buy [Add $<?= number_format($addon->price, 2, '.', '') ?>]</label></li>
                    	<?php if ($addon->text_prompt): ?>
							<li id="response_box_<?= $addon->id ?>" style="display:none">
                                <?php
                                $msg = $addon->prompt_label;
                                if (empty($msg)) {
                                    $msg = "What would you like to get out of this event?";
                                }
                                print $msg;
                                ?>
								
								<div>
									<textarea rows="<?= property($addon, 'box_rows', 5) ?>" name="response_<?= $addon->id ?>" style="width:500px"><?= htmlentities(property($data, 'response_' . $addon->id, '')) ?></textarea>
								</div>
							</li>
							<script>
							(function() {
								var checkMe = function() {
									var val = $('input:radio[name=addon_<?= $addon->id ?>]:checked').val();
									if (val == '1') {
										$('#response_box_<?= $addon->id ?>').show();
									} else {
										$('#response_box_<?= $addon->id ?>').hide();
									}
								};

								jQuery(function() {
									$('input:radio[name=addon_<?= $addon->id ?>]').click(checkMe);
								});

								checkMe();
							})();
							</script>
                    	<?php endif; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="spacer"></div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<?php if (!empty($master_classes)): ?>
<h3>Master Classes</h3>
<div class="highlight-box">
    <?php foreach ($master_classes as $addon): ?>
        <div class="addon">
            <div class="photo"><img src="<?= $addon->thumbnail_url('square') ?>"/></div>
            <div class="info">
                <div class="title"><?= $addon->name ?></div>
                <div class="description"><?= stripslashes($addon->description) ?></div>
                <ul class="prices">
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_0" value="0"<?= property($data, 'addon_' . $addon->id, 0) == 0 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_0">I am not interested in attending this class.</label></li>
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">I would like to attend this class</label></li>
                    <li id="response_box_<?= $addon->id ?>" style="display:none">
                        What would you like to get out of this class?
                        <div>
                            <textarea name="response_<?= $addon->id ?>" style="width:500px;height:120px"><?= htmlentities(property($data, 'response_' . $addon->id, '')) ?></textarea>
                        </div>
                    </li>
                    <script>
                    (function() {
                        var checkMe = function() {
                            var val = $('input:radio[name=addon_<?= $addon->id ?>]:checked').val();
                            if (val == '1') {
                                $('#response_box_<?= $addon->id ?>').show();
                            } else {
                                $('#response_box_<?= $addon->id ?>').hide();
                            }
                        };

                        jQuery(function() {
                            $('input:radio[name=addon_<?= $addon->id ?>]').click(checkMe);
                        });

                        checkMe();
                    })();
                    </script>
                </ul>
            </div>
            <div class="spacer"></div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<script>
var warnings = [];
<?php if (!empty($warnings)): ?>
	<?php foreach ($warnings as $warning): ?>
		warnings.push([<?php echo $warning['id'] ?>, "<?php echo $warning['type']; ?>", "<?= addslashes(str_replace("\n", "<br/>", $warning['message'])) ?>"]);
	<?php endforeach; ?>
<?php endif; ?>
var checkWarnings = function() {
	$('#warnings').empty();
	var count = 0;
	for (var key in warnings) {
		var warning = warnings[key];

		var checked = $("#addon_" + warning[0] + "_1:checked").size() > 0;
		
		if ((checked && warning[1] == "select") || (!checked && warning[1] == "noselect")) {
			$('<div/>').addClass('notice')
				.css({'padding': '5px', 'margin': '10px 0'})
				.append(warning[2])
				.appendTo("#warnings");
			count++;
		}
		
	}
	if (count > 0) {
		$('.modal').show();
	} else {
		$('#addon_form').submit();
	}
};
</script>
<div>
    <input type="button" value="Continue" onclick="checkWarnings()"/>
</div>

<div class="modal" style="display:none">
<div class="content">
    <h4>Please Confirm the Following:</h4>
    <div id="warnings"></div>
    <ul class="buttons">
        <li><input type="button" onclick="(function(){$('.modal').hide()})()" value="Revise your registration"/></li>
        <li><a href="javascript:void(0)" onclick="(function(){ $('#addon_form').submit() })()">Accept and Continue</a></li>
    </ul>
    <?php $_SESSION['registration']['warn'] = 1; ?>
</div>
</div>

</div>
</form>