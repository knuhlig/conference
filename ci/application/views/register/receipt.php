<html>
<head>
<script src="<?= plugins_url('/media/js/jquery.js', WPCI_FILE) ?>"></script>
<script src="<?= plugins_url('/media/js/kerning.js', WPCI_FILE) ?>"></script>
<style type="text/css">

    body {
        font-family: sans-serif;
        font-size: 14px;
    }

    .summary {
        margin-bottom: 20px;
        margin-top: 20px;
        width: 100%;
    }

    .summary td {
        border-top: 1px dotted #c1c1c1;
        padding: 5px 10px;
    }

    .price {
        text-align: right;
        width: 150px;
    }

    .total td {
        border-top: 1px solid black;
        font-weight: bold;
        border-bottom: 2px solid black;
    }

    .info {
        margin-left: 20px;
    }

    p {
        margin: 20px 0;
    }

    h3 {
        margin-top: 20px;
        margin-bottom: 5px;
    }

    img.logo {
        width: 100%;
    }

    div.wrap {
        width: 700px;
    }

    .footer {
        position: fixed;
        bottom: 0;
        width: 100%;
        text-align: center;
        color: #888;
    }
</style>
</head>
<body>
<div class="wrap">
    <div>
        <img class="logo" src="<?= plugins_url('/media/images/letterhead_logo.png', WPCI_FILE) ?>"/>
    </div>
    <h3 style="-letter-kern: -1px">RECEIPT</h3>

    <div><?= date('F j, Y', $order->date_paid) ?></div>
    <h3 style="-letter-kern: -1px">Recipient</h3>
    <div class="recipient">
        <div><?= $user->first_name . ' ' . $user->last_name ?></div>
        <div><?= $user->email ?></div>
    </div>

    <table cellpadding=0 cellspacing=0 class="summary">
        <tr>
            <td>Medicine X Registration: <?= $order->rate->name ?></td>
            <td class="price"><?= usd($order->registration_price); ?></td>
        </tr>
        <?php foreach ($order->associated('addon', true) as $orderaddon):
            if ($orderaddon->addon->master_class) continue;
        ?>
        <tr>
            <td>
            	<?php 
            	if ($orderaddon->waitlist) {
					print 'Waitlist for ' . $orderaddon->addon->name;
				} else {
					print $orderaddon->addon->name;
				}
            	?>
            </td>
            <td class="price"><?= usd($orderaddon->unit_price * $orderaddon->quantity) ?></td>
        </tr>
        <?php endforeach; ?>
        <tr class="total">
            <td>Total Paid</td>
            <td class="price"><?= usd($order->total_price()) ?></td>
        </tr>
    </table>

    <div>Reference Number: <?= $order->reference_number ?></div>

    <h3>Payment Address</h3>
    <p>
        Stanford Medicine X Conference <br/>
        Stanford University Medical Center <br/>
        Department of Anesthesia<br/>
        c/o Lorena Rincon-Cruz<br/>
        300 Pasteur Drive<br/>
        Grant Building, Room S268<br/>
        Stanford CA, 94305-5117
    </p>

    <p>
        Please contact the Stanford AIM Lab with any questions:
        <table class="info">
            <tr>
                <td>Phone</td>
                <td>(650) 723-4671</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>stanfordmedx@gmail.com</td>
            </tr>
        </table>
    </p>

    <p>
        Thank you!
    </p>
</div>
</body>
</html>