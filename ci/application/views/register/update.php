<?php 
$this->template->title('Update Registration');

$this->load->view('common/messages');
?>

<?php if (empty($addons) && empty($master_classes)): ?>
	<h3>Sorry, there are no updates available for your registration.</h3>
	<div><a href="<?php echo ci_url('user/home') ?>">User Home</a></div>
<?php else: ?>
<form method="POST" action="?<?= param_str($_GET) ?>">

<div class="select-addons">

<?php if (!empty($addons)): ?>
<h3>You may select additional registration options below</h3>
<div class="highlight-box">
    <?php foreach ($addons as $addon): ?>
        <div class="addon">
            <div class="photo"><img src="<?= $addon->thumbnail_url('square') ?>"/></div>
            <div class="info">
                <div class="title">
                	<?php if ($addon->sold_out) {
                		print 'Waitlist for ';
                	}?><?= $addon->name ?></div>
                <div class="description"><?= stripslashes($addon->description) ?></div>
                <ul class="prices">
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_0" value="0"<?= property($data, 'addon_' . $addon->id, 0) == 0 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_0">None</label></li>
                    <?php if ($addon->sold_out): ?>
                    	<li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">Sign up for waitlist [Add $<?= number_format(0, 2, '.', '') ?>]</label></li>
                    <?php else: ?>
                    	<li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">Buy [Add $<?= number_format($addon->price, 2, '.', '') ?>]</label></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="spacer"></div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<form method="POST" action="?<?= param_str($_GET) ?>">

<div class="select-addons">


<?php if (!empty($master_classes)): ?>
<h3>Master Classes</h3>
<div class="highlight-box">
    <?php foreach ($master_classes as $addon): ?>
        <div class="addon">
            <div class="photo"><img src="<?= $addon->thumbnail_url('square') ?>"/></div>
            <div class="info">
                <div class="title"><?= $addon->name ?></div>
                <div class="description"><?= stripslashes($addon->description) ?></div>
                <ul class="prices">
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_0" value="0"<?= property($data, 'addon_' . $addon->id, 0) == 0 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_0">I am not interested in attending this class.</label></li>
                    <li><input type="radio" name="addon_<?= $addon->id ?>" id="addon_<?= $addon->id ?>_1" value="1"<?= property($data, 'addon_' . $addon->id, 0) == 1 ? ' checked="checked"': '' ?>/> <label for="addon_<?= $addon->id ?>_1">I would like to attend this class</label></li>
                    <li id="response_box_<?= $addon->id ?>" style="display:none">
                        What would you like to get out of this class?
                        <div>
                            <textarea name="response_<?= $addon->id ?>" style="width:500px;height:120px"><?= htmlentities(property($data, 'response_' . $addon->id, '')) ?></textarea>
                        </div>
                    </li>
                    <script>
                    (function() {
                        var checkMe = function() {
                            var val = $('input:radio[name=addon_<?= $addon->id ?>]:checked').val();
                            if (val == '1') {
                                $('#response_box_<?= $addon->id ?>').show();
                            } else {
                                $('#response_box_<?= $addon->id ?>').hide();
                            }
                        };

                        jQuery(function() {
                            $('input:radio[name=addon_<?= $addon->id ?>]').click(checkMe);
                        });

                        checkMe();
                    })();
                    </script>
                </ul>
            </div>
            <div class="spacer"></div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>


<div>
    <input type="submit" value="Continue"/>
</div>
</div>
</form>
<?php endif; ?>