<?php
$rate = $order->rate;
$offer = $order->offer;

$orderaddons = $order->associated('addon', true);

$addons = $offer->get_available_addons($rate);
$master_classes = $offer->get_available_addons($rate, true);

$warnings = $order->get_warnings();
?>

<div class="select-addons">
<h4 style="margin-top:20px">
    <div style="margin-bottom:10px">Please review your order below.</div>
</h4>

<h3>Attendee Information &mdash; <a href="<?= modified_url(array('step' => 2)) ?>">Change</a></h3>

<div class="highlight-box">
    <div><?= $user->full_name(); ?></div>
    <div><?= $user->email; ?></div>
</div>

<h3 style="margin-top:40px">Registration Details &mdash; <a href="<?= modified_url(array('step' => 1, 'update' => 1)) ?>">Change</a></h3>
<table class="display-table" style="max-width:600px">
    <tr>
        <th>Item</th>
        <th style="width:100px;text-align:right">Price</th>
    </tr>
    <tr>
        <td><span style="font-weight:bold">Medicine X 2015 - September 25-27, 2015 &mdash;  <?= $rate->name ?></span></td>
        <td style="text-align:right"><?= usd($offer->calculate_price($rate)) ?></td>
    </tr>
    <?php
    $row = 1;
    foreach ($orderaddons as $orderaddon) {
        if ($orderaddon->addon->master_class) continue;
        $class = ($row % 2 == 0) ? '' : 'odd';
        print "<tr class='{$class}'>";
        print "<td>";
        print "<span style='color:#666;margin-left:20px'>";
		if ($orderaddon->waitlist) print "Waitlist for ";
		print "{$orderaddon->addon->name}</span>";
        if ($orderaddon->quantity > 1) print " ({$orderaddon->quantity})";
        print "</td>";
        print "<td style='text-align:right'>".usd($orderaddon->unit_price * $orderaddon->quantity)."</td>";
        print "</tr>";
        $row++;
    }
    ?>
    <tr class="footer">
        <th>Total</th>
        <th style="text-align:right"><?= usd($order->total_price()); ?></th>
    </tr>
</table>

<?php if ($master_class_available): ?>
<h3 style="margin-top:40px">Requested Master Classes &mdash; <a href="<?= modified_url(array('step' => 1)) ?>">Change</a></h3>
<div class="highlight-box">
<?php
$count = 0;
foreach ($orderaddons as $orderaddon) {
    if (!$orderaddon->addon->master_class) continue;
    print "<div style='font-weight:bold'>" . $orderaddon->addon->name . "</div>";
    print "<div style='margin-bottom:20px'>" . htmlentities(stripslashes($orderaddon->response)) . "</div>";
    $count++;
}
if ($count == 0) {
    print "None";
}
?>
</div>
<?php endif; ?>

<div style="margin:30px 0">
<?php if ($order->total_price() == 0): ?>
	<div>Your order does not require any payment. Click the button below to finalize your registration.</div>
	<input type="button" class="button-primary" value="Complete Order" onclick="parent.location='<?php echo ci_url('register/confirm_free/' . $order->id) ?>'"/>
<?php else:
    $settings = R::dispense('settings');

    $params = array();
    $params["access_key"]=$settings->cybs_access_key;
    $params["profile_id"]=$settings->cybs_profile_id;
    $params["transaction_uuid"]=uniqid();
    $params["signed_date_time"]=gmdate("Y-m-d\TH:i:s\Z");
    $params["locale"]="en";
    $params["transaction_type"]='sale';
    $params["reference_number"]=$order->id;
    $params["amount"]=$order->total_price();
    $params["currency"]='USD';
    $params["signed_field_names"]='access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency';
    $params["unsigned_field_names"] = '';
?>
<form action="<?= cybs_endpoint() ?>" method="post">
    <?php
        foreach($params as $name => $value) {
            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
        }
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . cybs_sign($params) . "\"/>\n";
    ?>
    <div>You will be directed to Cybersource, our secure payment processor.</div>
    <div><b>Please note that all orders are non-refundable.</b></div>
    <input type="submit" value="Pay Now" class="button-primary"/>

    <div style="margin-top:20px;font-size:11px;font-weight:bold;color:#555;font-style:italic">
    By registering for Medicine X, you agree for a portion of the registration fees to be applied to the pre-payment of food, beverage, and service charges during the 3-day conference.
    Because the majority of the conference takes place during the weekend, nearby food venues will be closed.
    If you wish to register for the conference without purchasing food, beverage and service charges you must contact the conference staff at 650-723-4671.
    Please note if you register in this manner, you will not be able to consume food served to conference attendees.
    </div>
</form>
<?php endif; ?>
</div>

<div class="notice" style="padding: 5px">
    If you'd like to come back to this order at a future time,
    <a href="<?= modified_url(array('send_email' => 1)) ?>">Click Here</a>.
    We will email <b><?= $user->contact_email() ?></b> with instructions.
</div>
</div>