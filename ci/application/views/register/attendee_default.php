
<h4 style="margin-top:20px">
    Already have an account? Attended Medicine X last year?
    <a href="<?= ci_url('user/login', array('r' => ci_url('register', $_GET))) ?>">Log in here</a>
</h4>

<?php
$this->load->view('common/form', array(
    'info' => array(
        'Attendee Information' => array(
        	'first_name' => array(
                'type' => 'text',
                'style' => 'width:200px'
            ),
            'last_name' => array(
                'type' => 'text',
                'style' => 'width:200px'
            ),
            'email' => array(
                'type' => 'text',
                'style' => 'width:300px'
            ),
        	'username' => array(
        		'type' => 'text',
        		'style' => 'width:200px',
        		'description' => 'Choose a username for the Medicine X site'
        	),
        	'password' => array(
        		'type' => 'password',
        		'style' => 'width:200px'
        	),
        	'password_confirm' => array(
        		'type' => 'password',
        		'style' => 'width:200px'
        	)
        ),
    	'If desired, you may enter alternate contact information below (e.g. administrative contact)' => array(
    		'admin_name' => array(
    			'type' => 'text',
    			'label' => 'Contact Name',
    			'style' => 'width:300px'
    		),
    		'admin_email' => array(
    			'type' => 'text',
    			'label' => 'Contact Email',
    			'style' => 'width:300px'
    		)
    	)
    ),
    'buttons' => array(
        'continue' => 'Continue'
    ),
	'data' => $_POST
));
?>