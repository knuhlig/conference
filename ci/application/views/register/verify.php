<?php
$this->template->title('Verify Affiliation');

$this->load->view('common/messages');
?>

<div>
<div><b><?php echo $order->rate->name ?></b></div>
In order to complete your registration, we must verify your eligibility with a photo ID from a degree-granting institution. Please upload a photo of your ID below (all information is strictly confidential). After completing this step, we will contact you with final confirmation of your registration. Thank you! 
</div>

<div style="margin-top:20px">
<?php 
$this->rb_form->inline_editor($order, $fields, $layout);
?>
</div>