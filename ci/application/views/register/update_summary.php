<?php
$this->template->title('Update Summary');
?>

<div class="select-addons">

<h4 style="margin-top:20px">
    <div style="margin-bottom:10px">Please review your order below.</div>
</h4>
<?php 
$total_price = 0;
$count = 0;
foreach ($orderaddons as $orderaddon) {
	if ($orderaddon->addon->master_class) continue;
	$count++;
}
if ($count > 0):
?>
<h3 style="margin-top:40px">Order Details &mdash; <a href="">Change</a></h3>
<table class="display-table" style="width:600px">
    <tr>
        <th>Item</th>
        <th style="width:100px;text-align:right">Price</th>
    </tr>
    <?php
    $row = 1;
    foreach ($orderaddons as $orderaddon) {
        if ($orderaddon->addon->master_class) continue;
        $class = ($row % 2 == 0) ? '' : 'odd';
        print "<tr class='{$class}'>";
        print "<td>";
        print "<span style='color:#666;margin-left:20px'>";
		if ($orderaddon->waitlist) print "Waitlist for ";
		print "{$orderaddon->addon->name}</span>";
        if ($orderaddon->quantity > 1) print " ({$orderaddon->quantity})";
        print "</td>";
        print "<td style='text-align:right'>".usd($orderaddon->unit_price * $orderaddon->quantity)."</td>";
        print "</tr>";
        $total_price += $orderaddon->unit_price * $orderaddon->quantity;
        $row++;
    }
    ?>
    <tr class="footer">
        <th>Total</th>
        <th style="text-align:right"><?= usd($total_price); ?></th>
    </tr>
</table>
<?php endif; ?>

<h3 style="margin-top:40px">Requested Master Classes &mdash; <a href="">Change</a></h3>
<div class="highlight-box">
<?php
$count = 0;
foreach ($orderaddons as $orderaddon) {
    if (!$orderaddon->addon->master_class) continue;
    print "<div style='font-weight:bold'>" . $orderaddon->addon->name . "</div>";
    print "<div style='margin-bottom:20px'>" . htmlentities(stripslashes($orderaddon->response)) . "</div>";
    $count++;
}
if ($count == 0) {
    print "None";
}
?>
</div>

<div style="margin:30px 0">
<?php if ($total_price == 0): ?>
	<div>Your order does not require any payment. Click the button below to finalize your registration.</div>
	<input type="button" class="button-primary" value="Complete Order" onclick="parent.location='<?php echo ci_url('register/confirm_free/u' . $update->id) ?>'"/>
<?php else: ?>
<form method="POST" action="<?= cybsFormUrl() ?>">
    <?php
        $map = array();
        $map['amount']= $total_price;
        $map['currency']= 'usd';
        $map['orderPage_transactionType']= "sale";
        $map['orderNumber']= 'u' . $update->id;
        InsertMapSignature($map);
    ?>
    <input type="submit" value="Pay Now" class="button-primary"/>
    <div>You will be directed to Cybersource, our secure payment processor.</div>
</form>
<?php endif; ?>
</div>

</div>