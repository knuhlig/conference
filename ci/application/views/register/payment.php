<?php 
$this->template->title('Special Payment');
?>
<div class="addon">
	<div class="photo"><?= stripslashes($payment->description) ?></div>
    <div class="info">
    	<div class="title" style="font-size:16px;margin-bottom:20px;font-weight:bold;margin-top:10px">Payment Information</div>
        <div class="description">
        	<?= stripslashes($payment->description) ?>
        </div>
        <ul class="prices">
        	<li>
            	<input type="radio" checked="checked" name="rate_required"/>
            	<label><?= $payment->name ?> [<?= usd($payment->price) ?>]</label>
            </li>
        </ul>
    </div>
    <div class="spacer"></div>
</div>
    
<form method="POST" action="<?= cybsFormUrl() ?>">
    <?php
        $map = array();
        $map['amount']= $payment->price;
        $map['currency']= 'usd';
        $map['orderPage_transactionType']= "sale";
        $map['orderNumber']= 'special';
        InsertMapSignature($map);
    ?>
    <div>You will be directed to Cybersource, our secure payment processor.</div>
    <div><b>Please note that all orders are non-refundable.</b></div>
    <input type="submit" value="Pay Now" class="button-primary"/>
</form>