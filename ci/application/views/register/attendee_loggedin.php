
<?php
$this->load->view('common/form', array(
    'info' => array(
        'Attendee Information' => array(
            'first_name' => array(
                'type' => 'text',
                'style' => 'width:300px'
            ),
            'last_name' => array(
                'type' => 'text',
                'style' => 'width:300px'
            ),
            'email' => array(
                'type' => 'text',
                'style' => 'width:300px'
            )
        )
    ),
    'buttons' => array(
        'continue' => 'Continue'
    )
));
?>

<div class="notice" style="padding:5px">
    Not you?
    <a href="<?= modified_url(array('f' => 1)) ?>">Switch user</a>
</div>