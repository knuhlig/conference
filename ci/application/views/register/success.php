<?php
$this->template->title('Registration Confirmation');
?>

<h3>Thank you! Your registration has been processed.</h3>
<div>
We have emailed <b><?= $this->auth->user->contact_email() ?></b> with confirmation of your registration.
</div>


<?php if (!empty($needs_update)): ?>
<div style="margin-top:20px">
	<b>Important:</b> In order to finalize your registration, your user profile needs to be updated. Please click "Update Profile" below and provide the following information:
	<ul style="list-style:square">
		<?php if (empty($user->first_name)): ?><li>First Name</li><?php endif; ?>
		<?php if (empty($user->last_name)): ?><li>Last Name</li><?php endif; ?>
		<?php if (empty($user->email)): ?><li>Email Address</li><?php endif; ?>
		<?php if (empty($user->photo_id)): ?><li>A portrait-style photo, at least 300x300 pixels in size</li><?php endif; ?>
		<?php if (empty($user->title)): ?><li>Title/Affiliation</li><?php endif; ?>
		<?php if (empty($user->country)): ?><li>Country</li><?php endif; ?>
	</ul> 
	<div>
		<input type="button" class="button button-primary" onclick="parent.location='<?php echo ci_url("user/edit_profile") ?>'" value="Update Profile">
	</div>
</div>
<?php else: ?>
<div style="margin-top:20px">
	<div><a class="button-primary" href="<?php echo ci_url("user/home") ?>">User Home</a></div>
</div>
<?php endif; ?>