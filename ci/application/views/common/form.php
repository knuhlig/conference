<?php
if (!empty($description)) {
    print "<h4>{$description}</h4>";
}
?>

<form id="common_form" method='POST' action='?<?= param_str($_GET) ?>' enctype='multipart/form-data'>
<?php

foreach ($info as $heading => $arr) {
    if (isset($multiple[$heading])) {
        for ($i = 0; $i < 3; $i++) {
            $this->load->view('common/form_group', array(
                'heading' => $heading,
                'arr' => $arr,
                'suffix' => '_' . $i
            ));
        }
    } else {
        $this->load->view('common/form_group', array(
            'heading' => $heading,
            'arr' => $arr,
            'suffix' => ''
        ));
    }
}

// wordpress rich text editor
foreach ($info as $heading => $arr) {
    foreach ($arr as $slug => $meta) {
        $type = property($meta, 'type', 'text');
        if ($type == 'html') {
            $content = property($data, $slug, '');
            print "<input type='hidden' name='{$slug}' id='{$slug}'/>";
            
            $desc = property($meta, 'description');
            if (!empty($desc)) {
            	print "<div style='font-weight:bold'>{$desc}</div>";
            }
            
            print "<div style=\"\" id=\"postdivrich\" class=\"postarea\">";
            the_editor(stripslashes($content));
            print "</div>";

            print "<script>jQuery(function() { $('#common_form').submit(function() { $('#{$slug}').val(tinyMCE.activeEditor.getContent()) })});</script>";
            break;
        }
    }
}
?>

<div style="margin-top:20px"><input type="hidden" name="form_action" id="form_action"/></div>
<?php
$type = 'submit';
foreach ($buttons as $action => $label) {
    $class = 'button';
    if (is_array($label)) {
        $class = $label[1];
        $label = $label[0];
    }
    print "<input class='{$class}' type='{$type}' value='{$label}' onclick=\"(function(){ $('#form_action').val('{$action}'); $('#common_form').submit() })()\"/>";
    $type = 'button';
}
?>
<script>
    jQuery(function() {
        $('#common_form').submit(function() {
            if (!$('#form_action').val()) {
                ('#form_action').val('submit');
            }
        });
    });
</script>

</form>
