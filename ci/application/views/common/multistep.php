

<ul class="multistep">
    <?php
    $num = 1;
    foreach ($steps as $key => $step) {
        if (!empty($step['hidden'])) continue;

        $url = modified_url(array('step' => $key));

        $class = '';
        if ($key < $cur_step) {
            $class = 'past';
        } else if ($key == $cur_step) {
            $class = 'current';
        }
        
        print "<li class='{$class}'>";
        print "<a href='{$url}'>";
        print 'Step ' . $num;
        if (!empty($step['title'])) {
            print "<div class='description'>" . property($step, 'title', '') . "</div>";
        }
        print "</a>";
        print "</li>";

        $num++;
    }
    ?>
</ul>