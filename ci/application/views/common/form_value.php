<?php

$months = array(
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December'
);

$hours = array(
    0 => '12',
    1 => '01',
    2 => '02',
    3 => '03',
    4 => '04',
    5 => '05',
    6 => '06',
    7 => '07',
    8 => '08',
    9 => '09',
    10 => '10',
    11 => '11',

);

if (!function_exists('linspace')) {
    function linspace($start, $stop, $interval=1, $lpad=null) {
        $arr = array();
        for ($i = $start; $i <= $stop; $i += $interval) {
            $val = $i;
            if (!is_null($lpad)) {
                $val = str_pad($val, $lpad, '0', STR_PAD_LEFT);
            }
            $arr[$val] = $val;
        }
        return $arr;
    }
}

if (!function_exists('form_dropdown')) {
    function form_dropdown($name, $opts, $value=null) {
        print "<select name='{$name}' id='{$name}'>";
        foreach ($opts as $val => $label) {
            $selection = '';
            if ($val == $value) $selection=" selected";
            print "<option value='{$val}'{$selection}>{$label}</option>";
        }
        print "</select>";
    }
}

switch ($type) {

	case 'bean':
		$arr = array();
		foreach ($opts as $bean) {
			$arr[$bean->id] = $bean->full_name();
		}
		form_dropdown($key, $arr, $form_value);
		break;
	
	case 'timestamp':
	    $date = getdate($form_value);
	    $date['ampm'] = 'am';
	
	    if ($date['hours'] > 12) {
	        $date['hours'] -= 12;
	        $date['ampm'] = 'pm';
	    }
	
	    form_dropdown($key . '_month', $months, $date['mon']);
	    form_dropdown($key . '_day', linspace(1, 31, 1, 2), $date['mday']);
	    form_dropdown($key . '_year', linspace(2013, 2015), $date['year']);
	    print "<span style='margin:0 10px'>at</span>";
	    form_dropdown($key . '_hour', $hours, $date['hours']);
	    print ":";
	    form_dropdown($key . '_minutes', linspace(0, 60, 1, 2), $date['minutes']);
	    form_dropdown($key . '_ampm', array('am' => 'am', 'pm' => 'pm'), $date['ampm']);
	    break;
	
	// dropdown
	case 'dropdown':
	    form_dropdown($key, $opts, $form_value);
	    break;
	
	// file fields
	case 'file':
	    print "<input type='file' multiple name='{$key}[]'/>";
	    $upload = R::load('upload', $form_value);
	    if ($upload->id) {
	        print "<div style='float:left'>".$upload->thumbnail()."</div>";
	    }
	    break;
	
	// text areas
	case 'textarea':
	    $form_value = stripslashes($form_value);
	    print "<div><textarea name='{$key}' style='{$style}'>{$form_value}</textarea></div>";
	    break;
	
	// radio buttons
	case 'radio':
	    $cls = property($item, 'display', '');
	    print "<ul class='{$cls}'>";
	    foreach ($item['opts'] as $value => $display_label) {
	        $selection = '';
	        if ($value == $form_value) {
	            $selection = " checked='checked'";
	        }
	        print "<li>";
	        if ($cls == 'horizontal') {
	            print "<label for='{$key}_{$value}'>{$display_label}</label><input type='radio' name='{$key}' id='{$key}_{$value}' value='{$value}'{$selection}/>";
	        } else {
	            print "<input type='radio' name='{$key}' id='{$key}_{$value}' value='{$value}'{$selection}/><label for='{$key}_{$value}'>{$display_label}</label>";
	        }
	
	        print "</li>";
	    }
	
	    if (property($item, 'withtext')) {
	        $text_key = $key . '_text';
	        $text_value = property($data, $text_key);
	        print "<li class='shifted'><input type='text' name='{$text_key}' value='{$text_value}'/></li>";
	    }
	
	    print "</ul>";
	    break;
	
	// checkbox
	case 'checkbox':
	    print "<input type='checkbox' id='cb_{$key}' style='{$style}'".($form_value ? "checked='checked'" : "")."/>";
	    print "<input type='hidden' id='{$key}' name='{$key}' value='{$form_value}'/>";
	    print '<script>jQuery(function() { $("#cb_'.$key.'").change(function() { $("#'.$key.'").val($("#cb_'.$key.'").attr("checked") ? 1 : 0); $("#'.$key.'").trigger("change") })});</script>';
	    break;
	
	// text field
	case 'currency':
	    if ($form_value != '') $form_value = number_format($form_value, 2, '.', '');
	    print "<span style='{$style}'>$<input type='text' id='{$key}' name='{$key}' value='{$form_value}'/></style>";
	    break;
	
	// password field
	case 'password':
	    print "<input type='password' name='{$key}' style='{$style}'/>";
	    break;
	
	// text field
	default:
	    print "<input type='text' name='{$key}' id='{$key}' style='{$style}' value='{$form_value}'/>";
	    break;

}
?>