
<h3><?= $heading ?></h3>
<div class="highlight-box">
<div class="form">
    <table class="form-table">
        <?php foreach ($arr as $key => $item):
            $key .= $suffix;
            $label = property($item, 'label', ucwords(str_replace('_', ' ', $key)));
            $description = property($item, 'description');
            $type = property($item, 'type', 'text');
            $style = property($item, 'style', '');
            $opts = property($item, 'opts', array());
            $deps = property($item, 'deps');
            $row_class = property($item, 'row_class', '') . (empty($deps) ? '' : ' hidden');

            $form_value = property($data, $key, property($item, 'default'));


            if ($type == 'html') {
                continue;
            }
            if ($type == 'hidden') {
                $hidden_value = property($data, $key);
                print "<input type='hidden' name='{$key}' value='{$hidden_value}'/>";
                continue;
            }
        ?>
        <tr valign="top" class="<?= $row_class ?>" id="row_<?= $key ?>">
            <th scope="row">
                <label for="<?= $key ?>"><?= $label; ?></label>
            </th>
            <td>
                <?php
                    if ($type == 'compound') {
                        foreach ($item['items'] as $k2 => $i2) {
                            $item_key = $key . '_' . $k2;
                            $this->load->view('common/form_value', array(
                                'type' => property($i2, 'type', 'text'),
                                'style' => property($i2, 'style', ''),
                                'key' => $item_key,
                                'form_value' => property($data, $item_key, property($i2, 'default')),
                                'opts' => property($i2, 'opts', array())
                            ));
                        }
                    } else {
                        $this->load->view('common/form_value', array(
                            'type' => $type,
                            'style' => $style,
                            'key' => $key,
                            'form_value' => $form_value,
                            'opts' => $opts
                        ));
                    }
                ?>
                <?php if (!empty($description)) : ?>
                    <span class="description"><?= $description ?></span>
                <?php endif; ?>
            </td>
        </tr>
        <?php
            if (!empty($deps)) {
                print "<script>";
                print "jQuery(function() {";
                print 'var checkMe = function() {';
                foreach ($deps as $k2 => $value) {
                    $op = '==';
                    if (is_array($value)) {
                        $op = $value[0];
                        $value = $value[1];
                    }
                    $k2 .= $suffix;
                    print 'if (!($("#'.$k2.'").val() '.$op.' "'.addslashes($value).'")) {$("#row_'.$key.'").hide(); return;}';
                }
                print '$("#row_'.$key.'").show()';
                print "};";
                foreach ($deps as $k2 => $value) {
                    $k2 .= $suffix;
                    print '$("#'.$k2.'").change(checkMe);';
                }
                print "checkMe();";
                print "});";
                print "</script>";
            }
        endforeach;
        ?>
    </table>
</div>
</div>