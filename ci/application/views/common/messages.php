<?php foreach ($this->messages->getMessages() as $type => $list): ?>
    <div class="messages">
    <div class="<?= $type ?>">
        <?php foreach ($list as $message): ?>
            <div><?= $message; ?></div>
        <?php endforeach; ?>
    </div>
    </div>
<?php endforeach; ?>