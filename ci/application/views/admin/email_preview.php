
<form class="rb-form" id="common_form_1" method="POST" action="">
	<input type="hidden" name="form_action" id="common_form_1_action"/>
	<input type="hidden" name="no_data" value="1"/>
</form>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2>Email Preview</h2>
    <div style="margin-bottom:20px">
    	<input class="button" type="button" value="Send" onclick="doAction('send')"/>
    	&nbsp;<a href="<?php echo modified_url(array()) ?>">&#171; Edit Email</a>
    </div>
	
	<h3>Preview</h3>
	
	<div style="padding:10px;border:1px solid #c1c1c1;background-color:#f0f0f0">
		<table>
			<tr>
				<td style="width:200px">To</td>
				<td><?php echo $email->to ?></td>
			</tr>
			<tr>
				<td>Subject</td>
				<td><b><?php echo $email->subject ?></b></td>
			</tr>
		</table>
	</div>
	
	<div style="margin-top:5px;padding:10px;border:1px solid #c1c1c1;background-color:white">
		<table>
			<tr>
				<td style="width:200px;vertical-align:top">Body</td>
				<td><?php echo $email->body ?></td>
			</tr>
		</table>
	</div>
	
	
	<?php if (!empty($email->attachments)): ?>
	<div style="margin-top:5px;padding:10px;border:1px solid #c1c1c1;background-color:#f0f0f0">
		<table>
			<tr>
				<td style="width:200px">Attachments</td>
				<td>
					<a href="<?php echo ci_url('admin/download?path=' . $email->attachments) ?>">
						<?php echo basename($email->attachments) ?>
					</a>
				</td>
			</tr>
		</table>
	</div>
	<?php endif; ?>
</div>