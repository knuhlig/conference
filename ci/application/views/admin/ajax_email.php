<h1>Email Queue</h1>
<div class="notice" style="padding:10px;margin:10px">
Warning: This can take a while if the email list is long. You should not leave this page until the messages have been sent.
</div>
<script>

var msgs = [];
var stop = false;

<?php foreach ($msgs as $msg): ?>
msgs.push([<?php echo $msg->id?>, '<?php echo $msg->access_key ?>']);
<?php endforeach; ?>


var stopSend = function() {
	stop = true;
	$('#cancel_btn').attr('disabled', 'disabled');
};

var sendMessage = function(idx) {
	var msg = msgs[idx];
	var id = msg[0];
	var key = msg[1];

	if (stop) {
		$('#status_' + id).html('<div class="error" style="padding:10px">Cancelled</div>');
	} else {
		$('#status_' + id).html('<div class="notice" style="padding:10px">Sending...</div>');
		$.post(siteUrl('email/api_send/' + key), {}, function(data) {
			if (data == 'ok') {
				$('#status_' + id).html('<div class="success" style="padding:10px">SENT</div>');
			} else {
				$('#status_' + id).html('<div class="error" style="padding:10px">ERROR</div>');
			}
			if (idx + 1 < msgs.length) {
				sendMessage(idx + 1);
			}
		});
	}
};

</script>

<div>
	<input type="button" value="Start" onclick="sendMessage(0)"/>
	<input type="button" id="cancel_btn" value="Cancel" onclick="stopSend()"/>
	<a href="<?php echo modified_url(array()) ?>">&#171; Return To Editor</a>
</div>

<h3>Status</h3>
<?php foreach ($msgs as $msg): ?>
<div style="border:1px solid #c1c1c1;margin-bottom:10px;padding:10px;background-color:white">
	<div style="font-weight:bold">To: <?php echo $msg->to ?></div>
	<div id="status_<?php echo $msg->id ?>"><div style="color:#666">Ready to send</div></div>
</div>
<?php endforeach; ?>

