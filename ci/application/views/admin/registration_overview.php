<form method="POST" action="" id="common_form">
	<input type="hidden" name="form_action" id="form_action"/>
</form>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

	var drawDateChart = function() {
		// Create and populate the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Revenue');
		data.addColumn('number', 'Registrations');

        <?php 
        $first_day = 90;
        $cur_day = date('z') + 1;
        foreach ($dates as $day_of_year => $arr): 
        	if ($day_of_year < $first_day) continue;
        	if ($day_of_year > $cur_day) break;
        ?>
			data.addRow(["<?= date('n/j', mktime(0,0,0,1,$day_of_year,2014)) ?>", <?= $arr['revenue'] ?>, <?= $arr['num_orders'] ?>]);
        <?php endforeach ?>


		// Create and draw the visualization.
		new google.visualization.ColumnChart(document.getElementById('date_chart')).
			draw(data, {
				width: 800,
				height: 300,
				vAxes: {
					0: {logScale: false, format:'$###,###,###.00'},
					1: {logScale: false}
				},
				series:{
					0: {targetAxisIndex:0},
					1: {targetAxisIndex:1},
				},
				bar: {
					'groupWidth': '97%'
				},
				'backgroundColor': 'transparent',
				'chartArea': {'left': '10%', 'top': '10%', 'width':'80%', 'height':'80%'}
			});
	};
	
	var drawComparisonChart = function() {
		// Create and populate the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Total - 2014');
		data.addColumn('number', 'Total - 2015');

        <?php 
        $first_day = 90;
        $cur_day = date('z') + 1;
        foreach ($dates as $day_of_year => $arr): 
        	if ($day_of_year < $first_day) continue;
        	if ($day_of_year > $cur_day + 42) break;
        ?>
			data.addRow(["<?= date('n/j', mktime(0,0,0,1,$day_of_year,2015)) ?>", <?= $arr['cumulative_compare'] ?>, <?= ($day_of_year > $cur_day) ? 'null' : $arr['cumulative'] ?>]);
        <?php endforeach ?>


		// Create and draw the visualization.
		new google.visualization.LineChart(document.getElementById('compare_chart')).
			draw(data, {
				width: 800,
				height: 300,
				legend: 'top',
				vAxes: {
					0: {logScale: false},
				},
				series:{
					0: {targetAxisIndex:0},
					1: {targetAxisIndex:0},
				},
				bar: {
					'groupWidth': '97%'
				},
				'backgroundColor': 'transparent',
				'chartArea': {'left': '10%', 'top': '10%', 'width':'80%', 'height':'80%'}
			});
	};

	var drawRevenueChart = function() {
		// Create and populate the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Revenue - 2014');
		data.addColumn('number', 'Revenue - 2015');

        <?php 
        $first_day = 90;
        $cur_day = date('z') + 1;
        foreach ($dates as $day_of_year => $arr): 
        	if ($day_of_year < $first_day) continue;
        	if ($day_of_year > $cur_day + 42) break;
        ?>
			data.addRow(["<?= date('n/j', mktime(0,0,0,1,$day_of_year,2015)) ?>", <?= $arr['revenue_compare'] ?>, <?= ($day_of_year > $cur_day) ? 'null' : $arr['cumulative_revenue'] ?>]);
        <?php endforeach ?>


		// Create and draw the visualization.
		new google.visualization.LineChart(document.getElementById('revenue_chart')).
			draw(data, {
				width: 800,
				height: 300,
				legend: 'top',
				vAxes: {
					0: {logScale: false, format: '$###,###,###'},
				},
				series:{
					0: {targetAxisIndex:0},
					1: {targetAxisIndex:0},
				},
				bar: {
					'groupWidth': '97%'
				},
				'backgroundColor': 'transparent',
				'chartArea': {'left': '10%', 'top': '10%', 'width':'80%', 'height':'80%'}
			});
	};

	var drawGoalChart = function() {
		var data = google.visualization.arrayToDataTable([
			['Label', 'Value'],
			['Total', <?php echo $num_orders; ?>],
		]);

		var options = {
			width: 190, height: 190,
			redFrom: 0,
			redTo: 0,
			yellowFrom: <?php echo round($goal * 0.5) ?>,
			yellowTo: <?php echo round($goal * 0.75) ?>,
			greenFrom: <?php echo round($goal * 0.75) ?>,
			greenTo: <?php echo $goal ?>,
			minorTicks: 5,
			min: 0,
			max: <?php echo $goal; ?>,
			'chartArea': {'left': '0%', 'top': '0%', 'width':'100%', 'height':'100%'}
		};

		var chart = new google.visualization.Gauge(document.getElementById('goal_chart'));
		chart.draw(data, options);
	};

	var drawPaidGoalChart = function() {
		var data = google.visualization.arrayToDataTable([
			['Label', 'Value'],
			['Paid', <?php echo $paid_orders; ?>],
		]);

		var options = {
			width: 190, height: 190,
			redFrom: 0,
			redTo: 0,
			yellowFrom: <?php echo round($paid_goal * 0.5) ?>,
			yellowTo: <?php echo round($paid_goal * 0.75) ?>,
			greenFrom: <?php echo round($paid_goal * 0.75) ?>,
			greenTo: <?php echo $paid_goal ?>,
			minorTicks: 5,
			min: 0,
			max: <?php echo $paid_goal; ?>,
			'chartArea': {'left': '0%', 'top': '0%', 'width':'100%', 'height':'100%'}
		};

		var chart = new google.visualization.Gauge(document.getElementById('paid_goal_chart'));
		chart.draw(data, options);
	};

	var drawPieChart = function() {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Category');
		data.addColumn('number', 'Revenue');
		data.addRows([
			<?php foreach ($rates as $row): ?>
			['<?php echo $row['rate']->short_label() ?>', <?php echo $row['revenue'] ?>],
			<?php endforeach; ?>
			<?php foreach ($addons as $row): ?>
			['<?php echo $row['addon']->short_label() ?>', <?php echo $row['revenue'] ?>],
			<?php endforeach; ?>
		]);

		// Set chart options
		var options = {
			'width':400,
			'height':300,
			'backgroundColor': 'transparent',
			'chartArea': {'left': '0%', 'top': '0%', 'width':'100%', 'height':'100%'}
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document.getElementById('summary_chart'));
		chart.draw(data, options);
	};

	var drawCharts = function() {
		//drawPieChart();
		drawDateChart();
		drawGoalChart();
		drawPaidGoalChart();
		drawComparisonChart();
		drawRevenueChart();
	};

	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {'packages':['corechart', 'gauge']});
	google.setOnLoadCallback(drawCharts);

</script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Registration Overview</h2>


    <?php $this->load->view('common/messages'); ?>

   	<div id="summary">
    <h3>Summary</h3>
    <div>
	    <table style="float:left" class="data-table">
	    	<tr>
	    		<th colspan="2">Summary</th>
	    	</tr>
			<tr>
				<td>All Registrations</td>
				<td><?php echo $all_orders; ?>
			</tr>
			<tr>
				<td>Counted Registrations</td>
				<td><?php echo $num_orders; ?>
			</tr>
			<tr>
				<td>Total Revenue Collected</td>
				<td><?php echo usd($total_revenue); ?>
			</tr>
			<tr>
				<td>Registration Revenue</td>
				<td><?php echo usd($registration_revenue); ?>
			</tr>
			<tr>
				<td>Add-On Revenue</td>
				<td><?php echo usd($addon_revenue); ?>
			</tr>
		</table>


   		<div id="goal_chart" style="float:left;margin-left:60px"></div>
		<div id="paid_goal_chart" style="float:left;margin-left:60px"></div>

		<div id="summary_chart" style="float:left;padding:20px;margin-right:20px"></div>
		<div class="spacer"></div>

		<h3>Daily Registrations</h3>		
   		<div id="date_chart" style=""></div>
		<div class="spacer"></div>
		
		<h3>Total Registrations</h3>
   		<div id="compare_chart" style=""></div>

		<h3>Revenue</h3>
   		<div id="revenue_chart" style=""></div>
		<div class="spacer"></div>
	</div>



	<h3>
		Rates (click for details)
		<input type="button" value="Export All Registrations" onclick="doAction('exportAll')"/>
	</h3>
	<table class="display-table">
		<tr>
			<th style="width:300px">Rate Name</th>
			<th style="width:90px">Number Purchased</th>
			<th style="width:70px">Revenue</th>
		</tr>
		<?php
		$count = 0;
		foreach ($rates as $row):
			if ($row['count'] == 0) continue;
			$count++;
		?>
			<tr class="<?php echo ($count %2 ? 'odd' : 'even') ?>">
				<td><a href="?page=rate_summary&id=<?php echo $row['rate']->id ?>"><?php echo $row['rate']->short_label() ?></a></td>
				<td><?php echo $row['count'] ?></td>
				<td><?php echo usd($row['revenue']) ?></td>
			</tr>
		<?php endforeach; ?>
	</table>

	<h3>Add-Ons</h3>
	<table class="display-table">
		<tr>
			<th style="width:300px">Add-On Name</th>
			<th style="width:90px">Number Purchased</th>
			<th style="width:70px">Revenue</th>
		</tr>
		<?php
		$count = 0;
		foreach ($addons as $row):
		$count++
		?>
			<tr class="<?php echo ($count %2 ? 'odd' : 'even') ?>">
				<td>
					<span style="margin-right:5px"><img src="<?php echo $row['addon']->thumbnail_url('tiny') ?>"/></span>
					<a href="?page=addon_summary&id=<?php echo $row['addon']->id ?>"><?php echo $row['addon']->short_label() ?></a>
				</td>
				<td><?php echo $row['count'] ?></td>
				<td><?php echo usd($row['revenue']) ?></td>
			</tr>
		<?php endforeach; ?>
	</table>

	<h3>Master Classes</h3>
	<table class="display-table">
		<tr>
			<th style="width:300px">Master Class</th>
			<th style="width:90px">Number Signed Up</th>
		</tr>
		<?php
		$count = 0;
		if (!empty($classes)):
		foreach ($classes as $row):
		$count++
		?>
			<tr class="<?php echo ($count %2 ? 'odd' : 'even') ?>">
				<td>
					<a href="?page=addon_summary&id=<?php echo $row['class']->id ?>"><?php echo $row['class']->short_label() ?></a></td>
				<td><?php echo $row['count'] ?></td>
			</tr>
		<?php
		endforeach;
		endif;
		?>
	</table>


	<h3>Recent Orders</h3>
	<?php $recent_orders->display(); ?>
	</div>
</div>
