<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2>General Settings</h2>

    <form method="post" action="">
        <table class="form-table">
        <?php foreach ($settings as $key => $info): ?>
            <tr valign="top">
                <th scope="row">
                    <label for="<?= $key ?>"><?= $info['label'] ?></label>
                </th>
                <td>
                    <input type="text" class="regular-text" name="<?= $key ?>" id="<?= $key ?>" value="<?= get_option($key) ?>"/>
                    <?php if (!empty($info['description'])) : ?>
                        <span class="description"><?= $info['description'] ?></span>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
        <div style="margin-top:20px">
            <input type="submit" name="submit" value="Save Changes" class="button-primary"/>
        </div>
    </form>
</div>