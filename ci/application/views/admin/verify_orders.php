
<form method="POST" action="" id="common_form">
	<input type="hidden" name="order_id" id="order_id"/>
</form>
<script>
	var acceptVerification = function(id) {
		$('#order_id').val(id);
		$('#common_form').submit();
	};
</script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Verify Registrations</h2>


    <?php $this->load->view('common/messages'); ?>
    
    <?php foreach ($orders as $order): 
    	$user = $order->r('user');
		$rate = $order->r('rate');
		$photo = $order->r('verification_photo');
	?>
	<div style="border:1px solid #c1c1c1;background-color:white;padding:10px;margin:10px">
		<div style="float:right;text-align:right">
			<div><input type="button" value="Accept and Send Confirmation" onclick="acceptVerification(<?php echo $order->id ?>)"/></div>
			<?php if (empty($photo)): ?>
				<div><a href="<?php echo ci_url('register/verify/' . $order->resume_key) ?>">Upload Photo</a></div>
			<?php endif; ?>
		</div>
		<div><?php echo $rate->name ?></div>
		<div><a href="?page=edit_user&id=<?php echo $user->id ?>"><?php echo $user->full_name() ?></a> (<?php echo $user->contact_email() ?>)</div>
		<?php if (!empty($photo)): ?>
			<div><img style="max-height:300px;max-width:400px" src="<?php echo $photo->url() ?>"/></div>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
</div>

