<script type="text/javascript" src="<?= ci_url('api/js') ?>"></script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Email Wizard</h2>

    <?php $this->load->view('common/messages'); ?>
    
	<div class="success" style="padding:10px">
    	<b>Available Variables</b>
		<pre><?php echo $template->notes ?></pre>
	</div>
    
    <?php $this->rb_form->editor($email); ?>
</div>