<form method="POST" action="" id="common_form" class="rb-form">
	<input type="hidden" name="form_action" id="form_action"/>
</form>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">   
	var drawCharts = function() {
		
	};

	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {'packages':['corechart']});
	google.setOnLoadCallback(drawCharts);
	
</script>
    
<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Add-On Summary: <?php echo $addon->short_label() ?></h2>


    <?php $this->load->view('common/messages'); ?>
    
   	<div id="summary">
    <h3>
    	Orders
    	<input type="button" value="Export Data" onclick="doAction('export_addon')"/>
    </h3>
    
	<table class="display-table">
		<tr>
			<?php if (!empty($event)): ?>
				<th style="width:50px">Enrollment</th>
			<?php endif; ?>
			<th style="width:100px">Date</th>
			<th style="width:150px">User</th>
			<?php if ($addon->master_class): ?>
				<th style="width:200px">Response</th>
			<?php else: ?>
				<th style="width:100px">Amount Paid</th>
			<?php endif; ?>
		</tr>
		<?php 
		$count = 0;
		foreach ($rows as $row): 
		$order = $row->order;
		if (!$order->paid) continue;
		$count++;
		$user = $order->user;
		?>
			<tr class="<?php echo ($count %2 ? 'odd' : 'even') ?>">
				<?php if (!empty($event)): ?>
					<td>
						<i 
							class="fa fa-check-circle-o event-selection<?= isset($attendees[$user->id]) ? " selected" : "" ?>" 
							style="font-size:20px"
							data-id="<?= $event->id ?>"
							data-user-id="<?= $user->id ?>"
						></i>
					</td>
				<?php endif; ?>
				<td>
					<?php echo date(DATE_FMT_FULL, $order->date_paid) ?>
					<?php if ($row->waitlist): ?>
						<div class="warn">Waitlist</div>
					<?php endif; ?>
				</td>
				<td>
					<div style="float:left;margin-right:10px"><img src="<?php echo $user->thumbnail_url('tiny') ?>"/></div>
					<a href="<?= $user->url() ?>"><?php echo $user->full_name() ?></a>
				</td>
				<?php if ($addon->master_class): ?>
					<td><?php echo htmlentities(stripslashes($row['response'])) ?></td>
				<?php else: ?>
					<td><?php echo usd($row['unit_price'] * $row['quantity']) ?></td>
				<?php endif; ?>
				
			</tr>
		<?php endforeach; ?>
	</table>
</div>

<script>
	$(".event-selection").click(function() {
		var el = $(this);
		var willAttend = (el.hasClass("selected") ? 0 : 1);

		var data = {
			"id" : el.data("id"),
			"user_id": el.data("user-id"),
			"will_attend": willAttend
		};

		var time = el.data("time");
		$(".time-" + time).removeClass("selected");

		el.removeClass("fa-check-circle-o");
		el.html("<img src='<?= media_url('images/ajax-loader.gif') ?>'/>");
		
		$.post("<?= ci_url('conference/attend') ?>", data, function(data) {
			el.html("");
			el.addClass("fa-check-circle-o");

			var res = $.parseJSON(data);

			if (res.status == "waitlist") {
				alert("This event is currently full. You are currently position "+res.position+" on the waitlist.");
				e.addClass("selected");
			} else {
				if (willAttend) {
					el.addClass("selected");
				}
			}
		});


	});
</script>