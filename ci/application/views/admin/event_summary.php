
<div class="wrap">
	<div id="icon-options-general" class="icon32"></div>
	<div><a href="?page=browse_event">Return to list</a></div>

	<h2 style="margin-bottom:20px">Event Summary: <?php echo $event->full_title() ?> <a class="button" href="?page=edit_event&id=<?= $event->id ?>">Edit</a></h2>

	<?php $this->load->view('common/messages'); ?>

	<table class="data-table" style="min-width:500px">
		<tr>
    		<th colspan="2">Event Information</th>
    	</tr>
		<tr>
			<td>Title</td>
			<td><a href="<?= $event->url() ?>"><?= $event->full_title() ?></a></td>
		</tr>
		<tr>
			<td>Day</td>
			<td><?= date('l, F j', $event->event_date) ?></td>
		</tr>
		<tr>
			<td>Time</td>
			<td><?= $event->time_interval() ?></td>
		</tr>
		<tr>
			<td>Location</td>
			<td><?= $event->location_name() ?></td>
		</tr>
		<tr>
			<td>Capacity</td>
			<td><?php
				$c = $event->get_room_capacity();
				if (empty($c)) $c = 'n/a';
				print $c;
			?></td>
		</tr>
		<tr>
			<td>Speakers</td>
			<td>
				<?php
					$speakers = $event->associated('speaker');
					foreach ($speakers as $speaker) {
						print "<div>";
						print "<div style='float:left;margin-right:10px'><img src='".$speaker->thumbnail_url('tiny')."'/></div>";
						print "<a href='".$speaker->url()."'>" . $speaker->full_name() . "</a>"; 
						print "<div style='clear:both'></div>";
						print "</div>";
					}

				?>
			</td>
		</tr>
	</table>

	<h3>Attendees</h3>
	<table class="display-table">
	<tr>
		<th style="width:100px">Order</th>
		<th style="width:200px">Signed Up</th>
		<th>User</th>
	</tr>
	<?php 
	$order = 0;
	foreach ($attendees as $item):
		$order++;
		$date = date(DATE_FMT_FULL, $item->date_updated);
		$user = $item->user;
	?>
		<tr>
			<td><?= $order ?></td>
			<td><?= $date ?></td>
			<td>
				<img src="<?= $user->thumbnail_url('tiny') ?>"/>
				<a href="<?= $user->url() ?>"><?= $user->full_name() ?></a>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>