<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">   
	var drawCharts = function() {
		
	};

	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {'packages':['corechart']});
	google.setOnLoadCallback(drawCharts);
	
</script>

<form id="common_form" method="POST" action="">
	<input type="hidden" name="form_action" id="form_action"/>
	<input type="hidden" name="no_data" value="1"/>
</form>
    
<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Rate Summary: <?php echo $rate->short_label() ?></h2>


    <?php $this->load->view('common/messages'); ?>
    
    <div>
    	<input type="button" value="Export Data" onclick="doAction('export')"/>
    	<a href="?page=registration_overview">&#171; Return to Overview</a>
    </div>
    
   	<div id="summary">
    <h3>Registered Users</h3>
    
	<table class="display-table">
		<tr>
			<th style="width:100px">Date</th>
			<th style="width:150px">User</th>
			<th style="width:100px">Total Paid</th>
		</tr>
		<?php 
		$count = 0;
		foreach ($orders as $order):
		if (!$order->paid) continue;
		$count++;
		$user = $order->user;
		?>
			<tr class="<?php echo ($count %2 ? 'odd' : 'even') ?>">
				<td><?php echo date(DATE_FMT_FULL, $order->date_paid) ?></td>
				<td>
					<div style="float:left;margin-right:10px"><img src="<?php echo $user->thumbnail_url('tiny') ?>"/></div>
					<a target="_blank" href="?page=edit_user&id=<?php echo $user->id ?>"><?php echo $user->full_name() ?></a><br/>
					<?php echo $user->title ?>
				</td>
				<td><?php echo usd($order->total_price()) ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>