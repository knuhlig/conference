<form method="POST" action="" id="common_form">
	<input type="hidden" name="form_action" id="form_action"/>
</form>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2 style="margin-bottom:20px">Medicine X Archives</h2>

	<?php $this->load->view('common/messages'); ?>

    <h3>Browse Archive</h3>
    <div class="highlight-box" style="width:640px">
		You may select an option below to browse the Medicine X archives. Note that this is
		a "browse-only" feature for the admin user. It will not affect the public Medicine X
		site.
		<form method="POST" action="" style="padding:20px 0 10px 10px">
			<select name="archive_key">
				<option value="2013">Medicine X 2013</option>
				<option value="2014">Medicine X 2014</option>
			</select>
			<input type="submit" class="button-primary" value="Browse Archive"/>
		</form>
	</div>
</div>
