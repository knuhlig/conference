<?php 
$api_key = 'AIzaSyCLWMwGXVm9rt-8G7BVivkVJwUybi3YZII';
?>


<script type="text/javascript" src="<?= ci_url('api/js') ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key ?>&sensor=false"></script>
<script type="text/javascript">
function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(39, -98),
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("email_map"), mapOptions);

	var addMarker = function(lat, lng, to, content) {
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			title: to
		});
		if (content) {
			var html = '<div><b>' + to + '</b></div><div>' + content + '</div>';
			var infowindow = new google.maps.InfoWindow({
				content: html
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, this);
			});
		}
	};
	<?php foreach ($msgs as $msg) {
		if ($msg->sent && $msg->lat) {
			print "addMarker({$msg->lat}, {$msg->lng}, '{$msg->to}', '".date(DATE_FMT_FULL, $msg->date_read)."');\n";
		}
	} ?>
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2>Email Stats</h2>
    <div><a href="">&#171; Return</a></div>
	
	<div id="email_map" style="width:100%;height:500px"></div>
	
	<h3>Recipients</h3>
	<table class="display-table">
	<tr>
		<th>To</th>
		<th>Sent?</th>
		<th>Read</th>
	</tr>
	<?php foreach ($msgs as $msg): ?>
	<tr>
		<td><?php echo $msg->to ?></td>
		<td><?php echo $msg->sent ? date(DATE_FMT_FULL, $msg->date_sent) : 'No' ?></td>
		<td><?php echo $msg->read ? date(DATE_FMT_FULL, $msg->date_read) : 'No' ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
</div>
