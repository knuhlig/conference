
<script type="text/javascript" src="<?= ci_url('api/js') ?>"></script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <?php if ($bean->id): ?>
    	<h2 style="margin-bottom:20px">Edit <?php echo $bean->type_label() ?>: <?php echo $bean->short_label() ?></h2>
    	<script>
			document.title = "Edit <?php echo $bean->type_label() ?>: <?php echo $bean->short_label() ?>";
    	</script>
    <?php else: ?>
    	<h2 style="margin-bottom:20px">New <?php echo $bean->type_label() ?></h2>
    	<script>
			document.title = "New <?php echo $bean->type_label() ?>";
    	</script>
    <?php endif; ?>

    <?php $this->load->view('common/messages'); ?>
    
    <?php $this->rb_form->editor($bean); ?>
</div>