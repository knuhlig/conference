
<script type="text/javascript" src="<?= ci_url('api/js') ?>"></script>

<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2>
    	<?php echo $title ?>
    	<a href="?page=edit_<?php echo $model ?>" class="button add-new-h2">Add New</a>
    	<a href="?page=browse_<?php echo $model ?>&export=1" class="button add-new-h2">Export Data</a>
    </h2>
    <div style="margin-top:20px">
    	<?php $tbl->display(); ?>
    </div>
</div>

