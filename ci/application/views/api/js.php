
var siteUrl = function(path) {
    return '<?= ci_url('') ?>' + path;
};

var deleteObject = function(model, id, page) {
	var msgs = {
		'event': 'Are you sure you want to delete this event?',
		'speaker': 'Are you sure you want to delete this speaker?',
		'submission': 'Are you sure you want to delete this submission?'
	};

	if (model in msgs) {
		var response = confirm(msgs[model]);
		if (!response) return;
	}

	if (!page) {
		page = 'browse_' + model;
	}

	$.post(siteUrl('admin/delete/' + model), {'id': id}, function(data) {
		if (data == 'ok') {
			parent.location = '?page=' + page;
		} else {
			alert('error: ' + data);
		}
	});
};

var markDuplicate = function(id) {
	var response = confirm("Mark this submission as a duplicate?");
	if (!response) return;

	$.post(siteUrl('submission/mark_duplicate/' + id), {}, function(data) {
		if (data == 'ok') {
			$('#info_' + id).remove();
		} else {
			alert("Error: " + data);
		}
	});
};

var doUpload = function(callback) {
	var saved_fn = window.send_to_editor;

	window.send_to_editor = function(html) {
		alert(html);
		window.send_to_editor = saved_fn;
		tb_remove();
	};

	tb_show("Upload a File", "media-upload.php?referer=edit_speaker&type=image&TB_iframe=true&post_id=0", false);
};


var deleteOffer = function(id) {
    var response = confirm("Are you sure you want to delete this offer?");
    if (!response) return;

    $.post(siteUrl('registration/delete_offer'), {'id': id}, function(data) {
        if (data == 'ok') {
            parent.location = parent.location;
        } else {
            alert('error: ' + data);
        }
    });
};

var deleteAddOn = function(id) {
    var response = confirm("Are you sure you want to delete this add-on?");
    if (!response) return;

    $.post(siteUrl('registration/delete_addon'), {'id': id}, function(data) {
        if (data == 'ok') {
            parent.location = parent.location;
        } else {
            alert('error: ' + data);
        }
    });
};

var sendResumeEmail = function(key) {
    $.post(siteUrl('register/resume_email/' + key), {}, function(data) {
        if (data == 'ok') {
            alert('An email has been sent!');
        } else {
            alert('An error occurred while sending the email');
        }
    });
};

var openWindow = function(url) {
	window.open(url, '_blank');
};

var doAction = function(form_id, action) {
	if (!action) {
		action = form_id;
		form_id = $('.rb-form').first().attr('id')
	}
	$('#' + form_id + '_action').val(action);
	$('#' + form_id).submit();
};

var modal = function(id) {
	var content = $(id);
	content.detach();
	content.show();

	var wrap = $('<div/>')
	.addClass('modal-wrap');

	var el = $('<div/>')
	.addClass('modal')
	.append(content)
	.appendTo(wrap);

	$('body').append(wrap);
};

var faceSelector = function(key, type) {
	var wrap = $('#img_' + key);
	var img = $('#img_' + key + ' img');
	var input = $('#' + key + '_' + type);


	wrap.css('position', 'relative')
		.css('overflow', 'hidden')
		.css('display', 'inline-block');


	var anchor = [0, 0];
	var selecting = false;
	var rect = false;

	var htmlOffsetY = 0;
	if ($('html').css('margin-top')) {
		var len = $('html').css('margin-top');
		htmlOffsetY = parseInt(len, 10);
	}

	var frame = $('<div/>')
		.css({
			'position': 'absolute',
			'top': '0',
			'left': '0',
			'border': '1px solid red',
			'box-shadow': '-1000px -1000px 0 2000px rgba(0, 0, 0, 0.5)',
		})
		.appendTo(wrap);

	var setFrame = function(x, y, w, h) {
		frame.css('left', x + 'px')
   			.css('top', y + 'px')
   			.css('width', w + 'px')
   			.css('height', h + 'px');
	};

	var mouseClick = function(e) {
		if (selecting) {
			selecting = false;
			input.val(rect);
		} else {
			selecting = true;
			var parentOffset = wrap.offset();
			parentOffset.top += htmlOffsetY;
	   		anchor = [e.pageX - parentOffset.left, e.pageY - parentOffset.top];
   		}
	};

	var mouseMove = function(e) {
		if (selecting) {
			var parentOffset = wrap.offset();
			parentOffset.top += htmlOffsetY;
   			var focus = [e.pageX - parentOffset.left, e.pageY - parentOffset.top];

   			var x = Math.min(focus[0], anchor[0]);
   			var y = Math.min(focus[1], anchor[1]);
   			var w = Math.abs(focus[0] - anchor[0]);
   			var h = Math.abs(focus[1] - anchor[1]);
   			rect = [
   				(x / img.width()).toFixed(3),
   				(y / img.height()).toFixed(3),
   				(w / img.width()).toFixed(3),
   				(h / img.height()).toFixed(3)
   			].join(',');
   			setFrame(x, y, w, h);
		}
	};

	img.click(mouseClick);
	frame.click(mouseClick);

	img.mousemove(mouseMove);
	frame.mousemove(mouseMove);

	img.css('cursor', 'crosshair');
	frame.css('cursor', 'crosshair');

	if (input.val()) {
		var arr = input.val().split(',');
		setFrame(arr[0] * img.width(), arr[1] * img.height(), arr[2] * img.width(), arr[3] * img.height());
	}
};