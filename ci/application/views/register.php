<?php $this->template->title('Register to attend Medicine X 2015'); ?>

<?php $this->load->view('common/messages'); ?>

<?php if (!empty($restrict)): ?>
    <div class="success" style="padding:10px">
        <div>This custom offer was created exclusively for <b><?= "{$restrict['first_name']} {$restrict['last_name']} ({$restrict['email']})" ?></b>.</div>
        <div>If this is not you, you will not be able to register using this offer.</div>
    </div>
<?php endif; ?>

<div style="margin-top:20px;margin-bottom:20px">
    <?php
    $this->load->view('common/multistep', array(
        'steps' => array(
            array('title' => 'Registration Options', 'hidden' => (!$has_options || !empty($_GET['rid']))),
            array('title' => 'Registration Details'),
            array('title' => 'Attendee Information'),
            array('title' => 'Summary')
        ),
        'cur_step' => $step
    ));
    ?>
</div>

<?= $content; ?>
