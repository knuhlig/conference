<?php
$this->template->title('Speaker Response');
$this->load->view('common/messages');
?>

<!--
<?php if (empty($success)): ?>
<form method="POST" action="">
<div>
	To accept your invitation to speak at Medicine X, please click the button below.
</div>
<div style="margin-top:20px">
	<input type="hidden" name="accept" value="1"/>
	<input type="submit" class="button button-primary" value="Accept Invitation"/>
</div>
</form>
<?php endif; ?>
-->

<?php 
if (!empty($speaker)) {
	$offer = $speaker->r('registration_offer');
	if (!empty($offer)) {
		print "<h3>Conference registration</h3>";
		print "<div>Click the link below to complete your registration with the conference.</div>";
		print "<div style='margin-top:20px'><input type='button' class='button-primary' value='Complete your registration' onclick=\"parent.location='".$offer->access_url($speaker->email)."'\"/></div>";
	}
}
?>