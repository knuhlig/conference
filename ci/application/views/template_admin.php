<div class="wrap">
    <h2 style="margin-bottom:20px"><?= $title ?></h2>

    <?php $this->load->view('common/messages'); ?>

    <?= $content ?>
</div>
