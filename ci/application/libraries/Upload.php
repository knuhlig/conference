<?php

class Upload {

    public $uploads_dir;
    public $private_dir;

    public function __construct() {
        $this->uploads_dir = WPCM_UPLOADS_DIR;
        $this->private_dir = PRIVATE_DIR;
    }

    public function upload($key, $private=false) {
        $single = true;

    	if (empty($_FILES[$key])) {
            return false;
        }

        $info = $_FILES[$key];

        $upload = $this->upload_single($info, $key, $private);
		return $upload;
    }

    private function upload_multiple($info, $key, $private=false) {
        var_dump($info);
        die();

        $num_files = sizeof($info['name']);

        $names = $info['name'];
        $types = $info['type'];
        $tmp_name = $info['tmp_name'];
        $errors = $info['error'];
        $sizes = $info['size'];

        $uploads = array();
        for ($i = 0; $i < $num_files; $i++) {
            $upload = $this->do_upload($tmp_name[$i], $names[$i], $types[$i], $key, $private);
            if (!empty($upload)) {
                $uploads[$upload->id] = $upload;
            }
        }

        return $uploads;
    }

	private function upload_single($info, $key, $private=false) {
        $name = $info['name'];
        $type = $info['type'];
        $tmp_name = $info['tmp_name'];
        $error = $info['error'];
        $size = $info['size'];
        return $this->do_upload($tmp_name, $name, $type, $key, $private);
	}

    public function do_upload($tmp_file, $name, $type, $key, $private=false) {
        if (empty($tmp_file)) {
            return false;
        }

        if ($private) {
        	$dst = $this->private_dir . $name;
        } else {
        	$dst = $this->uploads_dir . $name;
        }
        $info = pathinfo($dst);

        // find an unused path name
        $basename = $info['basename'];
        $filename = $basename;
        $extension = '';
        $pos = strpos($basename, '.');
        if ($pos !== false) {
        	$filename = substr($basename, 0, $pos);
        	$extension = substr($basename, $pos);
        }
        $path = $info['dirname'] . '/' . $filename . $extension;
        $i = 0;
        while (file_exists($path)) {
            $i++;
            $path = $info['dirname'] . '/' . $filename . '-' . $i . $extension;
        }

        $info = pathinfo($path);

        // move upload into place
        if (!copy($tmp_file, $path)) {
            if (!move_uploaded_file($tmp_file, $path)) {
                return false;
            }
        }

        // file has been moved successfully
        // Create the object.
        $upload = R::dispense('upload');
        $upload->filename = $info['basename'];
        $upload->upload_date = time();
        $upload->type = $type;
        $upload->is_private = $private;

        $CI =& get_instance();
        if ($CI->auth->logged_in) {
        	$upload->owner = $CI->auth->user;
        }

        R::store($upload);
        return $upload;
    }
}