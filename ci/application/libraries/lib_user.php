<?php


class Lib_User {

    /**
     * Adds a new user to the database.
     * @param $username The user's username
     * @param $password The user's password
     * @param $first_name The user's first name
     * @param $last_name The user's last name
     * @param $email The user's email address
     * @return The new user.
     */
    public function create_user($username, $password, $first_name, $last_name, $email) {


        // first, create the barebones user
        $user_id = wp_create_user($username, $password, $email); // @wordpress
        if (is_wp_error($user_id)) {
            throw new Exception($user_id->get_error_message());
        }

        // add new data fields
        $data = array(
            'ID' => $user_id,
            'first_name' => $first_name,
            'last_name' => $last_name
        );
        wp_update_user($data); // @wordpress

        // return the user data
        return get_userdata($user_id); // @wordpress
    }

    /**
     * Fetches a specific user from the  database.
     * @param $user_id ID of the user to fetch.
     * @return The specified user, as an associative array.
     */
    public function get_user($user_id) {
        return get_userdata($user_id); // @wordpress
    }

    public function get_user_by_username($username) {
        return get_user_by('login', $username);
    }

    public function get_user_by_email($email) {
        return get_user_by('email', $email);
    }

    public function to_array($user) {
        return array(
            'id' => $user->ID,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->user_nicename,
            'email' => $user->user_email
        );
    }

    /**
     * Updates a user.
     * @param $user The user data to update.
     * @return TRUE.
     */
    public function update_user($user) {
        wp_update_user($user); // @wordpress
    }

    /**
     * Deletes the user from the database.
     * @param $user_id ID of the user to delete
     * @return TRUE.
     */
    public function delete_user($user_id) {
        wp_delete_user($user_id); // @wordpress
    }

    /**
     * Returns whether the username exists in the database.
     * @param $username username to check.
     * @return TRUE if exists, FALSE otherwise.
     */
    public function user_exists($username) {
        return username_exists($username); // @wordpress
    }
}

?>