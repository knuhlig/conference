<?php

class Lib_Validation {

    public function validate($arr, $rules) {
        foreach ($rules as $key => $rule) {
            $this->validate_rule($arr, $key, $rule);
        }
    }

    private function validate_rule($arr, $key, $rule_list) {

        foreach ($rule_list as $rule) {
            if (is_string($rule)) {
                $this->validate_builtin($arr, $key, $rule);
            } else if (is_array($rule)) {
                $this->validate_custom($arr, $key, $rule);
            }
        }
    }

    private function validate_custom($arr, $key, $rule) {
        call_user_func_array($rule, array($arr, $key));
    }

    private function validate_builtin($arr, $key, $rule) {
        $parts = explode(":", $rule);

        $cmd = $parts[0];
        $arg_str = sizeof($parts) > 1 ? $parts[1] : '';
        $args = array_merge(array(property($arr, $key)), explode(",", $arg_str), array($arr, $key));

        $fn = '_builtin_' . $cmd;
        if (method_exists($this, $fn)) {
            call_user_func_array(array($this, $fn), $args);
        } else {
            throw new Exception('The builtin function ' . $cmd . ' does not exist');
        }
    }



    /* --------| Built-In Validation Functions |------------ */

    public function _builtin_notempty($value) {
        if (empty($value)) {
            throw new Exception("cannot be empty");
        }
    }

    public function _builtin_maxlen($value, $max) {
        $str = $value . '';
        if (strlen($str) > $max) {
            throw new Exception("max length exceeded");
        }
    }

    public function _builtin_minlen($value, $min) {
        $str = $value . '';
        if (strlen($str) < $min) {
            throw new Exception("must be at least {$min} characters");
        }
    }

    public function _builtin_email($value) {
        if (!preg_match('/[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i', $value)) {
            throw new Exception("not a valid email address");
        }
    }

    public function _builtin_matches($value, $match_key, $arr, $key) {
        $match_value = property($arr, $match_key);
        if ($value != $match_value) {
            throw new Exception("must match");
        }
    }
}

?>