<?php

class Settings {
	
	private $bean;
	
	public function __construct() {
		$this->bean = R::dispense('settings');
	}
	
	public function __get($key) {
		return $this->bean->$key;
	}
	
	public function get($key, $default=null) {
		$val = $default;
		if ($this->bean->$key) {
			$val = $this->bean->$key;
		}
		return $val;
	}
}