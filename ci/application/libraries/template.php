<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Template {

    private $CI;

    private $data;
    private $base;

    function __construct() {
        $this->CI =& get_instance();
        $this->data = array(
            'title' => '[Untitled Page]'
        );
        $this->base = 'template';

		remove_action('page_title', 'mk_page_title');
        add_action('page_title', array($this, 'do_page_title'));
        add_filter('breadcrumbs_plus_items', array($this, 'get_breadcrumbs'));
    }

    public function get_breadcrumbs($item) {
    	$item[] = $this->data['title'];
    	return $item;
    }

    public function do_page_title() {
    	$shadow_css = '';
    	$title = $this->data['title'];
    	$subtitle = '';

    	/*if ( $mk_options['page_title_shadow'] == 'true' ) {
			$shadow_css = 'mk-drop-shadow';
		}*/

    	echo '<section id="mk-page-introduce" class="intro-left">';
		echo '<div class="mk-grid">';
		echo '<h1 class="page-introduce-title '.$shadow_css.'">' . $title . '</h1>';

		if ( !empty($this->data['subtitle']) ) {
			echo '<div class="page-introduce-subtitle">';
			echo $this->data['subtitle'];
			echo '</div>';
		}

		$this->do_breadcrumbs();
		echo '<div class="clearboth"></div>';

		echo '</div>';
		echo '</section>';
    }

    public function do_breadcrumbs() {
    	global $mk_options;
		$breadcrumb_skin_class = $mk_options['breadcrumb_skin'];

		$output = '';
		if ( function_exists( 'mk_breadcrumbs_plus' ) ) {
			$output = mk_breadcrumbs_plus( array(
					'prefix' => '<div class="mk-breadcrumbs-inner '.$breadcrumb_skin_class.'-skin">',
					'suffix' => '</div>',
					'title' => false,
					'home' => __( 'Home', 'mk_framework' ),
					'front_page' => false,
					'bold' => false,
					'blog' => __( 'Blog', 'mk_framework' ),
					'echo' => false,
					//'post_id' => $post_id
				) );
		}
		echo $output;
    }

    public function set_base($base) {
    	$this->base = $base;
    }

    function title($title) {
        $this->data('title', $title);
    }

    function data($key, $value) {
        $this->data[$key] = $value;
    }

    public function display_content($content) {
        $this->data['content'] = $content;
    	$this->CI->load->view('template', $this->data);
    }

    function display($view, $data=array()) {
        $content = $this->CI->load->view($view, $data, true);
        $this->data['content'] = $content;
        $this->CI->load->view($this->base, $this->data);
    }

}