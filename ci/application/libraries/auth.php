<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * A simple authentication library that integrates with the Wordpress backend.
 */
class Auth {

	public $user;
	public $logged_in;

	public function __construct() {
		$wp_id = get_current_user_id();
		if ($wp_id) {
			$this->user = R::findOne('user', 'wp_id=:wp_id', array(
				':wp_id' => $wp_id
			));
			if (!empty($this->user)) {
				$this->logged_in = true;
			}
		}
	}

	public function login($email, $password, $remember=true) {
		// find desired user
		$user = R::findOne('user', 'email=?', array($email));

		// check credentials
		if (!$user || !$user->check_password($password)) {
			throw new Exception("Invalid username/password");
		}

		// perform the login
		$this->login_user($user, $remember);
	}

	public function logout() {
		$this->logged_in = null;
		$this->user = null;
		wp_logout();
	}

	public function login_user($user, $remember=true) {
		$this->user = $user;
		$this->logged_in = true;
		wp_set_auth_cookie($user->wp_id, $remember);
	}
	
	public function is_admin() {
		$username = $this->user->username;
		return $username == 'admin';
	}

	/* -------------------------- OLD ----------------------------------- */

	private function ocs_login($username, $password, $remember=false) {
		$CI =& get_instance();

		$user = $CI->lib_user->get_user_by_username($username);
		if ($user) {
			$ocs_password = get_user_meta($user->ID, 'ocs_password', true);
			if ($ocs_password) {
				$hash = md5($username . $password);
				if ($hash == $ocs_password) {
					$this->user = $this->build_user($user);
					$this->login_user($user->ID, $remember);
					return true;
				}
			}
		}

		// nope
		throw new Exception("Invalid username/password");
	}

}
