<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages {

    private $types;

    public function __construct() {
        $this->types = array();
    }

    public function add($type, $message) {
        if (!array_key_exists($type, $this->types)) {
            $this->types[$type] = array();
        }
        $this->types[$type][] = $message;
    }

    public function getMessages($type=NULL) {
        if (is_null($type)) return $this->types;

        if (!isset($this->types[$type])) {
            return null;
        }
        return $this->types[$type];
    }

    public function getMessageTypes() {
        return array_keys($this->types);
    }
}