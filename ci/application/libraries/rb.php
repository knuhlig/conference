<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Get Redbean. Must be done outside the constructor.
include(APPPATH.'/third_party/rb/rb.php');


class Rb {

    public $connected;

    private $beanHelper;

    function __construct($config = array()) {
        // Include database configuration
        include(APPPATH.'/config/database.php');

        $this->initialize(array_merge($config, $db[$active_group]));
    }
    
    public function add_database($key, $host, $user, $pass, $db) {
		// make redbean do the major configuration
		R::addDatabase($key, "mysql:host={$host};dbname={$db}", $user, $pass);
		$toolbox = R::$toolboxes[$key];
		
		// tweak toolbox for our purposes
		$adapter = $toolbox->getDatabaseAdapter();
		
		$writer = new PrefixQueryWriter($adapter);
		$writer->setPrefix('wp_cm_');
		
		$redbean = new RedBean_OODB($writer);
		$redbean->setBeanHelper($this->beanHelper);
		
		// update the toolbox
		R::$toolboxes[$key] = new RedBean_Toolbox($redbean, $adapter, $writer);
    }

    /**
     * Initializes the RedBeanPHP library
     * @param unknown $config
     */
    public function initialize($config) {
        // Database data
        $host = $config['hostname'];
        $user = $config['username'];
        $pass = $config['password'];
        $db = $config['database'];

		if (defined('ARCHIVE_KEY')) {
			$key = ARCHIVE_KEY;
			if (!isset($config['archive_map'][$key])) {
				show_error('The archive does not exist');
			}
			$db = $config['archive_map'][$key];
		}

        if (empty($host) || empty($user) || empty($pass) || empty($db)) {
            $this->connected = false;
        } else {
            $this->beanHelper = new RbBeanHelper();
            
            
            $this->add_database('default', $host, $user, $pass, $db);
            $this->add_database('medx2013', $host, $user, $pass, 'medx2013');
            $this->add_database('medx2014', $host, $user, $pass, 'medx2014');

			R::selectDatabase('default');
			
            if (!empty($config['freeze'])) {
                R::freeze(true);
            }

            // logging
            if (!empty($config['debug']) || true) {
                $this->logger = new RbLogger();
                R::debug(true, $this->logger);
            }

            $this->connected = true;
        }

        $db = ArchiveDB::get();
        if ($db) {
            R::selectDatabase($db);
        } else {
            R::selectDatabase('default');
        }
    }

    /**
     * Searches for beans matching the given criteria.
     * @param string $type The type of bean to find
     * @param array $terms Search terms, all of which must match
     * @param array $fields Fields to search
     * @param int $limit Number of results to return
     * @param int $offset First result to return
     * @param int $sort Sort string
     * @return List of users.
     */
    public function search($type, $terms, $fields, $limit=20, $offset=0, $sort='id', $cond='1') {
        // setup inputs
        if (!is_array($terms)) $terms = array($terms);
        if (!is_array($fields)) $fields = array($fields);
        $vars = array();
        $conditions = array();

        $conditions[] = $cond;

        // build the search condition
        $idx = 0;
        foreach ($terms as $key => $word) {
            $parts = array();
            foreach ($fields as $field) {
            	$var = ':term' . ($idx++);
            	$vars[$var] = $word;
                $parts[] = '`' . $field . '` like ' . $var;
            }
            $conditions[] = '(' . join(" or ", $parts) . ')';
        }
        $searchCond = join(" and ", $conditions);

        // count the total number of results
        $count = R::count($type, $searchCond, $vars);

        // now, find the subset that we actually want
        $vars[':offset'] = $offset;
        $vars[':row_count'] = $limit;
        $results = R::find($type,
            $searchCond . '
                order by ' . $sort . '
                limit :offset, :row_count',
            $vars
        );

        // package up the result
        return array(
            'total_count' => $count,
            'results' => $results
        );
    }

    /**
     * Finds duplicate beans.
     */
    public function findDuplicates($type, $fields, $sort='id') {
        if (!is_array($fields)) $fields = array($fields);

        $parts = array();
        foreach ($fields as $field) {
            $parts[] = "t1.{$field}=t2.{$field}";
        }
        $cond = join(" and ", $parts);

        $ids = R::getCol("select t1.id from {$type} t1, {$type} t2 where t1.id != t2.id and {$cond} group by t1.id");
        return array(
            'total_count' => sizeof($ids),
            'results' => R::find($type, 'id in ('.R::genSlots($ids).') order by ' . $sort, $ids)
        );
    }
}

class PrefixQueryWriter extends RedBean_QueryWriter_MySQL {

    private $prefix;

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }

    public function prefix($type) {
        return $this->prefix . $type;
    }


    public function esc($dbStructure, $dontQuote = false) {
    	print "<div>{$dbStructure}</div>";
    }

    public function safeTable($type, $noQuotes=false) {
        return parent::safeTable($this->prefix($type), $noQuotes);
    }

}

class RbBeanHelper extends RedBean_BeanHelper_Facade {

    private $loaded;

    public function __construct() {
        $this->loaded = array();
    }

    /**
	 * Fuse connector.
	 * Gets the model for a bean $bean.
	 * Allows you to implement your own way to find the
	 * right model for a bean and to do dependency injection
	 * etc.
	 *
	 * @param RedBean_OODBBean $bean bean
	 *
	 * @return type
	 */
	public function getModelForBean(RedBean_OODBBean $bean) {
		$modelName = RedBean_ModelHelper::getModelName($bean->getMeta('type'), $bean);

		// try to automatically load the appropriate model
		if (empty($this->loaded[$modelName]) && !class_exists($modelName)) {
		    //__autoload('Model_Bean');
		    $path = APPPATH.'models/'.strtolower($modelName).'.php';
		    if (file_exists($path)) {
		        include $path;
		    }
		    $this->loaded[$modelName] = true;
		}

		if (class_exists($modelName)) {
		    $obj = RedBean_ModelHelper::factory($modelName);
            $obj->loadBean($bean);
            return $obj;
		}

        return null;
	}
}

class RbLogger implements RedBean_Logger {

    public function log() {
        $args = func_get_args();
        log_message('error', $args[0]);
        for ($i = 1; $i < sizeof($args); $i++) {
            log_message('info', json_encode($args[$i]));
        }

        if (!defined('RB_VISUALIZE')) return;
        if (strpos($args[0], 'resultset') === 0) return;
        $sql = $args[0];
        for ($i = 1; $i < sizeof($args); $i++) {
            $sql .= json_encode($args[$i]);
        }
        print "<div style='font-family:monospace;background-color:#fcc;margin:2px;padding:5px'>" . $sql . "</div>";

        return;
        $bt = array_reverse(debug_backtrace());
        print "<table cellpadding=10>";
        foreach ($bt as $t) {
            print "<tr><td>" . join("</td><td>", properties($t, array('function', 'class', 'line', 'file'))) . "</td></tr>";
        }
        print "</table>";
    }
}