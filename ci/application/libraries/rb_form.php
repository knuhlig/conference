<?php

class Form_Exception extends Exception {
	public $key;

	public function __construct($key, $msg) {
		parent::__construct($msg);
		$this->key = $key;
	}

	public function getKey() {
		return $this->key;
	}
}

class Rb_Form {

	private $errors = array();
	private $num = 0;
	public $form_id;

	public function add_error_messages($fields) {
		$CI =& get_instance();
		$CI->messages->add('error', '<b>Please correct the errors below:</b>');
		foreach ($this->errors as $key => $list) {
			$field = $fields[$key];
			$label = property($field, 'label', ucwords(join(' ', explode('_', $key))));
			foreach ($list as $msg) {
				$CI->messages->add('error', "&nbsp;&nbsp;&nbsp;$label: " . $msg);
			}
		}
	}


	/// PUBLIC API

    public function editor($bean, $fields=null, $layout=null) {
    	$this->next_form();

    	if (is_null($fields)) {
	    	$fields = $bean->editor_fields();
	    	$layout = $bean->editor_layout();
    	}

		$settings = array(
			'inline' => false,
			'prefix' => ''
		);


    	print '<form class="rb-form" id="'.$this->form_id.'" method="POST" action="?' . param_str($_GET) . '" enctype="multipart/form-data">';
    	print '<input type="hidden" name="form_action" value="save" id="'.$this->form_id.'_action"/>';

    	print '<div id="poststuff" class="metabox-holder has-right-sidebar meta-box-sortables">';

    	$this->editor_sidebar($layout, $fields, $bean);

    	print '<div id="post-body"><div id="post-body-content">';

    	$html_keys = $this->editor_layout($layout, $fields, $bean, $settings);

    	print '</div></div>';
    	print '</div>';
    	print '</form>';
    }

    public function display($fields, $layout) {
    	$bean = R::dispense('dummy');
    	$this->inline_editor($bean, $fields, $layout);
    }

    private function next_form() {
    	$this->num++;
    	$this->form_id = "common_form_" . $this->num;
    }

    public function inline_editor($bean, $fields=null, $layout=null) {
    	$this->next_form();

    	if (is_null($fields)) {
	    	$fields = $bean->editor_fields();
	    	$layout = $bean->editor_layout();
    	}

		$settings = array(
			'inline' => true,
			'prefix' => '',
			'hide_labels' => property($layout, 'hide_labels', false)
		);

    	print '<form id="'.$this->form_id.'" method="POST" action="?' . param_str($_GET) . '" enctype="multipart/form-data">';
    	print '<input type="hidden" name="form_action" value="save" id="'.$this->form_id.'_action"/>';


    	$html_keys = $this->editor_layout($layout, $fields, $bean, $settings);

		$before_buttons = property($layout, 'before_buttons');
		if (!empty($before_buttons)) {
			print "<div>{$before_buttons}</div>";
		}

    	$buttons = property($layout, 'buttons', array(
    		'submit' => 'Save Changes'
    	));
    	foreach ($buttons as $action => $label) {
    		switch ($action) {
    			case 'submit':
    				print '<input type="submit" class="button button-primary" value="'.$label.'" onclick="doAction(\''.$this->form_id.'\', \''.$action.'\')"/>';
    				break;
    			default:
    				print '<input type="button" class="button" value="'.$label.'" onclick="doAction(\''.$this->form_id.'\', \''.$action.'\')"/>';
    				break;
    		}
    		print " ";
    	}

    	print '</form>';
    }

    /**
     * Handles form input.
     * @param unknown $bean
     * @throws Exception
     * @return multitype:
     */
    public function handle($bean, $fields=null, $layout=null, $validate=true) {
    	if (is_null($fields)) {
	    	$fields = $bean->editor_fields();
	    	$layout = $bean->editor_layout();
    	}

    	$data = array();
    	foreach ($layout['sections'] as $section) {
    		$res = $this->handle_section($section, $fields, $bean, $validate);
    		$data = array_merge($data, $res);
    	}
    	return $data;
    }

    public function set_error($key, $is_error=true) {
    	if ($is_error) {
    		$this->errors[$key] = true;
    	} else {
    		unset($this->errors[$key]);
    	}
    }

	public function has_errors() {
		return !empty($this->errors);
	}

	public function add_error($key, $msg) {
		$this->errors[$key][] = $msg;
	}

    public function action() {
    	return property($_POST, 'form_action');
    }




	// HELPERS

    /* ------------- FORM HANDLING --------------- */

    /**
     * Handles a single "section" of form input.
     * @param unknown $section
     * @param unknown $fields
     * @param unknown $bean
     * @return multitype:Ambigous <NULL, number, The>
     */
    private function handle_section($section, $fields, $bean, $validate=true) {
    	$data = array();
    	foreach ($section['fields'] as $key) {
    		$data[$key] = $this->handle_field($key, $fields[$key], $bean, $key, $validate);
    	}
    	return $data;
    }

    private function get_label($id, $field) {
    	return property($field, 'label', ucwords(join(" ", explode("_", $id))));
    }

    /**
     * Handles a single "field" of form input.
     * @param unknown $key
     * @param unknown $field
     * @param unknown $bean
     * @throws Exception
     */
    private function handle_field($id, $field, $bean, $key, $validate=true) {
    	$type = property($field, 'type', 'text');
		$label = $this->get_label($id, $field);
    	$value = null;
		$is_object = false;

    	switch ($type) {
    		case 'file':
    			$is_object = true;
    			$CI =& get_instance();
    			$prev = null;
    			if (property($_POST, $id . "_id")) {
    				$prev = R::load('upload', $_POST[$id.'_id']);
    			}

    			if (property($_POST, $id . '_delete')) {
    				$value = null;
    			} else {
    				$upload = $CI->upload->upload($id, property($field, 'private'));
	    			if ($upload) {
	    				if ($prev) {
	    					//R::trash($prev);
	    				}
	    				$value = $upload;
	    			} else {
	    				if ($prev) {
	    					$prev->face = property($_POST, $id . '_face');
	    					$prev->save();
	    					$value = $prev;
	    				}
	    			}
    			}
    			break;
    		case 'relation':
    			$relation = property($field, 'relation');
    			$model = property($field, 'model', $key);
    			if ($relation == 'one') {
    				$is_object = true;
    				$tmp = $bean->fetchAs($model)->$key;
    				$r_id = property($_POST, $id);
    				if (!empty($r_id)) {
	    				$value = R::load($model, $r_id);
    				} else {
    					$value = null;
    				}
    			} else {
    				$this->handle_multi_relation($id, $field, $bean, $key);
    				return;
    			}
    			break;
    		case 'timestamp':
    			$value = strtotime(property($_POST, $id));
    			break;
    		case 'html':
    			$value = stripslashes(property($_POST, $id, ''));
    			break;
    		case 'currency':
    			$value = property($_POST, $id);
    			if (!is_null($value) && strlen($value) == 0) $value = null;
    			break;
    		case 'own_multiple':
    			$count = property($_POST, $id . '_num');
    			$model = property($field, 'model', $key);
				$fields = property($field, 'fields');
    			$ownKey = 'own' . ucfirst($key);

				$list = array();

    			for ($i = 1; $i <= $count; $i++) {
    				$prefix = $id . '_' . $i . '_';
    				if (empty($_POST[$prefix.'create'])) {
    					continue;
    				}

					$r_id = property($_POST, $prefix.'id');
					if (!empty($r_id)) {
						$r = R::load($model, $r_id);
					} else {
    					$r = R::dispense($model);
    				}
    				$rfields = $r->editor_fields();

    				foreach ($fields as $h => $m) {
                        if (is_string($m)) {
                            $rkey = $m;
                            $ff = $rfields[$rkey];
                        } else {
                            $rkey = $h;
                            $ff = $m;
                        }
    					$this->handle_field($prefix.$rkey, $ff, $r, $rkey);
    				}
    				$list[] = $r;
    			}

    			$bean->$ownKey = $list;
    			return;
            case 'checkbox':
                $value = stripslashes(property($_POST, $id, property($field, 'default')));
                break;
    		default:
    			$value = stripslashes(property($_POST, $id));
    			break;
    	}

    	if (property($field, 'ignore-empty') && empty($value)) {
    		return null;
    	}


		if ($validate) {
			// do custom validation
			if (property($field, 'validate') || property($field, 'required')) {
				try {
					if (property($field, 'required')) {
						$this->validate_field($id, $value, 'notempty', $bean);
					}
					if (property($field, 'validate')) {
						$this->validate_field($key, $value, property($field, 'validate'), $bean);
					}
				} catch (Form_Exception $e) {
					$this->errors[$e->getKey()][] = $e->getMessage();
				}
			}
    	}

    	if (property($field, 'filter')) {
    		$filter = property($field, 'filter');
    		$value = $this->filter_value($value, $filter, $bean);
    	}

    	if (property($field, 'ignore')) {
    		return null;
    	}

    	if (empty($value) && $value !== '0') {
    		$value = null;
    	}

		if ($is_object) {
			$bean->removeProperty($key);
			$id_key = "{$key}_id";
			if (is_null($value)) {
				$bean->$id_key = null;
			} else {
				$bean->$id_key = $value->id;
			}
    	} else {
    		$bean->$key = $value;
    	}
    	return $value;
    }

    private function handle_multi_relation($id, $field, $bean, $key) {
    	$count = property($_POST, $id . '_num');

    	$related_model = property($field, 'model', $key);
    	$pivot_model = $bean->pivot_type($key);

    	$fields = property($field, 'fields');
    	//$ownKey = 'own' . ucfirst($key);
    	$ownKey = 'own' . ucfirst($pivot_model);

    	$fields = array(
    		$key => array(
    			'type' => 'relation',
    			'relation' => 'one',
    			'model' => $related_model
    		)
    	);
    	$fields = array_merge($fields, property($field, 'fields', array()));
    	$bean_key = $bean->type() . "_id";
    	$ids = array();

    	for ($i = 1; $i <= $count; $i++) {
    		$prefix = $id . '_' . $i . '_';
    		$r_id = property($_POST, $prefix.'id');
    		if (empty($_POST[$prefix.'create'])) {
    			continue;
    		}

    		$r_id = property($_POST, $prefix.'id');
    		if (!empty($r_id)) {
    			//$r = R::load($model, $r_id);
    			$r = R::load($pivot_model, $r_id);
    		} else {
    			//$r = R::dispense($model);
    			$r = R::dispense($pivot_model);

    		}
    		//$rfields = $r->editor_fields();

    		foreach ($fields as $rkey => $rfield) {
    			$this->handle_field($prefix.$rkey, $rfield, $r, $rkey);
    		}

    		$r->{$bean_key} = $bean->id;
    		R::store($r);

    		$ids[] = $r->id;
    	}

    	if (!empty($ids)) {
    		$trashcan = R::find($pivot_model, "{$bean_key}=? and id not in (".join(",", $ids).")", array($bean->id));
    	} else {
    		$trashcan = R::find($pivot_model, "{$bean_key}=?", array($bean->id));
    	}
    	R::trashAll($trashcan);
    }


    private function filter_value($value, $filter, $bean, $recurse=true) {
    	if (is_array($filter) && $recurse) {
    		foreach ($filter as $item) {
    			$value = $this->filter_value($value, $item, $bean, false);
    		}
    	} else {
    		$value = call_user_func($filter, $value, $bean);
    	}

    	return $value;
    }


    /* ------------- FORM CREATION ---------------- */

    private function editor_layout($info, $fields, $bean, $settings) {
    	// multi-section
    	if (isset($info['sections'])) {
    		$html_keys = array();
    		foreach ($info['sections'] as $section) {
    			$html_keys = array_merge($html_keys, $this->editor_layout($section, $fields, $bean, $settings));
    		}

    		if (!empty($html_keys)) {
    			$key = $html_keys[0];
    			$field = $fields[$key];
    			$this->editor_field_html($key, $field, $bean, $key, $settings);
    		}
    		return $html_keys;
    	}

    	// single section
    	return $this->editor_section($info, $fields, $bean, $settings);
    }

    private function editor_section($info, $fields, $bean, $settings) {
    	if (empty($settings['inline'])) {
    		print "<div class='postbox'>";
    		print '<div class="handlediv" title="Click to toggle"><br></div>';
    		print "<h3 class='hndle'><span>".property($info, 'title', '')."</span></h3>";
    		print "<div class='inside'>";
    	} else {
    		if (isset($info['title'])) {
				print '<h4 class="section-title">' . $info['title'] . '</h4>';
			}
    	}

		$wrap_class = 'highlight-box';
		if (!empty($settings['inline'])) {
			//$wrap_class = '';
		}

    	print "<div class=\"{$wrap_class}\">";

    	// fields
    	$html_keys = array();
    	print '<div class="form">';
    	foreach ($info['fields'] as $key) {
    		if (!isset($fields[$key])) {
    			throw new Exception("No field is defined with key {$key}");
    		}
    		if (!$this->editor_row($key, $fields[$key], $bean, $key, $settings)) {
    			$html_keys[] = $key;
    		}
    	}
    	print '</div>';
    	print '</div>';

    	if (empty($settings['inline'])) {
    		print "</div>";
    		print "</div>";
    	}
    	return $html_keys;
    }

    private function editor_sidebar($layout, $fields, $bean) {
    	$type = $bean->getMeta('type');

    	print '<div id="side-info-column" class="inner-sidebar">';
    	print '<div class="meta-box-sortables ui-sortable" id="side-sortables">';
    	print '<div class="postbox" id="submitdiv">';
    	print '<h3 class="hndle"><span>Options</span></h3>';
    	print '<div class="inside">';
    	print '<div class="submitbox" id="submitpost">';

    	print '<div id="misc-publishing-actions">';

    	$bean->editor_options();

    	if ($bean->id) {
    		print '<div class="misc-pub-section">Object ID: <b>'.$bean->id.'</b></div>';
    	} else {
    		print '<div class="misc-pub-section">Status: <b>Unsaved</b></b></div>';
    	}
    	if ($bean->created) print '<div class="misc-pub-section">Created: <b>'.date(DATE_FMT_FULL, $bean->created).'</b></div>';
    	if ($bean->last_updated) print '<div class="misc-pub-section">Last Updated: <b>'.date(DATE_FMT_FULL, $bean->last_updated).'</b></div>';
    	print '<div class="misc-pub-section misc-pub-section-last">Data type: <b>'.$bean->type_label().'</b></div>';
    	print '<div class="clear"></div>';
    	print '</div>';

    	print '<div id="major-publishing-actions">';
    	print '<div id="delete-action">';
    	if ($bean->id) {
    		print '<a href="javascript:deleteObject(\''.$type.'\', '.$bean->id.')" class="submitdelete deletion">Delete</a>';
    	}
    	print '</div>';
    	print '<div id="publishing-action">';
    	print '<input class="button button-primary" type="submit" value="Save Changes" onclick="(function(){ $(\'#'.$this->form_id.'_action\').val(\'save\'); $(\'#'.$this->form_id.'\').submit() })()"/>';
    	print '</div>';
    	print '<div class="clear"></div>';
    	print '</div>';

    	print '</div>';
    	print '</div>';
    	print '</div>';
    	print '</div>';
    	print '</div>';
    }

    private function editor_buttons() {
    	print '<div>';
    	print '</div>';
    }

    private function editor_row($id, $field, $bean, $key, $settings) {
    	$type = property($field, 'type', 'text');

    	// hack for tinymce
    	if ($settings['inline'] && $type == 'html') {
    		$type = 'textarea';
    	}

    	if ($type == 'html') {
    		return false;
    	}

    	$row_class = property($field, 'row_class', 'row');
    	if (!empty($this->errors[$id])) {
    		$row_class .= ' form-error';
    	}
    	print '<div class="'.$row_class.'" id="row_'.$id.'">';

    	// field label
    	$label = property($field, 'label', ucwords(join(' ', explode('_', $key))));
    	$label_class = '';
    	if (property($field, 'required')) {
    		$label_class = 'required';
    	}

    	$label_ref = $id;
    	if ($type == 'checkbox') {
    		$label_ref = 'cb_' . $id;
    	}

    	if (empty($settings['hide_labels'])) {
    		print '<div class="label"><label for="'.$label_ref.'" class="'.$label_class.'">'.$label.'</label></div>';
    		print '<div class="value">';
    	} else {
    		print "<div>";
    	}
    	// field value
    	$this->editor_field($id, $field, $bean, $key, $settings);
    	if (!empty($this->errors[$id])) {
    		$msg = $this->errors[$id][0];
    		print '<div class="error-msg">'.$msg.'</div>';
    	}
    	print '</div>';

    	print '</div>';
    	return true;
    }

    private function editor_field($id, $field, $bean, $key, $settings) {
    	$type = property($field, 'type', 'text');

    	if ($type == 'html') {
    		$type = 'rt';
    	}

    	$fn = 'editor_field_' . $type;

    	// make sure we can handle it
    	if (!method_exists($this, $fn)) {
    		throw new Exception("Unknown field type '{$type}'");
    	}

    	// delegate
    	$this->$fn($id, $field, $bean, $key, $settings);

    	$desc = property($field, 'description');
    	if (!empty($desc)) {
    		print "<span class='description'>{$desc}</span>";
    	}

    	$deps = property($field, 'deps');
    	if (!empty($deps)) {
    		$parts = array();
    		$parts[] = "<script>";
    		$parts[] = "jQuery(function() {";
    		$parts[] = "var checkMe = function() { ";
    		foreach ($deps as $dep) {
    			$parts[] = 'var el = $("#'.$dep[0].'");';
    			$parts[] = 'var val= el.is(":checkbox") ? el.is(":checked") : el.val();';
    			$parts[] = 'if (!(val '.$dep[1].' "'.$dep[2].'")) {$("#row_'.$key.'").hide(); return;}';
    		}
    		$parts[] = '$("#row_'.$key.'").show()';
    		$parts[] = "};";
    		foreach ($deps as $dep) {
    			$parts[] = '$("#'.$dep[0].'").change(checkMe);';
    		}
    		$parts[] = "checkMe();";
    		$parts[] = "})";
    		$parts[] = "</script>";
    		print join("", $parts);
    	}
    }



    /* ------------- FIELD DELEGATES ------------------------ */

    private function editor_field_relation($id, $field, $bean, $key, $settings) {
    	$type = property($field, 'relation');

    	if ($type == 'one') {
    		$this->editor_relation_single($id, $field, $bean, $key, $settings);
    	} else {
    		$this->editor_relation_multiple($id, $field, $bean, $key, $settings);
    	}
    }

    private function editor_relation_single($id, $field, $bean, $key, $settings) {
    	// one
    	$model = property($field, 'model', $key);
    	$sort = property($field, 'sort', 'id asc');
    	$cond = property($field, 'cond', '1');
		$search = property($field, 'search', false);

		$selected = null;
		if (!is_null($bean)) {
			$id_field = $key . "_id";
			$selected = $bean->$id_field;
		}

		if ($search) {
			$this->bean_search($id, $key, $field, $selected);
		} else {
	    	$dummy = R::dispense($model);
	    	$beans = R::find($model, "{$cond} order by {$sort}");
	    	$dummy->prepare_results($beans);



			$placeholder = property($field, 'placeholder', 'None');
			$rich = property($field, 'rich', false);
			$nested = property($settings, 'nested', false);
	    	$this->bean_selector($id, $beans, $selected, $placeholder, $rich, $nested);
		}
    }

    private function bean_search($id, $key, $field, $selected=null) {
    	$model = property($field, 'model', $key);
    	$sort = property($field, 'sort', 'id asc');
    	$cond = property($field, 'cond', '1');
		$search = property($field, 'search', false);

    	print '<input type="hidden" id="'.$id.'" name="'.$id.'" value="'.$selected.'"/>';

		$rand = random_key(6);
		$style = 'width:300px';
		print '<div style="position:relative" id="'.$id.'_container">';

		$placeholder_style = "display:" . ($selected ? "block" : "none");
		$wrap_style = "display:" . (!$selected ? "block" : "none");

		print "<div id=\"{$id}_searchwrap\" style=\"{$wrap_style}\">";
		print '<input type="text" style="'.$style.'" id="'.$id.$rand.'_search" placeholder="Search..."/>';
		print '</div>';

		print "<div class='bean-placeholder' id=\"{$id}_placeholder\" style=\"{$placeholder_style}\">";
		if (!empty($selected)) {
			$bean = R::load($model, $selected);
			print '<div class="bean-result">';

			$url = $bean->thumbnail_url('tiny');
			if ($url) {
				print '<img src="'.$url.'"/>';
			}
			print '<div class="result-label">'.$bean->short_label().'</div>';
			if ($bean->short_desc()) {
				print '<div class="result-desc">'.$bean->short_desc().'</div>';
			}
			print "</div>";
		}
		print '</div>';

		print "<div class='bean-results' id='{$id}_results'></div>";
		print "</div>";
		print "<script>
			jQuery(function() {
				var el = \$('#{$id}{$rand}_search');
				var searchWrap = \$('#{$id}_searchwrap');
				var resultsEl = \$('#{$id}_results');
				var placeholder = \$('#{$id}_placeholder');

				var attachClick = function(item, value) {
					item.click(function(e) {
						\$('#{$id}').val(value);
						resultsEl.hide();
						searchWrap.hide();
						placeholder.empty();
						placeholder.append(item.clone());
						placeholder.show();
					});
				};

				var processResults = function(data) {
					resultsEl.empty();
					var results = \$.parseJSON(data);
					for (var key in results) {
						var result = results[key];
						var rEl = \$('<div class=\"bean-result\"/>');
						if (result.thumb) {
							rEl.append('<img src=\"'+result.thumb+'\"/>');
						}
						rEl.append('<div class=\"result-label\">' + result.label + '</div>');
						if (result.desc) {
							rEl.append('<div class=\"result-desc\">' + result.desc + '</div>');
						}
						attachClick(rEl, result.id);
						resultsEl.append(rEl);
					}
					resultsEl.show();
				};

				var doSearch = function() {
					var params = {
						search: el.val(),
						cond: '{$cond}',
						type: '{$model}'
					};
					$.post('".ci_url('admin/ajax_search')."', params, processResults);
				};

				el.keydown(function(e) {
					if (e.which != 13) {
						return;
					}
					doSearch();
					e.stopPropagation();
					return false;
				});

				placeholder.click(function(e) {
					placeholder.hide();
					el.val('');
					searchWrap.show();
				});

				\$('#{$id}_container').click(function(e) {
					e.stopPropagation();
					return false;
				});

				\$(document).click(function(e) {
					var parents = \$(e).parents();
				    if(parents.index(\$('#{$id}_container')) == -1) {
				        if (\$('#{$id}').val()) {
				        	searchWrap.hide();
				        	placeholder.show();
				        } else {
				        	searchWrap.show();
				        	placeholder.hide();
				        }
				    }
				});
			});
		</script>";
    }

    private function multi_selector($id, $fields, $related, $settings) {
    	$related = array_values($related);

    	$num = sizeof($related);

    	print "<input type='hidden' id='{$id}_num' name='{$id}_num' value='{$num}'/>";
		print "<div id='{$id}_list'>";

		$prefix = $settings['prefix'];
		for ($i = 0; $i <= $num; $i++) {
			if ($i == 0) {
				$extrastyle = 'display:none';
				$r = R::dispense('dummy');
			} else {
				$extrastyle = '';
				$idx = $i - 1;
				if ($idx < sizeof ($related)) {
					$r = $related[$idx];
				} else {
					$r = R::dispense('dummy');
				}
			}

			ob_start();
			print "<div class='inline-relation' style='{$extrastyle}'><div id='{$id}_{$i}'>";
			print "<input type='hidden' name='{$id}_{$i}_create' value='1'/>";
			print "<input type='hidden' name='{$id}_{$i}_id' value='{$r->id}'/>";
			print "<div class='relation-toolbar relation-options'>";
			print "<i class='relation-remove fa fa-minus-square' data-num='{$i}' onclick=\"relationRemove(this,'{$id}')\"></i>";
			print "</div>";
			foreach ($fields as $fkey => $field) {
				$settings ['nested'] = true;
				$f_id = $id . '_' . $i . '_' . $fkey;
				$this->editor_row($f_id, $field, $r, $fkey, $settings);
			}
			print "</div></div>";
			$content = ob_get_clean();

			if ($i == 0) {
				$content = str_replace('script', 'tempscript', $content);
			}

			print $content;
		}
		print "</div>";

		print "<div class='relation-toolbar'>";
		print "<i id='{$id}_plus_btn' class='fa fa-plus-square'></i> Click the plus button to add {$settings['button_label']}.";
		print "<script>
    	    \$('#{$id}_plus_btn').click(function() {
    	    	var c = \$('.inline-relation', \$('#{$id}_list')).first().clone();
    	    	var num = parseInt(\$('#{$id}_num').val()) + 1;

    	    	\$('#{$id}_num').val(num);
    	    	var prev = c.html();
    	    	var next = prev
    	    		.replace(/{$id}_0/g, '{$id}_' + num)
    	    		.replace(/tempscript/g, 'script');
    	    	c.html(next);
    	    	c.show();
    			\$('#{$id}_list').append(c);
    		});
    	</script>";
		print "</div>";

		print "<script>
    	var relationRemove = function(el, key) {
    	var list = \$('#'+key+'_list');
    	if (\$('.inline-relation', list).size() == 1) {
    	return;
    	}
    			var item = \$(el).parent().parent().parent();
    			item.remove();
    		};
    	</script>";
	}

    private function editor_field_own_multiple($id, $field, $bean, $key, $settings) {
    	// get desired fields
    	$model = property($field, 'model');

    	$desired_fields = property($field, 'fields');
		
        $dummy = R::dispense($model);
		$all_fields = $dummy->editor_fields();

        $fields = array();		
        foreach ($desired_fields as $k => $v) {
            if (is_string($v)) {
                $fkey = $v;
                $f = $all_fields[$v];
            } else {
                $fkey = $k;
                $f = $v;
            }
            $fields[$fkey] = $f;
        }

        $settings['button_label'] = strtolower(property($field, 'label', $dummy->type_label()));

    	// get related
		$ownKey = 'own' . ucfirst($model);
		$related = $bean->$ownKey;
		if (empty($related)) {
			$related = array();
		} else {
			$related = array_values($related);
		}

		$this->multi_selector($id, $fields, $related, $settings);
    }

    private function editor_relation_multiple($id, $field, $bean, $key, $settings) {
    	// one
    	$model = property($field, 'model', $key);
    	$sort = property($field, 'sort', 'id asc');
    	$cond = property($field, 'cond', '1');

		$pivot_type = $bean->pivot_type($key);
		$ownKey = 'own' . ucfirst($pivot_type);

		$related = $bean->$ownKey;
    	$fields = array(
    		$key => array(
    			'type' => 'relation',
    			'relation' => 'one',
    			'model' => $model,
    			'cond' => property($field, 'cond', '1'),
    			'sort' => property($field, 'sort', 'id asc'),
    			'rich' => property($field, 'rich', false)
    		)
    	);
    	$fields = array_merge($fields, property($field, 'fields', array()));
    	$this->multi_selector($id, $fields, $related, $settings);
    }

    private function editor_field_rt($id, $field, $bean, $key, $settings) {
    	$toolbar_id = $id . '_toolbar';
    	$value = $bean->$key;

		print "<div class='rt-wrap'>";
		print '<div><div id="'.$toolbar_id.'" class="rt-toolbar" style="display: none;">
			  <i class="fa fa-bold" data-wysihtml5-command="bold"></i><!--
		   --><i class="fa fa-italic" data-wysihtml5-command="italic"></i><!--
		   --><i class="fa fa-underline" data-wysihtml5-command="underline"></i><!--
		   --><i class="fa fa-link" data-wysihtml5-command="createLink"></i><!--
		   --><i class="fa fa-picture-o" data-wysihtml5-command="insertImage"></i><!--
		   --><i class="fa fa-list-ul" data-wysihtml5-command="insertUnorderedList"></i><!--
		   --><i class="fa fa-list-ol" data-wysihtml5-command="insertOrderedList"></i>

			  <div data-wysihtml5-dialog="createLink" style="display: none" class="rt-dialog">
				<label>
				  <input data-wysihtml5-dialog-field="href" value="http://" class="text" style="width:200px">
				</label>
				<i class="fa fa-check-circle-o" data-wysihtml5-dialog-action="save"></i>
				<i class="fa fa-times-circle" data-wysihtml5-dialog-action="cancel"></i>
			  </div>
			  <div data-wysihtml5-dialog="insertImage" style="display: none" class="rt-dialog">
				<label>
				  <input data-wysihtml5-dialog-field="src" value="http://" class="text" style="width:200px">
				</label>
				<i class="fa fa-check-circle-o" data-wysihtml5-dialog-action="save"></i>
				<i class="fa fa-times-circle" data-wysihtml5-dialog-action="cancel"></i>
			  </div>
			</div></div>';
		print '<textarea class="rt" id="'.$id.'" name="'.$id.'" placeholder="" style="visibility:hidden;width:100%;box-sizing:border-box;'.property($field, 'style').'">'.$value.'</textarea>' . "\n";
		print "</div>";
		print '<script>
			var editor = new wysihtml5.Editor("'.$id.'", { // id of textarea element
			  toolbar:      "'.$toolbar_id.'", // id of toolbar element
			  parserRules:  wysihtml5ParserRules // defined in parser rules set
			});
			</script>';
    }

    private function editor_field_dropdown($id, $field, $bean, $key, $settings) {
    	$opts = property($field, 'opts', array());
    	$selected = property($bean, $key, property($field, 'default'));
    	$this->dropdown($id, $opts, $selected);
    }

    private function editor_field_text($id, $field, $bean, $key, $settings) {
    	$value = htmlentities(property($bean, $key, property($field, 'default')));

    	$default_style = 'width:100%';
    	$style = property($field, 'style', $default_style);

    	print '<input type="text" style="'.$style.'" name="'.$id.'" id="'.$id.'" value="'.$value.'"/>';
    }

    private function editor_field_currency($id, $field, $bean, $key, $settings) {
    	print '$ ';
    	$this->editor_field_text($id, $field, $bean, $key, $settings);
    }

    private function editor_field_password($id, $field, $bean, $key, $settings) {
    	$value = property($bean, $key, '');
    	$style = property($field, 'style', 'width:300px');
    	print '<input type="password" style="'.$style.'" name="'.$id.'" id="'.$id.'"/>';
    }

    private function editor_field_html($id, $field, $bean, $key, $settings) {
    	$value = property($bean, $key, property($field, 'default'));
    	$desc = property($field, 'description');

    	print '<input type="hidden" name="'.$id.'" id="'.$id.'"/>';

    	if (!empty($desc)) {
    		print '<div style="font-weight:bold">'.$desc.'</div>';
    	}

    	print '<div style="" id="postdivrich" class="postarea">';
    	the_editor(stripslashes($value));
        print '</div>';
    	print "<script>jQuery(function() { $('#".$this->form_id."').submit(function() { switchEditors.go('content', 'tinymce'); $('#{$id}').val(tinyMCE.activeEditor.getContent()) })});</script>";

    }

    private function editor_field_textarea($id, $field, $bean, $key, $settings) {
    	$value = stripslashes(property($bean, $key, property($field, 'default')));
    	$style = property($field, 'style', '');
    	print '<textarea name="'.$id.'" id="'.$id.'" style="'.$style.'">'.$value.'</textarea>';
    }

    private function editor_field_checkbox($id, $field, $bean, $key, $settings) {
    	$value = property($bean, $key, property($field, 'default'));

    	print '<input type="checkbox" name="'.$id.'" id="'.$id.'"'.($value ? 'checked="checked"' : '').' value="1"/>';
	}

    private function editor_field_file($id, $field, $bean, $key, $settings) {
		// fetch the file
    	$upload = $bean->fetchAs('upload')->$key;

    	$has_face = !property($field, 'no_face');

    	if ($upload) {
			print "<input type='hidden' name='" . $id . "_face' id='" . $id . "_face' value=\"".$upload->face."\"/>";
			print "<input type='hidden' name='" . $id . "_delete' id='" . $id . "_delete' value=\"0\"/>";
			print "<input type='hidden' name='" . $id . "_id' value=\"".$upload->id."\"/>";

    		if ($upload->is_image()) {
				list($iw, $ih) = $upload->image_size();
				if ($ih > 300) {
					$sc = 300 / $ih;
					$ih *= $sc;
					$iw *= $sc;
				}
				if ($iw > 400) {
					$sc = 400 / $iw;
					$ih *= $sc;
					$iw *= $sc;
				}

				print "<div style='float:right;text-align:right'>";
				if ($upload->face) {
					print "<script>jQuery(function() { faceSelector('".$id."', 'face') })</script>";
				} else {
					if ($has_face) print "<div><a href='javascript:void(0)' onClick=\"faceSelector('".$id."', 'face')\"/>Select Face</a></div>";
				}
				print "<div><a href='javascript:void(0)' onclick=\"(function(){ $('#".$id."_delete').val(1); doAction('{$this->form_id}', 'save') })()\" class='warn'>Delete Photo</a></div>";
				print "</div>";

				print "<div><div id='img_".$id."'><img id='" . $id . "_image' style='width:{$iw}px;height:{$ih}px' src='".$upload->thumbnail_url('full')."'/></div></div>";
    		} else {
				print "
				<div style='padding: 10px 0'>
					<img src='".$upload->icon_url()."'/>
					<a target='_blank' href='".$upload->url()."'>".$upload->filename."</a>
					<a style='margin-left:10px' href='javascript:void(0)' onclick=\"(function(){ $('#".$id."_delete').val(1); doAction('{$this->form_id}', 'save') })()\" class='warn'>Delete</a>
				</div>";
    		}
    	}

    	// print the field
    	if (!empty($bean->id)) {
    		print "<input type='file' name='{$id}' onchange=\"doAction('{$this->form_id}', 'save')\"/>";
    	} else {
    		print "<input type='file' name='{$id}'/>";
    	}
    }

    private function editor_field_timestamp($id, $field, $bean, $key, $settings) {
    	$time = property($bean, $key, property($field, 'default', time()));
        if ($time) {
		  $value = date(DATE_FMT_FULL, $time);
        } else {
          $value = '';
        }

    	print "<input type='text' name='{$id}' id='{$id}' value='".$value."'/>";
    	print "<script>
    		jQuery(function() {
    			\$('#{$id}').datepicker();
    		});
    	</script>";
    }




    /* ----------------------- VALIDATION ---------------------------- */

    public function validate_field($key, $value, $validate, $bean, $recurse=true) {
    	if (is_array($validate) && $recurse) {
    		// multiple
    		foreach ($validate as $item) {
    			$this->validate_field($key, $value, $item, $bean, false);
    		}
    	} else if (is_string($validate)) {
    		// builtin
    		$fn = 'validate_' . $validate;
    		$this->$fn($key, $value, $bean);
    	} else {
    		// custom callback
    		call_user_func($validate, $key, $value, $bean);
    	}

    	return true;
    }

    private function validate_confirm($key, $value, $bean) {
    	$confirm = property($_POST, $key . '_confirm');
    	if ($value != $confirm) {
    		throw new Form_Exception($key, "Passwords must match");
    	}
    }

    private function validate_notempty($key, $value, $bean) {
    	if (empty($value)) {
    		throw new Form_Exception($key, "required");
    	}
    }

    private function validate_email($key, $email, $bean) {
    	$isValid = true;
    	$atIndex = strrpos($email, "@");
    	if (is_bool($atIndex) && !$atIndex) {
    		$isValid = false;
    	} else {
    		$domain = substr($email, $atIndex+1);
    		$local = substr($email, 0, $atIndex);
    		$localLen = strlen($local);
    		$domainLen = strlen($domain);
    		if ($localLen < 1 || $localLen > 64) {
    			// local part length exceeded
    			$isValid = false;
    		} else if ($domainLen < 1 || $domainLen > 255) {
    			// domain part length exceeded
    			$isValid = false;
    		} else if ($local[0] == '.' || $local[$localLen-1] == '.') {
    			// local part starts or ends with '.'
    			$isValid = false;
    		} else if (preg_match('/\\.\\./', $local)) {
    			// local part has two consecutive dots
    			$isValid = false;
    		} else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
    			// character not valid in domain part
    			$isValid = false;
    		} else if (preg_match('/\\.\\./', $domain)) {
    			// domain part has two consecutive dots
    			$isValid = false;
    		} else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
    			// character not valid in local part unless
    			// local part is quoted
    			if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
    				$isValid = false;
    			}
    		}
    		if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
    		// domain not found in DNS
    			$isValid = false;
    		}
    	}

    	if (!$isValid) {
    		throw new Form_Exception($key, "please enter a valid email address");
    	}
    }


	/* ------------------------ COMMON FUNCTIONS -------------------- */

    private function dropdown($id, $opts, $selected=null, $placeholder=null) {
    	print '<select name="'.$id.'" id="'.$id.'">';

    	if (!is_null($placeholder)) {
    		print '<option value="">' . $placeholder . '</option>';
    	}

    	foreach ($opts as $value => $label) {
    		$selection = '';
    		if ($value == $selected && strlen($value) == strlen($selected)) $selection = ' selected';
    		print '<option'.$selection.' value="'.$value.'"">'.$label.'</option>';
    	}
    	print '</select>';
    }

    private function bean_selector($id, $beans, $selected=null, $placeholder=null, $rich=false) {
    	print '<select name="'.$id.'" id="'.$id.'" style="width:400px">';
    	if (!is_null($placeholder)) {
    		print '<option value="">' . $placeholder . '</option>';
    	}

    	foreach ($beans as $bean) {
    		$selection = '';
    		if ($bean->id == $selected) $selection = ' selected';
    		if ($rich) {
    			print '<option'.$selection.' value="'.$bean->id.'" data-imagesrc="'.$bean->thumbnail_url('inch').'" data-description="'.$bean->short_desc().'">'.$bean->short_label().'</option>';
    		} else {
    			print '<option'.$selection.' value="'.$bean->id.'">'.$bean->short_label().'</option>';
    		}
    	}
    	print '</select>';
  		// make it rich
  		print '<script> jQuery(function() { $("#'.$id.'").richDropdown(); });</script>';
    }

    /**
	 * Returns a timestamp value.
	 * @param unknown $input
	 * @param unknown $key
	 * @return number
	 */
    private function get_timestamp(&$input, $key) {
        $m = property($input, $key . '_month');
        $d = property($input, $key . '_day');
        $y = property($input, $key . '_year');
        $h = property($input, $key . '_hour');
        $min = property($input, $key . '_minutes', '00');
        $ampm = property($input, $key . '_ampm', 'am');

        if ($ampm == 'pm') {
            $h += 12;
        }

        $str = "{$y}-{$m}-{$d} {$h}:{$min}";
        return strtotime($str);
    }


	private static function months() {
		return array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
	}

	private static function hours() {
		return array(
			0 => '12',
			1 => '01',
			2 => '02',
			3 => '03',
			4 => '04',
			5 => '05',
			6 => '06',
			7 => '07',
			8 => '08',
			9 => '09',
			10 => '10',
			11 => '11',

		);
	}

}
