<?php

class Base_Model extends RedBean_SimpleModel {

	public function __construct() {}

	/**
	 * Relation map, stores property => model pairs.
	 * @var unknown
	 */
	protected $relation_map = array();

	/**
	 * Relation searcher.
	 * @param unknown $key
	 * @param unknown $args
	 */
	public function r($key) {
		if (isset($this->relation_map[$key])) {
			$model = $this->relation_map[$key];
			return $this->bean->fetchAs($model)->$key;
		}
		return $this->bean->$key;
	}

	public function editor_options() {}

	public function export_data() {
		return $this->bean->export();
	}

	public function clearAssociation($type) {
		$ctype = $this->pivot_type($type);
		$type_id = $this->type() . '_id';
		$own_key = 'own' . ucfirst($ctype);

		$beans = R::find($ctype, "{$type_id} = ?", array($this->id));
		R::trashAll($beans);

		$this->bean->$own_key = array();
	}

	public function associated($type, $with_data=false, $cond='1') {
		$fields = $this->editor_fields();
		$ctype = $this->pivot_type($type);
		$own_key = 'own' . ucfirst($ctype);
		$pivot = $this->bean->$own_key;
		$type_id = $type . '_id';

		if ($with_data) {
			$arr = array();
			foreach ($pivot as $bean) {
				if (!empty($bean->$type_id)) {
					$arr[] = $bean;
				}
			}
			return $arr;
		}


		$ids = array();
		foreach ($pivot as $bean) {
			if (!empty($bean->$type_id)) {
				$ids[] = $bean->$type_id;
			}
		}

		if (empty($ids)) return array();

		$id_tuple = "(" . join(",", $ids) . ")";
		$sql = "id in {$id_tuple} and {$cond}";

		if (isset($fields[$type])) {
			$field = $fields[$type];
			$type = property($field, 'model', $type);
		}
		$arr = R::find($type, $sql);
		return $arr;
	}

	public function pivot_type($type) {
		if (strcmp($this->type(), $type) > 0) {
			$ctype = $type . $this->type();
		} else {
			$ctype = $this->type() . $type;
		}
		return $ctype;
	}

	public function associate($type, $id, $data=array()) {
		// order consistently in both directions
		$ctype = $this->pivot_type($type);

		$obj = R::dispense($ctype);
		foreach ($data as $k => $v) {
			$obj->$k = $v;
		}

		$type_key = $type . "_id";
		$obj->$type_key = $id;

		$own_key = 'own' . ucfirst($ctype);
		$this->bean->{$own_key}[] = $obj;
		return $obj;
	}

	public function save() {
		R::store($this->bean);
	}

	public function browse_url() {
		$type = $this->bean->getMeta('type');
		return '?page=browse_' . $type;
	}



	public function is_unique($key, $value) {
		$count = 0;
		$type = $this->bean->getMeta('type');
		if ($this->id) {
			$count = R::count($type, "{$key} = :value and id != :id", array(
				':value' => $value,
				':id' => $this->id
			));
		} else {
			$count = R::count($type, "{$key} = :value", array(
				':value' => $value
			));
		}

		return $count == 0;
	}

	/**
	 * Called before the bean is updated in the database.
	 */
	public function update() {
		// set a creation time
		if (!$this->id || !$this->created) {
			$this->created = time();
		}

		// set the last updated time
		$this->last_updated = time();
	}

	public function type() {
		return $this->bean->getMeta('type');
	}

	/**
	 * Returns a human readable version of this object's type.
	 * @return string
	 */
	public function type_label() {
		return ucfirst($this->unbox()->getMeta('type'));
	}

	/**
	 * Returns a human readable plural version of this object's type.
	 * @return string
	 */
	public function pluralize_type() {
		return $this->type_label() . 's';
	}

	/**
	 * Returns the editor layout for this model.
	 * Meant to be overridden in subclasses.
	 */
	public function editor_layout() {
		return array(
			'title' => 'Information',
			'fields' => array_keys($this->editor_fields())
		);
	}

	/**
	 * Returns the editable fields for this model.
	 * Meant to be overridden in subclasses.
	 */
	public function editor_fields() {
		$fields = array();
		foreach ($this->bean as $key => $value) {
			$fields[$key] = array(
				'type' => 'text',
				'style' => 'width:200px'
			);
		}
		return $fields;
	}

	public function human_readable($key, $default='') {
		$fields = $this->editor_fields();
		$field = $fields[$key];
		$opts = property($field, 'opts', array());

		$value = $default;
		if (isset($opts[$this->$key])) {
			$value = $opts[$this->$key];
		}
		return $value;
	}

	/**
	 * Configures a browsing table for this model.
	 * @param Display_Table $tbl
	 */
	public function configure_table(Display_Table $tbl) {
	}

	public function prepare_results($items, $tbl=null) {
		$item = reset($items);

		if (!is_null($tbl) && empty($tbl->columns)) {
			$cols = array();
			$fields = $item->editor_fields();
			foreach ($fields as $key => $info) {
				$cols[$key] = ucfirst($key);
			}
			$tbl->columns = $cols;
		}
	}

	public function table_id($key) {
		return $this->$key . $this->controls();
	}


	/**
	 * Returns a short text label for this bean.
	 */
	public function short_label() {
		return $this->type_label . '(' . $this->id . ')';
	}

	/**
	 * Returns a short text description for this bean.
	 * @return string
	 */
	public function short_desc() {
		return '';
	}

	/**
	 * Returns the URL of a thumbnail image for this bean.
	 * @param string $size thumb|tiny
	 */
	public function thumbnail_url($size='thumb') {}


	public function controls($hover=true) {
		$type = $this->bean->getMeta('type');

		$cls = $hover ? 'hovershow' : '';

		$parts = array();
		$parts[] = "<div class='{$cls}' style='float:right'>";
		$parts[] = "<a href='?page=edit_{$type}&id={$this->id}'>Edit</a>";
		$parts[] = ' <a href="javascript:deleteObject(\''.$type.'\', '.$this->id.')" class="warn">Delete</a>';
		$parts[] = "</div>";

		return join("", $parts);
	}

	public function table_photo($key) {
		$photo = $this->r($key);
		if (!$photo) {
			return "<span class='warn'>no photo</span>";
		}
		return "<img src='".$photo->thumbnail_url('tiny') . "'/>";
	}


	/* --------- REDBEAN EXTENSIONS ------------------- */


	/**
	 * Preloads related beans. Relations are specified as an ordered
	 * array. Examples:
	 *   $types = array('location', 'speaker');
	 *   $types = array('location', 'photo@upload');
	 *   $types = array('speaker.photo@upload');
	 *   $types = array('speaker@user.photo@upload');
	 * @param unknown $items A set of beans to preload.
	 * @param unknown $types Relations to load
	 */
	public function preload($items, $types) {
		$preloads = self::parse_preloads($types);

		foreach ($preloads as $path) {

			$ids = array();
			foreach ($items as $item) {
				$arr = self::collect_id($item, $path);

				if (!empty($arr)) {
					$id = $arr[0];
					$obj = $arr[1];
					$ids[$id] = true;
				}
			}

			if (empty($ids)) {
				continue;
			}

			$type = end($path);
			if (isset($obj->box()->relation_map[$type])) {
				$type = $obj->box()->relation_map[$type];
			}


			$batch = R::batch($type, array_keys($ids));
			self::apply_batch($items, $batch, $path);

		}
	}

	private static function parse_preloads($types) {
		if (!is_array($types)) {
			$types = explode(',', $types);
		}

		$preloads = array();
		foreach ($types as $str) {
			$path = explode('.', $str);
			$preloads[] = $path;
		}

		return $preloads;
	}

	private static function collect_id($item, $path) {
		// follow the chain until last property
		$obj = $item;
		for ($i = 0; $i < sizeof($path) - 1; $i++) {
			$key = $path[$i];
			$key_id = $key . '_id';

			// broken chain, no such item
			if (!$obj->$key_id) {
				return null;
			}

			$obj = $obj->r($key);
		}

		$last = end($path);
		$last_id = $last . '_id';

		$id = $obj->$last_id;
		if (empty($id)) return null;
		return array($id, $obj);
	}



	private static function apply_batch($items, $batch, $path) {
		// re-key batch
		$batch_items = array();
		foreach ($batch as $batch_item) {
			$batch_items[$batch_item->id] = $batch_item;
		}

		foreach ($items as $item) {
			$set = true;

			// follow the chain until last property
			$obj = $item;
			for ($i = 0; $i < sizeof($path) - 1; $i++) {
				$key = $path[$i];
				$key_id = $key . '_id';

				// broken chain, no such item
				if (!$obj->$key_id) {
					$set = false;
					break;
				}

				$obj = $obj->$key;
			}

			if (!$set) {
				continue;
			}

			$last = end($path);
			$last_id = $last . '_id';

			$item_id = $obj->$last_id;
			if (isset($batch_items[$item_id])) {
				$obj->setProperty($last, $batch_items[$item_id]);
			}
		}
	}


}