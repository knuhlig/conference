<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

    /**
     * Sets up the basic controller and performs initialization tasks.
     * @param int $access_level The minimum level of access for the controller.
     */
    public function __construct($access_level=0)
    {
        parent::__construct();

        /*
        // load database and check connection
        $this->load->library('rb');
        if (!$this->rb->connected)
        {
            show_error(lang('error.db_not_connected'));
        }

        // load other libraries
        $this->load->library('lib_user', null, 'user');
        */
        $this->load->helper(array('url', 'cybersource'));
        $this->load->library(array('rb', 'auth', 'template', 'messages', 'lib_user', 'lib_validation', 'upload', 'settings'));

        R::dependencies(array(
        	'addonrate' => array('rate', 'addon'),
        	'addonoffer' => array('addon', 'offer'),
        	'addonorder' => array('addon', 'order'),
        	'offerrate' => array('offer', 'rate')
        ));
/*
        // security checks
        $this->auth->initialize();
        $this->auth->checkAccess($access_level);

        // other initial tasks
        //$this->_recordPageview();*/
    }

    /**
     * Records a pageview in the database.
     */
    private function _recordPageview()
    {
        $b = R::dispense('pageview');
        $b->request_time = $_SERVER['REQUEST_TIME'];
        if ($user = $this->auth->currentUser())
        {
            $b->user_id = $user->id;
        }
        if (isset($_SERVER['HTTP_REFERER']))
        {
            $b->referer = $_SERVER['HTTP_REFERER'];
        }
        $b->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $b->remote_addr = $this->input->ip_address();
        $b->request_uri = $_SERVER['REQUEST_URI'];
        R::store($b);
    }

    protected function _api_header() {
        header('Content-type: text/json');
    }

    protected function _api_response($data) {
        print json_encode($data);
    }

    protected function _require_login($dst='user/login', $msg='') {
        if (!$this->auth->logged_in) {
            redirect(ci_url($dst, array(
            	'r' => ci_url($_SERVER['REQUEST_URI']),
            	'm' => base64_encode($msg)
            )));
            exit();
        }
    }
    
    protected function _require_admin() {
    	$this->_require_login();
    	if (!$this->auth->is_admin()) {
    		show_error("This page is restricted.");
    	}
    }
}