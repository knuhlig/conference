<?php

class Display_Table {

    public $columns;
    public $sortable_columns;
    public $items;
    public $callback;
    public $page_size;
    public $default_sort;
    public $default_dir;
    public $customize;
    public $filters;
    public $filter_conds;

    public $rb_model;
    public $rb_cond;
    public $rb_fields;

    public function __construct($rb_model=null, $rb_cond='1', $rb_fields='id') {
        $this->columns = array();
        $this->sortable_columns = array();
        $this->items = array();
        $this->page_size = 25;
        $this->default_sort = 'id';
        $this->default_dir = 'asc';
        $this->rb_model = $rb_model;
        $this->rb_cond = $rb_cond;
        $this->rb_fields = $rb_fields;
        $this->filters = array();
    }

    public function set_params($arr) {
        $params = array();
        $params['search'] = property($arr, 'search', '');
        $params['offset'] = property($arr, 'offset', 0); // offset
        $params['count'] = $this->page_size; // limit
        $params['sort'] = property($arr, 'sort', $this->default_sort);
        $params['dir'] = property($arr, 'dir', $this->default_dir);
        $params['filter'] = property($arr, 'filter', 'all');
        $this->params = $params;
    }

    private function rb_search($search, $offset, $count, $sort, $dir, $filter) {
        $CI =& get_instance();
        $terms = explode(" ", $search);
        foreach ($terms as &$term) {
            $term = $term . '%';
        }
        $sort_cond = "{$sort} {$dir}";

        $cond = $this->rb_cond;
        if (!empty($this->filter_conds[$filter])) {
        	$cond = $this->filter_conds[$filter];
        }

        return $CI->rb->search($this->rb_model, $terms, $this->rb_fields, $count, $offset, $sort_cond, $cond);
    }
    
    public function fetch_results() {
    	if (!$this->callback) {
    		$res = call_user_func_array(array($this, 'rb_search'), $this->params);
    	} else {
    		$res = call_user_func_array($this->callback, $this->params);
    	}
    	return $res;
    }

    public function display() {

    	if ($this->customize) {
    		call_user_func_array($this->customize, array($this));
    	}

    	$this->set_params($_GET);

        if (!$this->callback) {
            $res = call_user_func_array(array($this, 'rb_search'), $this->params);
        } else {
            $res = call_user_func_array($this->callback, $this->params);
        }

        $this->items = $res['results'];
        $this->count = $res['total_count'];

        if (!empty($this->items)) {
        	$keys = array_keys($this->items);
        	$item = $this->items[$keys[0]];
        	$item->prepare_results($this->items, $this);
        }

        $this->print_search();
        $this->print_pagination();
        print "<table class='display-table'>";
        $this->print_header();
        $this->print_rows();
        print "</table>";
    }

    public function print_search() {

        $search = htmlentities($this->params['search']);
        print "<div class='search'>";
        print "<form method='GET' action='' id='tblform'>";

        if (!empty($this->filters)) {
        	print "<select name='filter' onchange=\"$('#tblform').submit()\">";
        	foreach ($this->filters as $f => $l) {
        		$sel = '';
        		if ($f == property($_GET, 'filter')) {
        			$sel = ' selected';
        		}
        		print "<option{$sel} value='{$f}'>{$l}</option>";
        	}
        	print "</select>";
        }

        print "<input type='text' name='search' value='{$search}' style='width:300px'/>";
        foreach ($_GET as $key => $value) {
            if ($key == 'search' || $key == 'offset' || $key == 'filter') continue;
            $value = htmlentities($value);
            print "<input type='hidden' name='{$key}' value='{$value}'/>";
        }
        print "<input type='submit' class='button' value='Search'/>";
        print "</form>";
        print "</div>";
    }

    public function print_pagination() {
        if ($this->count == 0) {
            return;
        }

        $page = floor($this->params['offset'] / $this->page_size) + 1;
        $num_pages = ceil($this->count / $this->page_size);

        $prev_offset = max(0, $this->params['offset'] - $this->page_size);
        $next_offset = min($this->page_size * ($num_pages - 1), $this->params['offset'] + $this->page_size);
        $last_offset = $this->page_size * ($num_pages - 1);

        print "<div class='pagination'>";
        print "<span style='margin-right:10px'>{$this->count} items</span>";

        print "<a href='".$this->pagination_url(0)."'>&laquo;</a>";
        print "<a href='".$this->pagination_url($prev_offset)."'>&lsaquo;</a>";

        print "<input type='text' value='{$page}' style='margin-left:12px' disabled='disabled'/>";
        print "<span style='margin-right:12px;margin-left:3px'>of {$num_pages}</span>";

        print "<a href='".$this->pagination_url($next_offset)."'>&rsaquo;</a>";
        print "<a href='".$this->pagination_url($last_offset)."'>&raquo;</a>";

        print "</div>";
        print "<div class='spacer'></div>";
    }



    public function print_rows() {
        if (empty($this->items)) {
            $this->print_empty();
        } else {
            $i = 0;
            foreach ($this->items as $item) {
                $this->print_row($i++, $item);
            }
        }
    }

    public function print_empty() {
        print "<tr class='empty'>";
        $num_cols = sizeof($this->columns);
        print "<td colspan='{$num_cols}'>No items found</td>";
        print "</tr>";
    }

    public function print_row($i, $item) {
        print "<tr class='".($i % 2 == 0 ? 'even' : 'odd')." {$i}'>";
        foreach ($this->columns as $key => $column) {
            $this->print_cell($key, $item);
        }
        print "</tr>";
    }

    public function print_cell($key, $item) {
        $align = 'left';
        $col = $this->columns[$key];
        if (is_array($col) && sizeof($col) >= 3) {
            $align = $col[2];
        }
        print "<td style='text-align:{$align}'>";

        $method = 'table_' . $key;
        $custom = $item->$method($key);
        if (!empty($custom)) {
       		$value = $custom;
        } else if (isset($item->$key)) {
            $value = $item->$key;
        } else {
            $value = "";
        }

        print $value;

        print "</td>";
    }

    public function print_header() {
        print "<tr>";
        foreach ($this->columns as $key => $column) {
            $this->print_column_header($key, $column);
        }
        print "</tr>";
    }

    public function print_column_header($key, $column) {
        $width = '';
        if (is_array($column)) {
            $label = $column[0];
            $width = $column[1];
        } else {
            $label = $column;
        }

        print "<th ".(!empty($width) ? "style='width:{$width}'" : "").">";
        if (isset($this->sortable_columns[$key])) {
            print "<a href='".$this->sort_url($key)."' class='sorter'>" . $label . "</a>";
        } else {
            print $label;
        }
        print "</th>";
    }

    public function pagination_url($offset) {
        $params = array_merge($_GET, array(
        	'filter' => $this->params['filter'],
            'sort' => $this->params['sort'],
            'dir' => $this->params['dir'],
            'search' => $this->params['search'],
            'offset' => $offset
        ));
        return "?" . param_str($params);
    }

    public function sort_url($key) {
        $val = $this->sortable_columns[$key];
        if (is_bool($val)) {
            $sort = $key;
        } else if (is_string($val)) {
            $sort = $val;
        }

        $search = $this->params['search'];
        $dir = $this->params['dir'];
        if ($sort == $this->params['sort']) {
            // reverse dir
            $dir = $this->params['dir'] == 'asc' ? 'desc': 'asc';
        }

        $params = array_merge($_GET, array(
            'sort' => $sort,
            'dir' => $dir,
            'search' => $search,
        	'filter' => $this->params['filter']
        ));
        return "?" . param_str($params);
    }

}

?>