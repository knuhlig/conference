<?php

class Admin extends Base_Controller {

	public function __construct() {
		parent::__construct();
		$this->_require_admin();

		$this->load->library('rb_form');
	}

	/**
	 * Displays an admin browsing interface for the specified object type.
	 * @param string $type
	 */
	public function browse($type) {
		$tbl = new Display_Table($type, '1', 'id');

		$bean = R::dispense($type);
		$bean->configure_table($tbl);

		if (!empty($_GET['export'])) {
			$beans = R::findAll($type, "order by {$tbl->default_sort} {$tbl->default_dir}");
			$this->export_beans($beans);
		}

		$this->load->view('admin/browse', array(
			'title' => $bean->pluralize_type(),
			'model' => $type,
			'tbl' => $tbl
		));
	}

	public function ajax_search() {
		$type = property($_POST, 'type');

		$tbl = new Display_Table($type, '1', 'id');
		$dummy = R::dispense($type);
		$dummy->configure_table($tbl);
		$tbl->page_size = 10;
		$tbl->rb_cond = property($_POST, 'cond', '1');

		$tbl->set_params($_POST);
		$res = $tbl->fetch_results();

		$data = array();
		foreach ($res['results'] as $bean) {
			$data[] = array(
				'id' => $bean->id,
				'label' => $bean->short_label(),
				'desc' => $bean->short_desc(),
				'thumb' => $bean->thumbnail_url('tiny')
			);
		}
		print json_encode($data);
	}

	/**
	 * Edits a specific object. The object ID should be passed
	 * in as a GET variable, e.g. $_GET['id']. If no ID is specified,
	 * a new object will be created upon saving.
	 * @param string $type
	 */
	public function edit($type) {
		// load the original bean
		$bean = $this->load_requested_bean($type);

		if (!empty($_POST) && empty($_POST['no_data'])) {
			// update the bean from form data
			$store = $this->rb_form->handle($bean);
		}

		if (!$this->rb_form->has_errors()) {
			if ($this->rb_form->action() == 'save') {
				$new = !$bean->id;

				try {
					// push changes to database
					$bean->save();
					
					if ($new) {
					$_GET['id'] = $bean->id;
					}

					// add a success message
					$browse_url = $bean->browse_url();
					$this->messages->add(
						'success',
						'Changes were saved successfully. <a href="'.$browse_url.'">Return to List</a>'
					);
				} catch (Exception $e) {
					$this->messages->add('error', $e->getMessage());
				}
				
			} else {
				$action = $this->rb_form->action();
				if (empty($action) && !empty($_GET['action'])) {
					$action = $_GET['action'];
					unset($_GET['action']);
				}
				$fn = 'action_' . $action;
				try {
					$res = $bean->$fn();
					if ($res) {
						return;
					}
				} catch (Exception $e) {
					$this->messages->add('error', $e->getMessage());
				}
			}
		} else {
			$fields = $bean->editor_fields();
			$this->rb_form->add_error_messages($fields);
		}

		// display the editor
		$data = array(
			'model' => $type,
			'bean' => $bean,
			'inline' => !empty($_GET['inline'])
		);
		$this->load->view('admin/editor', $data);
	}

	/**
	 * Deletes a single object. The object ID should be passed in as
	 * a GET parameter, e.g. $_GET['id'].
	 * @param string $type
	 */
	public function delete($type) {
		// make sure an id is specified
		if (empty($_POST['id'])) {
			show_error('No object ID was specified');
		}

		// load the bean
		$bean = $this->load_requested_bean($type, true);
		if (!$bean || !$bean->id) {
			show_error('No object was found with the specified ID.');
		}

		// delete the bean
		R::trash($bean);
		print 'ok';
	}

	public function email($type) {
		// load the original bean
		$bean = $this->load_requested_bean($type);


		$template = R::findOne('template', 'slug=?', array($type . '-email'));
		$email = R::dispense('email');

		$email->to = $bean->template_data();
		$email->is_template = 1;
		$email->subject = $template->title;
		$email->body = $template->body;
		$email->save();

		redirect("wp-admin/admin.php?page=edit_email&action=preview&id=" . $email->id);
		exit();
	}

	public function download() {
		$path = property($_GET, 'path');
		if (empty($path)) {
			show_error('no path given');
		}
		if (!file_exists($path)) {
			show_error("file not found");
		}


		//$type = mime_content_type($path);
		header('Content-type: application/pdf');// . $type);
		header('Content-disposition: attachment; filename="'.basename($path).'"');

		disable_ob();
		readfile($path);
	}

	public function event_summary() {
		$id = property($_GET, 'id');
		$e = R::load('event', $id);

		$attendees = $e->get_attendees();

		$data = array(
			'event' => $e,
			'attendees' => $attendees
		);
		$this->load->view('admin/event_summary', $data);
	}

	/**
	 * Loads and returns the requested bean.
	 * @param string $type
	 * @param boolean $post True if ID comes from POST
	 * @return bean
	 */
	private function load_requested_bean($type, $post=false) {
		// input source
		$input = ($post ? $_POST : $_GET);

		// load existing bean?
		$id = property($input, 'id');
		if (!empty($id)) {
			return R::load($type, $id);
		}

		// create a new bean
		return R::dispense($type);
	}


	public function install_connections() {
		$c = R::dispense('connection');
		R::store($c);
		R::trash($c);
		print "OK";
	}

	public function identity() {
		if (!empty($_POST['user'])) {
			$id = $_POST['user'];
			$user = R::load('user', $id);
			$this->auth->login_user($user);
			redirect(ci_url('user/home'));
			die();
		}

		$fields = array(
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
				'sort' => 'last_name asc, first_name asc',
				'search' => true
			)
		);
		$layout = array(
			'sections' => array(
				array(
					'fields' => array('user')
				)
			),
			'buttons' => array(
				'submit' => 'Assume Identity'
			)
		);
		$data = array(
			'fields' => $fields,
			'layout' => $layout
		);
		$this->template->set_base('template_admin');
		$this->template->display('admin/identity', $data);
	}

	/* ------------------------------------------------------------ */

	public function verify_orders() {

		if (!empty($_POST)) {
			$id = $_POST['order_id'];
			if (!empty($id)) {
				$order = R::load('order', $id);
				$user = $order->r('user');

				if (!$order->is_verified) {
	    			$order->is_verified = 1;
					$order->save();
					$this->messages->add('success', 'Information updated');

					// send email
					$tpl = R::findOne('template', 'slug=:slug', array(':slug' => 'registration-verified'));
					$email = $tpl->create_email(array(
						'first' => $user->first_name
					));
					$email->to = $user->contact_email();
					//$email->send();
				}
			}
		}

		$sql = "select o.*
	    			from wp_cm_order o, wp_cm_rate r
	    			where o.rate_id=r.id and
    					o.paid and r.verification != 'none' and not o.is_verified
	    			order by o.date_paid asc";
		$rows = R::getAll($sql);
		$orders = R::convertToBeans('order', $rows);

		$data['orders'] = $orders;
		$this->load->view('admin/verify_orders', $data);
	}
	
	private function registration_stats($dbKey='default') {
		if ($dbKey != 'default') {
			R::selectDatabase($dbKey);
		}
		
		$orders = R::find('order', 'paid order by date_paid desc');
		$addons = R::find('addon', 'not coalesce(master_class, 0) order by name asc');
		$classes = R::find('addon', 'master_class order by name asc');
		$rates = R::find('rate', '1 order by name asc');
		
		$d = array(
			'total_revenue' => 0,
			'num_orders' => 0,
			'paid_orders' => 0,
			'registration_revenue' => 0,
			'addons' => array(),
			'rates' => array(),
			'dates' => array()
		);

		for ($i = 0; $i <= 366; $i++) {
			$d['dates'][$i] = array(
				'num_orders' => 0,
				'revenue' => 0
			);
		}

		foreach ($addons as $addon) {
			$d['addons'][$addon->id] = array('addon' => $addon, 'revenue' => 0, 'count' => 0);
		}
		foreach ($classes as $addon) {
			$d['classes'][$addon->id] = array('class' => $addon, 'revenue' => 0, 'count' => 0);
		}
		foreach ($rates as $rate) {
			$d['rates'][$rate->id] = array('rate' => $rate, 'revenue' => 0, 'count' => 0);
		}


		foreach ($orders as $order) {
			$rate = $order->r('rate');
			$day_of_year = date('z', $order->date_paid) + 1;
			$price = $order->total_price();

			$d['all_orders']++;
			if ($rate && !$rate->exclude_total) {
				$d['num_orders']++;
				$d['dates'][$day_of_year]['num_orders']++;
				
				if ($order->registration_price > 0 && !$rate->exclude_badges) {
					$d['paid_orders']++;
				}
			}

			$d['total_revenue'] += $price;
			$d['registration_revenue'] += $order->registration_price;
			$d['dates'][$day_of_year]['revenue'] += $price;


			$addons = $order->associated('addon', true);
			foreach ($addons as $bean) {
				$addon = $bean->addon;
				if (isset($d['addons'][$addon->id])) {
					$d['addons'][$addon->id]['count'] += $bean->quantity;
					$d['addons'][$addon->id]['revenue'] += $bean->quantity * $bean->unit_price;
				}
				if (isset($d['classes'][$addon->id])) {
					$d['classes'][$addon->id]['count'] ++;
				}
			}

			$rate = $order->rate;
			if ($rate && isset($d['rates'][$rate->id])) {
				$d['rates'][$rate->id]['count']++;
				$d['rates'][$rate->id]['revenue'] += $order->registration_price;
			}
		}

		$total = 0;
		$revenue_total = 0;
		for ($i = 0; $i <= 366; $i++) {
			$cur = $d['dates'][$i]['num_orders'];
			$total += $cur;
			$revenue_total += $d['dates'][$i]['revenue'];
			$d['dates'][$i]['cumulative'] = $total;
			$d['dates'][$i]['cumulative_revenue'] = $revenue_total;
		}
		
		if ($dbKey != 'default') {
			R::selectDatabase('default');
		}
		return $d;
	}

	public function registration_overview() {
		if (!empty($_POST['form_action'])) {
			$orders = R::find('order', 'paid order by date_paid desc');
			$this->export_beans($orders);
		}
		

		$tbl = new Display_Table('order');
		$dummy = R::dispense('order');
		$dummy->configure_table($tbl);
		$tbl->default_sort = 'last_updated';
		$tbl->default_dir = 'desc';
		$tbl->page_size = 5;

		$d = $this->registration_stats();
		$d['addon_revenue'] = $d['total_revenue'] - $d['registration_revenue'];
		$d['recent_orders'] = $tbl;
		$d['goal'] = $this->settings->get('registration_goal', 100);
		$d['paid_goal'] = $this->settings->get('paid_goal', 100);
		
		$d2 = $this->registration_stats('medx2013');
		
		$offset = 44;
		
		foreach ($d['dates'] as $key => $arr) {
			$cmp_key = $key + $offset;
			$arr2 = property($d2['dates'], $cmp_key, array());
			$comparison = property($arr2, 'cumulative', 0);
			
			$d['dates'][$key]['cumulative_compare'] = $comparison;
			$d['dates'][$key]['revenue_compare'] = property($arr2, 'cumulative_revenue', 0);
		}
		
		$this->load->view('admin/registration_overview', $d);
	}

	public function addon_summary() {
		$id = $_GET['id'];
		$addon = R::load('addon', $id);

		$beans = $addon->associated('order', true);

		if (!empty($_POST['form_action']) || !empty($_GET['action'])) {
			$csv = array();
			foreach ($beans as $bean) {
				$order = $bean->order;
				if (!$order->paid) continue;

				if ($addon->master_class) {
					$arr = array(
						'Response' => stripslashes($bean->response)
					);
				} else {
					$arr = array(
						'Waitlist?' => ($bean->waitlist ? 'Waitlist' : ''),
						'Addon Price' => usd($bean->unit_price * $bean->quantity)
					);
				}
				$arr = array_merge($arr, $bean->order->export_data());
				$csv[] = $arr;
			}
			$this->export_data($csv);
		}

		$data = array();
		
		$data['addon'] = $addon;
		$data['rows'] = $beans;


		$event = $addon->event;
		if (!empty($event)) {
			$attendees = $event->get_attendees();
			$arr = array();
			foreach ($attendees as $attend) {
				$arr[$attend->user_id] = $attend;
			}
			$data['event'] = $event;
			$data['attendees'] = $arr;
		}

		$this->load->view('admin/addon_summary', $data);
	}

	private function array2csv(array $array) {
		if (count($array) == 0) {
			return null;
		}
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($array)));
		foreach ($array as $row) {
			fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}

	private function is_numeric($arr) {
		foreach ($arr as $k => $v) {
			if (!is_numeric($k)) return false;
		}
		return true;
	}

	private function export_beans($beans) {
		$csv = array();
		foreach ($beans as $bean) {
			$row = $bean->export_data();
			if (is_null($row)) {
				continue;
			}

			if ($this->is_numeric($row)) {
				$csv = array_merge($csv, $row);
			} else {
				$csv[] = $row;
			}
		}
		$this->export_data($csv);
	}

	private function export_data($arr) {
		$filename = "data.csv";

		// clear output buffering
		while (ob_get_level()) ob_end_clean();

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		print $this->array2csv($arr);
		exit();
	}

	public function rate_summary() {
		$id = $_GET['id'];

		$rate = R::load('rate', $id);
		$beans = $rate->withCondition('paid order by date_paid asc')->ownOrder;

		$action = $this->rb_form->action();
		if (!empty($action)) {
			if ($action == 'export') {
				$this->export_beans($beans);
			}
		}

		$data = array();
		$data['rate'] = $rate;
		$data['orders'] = $beans;
		$this->load->view('admin/rate_summary', $data);
	}

	public function upload_sync() {
		foreach (R::findAll('upload') as $u) {
			if (!$u->file_exists()) {
				print "<div><b>" . $u->filename . "</b></u>";
			} else {
				//print "<div><i>" . $u->filename . "</u>";
			}
		}
	}

	public function archive() {
		if (!empty($_POST)) {
			$key = $_POST['archive_key'];
			$_SESSION['archive_key'] = $key;
			$this->messages->add('success', 'You are now browsing the archives.');
		}
		$this->load->view('admin/archive');
	}
}