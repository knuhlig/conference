<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Base_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('rb_form');
	}

	public function register($id=null) {
		$u = R::dispense('user');
		$redirect = property($_GET, 'r', ci_url("user/home"));

		if (property($_GET, 'm')) {
			$msg = base64_decode($_GET['m']);
			$this->messages->add('success', $msg);
			unset($_GET['m']);
		}

		// fields and layot
		$fields = $u->editor_fields();
		$required = array(
			'email', 'password', 'first_name', 'last_name', 'title', 'country', 'photo');
		foreach ($required as $key) {
			$fields[$key]['required'] = true;
			unset($fields[$key]['ignore-empty']);
		}
		$layout = array(
			'sections' => array(
				array(
					'title' => 'Account Information',
					'fields' => array(
						'email', 'username', 'password', 'password_confirm')
				),
				array(
					'title' => 'Basic Information (required)',
					'fields' => array(
						'first_name', 'last_name', 'title', 'country', 'photo')
				),
				array(
					'title' => 'Additional Information (optional)',
					'fields' => array(
						'salutation', 'twitter_username', 'website', 'phone', 'fax', 'mailing_address', 'interests', 'biography')
				),

			),
			'buttons' => array(
				'submit' => 'Create Account'
			)
		);

		if (!empty($_POST)) {
			$store = $this->rb_form->handle($u, $fields, $layout);
			if (!$this->rb_form->has_errors()) {
				R::store($u);
				$this->auth->login_user($u, true);
				redirect($redirect);
				exit();
			}
			$this->rb_form->add_error_messages($fields);
		}

		// show the relevant content
		$data = array(
			'user' => $u,
			'fields' => $fields,
			'layout' => $layout
		);
        $this->template->set_base('template_nosidebar');
        $this->template->display('user/register', $data);
	}

	/**
	 * Displays the user homepage.
	 */
	public function home() {
		$this->_require_login();
		$user = $this->auth->user;

		$data = array();
		$data['user'] = $user;

		$res = R::find('order', 'user_id=:id and paid', array(':id' => $user->id));
		if ($res) {
			$data['orders'] = $res;
		}

		$res = R::find('submission', 'user_id=:id or id in (select submission_id from wp_cm_author where user_id=:id) order by submission_date asc', array(':id' => $user->id));
		if ($res) {
			$data['submissions'] = $res;
		}
		
		$data['completed_reviews'] = R::find('review', 'user_id=? and completed=1', array($user->id));
		$data['pending_reviews'] = R::find('review', 'user_id=? and not coalesce(completed, 0)', array($user->id));

        $this->template->set_base('template_nosidebar');
		$this->template->display('user/home', $data);
	}

	/**
	 * Allows a user to edit their profile.
	 */
	public function edit_profile() {
		$this->_require_login();
		$user = $this->auth->user;

		$fields = $user->editor_fields();
		$layout = array(
			'sections' => array(
				array(
					'title' => 'Basic Information',
					'fields' => array(
						'photo', 'first_name', 'last_name', 'email',
						'title', 'country')
				),
				array(
					'title' => 'Additional Information',
					'fields' => array(
						'twitter_username', 'website', 'interests', 'biography')
				),
				array(
					'title' => 'Contact Information',
					'fields' => array('phone', 'fax', 'mailing_address')
				)
			)
		);

		$was_complete = $user->is_complete();

		if (!empty($_POST)) {
			// update the bean from form data
			$store = $this->rb_form->handle($user, $fields, $layout);

			if (!$this->rb_form->has_errors()) {
				// push changes to database
				$user->save();
				$is_complete = $user->is_complete();

				if (!$was_complete && $is_complete) {
					$this->messages->add('success', 'Your profile is now complete. Thank you! <a href="'.ci_url('user/home').'">User Home</a>');
				} else {
					$this->messages->add('success', 'Your profile was updated successfully! <a href="'.ci_url('user/home').'">User Home</a>');
				}
			} else {
				$this->messages->add('error', 'Please correct errors in the form below.');
			}
		}



		$data = array();
		$data['user'] = $user;
		$data['fields'] = $fields;
		$data['layout'] = $layout;
		$this->template->display('user/edit_profile', $data);
	}

	/**
     * Shows the login form.
     */
    public function login() {

        $redirect = property($_GET, 'r', ci_url('user/home'));

        // redirect to user homepage if already logged in.
        if ($this->auth->logged_in) {
            redirect($redirect);
            exit();
        }

        // do the login
        if (!empty($_POST)) {
            try {
                // login as the user, or throw exception
                $this->auth->login(
                    property($_POST, 'email'),
                    property($_POST, 'password'),
					property($_POST, 'remember')
                );

                redirect($redirect);
                exit();
            } catch (Exception $e) {
                $this->messages->add('error', $e->getMessage());
            }
        }

        // show the login form.
		$this->template->display('user/login');
    }

    public function logout() {
    	$this->auth->logout();
    	redirect(ci_url("user/login"));
    	exit();
    }

    /**
     * Provides an interface for updating a user password. If the user is already
     * logged in, no access key is required.
     * @param string $key The access key (from reset email)
     */
    public function new_password($key=null) {
    	if (!is_null($key)) {
	    	$user = $this->find_reset_user($key);
    	} else {
    		if (!$this->auth->logged_in) {
    			show_error("Must be logged in.");
    		}
    		$user = $this->auth->user;
    	}

    	$data = array();

    	// update the password
    	if (!empty($_POST['password'])) {
    		$pw = property($_POST, 'password', '');
    		$pw2 = property($_POST, 'password_confirm', '');
    		if ($pw != $pw2) {
    			$this->messages->add('error', 'Passwords do not match');
    		} else {
    			$user->set_password($pw);
    			R::store($user);
    			$this->auth->login_user($user);
    			$data['reset_success'] = true;
    		}
    	}

    	$this->template->display('user/new_password', $data);
    }

    private function find_reset_user($key) {
    	// find the matching credentials
    	$bean = R::findOne('pwreset', 'reset_key=:key', array(
    		':key' => $key
    	));

    	// check validity
    	if (!$bean) {
    		show_error("The key is not valid");
    	}
    	if ($bean->is_expired()) {
    		show_error("Sorry, this link has expired. <a href='".ci_url('user/lost_password')."'>Click Here</a> to generate a new email.");
    	}

    	// update the bean
    	$bean->last_used = time();
    	R::store($bean);

    	return $bean->user;
    }

    public function lost_password() {
		$data = array();

    	if (!empty($_POST['email'])) {
    		$email = $_POST['email'];
    		$user = R::findOne('user', 'email=:email', array(
    			':email' => $email
    		));
    		if (!$user) {
    			$this->messages->add('error', 'The email address is not associated with any user account.');
    		} else {
    			$this->send_reset_email($user);
    			$data['email_sent'] = true;
    			$data['email'] = $email;
    		}
    	}
    	$this->template->display('user/reset_password', $data);
    }

    private function send_reset_email($user) {
    	$bean = $user->generate_reset_key();

    	$contact_email = $this->settings->contact_email;

    	$data = array(
    		'first_name' => $user->first_name,
    		'last_name' => $user->last_name,
    		'reset_link' => $bean->url(),
    		'contact_email' => $contact_email,
    	);

    	$tpl = R::findOne('template', 'slug=:slug', array(':slug' => 'password-reset'));
    	$msg = $tpl->create_email($data);
    	$msg->to = $user->contact_email();
    	$msg->send();

    }





    /* ------------- OLD ------------------------ */



    public function load_users() {
        $contents = file_get_contents('/tmp/ocs_users.sql');
        $lines = explode("\n", $contents);

        foreach ($lines as $line) {
            $line = trim($line);
            if (empty($line)) continue;
            $cols = explode("\t", $line);

            $data = array();
            $data['ID'] = $cols[0];
            $data['username'] = $cols[1];
            $data['password'] = $cols[2];
            $data['first_name'] = $cols[3];
            $data['last_name'] = $cols[4];
            $data['email'] = $cols[5];

            try {
                $user = $this->lib_user->create_user(
                    $data['username'],
                    $data['password'],
                    $data['first_name'],
                    $data['last_name'],
                    $data['email']);

                update_user_meta($user->ID, 'ocs_password', $data['password']);
                print "created: " . $data['first_name'] . ' ' . $data['last_name'] . '<br/>';
            } catch (Exception $e) {
                print "error: " . $data['first_name'] . ' ' . $data['last_name'] . ': ' . $e->getMessage() . '<br/>';
            }
           // print sizeof($cols) . "\n";
            //print $line . "\n";
        }
    }



    public function reset() {
        if (!empty($_POST['submit'])) {
            if (!empty($_POST['username'])) {
                $username = $_POST['username'];
                die($username);
            } else if (!empty($_POST['email'])) {

            }
        }
        $this->template->display('user/reset');
    }

    /**
     * API function: creates a new user.
     */
    public function api_create_user() {
        $this->_api_header();

        try {
            // validate the form input
            $this->lib_validation->validate($_POST, array(
                'username' => array('notempty', 'maxlen:16', array($this, '_validate_username')),
                'first_name' => array('notempty', 'maxlen:20'),
                'last_name' => array('notempty', 'maxlen:20'),
                'email' => array('email'),
                'password' => array('minlen:6', 'matches:confirm_password')
            ));

            // create the new user
            $user = $this->lib_user->create_user(
                $_POST['username'],
                $_POST['password'],
                $_POST['first_name'],
                $_POST['last_name'],
                $_POST['email']
            );

            // login as the new user
            $this->auth->login_user($user->ID);

            // print the success response
            $this->_api_response(array(
                'status' => 'ok',
                'redirect' => property($_POST, 'redirect', ci_url('user/home'))
            ));
        } catch (Exception $e) {
            // error response
            $this->_api_response(array(
                'status' => 'error',
                'message' => $e->getMessage()
            ));
        }
    }

    /**
     * API function: updates an existing user.
     */
    public function api_update_user() {
        $this->_api_header();

        try {
            // validate the form input
            $this->lib_validation->validate($_POST, array(
                'id' => array('notempty'),
                'first_name' => array('notempty', 'maxlen:20'),
                'last_name' => array('notempty', 'maxlen:20'),
                'email' => array('email')
            ));

            // create the new user
            $user = $this->lib_user->update_user(array(
                'ID' => $_POST['id'],
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                'user_email' => $_POST['email']
            ));

            // print the success response
            $this->_api_response(array(
                'status' => 'ok',
                'redirect' => property($_POST, 'redirect', ci_url('user/home'))
            ));
        } catch (Exception $e) {
            // error response
            $this->_api_response(array(
                'status' => 'error',
                'message' => $e->getMessage()
            ));
        }
    }

    /**
     * Checks that the username is not already taken.
     * @param $arr The array containing the username
     * @param $key The array key to check.
     */
    public function _validate_username($arr, $key) {
	    $item = property($arr, $key);
	    if ($this->lib_user->user_exists($item)) {
	        throw new Exception('username is already taken');
	    }
	}

	public function ocs() {
		$this->load->library('Upload');

		$rows = R::getAll("
			select
			    u.username,
			    u.password,
			    u.first_name,
			    u.last_name,
			    u.email,
			    u.twitter_username,
			    if(r.date_paid,1,0) as returner,
			    u.country,
			    u.url,
			    u.phone,
			    u.fax,
			    u.mailing_address,
			    s1.setting_value as profileImage,
			    s2.setting_value as biography,
			    s3.setting_value as interests
			from
			    conference.users u
			    left join conference.registrations r
			        on u.user_id=r.user_id
			    left join conference.user_settings s1
			        on u.user_id=s1.user_id and s1.setting_name='profileImage'
			    left join conference.user_settings s2
			        on u.user_id=s2.user_id and s2.setting_name='biography'
			    left join conference.user_settings s3
			        on u.user_id=s3.user_id and s3.setting_name='interests'");

		foreach ($rows as $row) {
			if ($row['username'] == 'admin') {
				continue;
			}

			$u = R::findOne('user', 'username=?', array($row['username']));

			if (!$u) {
				$u = R::dispense('user');
			}

			if (!$u->id) {
				print "Adding new:<br>";
			} else {
				print "Updating: ({$u->username}) " . $u->short_label() . "<br>";
			}

			if (empty($u->first_name) && !empty($row['first_name'])) {
				$u->first_name = $row['first_name'];
				print "<div>... setting first name</div>";
			}
			if (empty($u->last_name) && !empty($row['last_name'])) {
				$u->last_name = $row['last_name'];
				print "<div>... setting last name</div>";
			}
			if (empty($u->email) && !empty($row['email'])) {
				$u->email = $row['email'];
				print "<div>... setting email</div>";
			}
			if (empty($u->username) && !empty($row['username'])) {
				$u->username = $row['username'];
				print "<div>... setting username</div>";
			}
			if (empty($u->password) && !empty($row['password'])) {
				$u->password = $row['password'];
				print "<div>... setting password</div>";
			}
			if (empty($u->twitter_username) && !empty($row['twitter_username'])) {
				$u->twitter_username = $row['twitter_username'];
				print "<div>... setting twitter</div>";
			}
			if (empty($u->returner) && !empty($row['returner'])) {
				$u->returner = $row['returner'];
				print "<div>... setting returner</div>";
			}
			if (empty($u->website) && !empty($row['url'])) {
				$u->website = $row['url'];
				print "<div>... setting website</div>";
			}
			if (empty($u->country) && !empty($row['country'])) {
				$u->country = $row['country'];
				print "<div>... setting country</div>";
			}
			if (empty($u->phone) && !empty($row['phone'])) {
				$u->phone = $row['phone'];
				print "<div>... setting phone</div>";
			}
			if (empty($u->fax) && !empty($row['fax'])) {
				$u->fax = $row['fax'];
				print "<div>... setting fax</div>";
			}
			if (empty($u->biography) && !empty($row['biography'])) {
				$u->biography = $row['biography'];
				print "<div>... setting biography</div>";
			}
			if (empty($u->interests) && !empty($row['interests'])) {
				$str = $row['interests'];
				$str = str_replace('<p>', "\n", $str);
				$str = str_replace('</p>', '', $str);
				$str = trim($str);
				$u->interests = $str;
				print "<div>... setting interests</div>";
			}
			if (empty($u->mailing_address) && !empty($row['mailing_address'])) {
				$str = $row['mailing_address'];
				$str = str_replace('<p>', "\n", $str);
				$str = str_replace('</p>', '', $str);
				$str = trim($str);
				$u->mailing_address = $str;
				print "<div>... setting mailing address</div>";
			}

			if (empty($u->photo_id) && !empty($row['profileImage'])) {
				$obj = unserialize($row['profileImage']);
				$filename = $obj['uploadName'];
				if (!empty($filename)) {
					$res = preg_match('/\.([^.]+)$/', $filename, $matches);
					$type = 'image/jpg';
					if ($res) {
						$ext = $matches[1];
						$type = 'image/' . $ext;
					}

					// download the image
					$url = "http://medicinex.stanford.edu/conferences/public/site/{$filename}";
					$local_file = '/tmp/' . uniqid();
					$data = file_get_contents($url);
					file_put_contents($local_file, $data);

					$res = $this->upload->do_upload($local_file, $filename, $type);
					$u->photo = $res;
				}
			}

			try {
				$u->save();
				print "<h3>Saved</h3>";
			} catch (Exception $e) {
				print "<h3>" . $e->getMessage() . "</h3>";
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */