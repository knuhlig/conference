<?php

class Conference extends Base_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function livestream_registration() {
    	$has_order = false;
        if ($this->auth->logged_in) {
            $user = $this->auth->user;
            $count = R::count('order', 'paid=1 and user_id=?', array($user->id));
            if ($count > 0) {
                $has_order = true;
            }
        }

        if ($has_order) {
            redirect(ci_url('conference/livestream'));
            exit();
        }

    	$this->template->set_base('template_nosidebar');
    	$this->template->display('conference/livestream_registration');
    }

    public function donate() {
        $this->_require_login();

        $data = array();
        $data['summary'] = false;

        if (!empty($_POST)) {
            $d = R::dispense('donation');
            $d->user = $this->auth->user;
            $d->amount = property($_POST, 'amount');
            $d->donation_date = time();
            $d->twitter_id = property($_POST, 'twitter_id');
            R::store($d);

            $data['summary'] = true;
            $data['donation'] = $d;
        }
        $this->load->view('conference/donate', $data);
    }

    public function latest_tweets() {
        $mk_options = theme_option(THEME_OPTIONS);
        
        $username = 'StanfordMedX';
        $count = 10;

        $consumer_key = $mk_options['twitter_consumer_key'];
        $consumer_secret = $mk_options['twitter_consumer_secret'];
        $access_token = $mk_options['twitter_access_token'];
        $access_token_secret = $mk_options['twitter_access_token_secret'];

        $trans_name = 'wpcm_latest_tweets';
        $cache_time = 2 * 60;

        //delete_transient( $trans_name );
        if ( false === ( $twitterData = get_transient( $trans_name ) ) ) {
            // require the twitter auth class
            $theme_dir = get_template_directory();
            $twitter_dir = $theme_dir . "/framework/widgets/";

            @require_once $twitter_dir . 'twitteroauth/twitteroauth.php';
            $twitterConnection = new TwitterOAuth(
                $consumer_key, // Consumer Key
                $consumer_secret,    // Consumer secret
                $access_token,       // Access token
                $access_token_secret     // Access token secret
            );
            print "<h3>LOADING!</h3>";
            $twitterData = $twitterConnection->get(
                'statuses/user_timeline',
                array(
                    'screen_name'     => $username,
                    'count'           => $count,
                    'exclude_replies' => false
                )
            );
            if ( $twitterConnection->http_code != 200 ) {
                $twitterData = get_transient( $trans_name );
            }
            set_transient($trans_name, $twitterData, $cacheTime );
        };

        $twitter = get_transient( $trans_name );
        print json_encode($twitter);
    }

    public function livestream($key=null) {
        $has_order = false;
        if ($key == 'ixsE8VN3sW') {
            $has_order = true;
        } else if ($this->auth->logged_in) {
            $user = $this->auth->user;
            $count = R::count('order', 'paid=1 and user_id=?', array($user->id));
            if ($count > 0) {
                $has_order = true;
            }
        }

        if (!$has_order) {
            redirect(ci_url('conference/livestream_registration'));
            exit();
        }
    	
        $settings = R::dispense('settings');
        $view = R::dispense('streamview');
        if ($this->auth->logged_in) $view->user = $this->auth->user;
        $view->view_time = time();
        R::store($view);

        $data = array(
            'embed_code' => $settings->livestream_embed
        );

        $this->template->set_base('template_nosidebar');
        $this->template->display('conference/livestream', $data);
    }

    public function attendees() {
    	$user_data = R::getAll("
    			select u.*
    			from wp_cm_user u,
    				wp_cm_order o,
    				wp_cm_rate r
    			where u.id=o.user_id
    				and o.rate_id=r.id and not r.exclude_badges
    				and o.date_paid is not null
    			group by u.id
    			order by u.last_name asc, u.first_name asc
    	");

    	$users = R::convertToBeans('user', $user_data);
    	foreach ($users as $user) {
    		print $user->attendee_row();
    	}
    	// search offset count sort dir
    }

    public function login($key) {
    	$user = R::findOne('user', 'login_key = ?', array($key));
    	if (!$user) {
    		show_error('The login credentials you provided are invalid.');
    	}
    	$this->auth->login_user($user, true);
    	redirect(ci_url('user/home'));
    	exit();
    }

    /**
     * Detail page for a single event.
     * @param unknown $id
     */
    public function event($id) {
    	$this->load->helper('schedule_helper');
    	$event = R::load('event', $id);
    	if (!$event->id) {
    		show_error("Event not found");
    	}

        $this->template->set_base('template_nosidebar');
    	$this->template->display('conference/event_detail', array(
    		'event' => $event,
    	));
    }

    /**
     * Detail page for a single speaker.
     * @param unknown $id
     */
    public function speaker($id) {
    	$speaker = R::load('speaker', $id);
    	if (!$speaker->id) {
    		show_error("Speaker not found");
    	}

    	$data = array();
    	$data['speaker'] = $speaker;

    	$photo = $speaker->fetchAs('upload')->profile_photo;
    	if (!$photo) {
    		$photo = $speaker->fetchAs('upload')->photo;
    	}
    	if (!$photo) {
    		$photo = R::load('upload', 1);
    	}
    	$data['photo'] = $photo;

    	$data['talks'] = $speaker->associated('event', false, '1 order by event_date asc');

    	$this->template->set_base('template_nosidebar');
    	$this->template->display('conference/speaker', $data);
    }


    /**
     * Shortcode handler to display the schedule.
     */
    public function schedule() {
    	$this->load->helper('schedule_helper');

        $s = new Schedule();
        if ($this->auth->logged_in) {
            $user = $this->auth->user;
            $s->set_user($user);
            $order_count = R::count('order', 'user_id=:id and paid and rate_id not in (select id from wp_cm_rate where exclude_badges)', array(':id' => $user->id));
            if ($order_count) {
                $s->load_attendance($user);
            }
        }

        if (property($_GET, 'signups')) {
            $sql = "
            select e.*
            from wp_cm_event e left join wp_cm_location l
                on e.location_id=l.id
            where coalesce(e.disable_signups, 0)=0
            order by event_date asc, coalesce(l.importance, 5) desc, field(type, 'group') desc, l.name asc";
        } else {
            $sql = "
            select e.*
            from wp_cm_event e left join wp_cm_location l
                on e.location_id=l.id
            order by event_date asc, coalesce(l.importance, 5) desc, field(type, 'group') desc, l.name asc";    
        }
        
        $rows = R::getAll($sql);
        $events = R::convertToBeans('event', $rows);
        $s->load_events($events);

        if (property($_GET, 'pdf')) {
            $file = $s->create_pdf();
        } else {
    	   $s->print_schedule();
        }
    }

    private function create_default_attendance($user) {
        $events = R::find('event', 'location_id in (select id from wp_cm_location where is_default)');
        $arr = array();
        foreach ($events as $event) {
            // create the object
            $b = R::dispense('attendance');
            $b->user = $user;
            $b->event = $event;
            $b->will_attend = 1;
            $b->event_date = $event->event_date;
            $b->date_updated = time();
            $arr[] = $b;
        }
        R::storeAll($arr);
        return $arr;
    }

    public function attend() {
        if (property($_POST, 'user_id') && $this->auth->is_admin()) {
            $user = R::load('user', property($_POST, 'user_id'));
        } else {
            $user = $this->auth->user;
        }

        // in case an event is first added to the user's schedule from the backend
        // before they visit the schedule page.
        $count = R::count("attendance", "user_id=?", array($user->id));
        if ($count == 0) {
            $this->create_default_attendance($user);
        }

        $event = R::load('event', property($_POST, 'id'));
        $will_attend = property($_POST, 'will_attend');
        $response = array(
            'status' => 'ok'
        );
        R::exec("delete from wp_cm_attendance where event_date=? and user_id=?", array($event->event_date, $user->id));

        if ($will_attend) {
            $count = $event->attendee_count();
            $capacity = $event->get_room_capacity();

            if (!empty($capacity) && $count >= $capacity) {
                $is_full = true;
            }

            $is_full = $event->is_full();
            $b = R::dispense('attendance');
            $b->user = $user;
            $b->event = $event;
            $b->will_attend = $will_attend;
            $b->date_updated = time();
            $b->event_date = $event->event_date;
            R::store($b);

            if ($is_full) {
                $response = array(
                    'status' => 'waitlist',
                    'position' => $count - $capacity + 1
                );
            } else {
                $response = array(
                    'status' => 'ok',
                    'position' => $count + 1
                );
            }
        }

        print json_encode($response);
    }

    /**
     * Badge scan page.
     * @param unknown $key
     */
    public function scan($key=null) {
    	$this->connect($key, 'scan');
    }

    /**
     * Badge scan page.
     * @param unknown $key
     */
    public function like($key=null) {
    	$this->connect($key, 'like');
    }

    /**
     * Badge scan page.
     * @param unknown $key
     */
    public function meet($key=null) {
    	$this->connect($key, 'meet');
    }

    /**
     * Badge scan page.
     * @param unknown $key
     */
    public function friend($key=null) {
    	$this->connect($key, 'friend');
    }

    private function connect($key, $type) {
    	$this->_require_login();
    	$user = $this->auth->user;
    	$target = R::findOne('user', 'access_key=?', array($key));
    	if (!$target) {
    		show_error('The key provided is not valid');
    	}
    	if ($user->id != $target->id) {
    		$user->connect_to($target, $type);
    		if ($type == 'scan') {
    			$target->connect_to($user, $type);
    		}
    		header('Location: ' . $target->url() . "/" . $type);
    	} else {
    		header('Location: ' . $target->url());
    	}

    	exit();
    }

    public function user($id, $type=null) {
    	$user = $this->auth->user;

    	$target = R::load('user', $id);
    	if (empty($target)) {
    		show_error("The specified user was not found.");
    	}
    	if (empty($target->access_key)) {
    		$target->setMeta('tainted', true);
    		R::store($target);
    	}

    	$data = array();
    	switch ($type) {
    		case 'scan':
    			$data['message'] = "You just met " . $target->full_name() . "!";
    			break;
    		case 'meet':
    			$data['message'] = "You'd like to meet ".$target->full_name()."!";
    			break;
    		case 'friend':
    			$data['message'] = "You added ".$target->full_name()." as a friend!";
    			break;
    		case 'friend':
    			$data['message'] = "You liked ".$target->full_name()."'s profile!";
    			break;
    	}

    	$data['user'] = $target;
    	$data['forward'] = $target->load_connections();
    	$data['reverse'] = $target->load_connections(true);
    	$this->template->set_base('template_nosidebar');
    	$this->template->display('conference/user', $data);

    }

    private function user_profile($target, $type=null) {

    }


    /**
     * Shortcode to display confirmed speakers embedded in a page.
     * @param string $type
     */
    public function confirmed($type='core') {
    	$data = array();

    	switch ($type) {
    		case 'keynote':
    			$data['keynotes'] = R::find('event', "type=:type order by event_date asc", array(
    			':type' => $type
    			));
    			break;
    		case 'core':
    			$sql = "select s.*
	    			from wp_cm_event t, wp_cm_eventspeaker es, wp_cm_speaker s
	    			where t.id=es.event_id and es.speaker_id=s.id and
    					(t.type=:type or t.type='tech')
	    			group by s.id
	    			order by s.last_name asc, s.first_name asc";
    			$rows = R::getAll($sql, array(
    				':type' => $type
    			));
    			$data[$type] = R::convertToBeans('speaker', $rows);
    			break;
    		case 'session':
    			$sql = "select s.*
	    		from wp_cm_event t, wp_cm_eventspeaker es, wp_cm_speaker s
	    		where t.id=es.event_id and es.speaker_id=s.id and
    				t.type in ('oral', 'poster', 'workshop', 'panel', 'session')
	    		group by s.id
	    		order by s.last_name asc, s.first_name asc";
    			$rows = R::getAll($sql, array(
    				':type' => $type
    			));
    			$data['core'] = R::convertToBeans('speaker', $rows);
    			break;
    		case 'masterclass':
    			$sql = "select t.*
	    			from wp_cm_event t, wp_cm_eventspeaker es, wp_cm_speaker s
	    			where t.id=es.event_id and es.speaker_id=s.id and
    					t.type=:type
	    			group by s.id
	    			order by s.last_name asc, s.first_name asc";
    			$rows = R::getAll($sql, array(
    				':type' => $type
    			));
    			$data[$type] = R::convertToBeans('event', $rows);
    			break;

    	}

    	$this->load->view('conference/confirmed', $data);
    }

}