<?php

class Badges extends Base_Controller {

	public function __construct() {
		parent::__construct();
		$this->_require_login();
		
		$username = $this->auth->user->username;
		if ($username != 'admin') {
			show_error("This page is restricted.");
		}
		
		$this->load->library('rb_form');
	}
	
	public function order($id) {
		$badge = R::findOne('badge', 'order_id=?', array($id));
		if (!$badge || !$badge->id) {
			$order = R::load('order', $id);
			$badge = R::dispense('badge');
			$badge->populate($order);
			R::store($badge);
		}
		redirect('/wp-admin/admin.php?page=edit_badge&id=' . $badge->id);
		exit();
	}
	
	public function install() {
		// speaker ids
		print "<h3>Adding speaker to user</h3>";
		$user = R::findOne('user');
		$speaker = R::findOne('speaker');
		$order = R::findOne('order');
		
		$user->speaker = $speaker;
		R::store($user);
		
		$user->speaker = null;
		R::store($user);
		
		print "<h3>Auto discovering speakers</h3>";
		R::exec("update wp_cm_speaker s, wp_cm_user u set u.speaker_id=s.id where s.first_name=u.first_name and substring_index(substring_index(s.last_name, ',', 1), ' ', 1)=substring_index(substring_index(u.last_name, ',', 1), ' ', 1)");
	
		$us = R::find('user', 'speaker_id is not null order by last_name asc, first_name asc');
		foreach ($us as $u) {
			$s = $u->r('speaker');
			print "<div>" . $u->full_name() . " &mdash;&gt; " . $s->full_name() . "</div>";
		}
		
		print "<h3>Speakers with no User account</h3>";
		$rows = R::getAll("select id, first_name, last_name from wp_cm_speaker where id not in (select speaker_id from wp_cm_user where speaker_id is not null)");
		print "<table>";
		foreach ($rows as $row) {
			print "<tr><td>" . join("</td><td>" , $row) . "</td></tr>";
		}
		print "</table>";
		
		print "<h3>Installing Badge Module</h3>";
		$badge = R::dispense('badge');
		$badge->order = $order;
		
		R::store($badge);
		R::trash($badge);
		
		$qr_cache = WPCI_PATH . "/media/cache/qr";
		if (!file_exists($qr_cache)) {
			mkdir($qr_cache);
		}
		
		// done
		print "<a href='".site_url('wp-admin/admin.php?page=browse_badge')."'>Go To Badge System</a>";
	}
	
	public function lock($id) {
		$badge = R::load('badge', $id);
		$badge->status = Model_Badge::STATUS_LOCKED;
		R::store($badge);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function unlock($id) {
		$badge = R::load('badge', $id);
		$badge->status = Model_Badge::STATUS_UNLOCKED;
		R::store($badge);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function hide($id) {
		$badge = R::load('badge', $id);
		$badge->status = Model_Badge::STATUS_HIDDEN;
		R::store($badge);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function reset_qrs($id) {
		$badge = R::load('badge', $id);
		$user = $badge->r('user');
		$user->setMeta('tainted', true);
		
		R::store($user);
		
		$badge->qr_file = $user->qr_file(true);
		$badge->login_qr_file = $user->login_qr_file(true);
		
		R::store($user);
		R::store($badge);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function bitly_test($id) {
		$badge = R::load('badge', $id);
		$user = $badge->r('user');
	
		header('Content-type: text/plain');
		$user->bitly_test();
	}
	
	public function unhide($id) {
		$badge = R::load('badge', $id);
		$badge->status = Model_Badge::STATUS_UNLOCKED;
		R::store($badge);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function regenerate() {
		$d = R::dispense('badge');
		$beans = R::find('badge', 'status=?', array(Model_Badge::STATUS_UNLOCKED));
		R::trashAll($beans);
		redirect('/wp-admin/admin.php?page=browse_badge');
	}
	
	public function export($id=null) {
		if (!empty($id)) {
			$badges = R::find('badge', 'id=?', array($id));
		} else {
			$d = R::dispense('badge');
			$badges = R::find('badge', 'status != ? order by last_name asc, first_name asc', array(Model_Badge::STATUS_HIDDEN));
		}
		$is_packet = property($_GET, 'packet', false);
		$this->export_badges($badges, $is_packet);
	}
	
	private function export_badges($badges, $packet=false) {
		disable_ob();
		print "<h3>Generating Configuration File...</h3>";
		
		$jar_path = WPCI_PATH . "/bin/badges.jar";
		$template_path = WPCI_PATH . "/media/badge_files/";
		$var_file = "/medx/badges/badges_".uniqid().".txt";
		$out_file = "/medx/badges/generated_documents_".uniqid().".pdf";
		
		// create the badge file
		$lines = array();
		print "<div style='height:150px;overflow-y:scroll;border:1px solid #999;padding:5px'>";
		$count = 0;
		foreach ($badges as $badge) {
			if (property($_GET, 'nonspeakers')) {
				$is_special = $badge->is_speaker || $badge->color == 'gold' || $badge->color == 'black';
				if ($_GET['nonspeakers'] == 1 && $is_special) {
					continue;
				}
				if ($_GET['nonspeakers'] == 2 && !$is_special) {
					continue;
				}
			}
			if ($packet) {
				print "<div>Generating data for " . $badge->full_name() . " ({$badge->color})</div>";
			}
			print "<span style='display:inline-block;border: 1px solid #c1c1c1;background-color:#f0f0f0;margin:3px;padding:3px'>" . $badge->full_name() . "</span>";
			
			$lines[] = $badge->get_export_lines($packet);
			$lines[] = "";

			$count++;
			if ($count >= property($_GET, 'limit', 100000)) {
				break;
			}
		}
		print "</div>";
		$contents = join("\n", $lines);
		file_put_contents($var_file, $contents);
		
		if (property($_GET, 'die')) {
			die("Generated configuration file! " . $var_file);
		}
		
		// delete any existing outfile
		if (file_exists($out_file)) {
			unlink($out_file);
		}

		// run the PDF generator
		print "<h3>Starting the PDF Generator...</h3>";
		$cmd = "java -Xmx1024m -jar {$jar_path}";
		$params = array(
			"-basepath" => $template_path,
			"-varfile" => $var_file,
			"-outfile" => $out_file,
			"-verbose" => ""
		);
		
		if ($packet) {
			$params += array(
				'-packet' => ''
			);
		}
		
		if (property($_GET, 'limit')) {
			$params['-limit'] = property($_GET, 'limit');
		}
		
		if (property($_GET, 'trim')) {
			$params['-trim'] = '';
		}
		
		if (property($_GET, 'label')) {
			$params['-label'] = '';
		}
		
		foreach ($params as $key => $value) {
			$cmd .= " {$key} {$value}";
		}
		
		print "<div style='height:200px;overflow-y:scroll;border:1px solid #999;padding:5px'>";
		$this->run_cmd($cmd);
		print "</div>";

		// check the result?
		if (!file_exists($out_file)) {
			die("<h3>An error occurred and the PDF was not generated</h3>");
		}
		
		// yay :)
		print "<h3>Success!</h3>";
		print "<a href='".ci_url('admin/download', array('path' => $out_file)) . "' class='button-primary'>Download PDF</a>";
	}
	
	private function run_cmd($cmd) {
		$descriptorspec = array(
			0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
			1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
			2 => array("pipe", "w")    // stderr is a pipe that the child will write to
		);
		flush();
		$process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());
		echo "<pre>";
		if (is_resource($process)) {
			while ($s = fgets($pipes[1])) {
				print $s;
				flush();
			}
			while ($s = fgets($pipes[2])) {
				print $s;
				flush();
			}
		}
		echo "</pre>";
	}
	
}