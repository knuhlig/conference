<?php

class Settings extends Base_Controller {

    private $settings_info;

    public function __construct() {
        parent::__construct();

        $this->settings_info = array(
            CM_PATH => array(
                'label' => 'URL Identifier',
                'description' => 'This is the unique url prefix that redirects requests to this plugin.'
            )
        );
    }

    public function index() {
        // check for form submission
        if (isset($_POST['submit'])) {
            $this->update_settings();
        }

        // show the settings page
        $this->load->view('admin/settings', array(
            'settings' => $this->settings_info
        ));
    }

    private function update_settings() {
        foreach ($this->settings_info as $key => $info) {
            update_option($key, $_POST[$key]);
        }
    }

}