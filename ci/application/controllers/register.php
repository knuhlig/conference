<?php

class Register extends Base_Controller {

	private $restrict;
	private $offer;
	private $rates;
    private $step;
    private $addons;
    private $master_classes;
    private $order;

    public function __construct() {
    	parent::__construct();

    	$this->load->library('rb_form');
    }

    public function verify($key) {
    	$this->_require_login();

    	// find the order
    	$order = R::findOne('order', 'resume_key=:rk', array(':rk' => $key));
    	if (!$order) {
    		show_error('The key provided is not valid');
    	}

    	$fields = $order->editor_fields();
    	$layout = array(
    		'sections' => array(
    			array(
    				'title' => 'Upload a file',
    				'fields' => array('verification_photo')
    			)
    		)
    	);

    	if (!empty($_POST)) {
    		// update the bean from form data
			$store = $this->rb_form->handle($order, $fields, $layout);
			R::store($order);
			if (!empty($order->verification_photo)) {
				$this->messages->add('success', 'Thank you! Your verification photo has been saved.');
			}
    	}

    	$data = array();
    	$data['order'] = $order;
    	$data['fields'] = $fields;
    	$data['layout'] = $layout;
    	$this->template->display('register/verify', $data);
    }

    public function receipt($key=null) {
    	if (!empty($key)) {
	    	$order = R::findOne('order', 'resume_key=:rk', array(':rk' => $key));
	    	if (!$order) {
	    		show_error('The key provided is not valid');
	    	}
    	} else {
    		$this->_require_login();
    		$order = R::findOne('order', 'user_id=?', array($this->auth->user->id));
    		if (!$order) {
    			show_error('No registration was found.');
    		}
    	}

    	$file = $order->generate_receipt();
    	$info = pathinfo($file);
    	$filename = $info['filename'];

    	header('Content-type: application/pdf');
    	header('Content-Disposition: attachment; filename="'.$filename.'"');
    	readfile($file);
    }

    public function payment($key) {
    	$p = R::findOne('payment', 'access_key=?', array($key));
    	if (!$p || !$p->id) {
    		show_error('Key not found');
    	}

    	$data = array(
    		'payment' => $p
    	);

    	$this->template->set_base('template_nosidebar');
    	$this->template->display('register/payment', $data);

    }

    public function install_payments() {
    	$p = R::dispense('payment');
    	R::store($p);
    	R::trash($p);
    	print "OK!";
    }



    public function update($id) {
    	$this->_require_login();
    	$auth_user = $this->auth->user;

    	// find the order
    	$order = R::load('order', $id);
    	if (!$order || !$order->id) {
    		show_error("Invalid order");
    	}

    	// check that the user is allowed to update the order
    	$user = $order->user;
    	if ($auth_user->id != $user->id) {
    		show_error("Sorry, you cannot edit this order.");
    	}

    	// order information
    	$offer = $order->offer;
    	$rate = $order->rate;
    	$addons = $offer->get_available_addons($rate);
    	$master_classes = $offer->get_available_addons($rate, true);


    	// remove addons already purchased
    	$purchased = $order->associated('addon');
    	$ids = array();
    	foreach ($purchased as $addon) {
    		$ids['addon_' . $addon->id] = 1;
    		unset($addons[$addon->id]);
    		unset($master_classes[$addon->id]);
    	}

    	if (!empty($_POST)) {
    		list($update, $updateaddons) = $this->make_update($order, $addons, $master_classes);
    		if (empty($updateaddons)) {
    			$this->messages->add('error', 'To update your registration, please select at least one new item.');
    		} else {
    			$update->date_paid = null;
    			$update->save();

    			$data = array();
    			$data['update'] = $update;
    			$data['orderaddons'] = $updateaddons;

    			$this->template->set_base('template_nosidebar');
    			$this->template->display('register/update_summary', $data);
    		}
    	}

    	// show the form
    	$data = array();
    	$data['data'] = $ids;
    	$data['order'] = $order;
    	$data['addons'] = $addons;
    	$data['master_classes'] = $master_classes;
        $this->template->set_base('template_nosidebar');
    	$this->template->display("register/update", $data);
    }

    private function make_update($order, $addons, $master_classes) {
    	$update = R::dispense('regupdate');
    	$update->order = $order;

    	$orderaddons = array();
    	foreach ($addons as $addon) {
    		if (!empty($_POST['addon_' . $addon->id])) {
    			$orderaddon = $update->associate('addon', $addon->id, array(
    				'quantity' => 1,
    				'unit_price' => $addon->current_price(),
    				'waitlist' => $addon->sold_out
    			));
    			$orderaddons[$addon->id] = $orderaddon;
    		}
    	}
    	foreach ($master_classes as $addon) {
    		if (!empty($_POST['addon_' . $addon->id])) {
    			$response = trim($_POST['response_' . $addon->id]);
    			$orderaddon = $update->associate('addon', $addon->id, array(
    				'response' => $response,
    				'unit_price' => 0,
    				'quantity' => 1,
    				'waitlist' => $addon->sold_out
    			));
    			$orderaddons[$addon->id] = $orderaddon;
    		}
    	}

    	return array($update, $orderaddons);
    }

    public function index() {
    	$key = property($_GET, 'key');
        $this->initialize($key);

        if (!empty($_POST)) {
            if ($this->update_data($_POST, $_SESSION['registration'])) {
                $next_step = $this->step + 1;
                if (!empty($_GET['update'])) {
                	$next_step = 3;
                }
            	redirect(ci_url('register', array(
                    'key' => $_GET['key'],
                    'step' => $next_step,
                    'email' => $this->user_email,
                    'rid' => property($_GET, 'rid', '')
                )));
                exit();
            }
        }

        // send an email to resume the order
        if (property($_GET, 'send_email')) {
            $this->send_resume_email();
            $this->messages->add('success', 'An email was sent with further instructions.');
            unset($_GET['send_email']);
        }

        // show the relevant content
        $content = $this->load_content($this->step);
        $this->template->set_base('template_nosidebar');
        $this->template->display('register', array(
            'has_options' => sizeof($this->rates) > 1,
            'restrict' => $this->restrict,
            'content' => $content,
            'step' => $this->step
        ));
    }

    private function initialize($key) {
    	if (!defined('CYBS_TEST')) {
    		// Cybersource Test/Live Account. If TEST, test account will be used. Otherwise,
    		// live account will be used.
    		define('CYBS_TEST', $this->settings->cybs_test);
    	}

    	if (property($_GET, 'resume')) {
    		$this->resume_session($_GET['resume']);
    	}

    	$this->load_offer($key);

    	// check the session data/progress
    	$this->check_session();

    	// find the requested step
    	$step = property($_GET, 'step', 0);
    	if ($step == 0) {
    		if (sizeof($this->rates) == 1) {
    			$rate_id = array_shift(array_values($this->rates))->id;
    			$_SESSION['registration']['rate_id'] = $rate_id;
    			$step = 1;
    		} else if (property($_GET, 'rid')) {
    			$rate_id = $_GET['rid'];
    			$found = false;
    			foreach ($this->rates as $rate) {
    				if ($rate->id == $rate_id) {
    					$found = true;
    					break;
    				}
    			}
    			if (!$found) {
    				show_error('Invalid request.');
    			}
    			$_SESSION['registration']['rate_id'] = $rate_id;
    			$step = 1;
    		}
    	}

    	$this->validate_step($step);
    	$this->step = $step;
    }

    /**
     * Loads and validates information about the requested offer.
     */
    private function load_offer($key) {
    	// can't do anything without an access key
    	if (empty($key)) {
    		show_error('No access key was provided.');
    	}

    	// lookup registration offer from key
    	$offer = R::findOne('offer', 'access_key=?', array($key));
    	if (!$offer) {
    		show_error('The access key is not valid.');
    	}

    	// check offer validity (dates, email access, etc)
    	$user_email = property($_GET, 'email');
    	$res = $offer->check_validity($user_email);
    	if (is_string($res)) {
    		show_error($res);
    	}

    	// load registration choices
    	$rates = $offer->associated('rate');
    	if (empty($rates)) {
    		show_error('Sorry, this offer is not configured correctly.');
    	}

    	// ---------- ALL CLEAR -----------------

    	$this->restrict = $res;
    	$this->offer = $offer;
    	$this->user_email = $user_email;
    	$this->rates = $rates;
    }

    private function check_session() {
    	// start/reset the session if necessary
    	if (!isset($_GET['step'])) {
    		$_SESSION['registration'] = array();
    	}

    	// logout if requested
    	if (property($_GET, 'f')) {
    		$this->auth->logout();
    		unset($_SESSION['registration']['first_name']);
    		unset($_SESSION['registration']['last_name']);
    		unset($_SESSION['registration']['email']);
    		unset($_GET['f']);
    	}

    	// set user info if logged in
    	if ($this->auth->logged_in) {
    		$_SESSION['registration'] = array_merge($_SESSION['registration'], $this->auth->user->export());
    	}
    }

    private function validate_step($step) {
    	$s =& $_SESSION['registration'];

    	if ($step > 0) {
    		// make sure an option has been selected
    		if (!isset($s['rate_id'])) {
    			show_error('Sorry, you cannot proceed to this step yet.');
    		}

    		$this->rate = R::load('rate', $s['rate_id']);
    		$this->addons = $this->offer->get_available_addons($this->rate);
    		$this->master_classes = $this->offer->get_available_addons($this->rate, true);
    	}

    	if ($step > 2) {
    		// make sure user is logged in
    		$this->_require_login();

    		if (isset($s['order_id'])) {
    			$order = R::load('order', $s['order_id']);
    		} else {
    			$order = R::dispense('order');
    		}

    		// update the order
    		$order->offer = $this->offer;
    		$order->rate = $this->rate;
            if (!empty($this->rate->regtype)) {
    		  $order->regtype = $this->rate->regtype;
            }
    		$order->user = $this->auth->user;
    		$order->registration_price = $this->offer->calculate_price($this->rate);
    		$order->email = $this->user_email;
    		R::store($order);

    		$order->reference_number = $this->rate->id . '-' . $order->id;
    		R::store($order);

    		$order->clearAssociation('addon');
    		foreach ($this->addons as $addon) {
    			if (!empty($_SESSION['registration']['addon_' . $addon->id])) {
    				$response = null;
    				if ($addon->text_prompt) {
    					$response = trim($_SESSION['registration']['response_' . $addon->id]);
    				}
    				$order->associate('addon', $addon->id, array(
    					'response' => $response,
    					'quantity' => 1,
    					'unit_price' => $addon->current_price(),
    					'waitlist' => $addon->sold_out
    				));
    			}
    		}

    		foreach ($this->master_classes as $addon) {
    			if (!empty($_SESSION['registration']['addon_' . $addon->id])) {
    				$response = trim($_SESSION['registration']['response_' . $addon->id]);
    				$order->associate('addon', $addon->id, array(
    					'response' => $response,
    					'quantity' => 1,
    					'waitlist' => $addon->sold_out
    				));
    			}
    		}
    		R::store($order);

    		$s['order_id'] = $order->id;
    		$this->order = $order;
    	}
    }


    private function get_warnings() {
    	$warnings = array();

    	foreach ($this->addons as $addon) {
    		if ($addon->warning_type != 'none') {
    			$warnings[] = array(
    				'id' => $addon->id,
    				'type' => $addon->warning_type,
    				'message' => $addon->name . " &mdash; " . $addon->get_warning()
    			);
    		}
    	}

    	foreach ($this->master_classes as $addon) {
    		if ($addon->warning_type != 'none') {
    			$warnings[] = array(
    				'id' => $addon->id,
    				'type' => $addon->warning_type,
    				'message' => $addon->name . " &mdash; " . $addon->get_warning()
    			);
    		}
    	}

    	return $warnings;
    }

    private function load_content($step) {
    	switch ($step) {
    		case 0:
    			return $this->load->view('register/select_rate', array(
    			'rates' => $this->rates
    			), true);
    		case 1:
    			return $this->load->view('register/select_addons', array(
    			'offer' => $this->offer,
    			'rate' => $this->rate,
    			'addons' => $this->addons,
    			'master_classes' => $this->master_classes,
    			'data' => $_SESSION['registration'],
    			'warnings' => $this->get_warnings()
    			), true);
    		case 2:
    			if ($this->auth->logged_in) {
    				return $this->load->view('register/attendee_loggedin', array(
    					'data' => $_SESSION['registration']
    				), true);
    			}

    			return $this->load->view('register/attendee_default', array(
    				'data' => $_SESSION['registration'],
    			), true);
    		case 3:
    			$total = $this->offer->calculate_price($this->rate);
    			foreach ($this->addons as $addon) {
    				if (!empty($_SESSION['registration']['addon_' . $addon->id])) {
    					$total += $addon->price;
    				}
    			}
    			return $this->load->view('register/summary', array(
    				'user' => $this->auth->user,
    				'order' => $this->order,
    				'master_class_available' => !empty($this->master_classes)
    			), true);
    	}

    	ob_start();
    	var_dump($_SESSION);
    	return ob_get_clean();
    }

    private function update_data(&$input, &$output) {
    	switch ($this->step) {
    		case 0:
    			$id = property($input, 'rate_id');
    			if (empty($id)) {
    				show_error('Not a valid selection');
    			}
    			$found = false;
    			foreach ($this->rates as $rate) {
    				if ($rate->id == $id) {
    					$found = true;
    					break;
    				}
    			}
    			if (!$found) {
    				show_error('Not a valid selection');
    			}
    			$output['rate_id'] = $id;
    			break;
    		case 1:
    			foreach ($this->addons as $addon) {
    				$key = "addon_{$addon->id}";
    				$value = property($input, $key);
    				$output[$key] = $value;
    				
    				if ($addon->text_prompt) {
						$key = "response_{$addon->id}";
						$value = property($input, $key);
						$output[$key] = $value;
    				}
    			}
    			foreach ($this->master_classes as $addon) {
    				$key = "addon_{$addon->id}";
    				$value = property($input, $key);
    				$output[$key] = $value;

    				$key = "response_{$addon->id}";
    				$value = property($input, $key);
    				$output[$key] = $value;
    			}
    			break;
    		case 2:
    			if ($this->auth->logged_in) {
    				$user = $this->auth->user;
    				$old_email = $user->email;

    				foreach (properties($_POST, array('first_name', 'last_name', 'email')) as $key => $value) {
    					$user->$key = $value;
    					$output[$key] = $value;
    				}
    				$new_email = $user->email;
    				if ($old_email != $new_email && email_exists($new_email)) {
    					$this->messages->add('error', "Sorry, this email is already taken.");
    					return false;
    				}
    				$user->save();
    			} else {
    				try {
    					$user = R::dispense('user');

    					// create the user
    					$this->rb_form->handle($user);

    					if (empty($user->password)) {
    						throw new Exception("Please enter a password");
    					}

    					$user->save();
    					$this->auth->login_user($user);
    				} catch (Exception $e) {
    					$this->messages->add('error', $e->getMessage());
    					return false;
    				}
    			}
    			break;
    	}

    	return true;
    }

    public function confirm() {
    	// check the integrity of this request
    	if (!cybs_verify_signature($_POST)) {
    		show_error('The response could not be verified.');
    	}

    	// check the payment decision
    	$decision = $_POST['decision'];
    	if ($decision != 'ACCEPT') {
    		show_error('Sorry, the payment was not accepted.');
    	}

    	$order_id = $_POST['req_reference_number'];

    	if ($order_id == 'special') {
    		$this->template->display('register/success');
    	} else if ($order_id[0] == 'd') {
            $id = substr($order_id, 1);
            $donation = R::load('donation', $id);
            $this->complete_donation($donation);
        } else if ($order_id[0] == 'u') {
    		$id = substr($order_id, 1);
    		$update = R::load('regupdate', $id);

    		if (!$update->id) {
    			show_error('Sorry, an error occurred. Reason: update not found');
    		}

    		$this->complete_update($update);
    	} else {
	    	$order = R::load('order', $order_id);

	    	// ok
	    	if (!$order->id) {
	    		show_error('Sorry, an error occurred. Reason: order not found');
	    	}

	    	$this->complete_order($order);
    	}
    }

    public function confirm_free($order_id) {
    	if ($order_id[0] == 'u') {
    		$id = substr($order_id, 1);
    		$update = R::load('regupdate', $id);
    		if (!$update->id) {
    			show_error('Sorry, an error occurred. Reason: update not found');
    		}

    		if ($update->total_price() > 0) {
    			show_error('Sorry, your order cannot be confirmed on this page because it is not free.');
    		}

    		$this->complete_update($update);
    	} else {
    		$order = R::load('order', $order_id);

    		// ok
    		if (!$order->id) {
    			show_error('Sorry, an error occurred. Reason: order not found');
    		}

    		if ($order->total_price() > 0) {
    			show_error('Sorry, your order cannot be confirmed on this page because it is not free.');
    		}

    		$this->complete_order($order);
    	}
    }


    private function send_receipt($order) {
        $email_slug = 'registration-confirmation';
        $rate = $order->r('rate');
        if ($rate) {
            $slug = $rate->email_slug;
            if (!empty($slug)) {
                $email_slug = $slug;
            }
        }

    	$tpl = R::findOne('template', 'slug=:slug', array(':slug' => $email_slug));
        if ($tpl) {
            $pdffile = $order->generate_receipt();
        	$email = $tpl->create_email(array(
        		'first' => $this->auth->user->first_name,
        		'update_url' => ci_url('register/update/' . $order->id)
        	));
        	$email->to = $this->auth->user->contact_email();
        	$email->attachments = $pdffile;
        	$email->send();
        }
    }

    private function send_update_receipt($order) {
    	$pdffile = $order->generate_receipt();

    	$tpl = R::findOne('template', 'slug=:slug', array(':slug' => 'registration-update'));
    	$email = $tpl->create_email(array(
    		'first' => $this->auth->user->first_name,
    	));
    	$email->to = $this->auth->user->contact_email();
    	$email->attachments = $pdffile;
    	$email->send();
    }

    private function complete_donation($donation) {
        $donation->is_paid = true;
        R::store($donation);
        $data = array('paid' => true);

        if (!empty($donation->twitter_id)) {
            $twitter_id = $donation->twitter_id;
            $this->load->library('twitter');
            $this->twitter->send_tweet("Thank you to {$twitter_id} for supporting Stanford Medicine X!");
        }


        $this->load->view('conference/donate', $data);

    }

    private function complete_update($update) {
    	$update->date_paid = time();

    	$order = $update->order;
    	$orderaddons = $order->associated('addon');
    	$updateaddons = $update->associated('addon', true);

    	foreach ($updateaddons as $updateaddon) {
    		$addon = $updateaddon->addon;
    		if (isset($orderaddons[$addon->id])) continue;
    		$order->associate('addon', $addon->id, array(
    			'unit_price' => $updateaddon->unit_price,
    			'response' => $updateaddon->response,
    			'quantity' => $updateaddon->quantity,
    			'waitlist' => $updateaddon->waitlist
    		));
    	}
    	R::store($update);
    	R::store($order);


    	$this->send_update_receipt($order);
    	$this->template->display('register/success');
    }

    private function complete_order($order) {
    	// mark the order as paid
    	$order->paid = 1;
    	$order->date_paid = time();
    	R::store($order);

    	$this->send_receipt($order);

    	$data = array();

    	$user = $this->auth->user;
    	$data['user'] = $user;

    	if (!$user->is_complete()) {
    		$data['needs_update'] = true;
    	}
    	$this->template->display('register/success', $data);
    	unset($_SESSION['registration']);
    }




    private function send_resume_email() {
        $tpl = R::dispense('template');
        $tpl->title = 'Medicine X Pre-Registration';
        $tpl->body = "Hello [first],<br><br>Thank you for pre-registering with Medicine X!<br><br>Click here to resume your order: [link]<br><br>Thank you,<br>Medicine X 2015 Organizing Team";

        $email = $tpl->create_email(array(
            'first' => $this->auth->user->first_name,
            'link' => '<a href="'.$this->order->resume_url().'">Resume Registration</a>'
        ));
        $email->to = $this->auth->user->contact_email();
        $email->send();
    }

    private function resume_session($key) {
        // find the order
        $order = R::findOne('order', 'resume_key=:rk', array(':rk' => $key));
        if (!$order) {
            show_error('The key provided is not valid');
        }

        $offer = $order->offer;
        $rate = $order->rate;


        $_SESSION['registration'] = array();
        $s =& $_SESSION['registration'];

        // repopulate the selected option
        $s['order_id'] = $order->id;
        $s['rate_id'] = $rate->id;

        // addons
        $orderaddons = $order->associated('addon', true);
        foreach ($orderaddons as $orderaddon) {
        	$s['addon_' . $orderaddon->addon->id] = 1;
        	if ($orderaddon->addon->master_class || $orderaddon->addon->text_prompt) {
                $s['response_' . $orderaddon->addon->id] = $orderaddon->response;
            }
        }


        // login the order user
        $this->auth->login_user($order->user);

        // redirect to the order summary page
        redirect(ci_url('register', array(
            'key' => $offer->access_key,
            'step' => 3,
            'email' => $order->email
        )));
        exit();
    }



}

