<?php

class Submission extends Base_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('rb_form');
        $this->template->set_base('template_nosidebar');
	}

	public function mark_duplicate($id) {
		$this->_require_admin();
		$s = R::load('submission', $id);
		if (!$s || !$s->id) {
			die("object not found");
		}

		$s->is_duplicate = true;
		R::store($s);
		print "ok";
	}
	

	public function confirm($key) {
		$author = R::findOne('author', 'access_key=?', array($key));
		if (!$author) {
			show_error('An error occurred locating the submission');
		}

		$submission = $author->submission;
		$track = $submission->track;
		$category = $submission->category;

		if ($category->is_panel) {
			$email = $author->email;
			if (R::count('user', 'email=?', array($email))) {
				$this->_require_login();
			} else {
				$this->_require_login('user/register');
			}
		}

		$user = $this->auth->user;

		// match author to user
		$u = $author->r('user');
		if (!$u) {
			$author->user = $user;
			$author->save();
		}

		if (!empty($_POST)) {
			$response = $_POST['response'];
			if ($response) {
				$author->status = $response;
				$author->save();
				$submission->update_progress();
			}
		}

		// show review/confirmation page
		$this->template->set_base('template_nosidebar');
		$this->template->display('submission/author_review', array(
			'submission' => $submission,
			'attachments' => $submission->ownAttachment,
			'author' => $author
		));
	}


	public function accepted() {
		$submissions = R::find('submission', 'status=? order by track_id asc', array('accepted'));
		
		$tracks = array();
		foreach ($submissions as $s) {
			$tracks[$s->track->title][] = $s;
		}
		$data = array();
		$data['tracks'] = $tracks;
		
		$this->load->view('submission/list', $data);
	}

	public function overview() {
		$this->_require_admin();
		$data = array();

		if (!empty($_POST['submission_id'])) {
			$ids = $_POST['reviewers'];
			$s = R::load('submission', $_POST['submission_id']);
			foreach ($ids as $id) {
				$r = R::dispense('review');
				$r->submission_id = $s->id;
				$r->user_id = $id;
				R::store($r);
			}
		}

		
		$all = R::find('submission', 'submission_date is not null and not coalesce(is_duplicate, 0)');
		$tracks = array();
		$categories = array();
		foreach ($all as $s) {
			$tracks[$s->track_id][$s->id] = $s;
			$categories[$s->category_id][$s->id] = $s;
		}
		
		$data['reviewers'] = R::find('user', 'is_reviewer order by last_name asc, first_name asc');

		
		$data['unassigned'] = R::find('submission', 'not coalesce(is_duplicate,0) and submission_date is not null and id not in (select submission_id from wp_cm_review)');
		$stats = array();
		$stats['Accepted Abstracts'] = R::count('submission', 'status=?', array('accepted'));
		$stats['Rejected Abstracts'] = R::count('submission', 'status=?', array('rejected'));
		$stats['In Review'] = R::count('submission', 'id in (select submission_id from wp_cm_review where not coalesce(completed, 0))');
		$stats['Unassigned'] = sizeof($data['unassigned']);
		$stats['Not Yet Submitted'] = R::count('submission', 'submission_date is null');
		$stats['Duplicates'] = R::count('submission', 'is_duplicate');
		
		$data['tracks'] = $tracks;
		$data['categories'] = $categories;
		$data['stats'] = $stats;


		$this->load->view('submission/overview', $data);
	}
	
	public function view($id) {
		if ($this->settings->submissions_closed) {
			$this->load->view('submission/closed');
			return;
		}

		$submission = R::load('submission', $id);
		if (empty($submission->id)) {
			show_error("The submission was not found");
		}
		
		$data = array(
			'submission' => $submission
		);
		$this->template->display('submission/view', $data);
	}

	public function review($id) {
		// authorization
		$this->_require_login();
		$submission = R::load('submission', $id);
		if (empty($submission->id)) {
			show_error("The submission was not found");
		}

		$is_reviewer = false;
		if ($this->auth->is_admin()) {
			$is_reviewer = true;
		} else if ($submission->is_reviewer($this->auth->user)) {
			$is_reviewer = true;
		}
		$is_author = $submission->user_id == $this->auth->user->id;

		if (!$is_author && !$is_reviewer) {
			show_error("You do not have permission to view this page");
		}
		
		$review = R::findOne('review', 'submission_id=? and user_id=?', array($submission->id, $this->auth->user->id));
		if (!$review) {
			$review = R::dispense('review');
			$review->user = $this->auth->user;
			$review->submission = $submission;
		}

		$review_layout = array(
			'sections' => array(
				array(
					'fields' => array('score', 'recommendation', 'plos_one', 'comments')
				)
			),
			'buttons' => array(
				'save' => 'Save Changes',
				'finish' => 'Submit Review'
			)
		);

		if (!empty($_POST)) {
			$action = $this->rb_form->action();
			if ($action == 'comment') {
				$c = R::dispense('comment');
				$this->rb_form->handle($c);
				$c->user = $this->auth->user;
				$c->submission = $submission;
				if (!$is_reviewer) {
					$c->is_public = true;
				}
				R::store($c);
			} else {
				$this->rb_form->handle($review, $review->editor_fields(), $review_layout);
				if ($action == 'finish') {
					$review->completed = true;
					$this->messages->add('success', 'The review has been submitted successfully. <a href="'.ci_url('user/home').'">Home</a>');
				} else {
					$this->messages->add('success', 'Changes saved successfully.');
				}
				R::store($review);
			}

		}

		$data = array(
			'submission' => $submission,
			'attachments' => $submission->ownAttachment,
			'comments' => $submission->ownComment,
			'review_layout' => $review_layout,
			'is_reviewer' => $is_reviewer,
			'review' => $review
		);
		$this->template->display('submission/review', $data);
	}


	public function reviewers() {
		$this->_require_admin();

		if (!empty($_POST['user'])) {
			$id = $_POST['user'];
			$user = R::load('user', $id);
			if (!empty($user->id)) {
				$user->is_reviewer = 1;
				R::store($user);
				$this->messages->add('success', 'The reviewer was added successfully.');
			}
		}

		$fields = array(
			'user' => array(
				'type' => 'relation',
				'relation' => 'one',
				'model' => 'user',
				'cond' => '(is_reviewer=0 or is_reviewer is null)',
				'sort' => 'last_name asc, first_name asc',
				//'rich' => true,
				'deps' => array(
					array("q", "!=", "")
				),
				'search' => true
			)
		);
		$layout = array(
			'sections' => array(
				array(
					'fields' => array('user')
				)
			),
			'buttons' => array(
				'submit' => 'Add Reviewer'
			)
		);
		$data = array(
			'fields' => $fields,
			'layout' => $layout,
			'reviewers' => R::find('user', 'is_reviewer=1 order by last_name asc, first_name asc')
		);
		$this->template->set_base('template_admin');
		$this->template->display('submission/reviewers', $data);
	}


	public function begin($id=null) {
		//error_reporting(E_ALL);
		//ini_set('display_errors', 'On');
		$this->_require_login('user/register', 'To submit an abstract, first create an account or <a href="'.ci_url('user/login', array('r' => ci_url('submission/submit'))).'">Login</a>');

 
		if (!empty($_POST)) {
			$tid = property($_POST, 'track');
			$cid = property($_POST, 'category');
			if (!is_numeric($tid) || !is_numeric($cid)) show_error('Error submitting the form');
			redirect(ci_url("submission/submit/{$tid}/{$cid}"));
			exit();
		}

		$s = R::dispense('submission');
		$settings = R::dispense('settings');
		$instructions = $settings->submission_info;
		$copyright = $settings->copyright_info;

		$fields = $s->editor_fields();
		$layout = array(
			'sections' => array(
				array(
					'fields' => array(
						'track', 'category')
				),
			),
			'buttons' => array(
				'submit' => 'Continue'
			)
		);

		// show the relevant content
		$data = array(
			's' => $s,
			'fields' => $fields,
			'layout' => $layout,
			'instructions' => $instructions,
			'copyright' => $copyright
		);
        $this->template->display('submission/begin', $data);
	}

 	public function submit($tid=null, $cid=null, $id=null) {
 		if (empty($tid) || empty($cid)) {
 			redirect(ci_url("submission/begin"));
 			exit();
 		}
 		$track = R::load('track', $tid);
 		$category = R::load('category', $cid);


		$this->_require_login('user/register', 'To submit an abstract, first create an account or <a href="'.ci_url('user/login', array('r' => ci_url('submission/submit'))).'">Login</a>');

		$u = $this->auth->user;

		if (is_null($id)) {
			$s = R::dispense('submission');
			$s->user = $this->auth->user;
			$s->track = $track;
			$s->category = $category;

			// auto-populate first author
			$a = R::dispense('author');
			$a->first_name = $u->first_name;
			$a->last_name = $u->last_name;
			$a->email = $u->email;
			$a->is_primary = true;
			$s->ownAuthor = array($a);
		} else {
			$s = R::load('submission', $id);
			if (!empty($s->submission_date)) {
				show_error('Sorry, this abstract has already been submitted');
			}
			if ($u->id != $s->user_id) {
				show_error('Sorry, this abstract seems to have been submitted by someone else.');
			}
			$this->messages->add('success', "Your abstract has been saved! You can come back later by visiting your account page here: <a href='".ci_url('user/home')."'>Account Page</a>");
		}

		if (!empty($_GET['m'])) {
			$this->messages->add('success', base64_decode($_GET['m']));
			unset($_GET['m']);
		}

		$settings = R::dispense('settings');
		$instructions = $settings->submission_info;
		$copyright = $settings->copyright_info;

		$fields = $s->editor_fields();

		if ($category->is_panel) {
	 		$fields['author'] = array(
				'type' => 'own_multiple',
				'model' => 'author',
				'label' => 'Panelists',
				'description' => 'Medicine X panels must include a moderator and 2-4 panelists.',
				'fields' => array('first_name', 'last_name', 'email', 'phone', 
					'is_primary' => array(
						'type' => 'checkbox',
						'label' => 'Moderator?'
					))
			);
			$instructions = "Use the form below to submit a panel to Medicine X 2015.";
 		} else if ($category->single_presenter) {
 			$fields['author']['fields'] = array(
 				'first_name', 'last_name', 'email', 'phone', 
				'is_primary' => array(
					'type' => 'checkbox',
					'label' => 'Presenter?'
			));
			$fields['author']['description'] = 'Additional authors will be notified via email and must consent to inclusion.';
 		} else {
 			$fields['author'] = array(
				'type' => 'own_multiple',
				'model' => 'author',
				'label' => 'Presenters',
				'fields' => array('first_name', 'last_name', 'email', 'phone', 'is_primary' => array(
					'type' => 'checkbox',
					'label' => 'Primary Contact'
				))
			);
 		}


		$layout = array(
			'sections' => array(
				array(
					'fields' => array(
						'title', 'author',
						'description', 'attachment', 'comments')
				),
			),
			'buttons' => array(
				'save' => 'Save Changes',
				'submit' => 'Submit Abstract'
			),
			'before_buttons' => $copyright
		);

		if (!empty($_POST)) {
			if ($this->rb_form->action() == 'save') {
				// save but don't submit
				$store = $this->rb_form->handle($s, $fields, $layout, false);
				$s->save();
				redirect($s->edit_url());
				exit();
			} else {
				// validate
				$store = $this->rb_form->handle($s, $fields, $layout);
				// higher-level validation?
				$found = false;
				$count = 0;
				foreach ($s->ownAuthor as $author) {
					if ($author->is_primary) {
						$found = true;
						$count++;
					}
				}
				if (!$found) {
					$this->rb_form->add_error('primary', 'At least one author must be marked as primary');
				}

				if ($category->is_panel) {
					if ($count > 1) {
						$this->rb_form->add_error('author', 'Only one moderator may be selected.');
					}
					$c = sizeof($s->ownAuthor);
					if ($c < 3 || $c > 5) {
						$this->rb_form->add_error('author', 'You must list a moderator and between 2 and 4 panelists.');
					}


				}

				// check validation
				if (!$this->rb_form->has_errors()) {
					// mark submitted
					$res = $s->submit();

					if ($res) {
						redirect(ci_url('submission/success'));
						exit();
					} else {
						redirect(ci_url('submission/pending'));
						exit();
					}
				} else {
					$this->rb_form->add_error_messages($fields);
				}
			}
		}

		// show the relevant content
		$data = array(
			's' => $s,
			'fields' => $fields,
			'layout' => $layout,
			'instructions' => $instructions,
			'copyright' => $copyright
		);
        $this->template->set_base('template_nosidebar');
        $this->template->display('submission/submit', $data);
	}

	public function success() {
        $this->template->set_base('template_nosidebar');
        $this->template->display('submission/success', $data);
	}

	public function pending() {
        $this->template->set_base('template_nosidebar');
        $this->template->display('submission/pending', $data);
	}

	public function install() {
		$CI =& get_instance();

		$a = R::dispense('author');
		$a->first_name = 'Karl';
		$a->last_name = 'Uhlig';
		$a->submission_id = null;
		R::store($a);

		$t = R::dispense('track');
		$t->title = 'INSTALLING';
		R::store($t);

		$c = R::dispense('category');
		$c->title = 'INSTALLING';
		R::store($c);

		$at = R::dispense('attachment');
		$at->title = 'TEST';
		$at->submission_id = null;
		R::store($at);

		$s = R::dispense('submission');
		$s->title = 'INSTALLING';
		$s->description = 'DESCRIPTION GOES HERE.';
		$s->user = $CI->auth->user;
		$s->track = $t;
		$s->category = $c;
		$s->ownAuthor = array($a);
		$s->ownAttachment = array($at);
		R::store($s);

		R::trash($s);
		R::trash($t);
		R::trash($c);
		R::trash($a);
		R::trash($at);
		print "OK";
	}

}

