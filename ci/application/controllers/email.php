<?php

class Email extends Base_Controller {

	
	public function o($key) {
		$email = R::findOne('email', 'access_key=?', array($key));
		
		if ($email) {
			$email->read = true;
			$email->date_read = time();
			
			// geolocate
			$ip = CM_REMOTE_IP;
			$res = $this->geo_ip($ip);
			
			if (!empty($res->success)) {
				$email->lat = $res->latitude;
				$email->lng = $res->longitude;
				$email->country = $res->country;
				$email->region = $res->region;
				$email->city = $res->city;
			}
			
			$email->save();
		}
		
		// Output a 1x1 transparent GIF
		header('Content-type: image/gif');
		echo base64_decode('R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');
	}
	
	public function unsubscribe($key) {
		$email = R::findOne('email', 'access_key=?', array($key));
		
		$addr = $email->get_recipient_email();
		if (!$addr) {
			show_error("Sorry, an error occurred");
		}
		
		$onList = R::count("emailblacklist", "email=?", array($addr));
		
		if (!$onList) {
			$bean = R::dispense('emailblacklist');
			$bean->email = $addr;
			R::store($bean);
		}
		print "<h3>Thank you, your preferences have been saved.</h3>";
	}
	
	public function api_send($key) {
		$email = R::findOne('email', 'access_key=?', array($key));
		if (!$email) {
			die("not found");
		}
		
		$email->send();
		print "ok";
	}
	
	
	private function geo_ip($ip) {
		$api_url = "https://ether5.stanford.edu/geo/?ip={$ip}";
		$res = file_get_contents($api_url);
		return json_decode($res);
	}
}