<?php

class Speaker extends Base_Controller {
	
	public function accept($key) {
		$speaker = R::findOne('speaker', 'access_key=?', array($key));
		if (!$speaker) {
			show_error('Speaker not found');
		}
		$data = array();
		$data['speaker'] = $speaker;
		
		$speaker->status = 'accept';
		$speaker->save();
		$this->messages->add('success', 'Thank you! Your response has been recorded.');
		$data['success'] = true;
		
		$this->template->display('speaker/accept', $data);
	}
	
	public function decline($key) {
		$speaker = R::findOne('speaker', 'access_key=?', array($key));
		if (!$speaker) {
			show_error('Speaker not found');
		}
		$data = array();
	
		$speaker->status = 'decline';
		$speaker->save();
		$this->messages->add('success', 'Thank you! Your response has been recorded.');
	
		$this->template->display('speaker/accept', $data);
	}
	
	
	
	public function make_keys() {
		foreach (R::findAll('speaker') as $speaker) {
			$speaker->setMeta('tainted', true);
			$speaker->save();	
		}
		print "OK";
	}
	
}