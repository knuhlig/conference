<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API extends Base_Controller {


    public function js() {
        header('Content-type: text/javascript');
        $this->load->view('api/js');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */