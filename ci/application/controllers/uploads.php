<?php

class Uploads extends Base_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function restricted($id) {
		$upload = R::load('upload', $id);

		if (!$this->auth->logged_in) {
			redirect(media_url('images/access_blocked.png'));
			exit();
		}
		if (!empty($upload->owner_id)) {
			$user = $this->auth->user;
			if (!$user->is_superuser && $upload->owner_id != $user->id) {
				redirect(media_url('images/access_blocked.png'));
				exit();
			}
		}

		header('Content-type: ' . $upload->type);
		readfile($upload->abspath());
	}

	public function full($id) {
	  $bean = R::load('upload', $id);
	  if (!$bean) {
	    die("Not found");
	  }
	  redirect($bean->url());
	}

	public function thumb($id) {
		$this->send_image('thumb', $id);
	}

	public function tile($id) {
		$this->send_image('tile', $id);
	}

	public function square($id) {
		$this->send_image('square', $id);
	}

	public function tiny($id) {
		$this->send_image('tiny', $id);
	}

	public function wide($id) {
		$this->send_image('wide', $id);
	}

	public function inch($id) {
		$this->send_image('inch', $id);
	}

	public function crop($id, $w, $h) {
		$bean = R::load('upload', $id);
		if (!$bean->id) {
			die("Upload not found");
		}

		$img = $bean->generate_thumbnail($w, $h);

		// output the thumbnail
		header('Content-type: image/jpeg');
		imagejpeg($img, null, 100);
	}

	private function send_image($size, $id) {
		$cache_file = WPCM_UPLOADS_DIR . 'cache/'.$size.'_' . $id . '.jpg';

		$bean = R::load('upload', $id);
		if (!$bean->id) {
			die("Upload not found");
		}

		// check cache
		if (file_exists($cache_file)) {
			if (filemtime($cache_file) >= $bean->last_updated) {
				redirect(WPCM_UPLOADS_URL.'cache/'.$size.'_' . $id . '.jpg');
				exit();
			}
		}

		$sizes = array(
			'tiny' => array(50, 50, 0),
			'inch' => array(72, 72, 0),
			'square' => array(150, 150, 0),
			'tile' => array(160, 160, 0),
			'thumb' => array(162, 213, 0),
			'wide' => array(320, 200, 0.4)
		);
		$wh = $sizes[$size];

		if ($bean->face) {
			$img = $bean->u_crop($wh[0], $wh[1], $wh[2]);
		} else {
			$img = $bean->generate_thumbnail($wh[0], $wh[1]);
		}

		// cache write
		imagejpeg($img, $cache_file, 100);


		// output the thumbnail
		header('Content-type: image/jpeg');
		imagejpeg($img, null, 100);
	}

}