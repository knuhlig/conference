<?php
/**
 * Plugin Installation
 */

/*
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

$sql = "CREATE TABLE `wp_cm_addon` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`description` text COLLATE utf8_unicode_ci,
`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`price` decimal(14,2) unsigned DEFAULT NULL,
`cap` int(10) unsigned DEFAULT NULL,
`master_class` tinyint(1) DEFAULT NULL,
`display_policy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`warning_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`warning` text COLLATE utf8_unicode_ci,
`trashed` tinyint(1) DEFAULT '0',
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
`photo_id` int(11) unsigned DEFAULT NULL,
`add_all` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_addon_upload` (`photo_id`),
CONSTRAINT `cons_fk_wp_cm_addon_photo_id_id` FOREIGN KEY (`photo_id`) REFERENCES `wp_cm_upload` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_addonoffer` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`price` double DEFAULT NULL,
`addon_id` int(11) unsigned DEFAULT NULL,
`offer_id` int(11) unsigned DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_addonoffer_offer` (`offer_id`),
KEY `index_foreignkey_addonoffer_addon` (`addon_id`),
CONSTRAINT `cons_fk_wp_cm_addonoffer_addon_id_id_casc` FOREIGN KEY (`addon_id`) REFERENCES `wp_cm_addon` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
CONSTRAINT `cons_fk_wp_cm_addonoffer_offer_id_id_casc` FOREIGN KEY (`offer_id`) REFERENCES `wp_cm_offer` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_addonorder` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`unit_price` double DEFAULT NULL,
`addon_id` tinyint(3) unsigned DEFAULT NULL,
`order_id` int(11) unsigned DEFAULT NULL,
`quantity` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
`response` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_addonorder_order` (`order_id`),
CONSTRAINT `cons_fk_wp_cm_addonorder_order_id_id_casc` FOREIGN KEY (`order_id`) REFERENCES `wp_cm_order` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_addonrate` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`addon_id` int(11) unsigned DEFAULT NULL,
`rate_id` int(11) unsigned DEFAULT NULL,
`price` double DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_addonrate_rate` (`rate_id`),
KEY `index_foreignkey_addonrate_addon` (`addon_id`),
CONSTRAINT `cons_fk_wp_cm_addonrate_addon_id_id_casc` FOREIGN KEY (`addon_id`) REFERENCES `wp_cm_addon` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
CONSTRAINT `cons_fk_wp_cm_addonrate_rate_id_id_casc` FOREIGN KEY (`rate_id`) REFERENCES `wp_cm_rate` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_email` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`body` text COLLATE utf8_unicode_ci,
		`created` int(11) unsigned DEFAULT NULL,
		`last_updated` int(11) unsigned DEFAULT NULL,
		`type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`attachments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_event` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`photo_id` int(11) unsigned DEFAULT NULL,
		`speaker_id` int(11) unsigned DEFAULT NULL,
		`scheduled` tinyint(1) DEFAULT '0',
		`description` text COLLATE utf8_unicode_ci,
		`event_date` int(11) unsigned DEFAULT NULL,
		`duration` tinyint(3) unsigned DEFAULT NULL,
		`created` int(11) unsigned DEFAULT NULL,
		`last_updated` int(11) unsigned DEFAULT NULL,
		PRIMARY KEY (`id`),
		KEY `index_foreignkey_talk_upload` (`photo_id`),
		KEY `index_foreignkey_talk_speaker` (`speaker_id`),
		KEY `index_foreignkey_event_speaker` (`speaker_id`),
		KEY `index_foreignkey_event_upload` (`photo_id`),
		CONSTRAINT `cons_fk_wp_cm_event_photo_id_id` FOREIGN KEY (`photo_id`) REFERENCES `wp_cm_upload` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
		CONSTRAINT `cons_fk_wp_cm_event_speaker_id_id` FOREIGN KEY (`speaker_id`) REFERENCES `wp_cm_speaker` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
		) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_offer` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`description` text COLLATE utf8_unicode_ci,
		`start_date` int(11) unsigned DEFAULT NULL,
		`end_date` int(11) unsigned DEFAULT NULL,
		`access` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`email_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`created` int(11) unsigned DEFAULT NULL,
		`last_updated` int(11) unsigned DEFAULT NULL,
		`access_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`customize_prices` tinyint(3) unsigned DEFAULT NULL,
		`price` int(11) unsigned DEFAULT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_offerrate` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`rate_id` int(11) unsigned DEFAULT NULL,
		`offer_id` int(11) unsigned DEFAULT NULL,
		PRIMARY KEY (`id`),
		KEY `index_foreignkey_offerrate_offer` (`offer_id`),
		KEY `index_foreignkey_offerrate_rate` (`rate_id`),
		CONSTRAINT `cons_fk_wp_cm_offerrate_rate_id_id_casc` FOREIGN KEY (`rate_id`) REFERENCES `wp_cm_rate` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
		CONSTRAINT `cons_fk_wp_cm_offerrate_offer_id_id_casc` FOREIGN KEY (`offer_id`) REFERENCES `wp_cm_offer` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
		) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_order` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`registration_price` int(11) unsigned DEFAULT NULL,
		`email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`resume_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`offer_id` int(11) unsigned DEFAULT NULL,
		`rate_id` int(11) unsigned DEFAULT NULL,
		`user_id` int(11) unsigned DEFAULT NULL,
		`date_paid` int(11) unsigned DEFAULT NULL,
		`created` int(10) unsigned DEFAULT NULL,
		`paid` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
		`last_updated` int(11) unsigned DEFAULT NULL,
		`regtype_id` int(11) unsigned DEFAULT NULL,
		PRIMARY KEY (`id`),
		KEY `index_foreignkey_order_offer` (`offer_id`),
		KEY `index_foreignkey_order_rate` (`rate_id`),
		KEY `index_foreignkey_order_user` (`user_id`),
		KEY `index_foreignkey_order_regtype` (`regtype_id`),
		CONSTRAINT `cons_fk_wp_cm_order_regtype_id_id` FOREIGN KEY (`regtype_id`) REFERENCES `wp_cm_regtype` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
		CONSTRAINT `cons_fk_wp_cm_order_offer_id_id` FOREIGN KEY (`offer_id`) REFERENCES `wp_cm_offer` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
		CONSTRAINT `cons_fk_wp_cm_order_rate_id_id` FOREIGN KEY (`rate_id`) REFERENCES `wp_cm_rate` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
		CONSTRAINT `cons_fk_wp_cm_order_user_id_id` FOREIGN KEY (`user_id`) REFERENCES `wp_cm_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
		) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_pwreset` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`reset_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		`expires` int(11) unsigned DEFAULT NULL,
		`user_id` int(11) unsigned DEFAULT NULL,
		`created` int(11) unsigned DEFAULT NULL,
		`last_updated` int(11) unsigned DEFAULT NULL,
		`last_used` int(11) unsigned DEFAULT NULL,
		PRIMARY KEY (`id`),
		KEY `index_foreignkey_pwreset_user` (`user_id`),
		CONSTRAINT `cons_fk_wp_cm_pwreset_user_id_id` FOREIGN KEY (`user_id`) REFERENCES `wp_cm_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
		) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_rate` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`description` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
`price` int(11) unsigned DEFAULT NULL,
`early_enabled` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
`early_price` int(11) unsigned DEFAULT NULL,
`early_end` int(11) unsigned DEFAULT NULL,
`late_enabled` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
`late_price` int(11) unsigned DEFAULT NULL,
`late_start` int(11) unsigned DEFAULT NULL,
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
`regtype_id` int(11) unsigned DEFAULT NULL,
`verification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_rate_regtype` (`regtype_id`),
CONSTRAINT `cons_fk_wp_cm_rate_regtype_id_id` FOREIGN KEY (`regtype_id`) REFERENCES `wp_cm_regtype` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_regtype` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
`badge_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_speaker` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`home_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`cell_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`fax_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`home_street1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`home_street2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_home_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_cell_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`asst_fax_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`photo_id` int(11) unsigned DEFAULT NULL,
`title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`biography` text COLLATE utf8_unicode_ci,
`bio_snippet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
`created` int(11) unsigned DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_speaker_upload` (`photo_id`),
CONSTRAINT `cons_fk_wp_cm_speaker_photo_id_id` FOREIGN KEY (`photo_id`) REFERENCES `wp_cm_upload` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_template` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`body` text COLLATE utf8_unicode_ci,
`notes` text COLLATE utf8_unicode_ci,
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_upload` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`upload_date` int(11) unsigned DEFAULT NULL,
`type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);

$sql = "CREATE TABLE `wp_cm_user` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`bio_snippet` set('1') COLLATE utf8_unicode_ci DEFAULT NULL,
`biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`created` int(11) unsigned DEFAULT NULL,
`last_updated` int(11) unsigned DEFAULT NULL,
`photo_id` int(11) unsigned DEFAULT NULL,
`username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`wp_id` tinyint(3) unsigned DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `index_foreignkey_user_upload` (`photo_id`),
CONSTRAINT `cons_fk_wp_cm_user_photo_id_id` FOREIGN KEY (`photo_id`) REFERENCES `wp_cm_upload` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
dbDelta($sql);
*/